source 'https://rubygems.org'
git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby '2.4.1'

gem 'rails', '~> 5.1.2'
gem 'puma', '~> 3.7'
gem 'pg'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'slim-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'pundit'
gem 'webpacker'
gem 'foreman'
gem 'recaptcha', require: 'recaptcha/rails'
gem 'simple_form'
gem 'stripe'
gem 'omniauth'
gem 'omniauth-oauth2', '1.3.1'
gem 'omniauth-linkedin-oauth2'
gem 'activerecord-session_store'
gem 'virtus'
gem 'draper'
gem 'rails-controller-testing'
gem 'aasm'
gem 'delayed_job_active_record'
gem 'daemons'
gem 'carrierwave'
gem 'fog'
gem 'mini_magick'
gem 'bootsnap', require: false
gem 'rollbar'
gem 'inline_svg'

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'devise'
gem 'devise_invitable'
gem 'high_voltage'

gem 'dotenv-rails' #we use dotenv in production
gem 'mandrill-api'

gem 'humanize_boolean'
gem 'kaminari'
gem "roo", "~> 2.7.0"

gem 'friendly_id', '~> 5.2.4'

group :development do
  gem 'awesome_print'
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rails-console'
  gem 'capistrano-rbenv'
  gem 'capistrano3-puma'
  gem 'capistrano-yarn'
  gem 'capistrano3-delayed-job'

  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # gem 'i18n-debug'

  # gem 'better_errors'
  gem 'binding_of_caller'
  gem 'rails_layout'
  gem 'spring-commands-rspec'
  gem 'letter_opener'

  gem 'meta_request'
  gem 'rails-erd', require: false
end

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'rspec-rails'
  gem 'rubocop'
  gem 'pry'
end

group :test do
  gem 'database_cleaner'
  gem 'launchy'
end
