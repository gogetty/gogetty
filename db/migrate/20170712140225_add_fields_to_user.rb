class AddFieldsToUser < ActiveRecord::Migration[5.1]
  def change
    change_table "users" do |t|
      t.string :company_name
      t.string :title
      t.string :surname
      t.boolean :company_representative
      t.boolean :terms_agreed
      t.boolean :newsletter_subscription
    end
  end
end
