class AddStripeInvoiceToBilling < ActiveRecord::Migration[5.1]
  def change
    add_column :billings, :stripe_invoice, :string
  end
end
