class AddAlwaysVisibleToCompanyProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :company_profiles, :always_visible, :boolean, default: true
  end
end
