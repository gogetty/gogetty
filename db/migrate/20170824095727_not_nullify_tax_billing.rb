class NotNullifyTaxBilling < ActiveRecord::Migration[5.1]
  def change
    change_table :billings do |t|
      t.change :tax, :integer, null: false
      t.change :tax_value, :integer, null: false
    end
  end
end
