class AddToCompanyProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :employees_counts do |t|
      t.string :count
    end

    change_table :company_profiles do |t|
      t.integer :employees_count_id
      t.decimal :liabilities, precision: 15, scale: 2
      t.decimal :revenue, precision: 15, scale: 2

      t.index :employees_count_id
    end
    add_foreign_key :company_profiles, :employees_counts
  end
end
