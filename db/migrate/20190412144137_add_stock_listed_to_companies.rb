class AddStockListedToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :stock_listed, :boolean, default: false
  end
end
