class AddYearToCompanyProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :company_profiles, :goal_year, :integer, null: false
  end
end
