class CouponLocal < ActiveRecord::Migration[5.1]
  def change
    change_table :billings do |t|
      t.integer :coupon_amount_off
      t.integer :coupon_percent_off
    end
  end
end
