class CreateTellFriend < ActiveRecord::Migration[5.1]
  def change
    create_table :tell_friends do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :custom_message
      t.integer :user_id

      t.index :user_id
    end
    add_foreign_key :tell_friends, :users
  end
end
