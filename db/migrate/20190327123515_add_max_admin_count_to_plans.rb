class AddMaxAdminCountToPlans < ActiveRecord::Migration[5.1]
  def change
    change_table :plans do |t|
      t.integer :max_admin_count
    end

    {
      'business_basic' => 1,
      'diverse_business' => 20,
      'complete_business' => 30 
    }.each_pair { |k, v| Plan.find_by(name: k)&.update(max_admin_count: v)}
  end
end
