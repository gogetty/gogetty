class SeedDefaultPlans < ActiveRecord::Migration[5.1]
  def change
    Plan.create(name: 'diverse_business',
                monthly_cost: 50_000,
                annual_cost: 418_800,
                commit: 'Choose Business',
                monthly_stripe_name: 'diverse_business_monthly',
                annual_stripe_name: 'diverse_business_yearly')
    Plan.create(name: 'business_basic',
                monthly_cost: 0,
                annual_cost: 0,
                commit: 'Choose Business Basic')
  end
end
