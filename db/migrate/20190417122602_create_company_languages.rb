class CreateCompanyLanguages < ActiveRecord::Migration[5.1]
  def change
    create_table :company_languages do |t|
      t.integer :language_id
      t.integer :company_id
      t.integer :count, default: 1

      t.timestamps
    end
  end
end
