class CreateCompanyInformationRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :company_information_requests do |t|
      t.integer :user_id, null: false
      t.integer :company_id, null: false
      t.text :comment
      t.boolean :resolved, null: false, default: false

      t.index :user_id
      t.index :company_id

      t.timestamps
    end
    add_foreign_key :company_information_requests, :users
    add_foreign_key :company_information_requests, :companies
  end
end
