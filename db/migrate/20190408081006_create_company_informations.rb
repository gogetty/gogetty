class CreateCompanyInformations < ActiveRecord::Migration[5.1]
  def change
    create_table :company_informations do |t|
      t.integer :company_id
      t.string :first_video
      t.string :second_video
      t.text :diversity_plans
      t.text :contact

      t.timestamps
      t.index :company_id, unique: true
    end
    add_foreign_key :company_informations, :companies
  end
end
