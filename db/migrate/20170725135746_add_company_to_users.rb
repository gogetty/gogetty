class AddCompanyToUsers < ActiveRecord::Migration[5.1]
  def change
    change_table :users do |t|
      t.integer :company_id
      t.boolean :superadmin, null: false, default: false
      t.index :company_id
    end
    add_foreign_key :users, :companies
  end
end
