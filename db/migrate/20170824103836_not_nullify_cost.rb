class NotNullifyCost < ActiveRecord::Migration[5.1]
  def change
    change_table :billings do |t|
      t.change :cost, :integer, null: false
    end
  end
end
