class AddManagementLevelToCompanyProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :company_profiles, :now_middle_man_count, :integer
    add_column :company_profiles, :now_middle_woman_count, :integer
    add_column :company_profiles, :goal_middle_man_count, :integer
    add_column :company_profiles, :goal_middle_woman_count, :integer
    add_column :company_profiles, :director_name, :string, default: 'Board of Directors'
    add_column :company_profiles, :cxo_name, :string, default: 'CxO Level'
    add_column :company_profiles, :vp_name, :string, default: 'VP Level'
    add_column :company_profiles, :middle_name, :string, default: 'MM'
    add_column :company_profiles, :director_hide, :boolean
    add_column :company_profiles, :cxo_hide, :boolean
    add_column :company_profiles, :vp_hide, :boolean
    add_column :company_profiles, :middle_hide, :boolean
  end
end
