class AddTimestampsToPlansUser < ActiveRecord::Migration[5.1]
  def change
    change_table :plans_users do |t|
      t.timestamps
    end
  end
end
