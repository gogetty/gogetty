class DefaultToDenmark < ActiveRecord::Migration[5.1]
  def change
    country = Country.find_by(en_name: 'Denmark')
    country ||= Country.create(en_name: 'Denmark')
    Company.where(country_id: nil).update_all(country_id: country.id)
  end
end
