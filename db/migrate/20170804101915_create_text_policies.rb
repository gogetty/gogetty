class CreateTextPolicies < ActiveRecord::Migration[5.1]
  def change
    create_table :text_policies do |t|
      t.string :name, null: false
      t.integer :company_id, null: false
      t.timestamps

      t.index :company_id
    end
    add_foreign_key :text_policies, :companies
  end
end
