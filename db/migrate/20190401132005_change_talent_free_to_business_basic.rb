class ChangeTalentFreeToBusinessBasic < ActiveRecord::Migration[5.1]
  Plan.find_by(name: 'talent_free')&.update(
    name: 'business_basic',
    commit: 'Choose Business Basic')
end
