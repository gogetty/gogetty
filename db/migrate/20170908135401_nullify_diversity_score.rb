class NullifyDiversityScore < ActiveRecord::Migration[5.1]
  def change
    change_table :company_profiles do |t|
      t.change :diversity_score, :integer, null: false, default: 0
      t.change :goal_diversity_score, :integer, null: false, default: 0
    end
  end
end
