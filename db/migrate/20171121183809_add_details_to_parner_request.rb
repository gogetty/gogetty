class AddDetailsToParnerRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :partner_requests, :surname, :string
    add_column :partner_requests, :company_name, :string
    add_column :partner_requests, :product, :text
  end
end
