class UpdateDiversityRequests < ActiveRecord::Migration[5.1]
  def change
    change_table :diversity_requests do |t|
      t.remove :now_directors
      t.remove :goal_directors
      t.remove :now_cxo
      t.remove :goal_cxo
      t.remove :now_vp
      t.remove :goal_vp
      t.remove :goal_year
      t.text :response
    end
  end
end
