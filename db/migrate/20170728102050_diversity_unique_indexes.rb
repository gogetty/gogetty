class DiversityUniqueIndexes < ActiveRecord::Migration[5.1]
  def change
    remove_index(:companies_diversity_policies, name: 'index_companies_diversity_policies1')
    remove_index(:companies_diversity_policies, name: 'index_companies_diversity_policies2')
    change_table :companies_diversity_policies do |t|
      t.index [:company_id, :diversity_policy_id],
              name: 'index_companies_diversity_policies1',
              unique: true
      t.index [:diversity_policy_id, :company_id],
              name: 'index_companies_diversity_policies2',
              unique: true
    end
  end
end
