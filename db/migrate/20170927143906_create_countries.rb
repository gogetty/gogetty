class CreateCountries < ActiveRecord::Migration[5.1]
  def change
    create_table :countries do |t|
      t.string :en_name, null: false, unique: true
    end

    change_table :companies do |t|
      t.integer :country_id

      t.index :country_id
    end
    add_foreign_key :companies, :countries
  end
end
