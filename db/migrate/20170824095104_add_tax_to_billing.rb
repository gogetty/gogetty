class AddTaxToBilling < ActiveRecord::Migration[5.1]
  def change
    change_table :billings do |t|
      t.integer :tax
      t.integer :tax_value
    end
  end
end
