class CreateRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :policy_requests do |t|
      t.timestamps
      t.integer :user_id, null: false
      t.integer :company_id, null: false
      t.text :comment
      t.boolean :resolved, null: false, default: false

      t.index :user_id
      t.index :company_id
    end
    add_foreign_key :policy_requests, :users
    add_foreign_key :policy_requests, :companies

    create_table :diversity_policies_policy_requests do |t|
      t.integer :policy_request_id, null: false
      t.integer :diversity_policy_id, null: false
      t.boolean :resolved, null: false, default: false

      t.index :policy_request_id
      t.index :diversity_policy_id
    end
    add_foreign_key :diversity_policies_policy_requests, :policy_requests
    add_foreign_key :diversity_policies_policy_requests, :diversity_policies

    create_table :text_policies_policy_requests do |t|
      t.integer :policy_request_id, null: false
      t.string :name, null: false
      t.boolean :resolved, null: false, default: false

      t.index :policy_request_id
    end
    add_foreign_key :text_policies_policy_requests, :policy_requests
  end
end
