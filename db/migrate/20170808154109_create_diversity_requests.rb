class CreateDiversityRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :diversity_requests do |t|
      t.timestamps
      t.integer :user_id, null: false
      t.integer :company_id, null: false
      t.text :comment
      t.boolean :resolved, null: false, default: false
      t.boolean :now_directors
      t.boolean :goal_directors
      t.boolean :now_cxo
      t.boolean :goal_cxo
      t.boolean :now_vp
      t.boolean :goal_vp
      t.integer :goal_year

      t.index :user_id
      t.index :company_id
    end
    add_foreign_key :diversity_requests, :users
    add_foreign_key :diversity_requests, :companies
  end
end
