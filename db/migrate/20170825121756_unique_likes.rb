class UniqueLikes < ActiveRecord::Migration[5.1]
  def change
    add_index :likes, [:post_id, :created_by], unique: true
  end
end
