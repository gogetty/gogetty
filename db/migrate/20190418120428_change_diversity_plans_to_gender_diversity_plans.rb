class ChangeDiversityPlansToGenderDiversityPlans < ActiveRecord::Migration[5.1]
  def change
    rename_column :company_informations, :diversity_plans, :gender_diversity_plans
  end
end
