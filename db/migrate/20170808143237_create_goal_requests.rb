class CreateGoalRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :goal_requests do |t|
      t.timestamps
      t.integer :user_id, null: false
      t.integer :company_id, null: false
      t.text :comment
      t.boolean :resolved, null: false, default: false

      t.index :user_id
      t.index :company_id
    end
    add_foreign_key :goal_requests, :users
    add_foreign_key :goal_requests, :companies
  end
end
