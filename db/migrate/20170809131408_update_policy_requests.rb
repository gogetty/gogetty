class UpdatePolicyRequests < ActiveRecord::Migration[5.1]
  def change
    change_table :policy_requests do |t|
      t.text :response
    end
  end
end
