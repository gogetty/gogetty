class AddTaxToPlans < ActiveRecord::Migration[5.1]
  def change
    change_table :plans do |t|
      t.integer :tax
    end
  end
end
