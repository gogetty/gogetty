class AddNotPayedToUserPlan < ActiveRecord::Migration[5.1]
  def change
    add_column :plans_users, :not_payed_pro, :boolean, null: false, default: false
  end
end
