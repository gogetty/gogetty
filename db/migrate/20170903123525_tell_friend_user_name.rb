class TellFriendUserName < ActiveRecord::Migration[5.1]
  def change
    change_table :tell_friends do |t|
      t.string :full_name
    end
  end
end
