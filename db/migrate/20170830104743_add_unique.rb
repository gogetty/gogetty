class AddUnique < ActiveRecord::Migration[5.1]
  def change
    Company.update_all industry_id: nil
    Company.update_all owner_type_id: nil
    CompanyProfile.update_all employees_count_id: nil
    EmployeesCount.all.group_by(&:count).values.each do |dup|
      dup.pop #leave one
      dup.each(&:destroy) #destroy other
    end
    Industry.all.group_by(&:name).values.each do |dup|
      dup.pop #leave one
      dup.each(&:destroy) #destroy other
    end
    OwnerType.all.group_by(&:name).values.each do |dup|
      dup.pop #leave one
      dup.each(&:destroy) #destroy other
    end
    add_index   :employees_counts,   :count,   unique: true
    add_index   :industries,         :name,     unique: true
    add_index   :owner_types,        :name,     unique: true
  end
end
