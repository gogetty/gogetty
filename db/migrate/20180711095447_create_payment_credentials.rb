class CreatePaymentCredentials < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_credentials do |t|
      t.string :stripe_customer_id
      t.integer :last4
      t.string :exp_month
      t.string :exp_year
      t.string :card_type
      t.integer :user_id
      t.timestamps
      t.index :user_id
    end
    add_foreign_key :payment_credentials, :users
  end
end
