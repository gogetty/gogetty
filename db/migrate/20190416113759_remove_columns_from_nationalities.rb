class RemoveColumnsFromNationalities < ActiveRecord::Migration[5.1]
  def up
    remove_column :nationalities, :company_id
    remove_column :nationalities, :count
  end

  def down
    add_column :nationalities, :company_id, :integer
    add_column :nationalities, :count, :integer, default: 1
  end
end
