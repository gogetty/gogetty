class CreateCompanyNationalities < ActiveRecord::Migration[5.1]
  def change
    create_table :company_nationalities do |t|
      t.integer :nationality_id
      t.integer :company_id
      t.integer :count, default: 1

      t.timestamps
    end

    add_index :company_nationalities, [:nationality_id, :company_id]
  end
end
