class NotNullifyTax < ActiveRecord::Migration[5.1]
  def change
    change_table :plans do |t|
      t.change :tax, :integer, null: false
    end
  end
end
