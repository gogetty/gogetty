class RemoveColumnsFromLanguages < ActiveRecord::Migration[5.1]
  def up
    remove_column :languages, :company_id
    remove_column :languages, :count
  end

  def down
    add_column :languages, :company_id, :integer
    add_column :languages, :count, :integer, default: 1
  end
end
