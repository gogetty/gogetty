class CreateAdminInvites < ActiveRecord::Migration[5.1]
  def change
    create_table :admin_invites do |t|
      t.string :email
      t.integer :user_id
      t.integer :company_id
      t.boolean :cancelled, null: false, default: false
      t.boolean :accepted, null: false, default: false
      t.timestamps

      t.index :user_id
      t.index :company_id
    end
    add_foreign_key :admin_invites, :users
    add_foreign_key :admin_invites, :companies

  end
end
