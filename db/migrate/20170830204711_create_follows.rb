class CreateFollows < ActiveRecord::Migration[5.1]
  def change
    create_table :follows do |t|
      t.integer :user_id, null: false
      t.integer :company_id, null: false
      t.timestamps
      t.index [:user_id, :company_id], unique: true
    end
    add_foreign_key :follows, :users
    add_foreign_key :follows, :companies
  end
end
