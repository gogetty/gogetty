class AddCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :industries do |t|
      t.string   :name,           null: false
    end

    create_table :owner_types do |t|
      t.string   :name,           null: false
    end

    create_table :companies do |t|
      t.timestamps
      t.string   :name,           null: false
      t.string   :logo
      t.integer  :industry_id
      t.string   :website
      t.integer  :size_id
      t.string   :city
      t.integer  :founded_year
      t.integer  :owner_type_id

      t.index    :industry_id
      t.index    :owner_type_id
    end

    add_foreign_key :companies, :industries
    add_foreign_key :companies, :owner_types
  end
end
