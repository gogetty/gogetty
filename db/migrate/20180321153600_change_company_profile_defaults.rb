class ChangeCompanyProfileDefaults < ActiveRecord::Migration[5.1]
  def change
    change_column_default :company_profiles, :now_director_man_count, 0
    change_column_default :company_profiles, :now_director_woman_count, 0
    change_column_default :company_profiles, :goal_director_man_count, 0
    change_column_default :company_profiles, :goal_director_woman_count, 0
    change_column_default :company_profiles, :now_cxo_man_count, 0
    change_column_default :company_profiles, :now_cxo_woman_count, 0
    change_column_default :company_profiles, :goal_cxo_man_count, 0
    change_column_default :company_profiles, :goal_cxo_woman_count, 0
    change_column_default :company_profiles, :now_vp_man_count, 0
    change_column_default :company_profiles, :now_vp_woman_count, 0
    change_column_default :company_profiles, :goal_vp_man_count, 0
    change_column_default :company_profiles, :goal_vp_woman_count, 0
    change_column_default :company_profiles, :now_middle_man_count, 0
    change_column_default :company_profiles, :now_middle_woman_count, 0
    change_column_default :company_profiles, :goal_middle_man_count, 0
    change_column_default :company_profiles, :goal_middle_woman_count, 0
    change_column_default :company_profiles, :middle_name, 'Middle Management'
  end
end
