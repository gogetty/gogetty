class AddPercentagesToCompanyProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :company_profiles, :now_director_woman_percentage, :integer, default: 0
    add_column :company_profiles, :now_director_man_percentage, :integer, default: 0
    add_column :company_profiles, :goal_director_woman_percentage, :integer, default: 0
    add_column :company_profiles, :goal_director_man_percentage, :integer, default: 0
    add_column :company_profiles, :now_cxo_woman_percentage, :integer, default: 0
    add_column :company_profiles, :now_cxo_man_percentage, :integer, default: 0
    add_column :company_profiles, :goal_cxo_woman_percentage, :integer, default: 0
    add_column :company_profiles, :goal_cxo_man_percentage, :integer, default: 0
    add_column :company_profiles, :now_middle_woman_percentage, :integer, default: 0
    add_column :company_profiles, :now_middle_man_percentage, :integer, default: 0
    add_column :company_profiles, :goal_middle_woman_percentage, :integer, default: 0
    add_column :company_profiles, :goal_middle_man_percentage, :integer, default: 0
    add_column :company_profiles, :now_vp_woman_percentage, :integer, default: 0
    add_column :company_profiles, :now_vp_man_percentage, :integer, default: 0
    add_column :company_profiles, :goal_vp_woman_percentage, :integer, default: 0
    add_column :company_profiles, :goal_vp_man_percentage, :integer, default: 0
    # for total employee count to calculate numbers from percentage
    add_column :company_profiles, :now_director_total, :integer, default: 0
    add_column :company_profiles, :goal_director_total, :integer, default: 0
    add_column :company_profiles, :now_cxo_total, :integer, default: 0
    add_column :company_profiles, :goal_cxo_total, :integer, default: 0
    add_column :company_profiles, :now_middle_total, :integer, default: 0
    add_column :company_profiles, :goal_middle_total, :integer, default: 0
    add_column :company_profiles, :now_vp_total, :integer, default: 0
    add_column :company_profiles, :goal_vp_total, :integer, default: 0
    # to show percentage or numbers
    add_column :company_profiles, :director_in_percenatge, :boolean, default: false
    add_column :company_profiles, :cxo_in_percenatge, :boolean, default: false
    add_column :company_profiles, :middle_in_percenatge, :boolean, default: false
    add_column :company_profiles, :vp_in_percenatge, :boolean, default: false
    add_column :company_profiles, :total_diversity_score, :integer, default: 0

    #Sync Numbers to Percentage
    CompanyProfile.all.each do |profile|
      now_director_woman_percentage = (profile.now_director_woman_count.to_f/(profile.now_director_woman_count.to_i + profile.now_director_man_count.to_i)) * 100 if profile.now_director_woman_count.to_i > 0 || profile.now_director_man_count.to_i > 0
      now_director_man_percentage = 100 - now_director_woman_percentage.round if now_director_woman_percentage.present?
      now_director_total = profile.now_director_woman_count.to_i + profile.now_director_man_count.to_i

      goal_director_woman_percentage = (profile.goal_director_woman_count.to_f/(profile.goal_director_woman_count.to_i + profile.goal_director_man_count.to_i)) * 100 if profile.goal_director_woman_count.to_i > 0 || profile.goal_director_man_count.to_i > 0
      goal_director_man_percentage = 100 - goal_director_woman_percentage.round if goal_director_woman_percentage.present?
      goal_director_total = profile.goal_director_woman_count.to_i+ profile.goal_director_man_count.to_i

      now_cxo_woman_percentage = (profile.now_cxo_woman_count.to_f/(profile.now_cxo_woman_count.to_i + profile.now_cxo_man_count.to_i)) * 100 if profile.now_cxo_woman_count.to_i > 0 || profile.now_cxo_man_count.to_i > 0
      now_cxo_man_percentage = 100 - now_cxo_woman_percentage.round if now_cxo_woman_percentage.present?
      now_cxo_total = profile.now_cxo_woman_count.to_i + profile.now_cxo_man_count.to_i

      goal_cxo_woman_percentage = (profile.goal_cxo_woman_count.to_f/(profile.goal_cxo_woman_count.to_i+ profile.goal_cxo_man_count.to_i)) * 100 if profile.goal_cxo_woman_count.to_i > 0 || profile.goal_cxo_man_count.to_i > 0
      goal_cxo_man_percentage = 100 - goal_cxo_woman_percentage.round if goal_cxo_woman_percentage.present?
      goal_cxo_total = profile.goal_cxo_woman_count.to_i+ profile.goal_cxo_man_count.to_i

      now_vp_woman_percentage = (profile.now_vp_woman_count.to_f/(profile.now_vp_woman_count.to_i+ profile.now_vp_man_count.to_i)) * 100 if profile.now_vp_woman_count.to_i > 0 || profile.now_vp_man_count.to_i > 0
      now_vp_man_percentage = 100 - now_vp_woman_percentage.round if now_vp_woman_percentage.present?
      now_vp_total = profile.now_vp_woman_count.to_i+ profile.now_vp_man_count.to_i

      goal_vp_woman_percentage = (profile.goal_vp_woman_count.to_f/(profile.goal_vp_woman_count.to_i+ profile.goal_vp_man_count.to_i)) * 100 if profile.goal_vp_woman_count.to_i > 0 || profile.goal_vp_man_count.to_i > 0
      goal_vp_man_percentage = 100 - goal_vp_woman_percentage.round if goal_vp_woman_percentage.present?
      goal_vp_total = profile.goal_vp_woman_count.to_i+ profile.goal_vp_man_count.to_i

      now_middle_woman_percentage = (profile.now_middle_woman_count.to_f/(profile.now_middle_woman_count.to_i+ profile.now_middle_man_count.to_i)) * 100 if profile.now_middle_woman_count.to_i > 0 || profile.now_middle_man_count.to_i > 0
      now_middle_man_percentage = 100 - now_middle_woman_percentage.round if now_middle_woman_percentage.present?
      now_middle_total = profile.now_middle_woman_count.to_i+ profile.now_middle_man_count.to_i

      goal_middle_woman_percentage = (profile.goal_middle_woman_count.to_f/(profile.goal_middle_woman_count.to_i+ profile.goal_middle_man_count.to_i)) * 100 if profile.goal_middle_woman_count.to_i > 0 || profile.goal_middle_man_count.to_i > 0
      goal_middle_man_percentage = 100 - goal_middle_woman_percentage.round if goal_middle_woman_percentage.present?
      goal_middle_total = profile.goal_middle_woman_count.to_i+ profile.goal_middle_man_count.to_i

      profile.update_attributes(
        now_director_woman_percentage: now_director_woman_percentage.to_i.round,
        now_director_man_percentage: now_director_man_percentage.to_i.round,
        now_director_total: now_director_total,
        goal_director_woman_percentage: goal_director_woman_percentage.to_i.round,
        goal_director_man_percentage: goal_director_man_percentage.to_i.round,
        goal_director_total: goal_director_total,

        now_cxo_woman_percentage: now_cxo_woman_percentage.to_i.round,
        now_cxo_man_percentage: now_cxo_man_percentage.to_i.round,
        now_cxo_total: now_cxo_total,
        goal_cxo_woman_percentage: goal_cxo_woman_percentage.to_i.round,
        goal_cxo_man_percentage: goal_cxo_man_percentage.to_i.round,
        goal_cxo_total: goal_cxo_total,

        now_vp_woman_percentage: now_vp_woman_percentage.to_i.round,
        now_vp_man_percentage: now_vp_man_percentage.to_i.round,
        now_vp_total: now_vp_total,
        goal_vp_woman_percentage: goal_vp_woman_percentage.to_i.round,
        goal_vp_man_percentage: goal_vp_man_percentage.to_i.round,
        goal_vp_total: goal_vp_total,

        now_middle_woman_percentage: now_middle_woman_percentage.to_i.round,
        now_middle_man_percentage: now_middle_man_percentage.to_i.round,
        now_middle_total: now_middle_total,
        goal_middle_woman_percentage: goal_middle_woman_percentage.to_i.round,
        goal_middle_man_percentage: goal_middle_man_percentage.to_i.round,
        goal_middle_total: goal_middle_total
      )
      ::Command::CompanyProfile::CalcDiversityScore.new(profile).call
    end
  end
end
