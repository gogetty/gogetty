class DestroyManagers < ActiveRecord::Migration[5.1]
  def change
    remove_column :company_profiles, :now_manager_man_count, :boolean
    remove_column :company_profiles, :now_manager_woman_count, :boolean
    remove_column :company_profiles, :goal_manager_man_count, :boolean
    remove_column :company_profiles, :goal_manager_woman_count, :boolean
  end
end
