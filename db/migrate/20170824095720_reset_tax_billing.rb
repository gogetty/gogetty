class ResetTaxBilling < ActiveRecord::Migration[5.1]
  def change
    Billing.reset_column_information
    Billing.all.each do |billing|
      billing.tax = 0
      billing.tax_value = 0
      billing.save!
    end
  end
end
