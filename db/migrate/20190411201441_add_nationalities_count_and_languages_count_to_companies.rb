class AddNationalitiesCountAndLanguagesCountToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :nationalities_count, :integer, default: 0
    add_column :companies, :languages_count, :integer, default: 0
  end
end
