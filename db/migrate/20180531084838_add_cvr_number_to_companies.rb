class AddCvrNumberToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :cvr_number, :integer
  end
end
