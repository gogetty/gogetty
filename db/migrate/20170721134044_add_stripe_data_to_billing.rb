class AddStripeDataToBilling < ActiveRecord::Migration[5.1]
  def change
    change_table :billings do |t|
      t.string :stripe_sub
      t.string :stripe_charge
    end
  end
end
