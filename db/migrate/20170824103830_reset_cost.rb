class ResetCost < ActiveRecord::Migration[5.1]
  def change
    Billing.reset_column_information
    Billing.all.each do |billing|
      billing.cost = billing.amount_in_cents
      billing.save
    end
  end
end
