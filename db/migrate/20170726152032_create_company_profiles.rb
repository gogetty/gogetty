class CreateCompanyProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :company_profiles do |t|
      t.integer :company_id
      t.integer :now_director_woman_count
      t.integer :now_director_man_count
      t.integer :goal_director_woman_count
      t.integer :goal_director_man_count
      t.integer :now_cxo_woman_count
      t.integer :now_cxo_man_count
      t.integer :goal_cxo_woman_count
      t.integer :goal_cxo_man_count
      t.integer :now_manager_woman_count
      t.integer :now_manager_man_count
      t.integer :goal_manager_woman_count
      t.integer :goal_manager_man_count
      t.integer :now_vp_woman_count
      t.integer :now_vp_man_count
      t.integer :goal_vp_woman_count
      t.integer :goal_vp_man_count
      t.timestamps

      t.index :company_id, unique: true
    end
    add_foreign_key :company_profiles, :companies

    create_table :diversity_policies do |t|
      t.string :name
      t.boolean :always_visible, null: false, default: false
      t.boolean :hidden, null: false, default: false
      t.timestamps

      t.index :name, unique: true
    end

    create_table :companies_diversity_policies do |t|
      t.integer :company_id
      t.integer :diversity_policy_id
      t.timestamps

      t.index [:company_id, :diversity_policy_id], name: 'index_companies_diversity_policies1'
      t.index [:diversity_policy_id, :company_id], name: 'index_companies_diversity_policies2'
    end
    add_foreign_key :companies_diversity_policies, :companies
    add_foreign_key :companies_diversity_policies, :diversity_policies

    create_table :goals_ways do |t|
      t.integer :company_id
      t.string :description
      t.boolean :hidden, null: false, default: false
      t.timestamp

      t.index :company_id
    end
    add_foreign_key :goals_ways, :companies

  end
end
