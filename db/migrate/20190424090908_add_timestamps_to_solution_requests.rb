class AddTimestampsToSolutionRequests < ActiveRecord::Migration[5.1]
  def change
    add_timestamps :solution_requests, default: Time.zone.now
    change_column_default :solution_requests, :created_at, from: Time.zone.now, to: nil
    change_column_default :solution_requests, :updated_at, from: Time.zone.now, to: nil
  end
end
