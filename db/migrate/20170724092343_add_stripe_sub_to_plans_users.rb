class AddStripeSubToPlansUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :plans_users, :stripe_sub, :string, null: false
  end
end
