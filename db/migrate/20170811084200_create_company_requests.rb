class CreateCompanyRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :company_requests do |t|
      t.timestamps
      t.integer :user_id, null: false
      t.string :name, null: false
      t.index :user_id
      t.boolean :resolved, null: false, default: false
    end
    add_foreign_key :company_requests, :users
  end
end
