class AddMonthlyBillingPermissionToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :monthly_billing_permission, :boolean, default: false
  end
end
