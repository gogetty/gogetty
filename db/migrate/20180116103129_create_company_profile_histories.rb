class CreateCompanyProfileHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :company_profile_histories do |t|
      t.integer :company_id
      t.integer :now_director_woman_count
      t.integer :now_director_man_count
      t.integer :goal_director_woman_count
      t.integer :goal_director_man_count
      t.integer :now_cxo_woman_count
      t.integer :now_cxo_man_count
      t.integer :goal_cxo_woman_count
      t.integer :goal_cxo_man_count
      t.integer :now_vp_woman_count
      t.integer :now_vp_man_count
      t.integer :goal_vp_woman_count
      t.integer :goal_vp_man_count
      t.integer :goal_year
      t.integer :diversity_score

      t.timestamps
    end
  end
end
