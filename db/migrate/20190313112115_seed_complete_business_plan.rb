class SeedCompleteBusinessPlan < ActiveRecord::Migration[5.1]
  def change
    Plan.find_or_create_by(name: 'complete_business') do |plan|
      plan.monthly_cost = 120_000
      plan.annual_cost = 1_115_200
      plan.commit = 'Choose Complete Business'
      plan.monthly_stripe_name = 'complete_business_monthly'
      plan.annual_stripe_name = 'complete_business_yearly'
      plan.tax = 25
    end
  end
end
