class CreateNationalitiesAndLanguages < ActiveRecord::Migration[5.1]
  def change
    create_table :nationalities do |t|
      t.integer :company_id
      t.string :name
      t.integer :count

      t.index :company_id
      t.timestamps
    end

    create_table :languages do |t|
      t.integer :company_id
      t.string :name
      t.integer :count

      t.index :company_id
      t.timestamps
    end

    add_foreign_key :nationalities, :companies
    add_foreign_key :languages, :companies
  end
end
