class AddDiversityScore < ActiveRecord::Migration[5.1]
  def change
    change_table :company_profiles do |t|
      t.integer :diversity_score
    end
  end
end
