class ChangeCompletePlanNameToInclusive < ActiveRecord::Migration[5.1]
  def self.up
    plan = Plan.find_by(name: 'complete_business')
    plan&.update(
      name: 'inclusive_business',
      annual_stripe_name: 'inclusive_business_yearly',
      monthly_stripe_name: 'inclusive_business_monthly'
    )
  end

  def self.down
    plan = Plan.find_by(name: 'inclusive_business')
    plan&.update(
      name: 'complete_business',
      annual_stripe_name: 'complete_business_yearly',
      monthly_stripe_name: 'complete_business_monthly'
    )
  end
end
