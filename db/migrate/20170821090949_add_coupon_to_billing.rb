class AddCouponToBilling < ActiveRecord::Migration[5.1]
  def change
    change_table :billings do |t|
      t.string :coupon
    end
  end
end
