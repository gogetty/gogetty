class AddCancelScheduledToPlansUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :plans_users, :cancel_scheduled, :boolean, null: false, default: false
  end
end
