class CreatePlatformSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :platform_settings do |t|
      t.text :google_analytics_head
      t.text :google_analytics_body
    end
    PlatformSettings.create(google_analytics_head: '', google_analytics_body: '')
  end
end
