class ChangeDefaultPlans < ActiveRecord::Migration[5.1]
  def change
    plan = Plan.find_by(commit: 'Choose Business')
    plan.commit = 'Choose Diverse Business'
    plan.tax = 25
    plan.save!
  end
end
