class ChangeTalentFreePlanNameToBusinessBasic < ActiveRecord::Migration[5.1]
  Plan.find_by(name: 'business_basic')&.update(
    name: 'business_basic',
    commit: 'Choose Business Basic')
end
