class CreateBillings < ActiveRecord::Migration[5.1]
  def change
    create_table :billings do |t|
      t.timestamps
      t.integer :user_id
      t.integer :plan_id, null: false
      t.integer :amount_in_cents, null: false
      t.integer :state, default: Billing.states[:new_registered]
      t.string :stripe_token
      t.date :finalized_on
      t.datetime :next_retry
      t.integer :retry_count
      t.string :stripe_error
      t.boolean :monthly

      t.index :user_id
      t.index :plan_id
      t.index :next_retry
    end

    add_foreign_key :billings, :users
    add_foreign_key :billings, :plans
  end
end
