class GoalsWayUnique < ActiveRecord::Migration[5.1]
  def change
    change_table :goals_ways do |t|
      t.index [:company_id, :description], unique: true
    end
  end
end
