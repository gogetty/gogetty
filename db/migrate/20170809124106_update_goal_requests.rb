class UpdateGoalRequests < ActiveRecord::Migration[5.1]
  def change
    change_table :goal_requests do |t|
      t.remove :comment
      t.text :response
    end
  end
end
