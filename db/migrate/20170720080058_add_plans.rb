class AddPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :plans do |t|
      t.string :name, null: false
      t.integer :monthly_cost, null: false
      t.integer :annual_cost, null: false
      t.boolean :active, null: false, default: true
      t.string :commit, null: false
      t.string :annual_stripe_name
      t.string :monthly_stripe_name

      t.index :name, unique: true
    end

    create_table :plans_users do |t|
      t.integer :user_id, null: false
      t.integer :plan_id, null: false
      t.date :activated_on, null: false
      t.date :payed_for_to, null: false
      t.date :cancelled_on
      t.index [:user_id, :plan_id]
      t.index [:plan_id, :user_id]
    end

    add_foreign_key :plans_users, :plans
    add_foreign_key :plans_users, :users
  end
end
