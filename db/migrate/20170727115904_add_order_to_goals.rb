class AddOrderToGoals < ActiveRecord::Migration[5.1]
  def change
    change_table :goals_ways do |t|
      t.integer :order, null: false, defualt: 1
      t.timestamps

      t.index [:company_id, :order], unique: true
    end
  end
end
