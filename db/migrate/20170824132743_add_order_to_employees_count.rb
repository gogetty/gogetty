class AddOrderToEmployeesCount < ActiveRecord::Migration[5.1]
  def change
    change_table :employees_counts do |t|
      t.integer :order, null: false, default: 1
    end
  end
end
