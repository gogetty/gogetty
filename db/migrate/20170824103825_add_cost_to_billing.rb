class AddCostToBilling < ActiveRecord::Migration[5.1]
  def change
    change_table :billings do |t|
      t.integer :cost
    end
  end
end
