class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.text :text, null: false
      t.integer :created_by, null: false
      t.integer :company_id, null: false
      t.timestamps

      t.index :created_by
      t.index :company_id
    end
    add_foreign_key :posts, :users, column: :created_by
    add_foreign_key :posts, :companies

    create_table :comments do |t|
      t.integer :created_by, null: false
      t.integer :post_id, null: false
      t.text :text, null: false
      t.timestamps

      t.index :created_by
      t.index :post_id
    end
    add_foreign_key :comments, :users, column: :created_by
    add_foreign_key :comments, :posts

    create_table :likes do |t|
      t.integer :created_by, null: false
      t.integer :post_id, null: false
      t.timestamps

      t.index :created_by
      t.index :post_id
    end
    add_foreign_key :likes, :users, column: :created_by
    add_foreign_key :likes, :posts
  end
end
