class RemoveMaxAdminCountFromPlans < ActiveRecord::Migration[5.1]
  def up
    remove_column :plans, :max_admin_count, :integer
  end

  def down
    add_column :plans, :max_admin_count, :integer
  end
end
