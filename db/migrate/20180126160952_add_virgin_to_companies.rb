class AddVirginToCompanies < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :virgin, :boolean
  end
end
