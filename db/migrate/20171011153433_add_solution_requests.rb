class AddSolutionRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :solution_requests do |t|
      t.string :name, null: false
      t.string :company, null: false
      t.integer :user_id, null: false

      t.index :user_id
    end

    add_foreign_key :solution_requests, :users
  end
end
