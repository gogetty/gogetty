class SeedTax < ActiveRecord::Migration[5.1]
  def change
    Plan.reset_column_information
    Plan.all.each do |plan|
      plan.tax = 0
      plan.save!
    end
  end
end
