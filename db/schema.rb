# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190521134547) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "admin_invites", force: :cascade do |t|
    t.string "email"
    t.integer "user_id"
    t.integer "company_id"
    t.boolean "cancelled", default: false, null: false
    t.boolean "accepted", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_admin_invites_on_company_id"
    t.index ["user_id"], name: "index_admin_invites_on_user_id"
  end

  create_table "admin_requests", force: :cascade do |t|
    t.integer "user_id"
    t.integer "company_id"
    t.boolean "cancelled", default: false, null: false
    t.boolean "accepted", default: false, null: false
    t.boolean "revoked", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_admin_requests_on_company_id"
    t.index ["user_id"], name: "index_admin_requests_on_user_id"
  end

  create_table "billings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "plan_id", null: false
    t.integer "amount_in_cents", null: false
    t.integer "state", default: 0
    t.string "stripe_token"
    t.date "finalized_on"
    t.datetime "next_retry"
    t.integer "retry_count"
    t.string "stripe_error"
    t.boolean "monthly"
    t.string "stripe_sub"
    t.string "stripe_charge"
    t.string "stripe_invoice"
    t.string "coupon"
    t.integer "coupon_amount_off"
    t.integer "coupon_percent_off"
    t.integer "tax", null: false
    t.integer "tax_value", null: false
    t.integer "cost", null: false
    t.index ["next_retry"], name: "index_billings_on_next_retry"
    t.index ["plan_id"], name: "index_billings_on_plan_id"
    t.index ["user_id"], name: "index_billings_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.integer "created_by", null: false
    t.integer "post_id", null: false
    t.text "text", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by"], name: "index_comments_on_created_by"
    t.index ["post_id"], name: "index_comments_on_post_id"
  end

  create_table "companies", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", null: false
    t.string "logo"
    t.integer "industry_id"
    t.string "website"
    t.integer "size_id"
    t.string "city"
    t.integer "founded_year"
    t.integer "owner_type_id"
    t.string "avatars"
    t.integer "country_id"
    t.boolean "virgin"
    t.integer "cvr_number"
    t.integer "nationalities_count", default: 0
    t.integer "languages_count", default: 0
    t.boolean "stock_listed", default: false
    t.string "slug"
    t.index ["country_id"], name: "index_companies_on_country_id"
    t.index ["industry_id"], name: "index_companies_on_industry_id"
    t.index ["owner_type_id"], name: "index_companies_on_owner_type_id"
    t.index ["slug"], name: "index_companies_on_slug", unique: true
  end

  create_table "companies_diversity_policies", force: :cascade do |t|
    t.integer "company_id"
    t.integer "diversity_policy_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id", "diversity_policy_id"], name: "index_companies_diversity_policies1", unique: true
    t.index ["diversity_policy_id", "company_id"], name: "index_companies_diversity_policies2", unique: true
  end

  create_table "company_information_requests", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "company_id", null: false
    t.text "comment"
    t.boolean "resolved", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_company_information_requests_on_company_id"
    t.index ["user_id"], name: "index_company_information_requests_on_user_id"
  end

  create_table "company_informations", force: :cascade do |t|
    t.integer "company_id"
    t.string "first_video"
    t.string "second_video"
    t.text "gender_diversity_plans"
    t.text "contact"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "diversity_plans_title", default: "Read our diversity plans for 2020"
    t.text "ethnicity_diversity_plans"
    t.index ["company_id"], name: "index_company_informations_on_company_id", unique: true
  end

  create_table "company_languages", force: :cascade do |t|
    t.integer "language_id"
    t.integer "company_id"
    t.integer "count", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "company_nationalities", force: :cascade do |t|
    t.integer "nationality_id"
    t.integer "company_id"
    t.integer "count", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nationality_id", "company_id"], name: "index_company_nationalities_on_nationality_id_and_company_id"
  end

  create_table "company_profile_histories", force: :cascade do |t|
    t.integer "company_id"
    t.integer "now_director_woman_count"
    t.integer "now_director_man_count"
    t.integer "goal_director_woman_count"
    t.integer "goal_director_man_count"
    t.integer "now_cxo_woman_count"
    t.integer "now_cxo_man_count"
    t.integer "goal_cxo_woman_count"
    t.integer "goal_cxo_man_count"
    t.integer "now_vp_woman_count"
    t.integer "now_vp_man_count"
    t.integer "goal_vp_woman_count"
    t.integer "goal_vp_man_count"
    t.integer "goal_year"
    t.integer "diversity_score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "company_profiles", force: :cascade do |t|
    t.integer "company_id"
    t.integer "now_director_woman_count", default: 0
    t.integer "now_director_man_count", default: 0
    t.integer "goal_director_woman_count", default: 0
    t.integer "goal_director_man_count", default: 0
    t.integer "now_cxo_woman_count", default: 0
    t.integer "now_cxo_man_count", default: 0
    t.integer "goal_cxo_woman_count", default: 0
    t.integer "goal_cxo_man_count", default: 0
    t.integer "now_vp_woman_count", default: 0
    t.integer "now_vp_man_count", default: 0
    t.integer "goal_vp_woman_count", default: 0
    t.integer "goal_vp_man_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "employees_count_id"
    t.decimal "liabilities", precision: 15, scale: 2
    t.decimal "revenue", precision: 15, scale: 2
    t.integer "goal_year", null: false
    t.integer "diversity_score", default: 0, null: false
    t.integer "goal_diversity_score", default: 0, null: false
    t.boolean "always_visible", default: true
    t.integer "now_middle_man_count", default: 0
    t.integer "now_middle_woman_count", default: 0
    t.integer "goal_middle_man_count", default: 0
    t.integer "goal_middle_woman_count", default: 0
    t.string "director_name", default: "Board of Directors"
    t.string "cxo_name", default: "CxO Level"
    t.string "vp_name", default: "VP Level"
    t.string "middle_name", default: "Middle Management"
    t.boolean "director_hide"
    t.boolean "cxo_hide"
    t.boolean "vp_hide"
    t.boolean "middle_hide"
    t.integer "now_director_woman_percentage", default: 0
    t.integer "now_director_man_percentage", default: 0
    t.integer "goal_director_woman_percentage", default: 0
    t.integer "goal_director_man_percentage", default: 0
    t.integer "now_cxo_woman_percentage", default: 0
    t.integer "now_cxo_man_percentage", default: 0
    t.integer "goal_cxo_woman_percentage", default: 0
    t.integer "goal_cxo_man_percentage", default: 0
    t.integer "now_middle_woman_percentage", default: 0
    t.integer "now_middle_man_percentage", default: 0
    t.integer "goal_middle_woman_percentage", default: 0
    t.integer "goal_middle_man_percentage", default: 0
    t.integer "now_vp_woman_percentage", default: 0
    t.integer "now_vp_man_percentage", default: 0
    t.integer "goal_vp_woman_percentage", default: 0
    t.integer "goal_vp_man_percentage", default: 0
    t.integer "now_director_total", default: 0
    t.integer "goal_director_total", default: 0
    t.integer "now_cxo_total", default: 0
    t.integer "goal_cxo_total", default: 0
    t.integer "now_middle_total", default: 0
    t.integer "goal_middle_total", default: 0
    t.integer "now_vp_total", default: 0
    t.integer "goal_vp_total", default: 0
    t.boolean "director_in_percenatge", default: false
    t.boolean "cxo_in_percenatge", default: false
    t.boolean "middle_in_percenatge", default: false
    t.boolean "vp_in_percenatge", default: false
    t.integer "total_diversity_score", default: 0
    t.index ["company_id"], name: "index_company_profiles_on_company_id", unique: true
    t.index ["employees_count_id"], name: "index_company_profiles_on_employees_count_id"
  end

  create_table "company_requests", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", null: false
    t.string "name", null: false
    t.boolean "resolved", default: false, null: false
    t.index ["user_id"], name: "index_company_requests_on_user_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "en_name", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "diversity_policies", force: :cascade do |t|
    t.string "name"
    t.boolean "always_visible", default: false, null: false
    t.boolean "hidden", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_diversity_policies_on_name", unique: true
  end

  create_table "diversity_policies_policy_requests", force: :cascade do |t|
    t.integer "policy_request_id", null: false
    t.integer "diversity_policy_id", null: false
    t.boolean "resolved", default: false, null: false
    t.index ["diversity_policy_id"], name: "index_diversity_policies_policy_requests_on_diversity_policy_id"
    t.index ["policy_request_id"], name: "index_diversity_policies_policy_requests_on_policy_request_id"
  end

  create_table "diversity_requests", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", null: false
    t.integer "company_id", null: false
    t.text "comment"
    t.boolean "resolved", default: false, null: false
    t.text "response"
    t.index ["company_id"], name: "index_diversity_requests_on_company_id"
    t.index ["user_id"], name: "index_diversity_requests_on_user_id"
  end

  create_table "employees_counts", force: :cascade do |t|
    t.string "count"
    t.integer "order", default: 1, null: false
    t.index ["count"], name: "index_employees_counts_on_count", unique: true
  end

  create_table "follows", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "company_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id", "company_id"], name: "index_follows_on_user_id_and_company_id", unique: true
  end

  create_table "goal_requests", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", null: false
    t.integer "company_id", null: false
    t.boolean "resolved", default: false, null: false
    t.text "response"
    t.index ["company_id"], name: "index_goal_requests_on_company_id"
    t.index ["user_id"], name: "index_goal_requests_on_user_id"
  end

  create_table "goals_ways", force: :cascade do |t|
    t.integer "company_id"
    t.string "description"
    t.boolean "hidden", default: false, null: false
    t.integer "order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id", "description"], name: "index_goals_ways_on_company_id_and_description", unique: true
    t.index ["company_id", "order"], name: "index_goals_ways_on_company_id_and_order", unique: true
    t.index ["company_id"], name: "index_goals_ways_on_company_id"
  end

  create_table "industries", force: :cascade do |t|
    t.string "name", null: false
    t.index ["name"], name: "index_industries_on_name", unique: true
  end

  create_table "languages", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "likes", force: :cascade do |t|
    t.integer "created_by", null: false
    t.integer "post_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by"], name: "index_likes_on_created_by"
    t.index ["post_id", "created_by"], name: "index_likes_on_post_id_and_created_by", unique: true
    t.index ["post_id"], name: "index_likes_on_post_id"
  end

  create_table "nationalities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "owner_types", force: :cascade do |t|
    t.string "name", null: false
    t.index ["name"], name: "index_owner_types_on_name", unique: true
  end

  create_table "partner_requests", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "surname"
    t.string "company_name"
    t.text "product"
  end

  create_table "payment_credentials", force: :cascade do |t|
    t.string "stripe_customer_id"
    t.integer "last4"
    t.string "exp_month"
    t.string "exp_year"
    t.string "card_type"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_payment_credentials_on_user_id"
  end

  create_table "plans", force: :cascade do |t|
    t.string "name", null: false
    t.integer "monthly_cost", null: false
    t.integer "annual_cost", null: false
    t.boolean "active", default: true, null: false
    t.string "commit", null: false
    t.string "annual_stripe_name"
    t.string "monthly_stripe_name"
    t.integer "tax", null: false
    t.index ["name"], name: "index_plans_on_name", unique: true
  end

  create_table "plans_users", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "plan_id", null: false
    t.date "activated_on", null: false
    t.date "payed_for_to", null: false
    t.date "cancelled_on"
    t.string "stripe_sub", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "cancel_scheduled", default: false, null: false
    t.boolean "not_payed_pro", default: false, null: false
    t.index ["plan_id", "user_id"], name: "index_plans_users_on_plan_id_and_user_id"
    t.index ["user_id", "plan_id"], name: "index_plans_users_on_user_id_and_plan_id"
  end

  create_table "platform_settings", force: :cascade do |t|
    t.text "google_analytics_head"
    t.text "google_analytics_body"
  end

  create_table "policy_requests", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", null: false
    t.integer "company_id", null: false
    t.text "comment"
    t.boolean "resolved", default: false, null: false
    t.text "response"
    t.index ["company_id"], name: "index_policy_requests_on_company_id"
    t.index ["user_id"], name: "index_policy_requests_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.text "text", null: false
    t.integer "created_by", null: false
    t.integer "company_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo"
    t.string "title"
    t.string "video"
    t.index ["company_id"], name: "index_posts_on_company_id"
    t.index ["created_by"], name: "index_posts_on_created_by"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "solution_requests", force: :cascade do |t|
    t.string "name", null: false
    t.string "company", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_solution_requests_on_user_id"
  end

  create_table "tell_friends", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "custom_message"
    t.integer "user_id"
    t.string "full_name"
    t.index ["user_id"], name: "index_tell_friends_on_user_id"
  end

  create_table "text_policies", force: :cascade do |t|
    t.string "name", null: false
    t.integer "company_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_text_policies_on_company_id"
  end

  create_table "text_policies_policy_requests", force: :cascade do |t|
    t.integer "policy_request_id", null: false
    t.string "name", null: false
    t.boolean "resolved", default: false, null: false
    t.index ["policy_request_id"], name: "index_text_policies_policy_requests_on_policy_request_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "role", default: 0
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.string "company_name"
    t.string "title"
    t.string "surname"
    t.boolean "company_representative"
    t.boolean "terms_agreed"
    t.boolean "newsletter_subscription"
    t.string "provider"
    t.string "uid"
    t.string "stripe_customer_id"
    t.integer "company_id"
    t.boolean "superadmin", default: false, null: false
    t.string "avatar"
    t.boolean "monthly_billing_permission", default: false
    t.index ["company_id"], name: "index_users_on_company_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "admin_invites", "companies"
  add_foreign_key "admin_invites", "users"
  add_foreign_key "admin_requests", "companies"
  add_foreign_key "admin_requests", "users"
  add_foreign_key "billings", "plans"
  add_foreign_key "billings", "users"
  add_foreign_key "comments", "posts"
  add_foreign_key "comments", "users", column: "created_by"
  add_foreign_key "companies", "countries"
  add_foreign_key "companies", "industries"
  add_foreign_key "companies", "owner_types"
  add_foreign_key "companies_diversity_policies", "companies"
  add_foreign_key "companies_diversity_policies", "diversity_policies"
  add_foreign_key "company_information_requests", "companies"
  add_foreign_key "company_information_requests", "users"
  add_foreign_key "company_informations", "companies"
  add_foreign_key "company_profiles", "companies"
  add_foreign_key "company_profiles", "employees_counts"
  add_foreign_key "company_requests", "users"
  add_foreign_key "diversity_policies_policy_requests", "diversity_policies"
  add_foreign_key "diversity_policies_policy_requests", "policy_requests"
  add_foreign_key "diversity_requests", "companies"
  add_foreign_key "diversity_requests", "users"
  add_foreign_key "follows", "companies"
  add_foreign_key "follows", "users"
  add_foreign_key "goal_requests", "companies"
  add_foreign_key "goal_requests", "users"
  add_foreign_key "goals_ways", "companies"
  add_foreign_key "likes", "posts"
  add_foreign_key "likes", "users", column: "created_by"
  add_foreign_key "payment_credentials", "users"
  add_foreign_key "plans_users", "plans"
  add_foreign_key "plans_users", "users"
  add_foreign_key "policy_requests", "companies"
  add_foreign_key "policy_requests", "users"
  add_foreign_key "posts", "companies"
  add_foreign_key "posts", "users", column: "created_by"
  add_foreign_key "solution_requests", "users"
  add_foreign_key "tell_friends", "users"
  add_foreign_key "text_policies", "companies"
  add_foreign_key "text_policies_policy_requests", "policy_requests"
  add_foreign_key "users", "companies"
end
