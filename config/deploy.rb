# config valid only for current version of Capistrano
lock "3.9.0"

set :application, 'gogetty'

set :repo_url, 'git@gitlab.ideamotiv.com:ideamotive/gogetty.git'
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# set :format, :airbrussh
set :format, :pretty
set :log_level, :debug

set :linked_files, fetch(:linked_files, []).push('config/secrets.yml.key',
                                                 'config/database.yml', '.env')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache',
                                                'tmp/sockets', 'vendor/bundle',
                                                'public/system', 'public/uploads',
                                                'node_modules')

set :keep_releases, 5

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.4.1'

namespace :deploy do
  task :myrestart do
    invoke 'puma:config'
    # invoke 'puma:restart'
    # Rake::Task['puma:restart'].reenable
  end

  after :finishing, 'deploy:cleanup'
end

after 'deploy:cleanup', 'deploy:myrestart'
