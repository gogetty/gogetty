set :user, 'stagings'
set :stage, :staging
set :rails_env, :staging
set :deploy_to, "/home/#{fetch(:user)}/app/#{fetch(:application)}/staging/"

if ENV['BRANCH']
  set :branch, ENV['BRANCH']
else
  set :branch, :dev
end
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# set :rollbar_token, 'xxxx'
# set :rollbar_env, Proc.new { fetch :stage }
# set :rollbar_role, Proc.new { :app }

server 's.ideamotiv.com', # 's.ideamotive.co'
  user: fetch(:user),
  roles: %w{web app db},
  ssh_options: {
    forward_agent: true,
    port: 22
  }

set :puma_user, fetch(:user)
set :puma_rackup, -> { File.join(current_path, 'config.ru') }
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_bind, ["unix://#{shared_path}/tmp/sockets/puma.sock"]    #accept array for multi-bind
set :puma_default_control_app, "unix://#{shared_path}/tmp/sockets/pumactl.sock"
set :puma_conf, "#{shared_path}/puma.rb"
set :puma_access_log, "#{shared_path}/log/puma_access.log"
set :puma_error_log, "#{shared_path}/log/puma_error.log"
set :puma_role, :app
set :puma_env, fetch(:rack_env, fetch(:rails_env, 'staging'))
set :puma_threads, [0, 8]
set :puma_workers, 3
set :puma_worker_timeout, nil
set :puma_init_active_record, true
set :puma_preload_app, true
set :puma_plugins, []  #accept array of plugins
