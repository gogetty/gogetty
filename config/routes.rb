# frozen_string_literal: true

Rails.application.routes.draw do
  scope '(:locale)', locale: /#{I18n.available_locales.join("|")}/ do
    Rails.application.routes.draw do
      root to: 'pages#show', id: 'talents'
      devise_for :users, controllers: {registrations: 'registrations',
                                       omniauth_callbacks: 'omniauth_callbacks',
                                       confirmations: 'confirmations'}
      # get 'high_voltage/pages', to: 'high_voltage#pages'
      get '/pages/*id' => 'pages#show'
      get '/companies/:company_id/posts/:id', to: 'user_logged/posts#show', as: 'post'
      resources :after_signups, only: [:index]
      resources :subscriptions, only: %i[new create]
      get '/subscriptions/edit', to: 'subscriptions#edit', as: 'edit_subscription'
      get '/subscriptions/confirm', to: 'subscriptions#confirm', as: 'confirm_subscription_upgrade'
      put '/subscriptions/upgrade', to: 'subscriptions#update', as: 'update_subscription'
      resources :stripe_hooks, only: [:create]
      resources :mandrill_hooks, only: [:create]
      resources :partner_requests, only: [:create]
      resource :resend_confirmations, only: %i[new create]
      resources :tell_friends, only: [:create]
      resources :solutions, only: [:index]
      resources :company_searches, only: [:index]
      get '/create_company_searches', to: 'company_searches#create', as: 'get_create_company_searches'
      post '/create_company_searches', to: 'company_searches#create', as: 'post_create_company_searches'
      namespace :user_logged do
        get '/company_admins/:id', to: 'company_admins#create', as: 'become_admin'
        resources :company_nationalities, only: [:create, :update, :destroy]
        resources :company_languages, only: [:create, :update, :destroy]
        resources :company_profiles, only: [:update]
        resources :company_data, only: [:update]
        resource :companies, only: %i[new create edit update]
        resources :companies, only: [:show]
        resources :company_informations, only: [:update]
        resources :company_information_requests, only: [:create]
        resources :companies_diversity_policies, only: %i[create destroy]
        resources :text_policies, only: %i[create destroy]
        resources :goals_ways, only: %i[create destroy]
        resources :charges, only: %i[new create]
        resource :account_settings, only: %i[show edit update]
        resources :policy_requests, only: [:create]
        resources :policy_request_fields, only: [:create]
        resources :goal_requests, only: [:create]
        resources :diversity_requests, only: [:create]
        resources :company_requests, only: [:create]
        resources :coupons, only: [:create]
        resources :comments, only: %i[create destroy]
        resources :likes, only: [:create]
        resources :posts, only: %i[create update destroy]
        resources :follows, only: [:create]
        resource :walls, only: [:show]
        namespace :solutions do
          resource :axiom, only: [:show]
          resource :ontheagenda, only: [:show]
          resource :inspired_beyond_babies, only: [:show]
          resource :potential_company, only: [:show]
          resource :engaged_leadership, only: [:show]
          resource :introdus, only: [:show]
        end
        resources :solution_requests, only: [:create]
        namespace :account_settings do
          resources :billings, only: [:index]
          resources :upcoming_payments, only: [:index]
          resources :plans_users, only: %i[index destroy]
          resources :admin_invites, only: %i[index update]
          resources :notifications, only: %i[index update]
          resources :payment_credentials, only: [:edit, :new, :update, :create]
        end
        resource :company_settings, only: %i[show edit update]
        namespace :company_settings do
          resources :admins, only: %i[index update destroy]
          resources :admin_invites, only: %i[create update]
          resources :admin_requests, only: %i[update destroy]
          resources :info_requests, only: [:index]
          resources :diversity_requests, only: [:index]
          resources :goal_requests, only: [:index]
          resources :policy_requests, only: [:index]
          resources :followers_stats, only: [:index]
        end
        resource :platform_settings, only: [:show, :update]
        namespace :platform_settings do
          resources :users, only: %i[index show update destroy]
          resources :companies
          resources :requests, only: [:index]
          resources :plans, only: %i[index edit update]
          resources :employees_counts, except: [:show]
          resources :industries, except: [:show]
          resources :company_information_requests, only: [:index]
          resources :diversity_requests, only: [:index]
          resources :goal_requests, only: [:index]
          resources :policy_requests, only: [:index]
          resources :request_companies, only: [:create]
          resources :nationalities, except: [:show]
          resources :languages, except: [:show]
          resource :user_exports, only: [:show]
        end
      end
    end
  end
end
