# frozen_string_literal: true

namespace :company do
  desc 'Update companies nationalities and languages counters'
  task update_counters: :environment do
    Company.find_each do |company|
      Company.reset_counters(company.id, :nationalities)
      Company.reset_counters(company.id, :languages)
    end
  end
end
