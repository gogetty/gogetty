# frozen_string_literal: true

namespace :diversity_score do
  desc 'Recalculating diversity score in all company profiles'
  task recalculate: :environment do
    CompanyProfile.all.each do |company_profile|
      ::Command::CompanyProfile::CalcDiversityScore.new(company_profile).call
    end
  end
end
