# frozen_string_literal: true

namespace :goal_year do
  desc 'Update goal year'
  task set2019: :environment do
    CompanyProfile.all.each do |company_profile|
      company_profile.update_attributes(goal_year: Time.zone.today.year + 1)
    end
  end

  # rubocop:disable SkipsModelValidations
  task update: :environment do
    Company.all.each do |company|
      if company.virgin
        company.company_profile.update_attributes(goal_year: Time.zone.today.year + 2)
        company.company_profile_histories.last.touch
      end
    end
  end
end
