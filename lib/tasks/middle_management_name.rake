# frozen_string_literal: true

namespace :middle_management_name do
  desc 'Set default middle management name in company profiles'
  task default: :environment do
    CompanyProfile.all.each do |company_profile|
      company_profile.update_attributes(middle_name: 'Middle Management')
    end
  end
end
