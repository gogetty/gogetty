# frozen_string_literal: true

namespace :companies do
  desc 'Generate slugs based on company name for URLs'
  task generate_slugs: :environment do
    Company.find_each { |company| company.save(touch: false) }
  end
end
