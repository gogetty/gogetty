# frozen_string_literal: true

namespace :company_profile do # rubocop:disable Metrics/BlockLength
  desc 'Set company profile fields to 0 instead of nil'
  task deletenil: :environment do
    CompanyProfile.where(now_director_man_count: nil).each do |company_profile|
      company_profile.update_attributes(
        now_director_man_count: 0,
        now_director_woman_count: 0,
        goal_director_man_count: 0,
        goal_director_woman_count: 0,
        now_cxo_man_count: 0,
        now_cxo_woman_count: 0,
        goal_cxo_man_count: 0,
        goal_cxo_woman_count: 0,
        now_vp_man_count: 0,
        now_vp_woman_count: 0,
        goal_vp_man_count: 0,
        goal_vp_woman_count: 0
      )
    end

    CompanyProfile.where(now_middle_man_count: nil).each do |company_profile|
      company_profile.update_attributes(
        now_middle_man_count: 0,
        now_middle_woman_count: 0,
        goal_middle_man_count: 0,
        goal_middle_woman_count: 0
      )
    end
  end
end
