# frozen_string_literal: true

namespace :company_profile_history do
  desc 'Creating company profile histories for every company'
  task create: :environment do
    CompanyProfile.all.each do |company_profile|
      ::Command::CompanyProfileHistory::Create.new(company_profile).call
    end
  end
end
