# frozen_string_literal: true

namespace :virgin_company do
  desc 'Set virgin as true if company has never had any admins'
  task set: :environment do
    Company.all.each do |company|
      if company.updated_at == company.created_at
        company.update_attributes(virgin: true)
      else
        company.update_attributes(virgin: false)
      end
    end
  end
end
