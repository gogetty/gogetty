# frozen_string_literal: true

task stats: 'todolist:statsetup'

namespace :todolist do
  task :statsetup do
    require 'rails/code_statistics'
    ::STATS_DIRECTORIES << %w[Policies app/policies]
    ::STATS_DIRECTORIES << %w[Decorators app/decorators]
    ::STATS_DIRECTORIES << %w[Forms app/forms]
    ::STATS_DIRECTORIES << %w[Services app/services]
    ::STATS_DIRECTORIES << %w[Uploaders app/uploaders]
    ::STATS_DIRECTORIES << %w[JavaScripts (webpack) app/javascript/application/scripts]
  end
end
