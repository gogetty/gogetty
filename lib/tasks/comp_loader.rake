# frozen_string_literal: true

require 'csv'

# rubocop:disable BlockLength
namespace :comp_loader do
  desc 'Loads companies from csv'
  task load: :environment do
    CSV.foreach(Rails.root.join('csv_seeds', '1000_companies.csv'), col_sep: ',') do |row|
      c = Company.find_by(name: row[0])
      if c
        puts "Company '#{row[0]}' already exists, omitting.."
        next
      end
      country_id = Country.find_by(en_name: 'Denmark')
      c = Company.new(name: row[0], country_id: country_id)
      puts "Created company '#{c.name}'"
      c.build_company_profile(
        now_director_woman_count: row[1],
        now_director_man_count: row[2],
        now_cxo_woman_count: row[3],
        now_cxo_man_count: row[4],
        now_vp_woman_count: row[5],
        now_vp_man_count: row[6],
        goal_year: Time.zone.now.year + 1
      )
      puts "Diversity score for '#{c.name}' " +
           ::Command::CompanyProfile::CalcDiversityScore.new(c.company_profile).call.data.to_s
      c.save
    end
    CSV.foreach(Rails.root.join('csv_seeds', 'industries.csv'), col_sep: ',') do |row|
      c = Company.find_by(name: row[0])
      if c
        c.city = row[3]
        c.founded_year = row[4]
        i = Industry.find_by(name: row[1])
        i ||= Industry.create(name: row[1])
        c.industry_id = i.id
        puts "Added industry data to company '#{c.name}'"
        c.save!
      end
    end
  end

  task load_belgium: :environment do
    CSV.foreach(Rails.root.join('csv_seeds', '100_Belgium.csv'), col_sep: ';') do |row|
      c = Company.find_by(name: row[0])
      if c
        puts "Company '#{row[0]}' already exists, omitting.."
        next
      end
      country_id = Country.find_by(en_name: 'Belgium').id
      c = Company.new(name: row[0], country_id: country_id, city: row[3], founded_year: row[4])
      puts "Created company '#{c.name}'"
      c.build_company_profile(
        now_director_woman_count: row[5],
        now_director_man_count: row[6],
        now_cxo_woman_count: row[7],
        now_cxo_man_count: row[8],
        now_vp_woman_count: row[9],
        now_vp_man_count: row[10],
        goal_year: Time.zone.now.year + 1
      )
      puts "Diversity score for '#{c.name}' " +
           ::Command::CompanyProfile::CalcDiversityScore.new(c.company_profile).call.data.to_s
      i = Industry.find_by(name: row[1])
      i ||= Industry.create(name: row[1])
      c.industry_id = i.id
      puts "Added industry data to company '#{c.name}'"
      c.save
    end
  end

  task load_finland: :environment do
    CSV.foreach(Rails.root.join('csv_seeds', '100_Finland.csv'), col_sep: ';') do |row|
      c = Company.find_by(name: row[0])
      if c
        puts "Company '#{row[0]}' already exists, omitting.."
        next
      end
      country_id = Country.find_by(en_name: 'Finland').id
      c = Company.new(name: row[0], country_id: country_id, city: row[3], founded_year: row[4])
      puts "Created company '#{c.name}'"
      c.build_company_profile(
        goal_year: Time.zone.now.year + 1
      )
      puts "Diversity score for '#{c.name}' " +
           ::Command::CompanyProfile::CalcDiversityScore.new(c.company_profile).call.data.to_s
      i = Industry.find_by(name: row[1])
      i ||= Industry.create(name: row[1])
      c.industry_id = i.id
      puts "Added industry data to company '#{c.name}'"
      c.save
    end
  end

  task load_netherlands: :environment do
    CSV.foreach(Rails.root.join('csv_seeds', '100_Netherlands.csv'), col_sep: ';') do |row|
      c = Company.find_by(name: row[0])
      if c
        puts "Company '#{row[0]}' already exists, omitting.."
        next
      end
      country_id = Country.find_by(en_name: 'Netherlands').id
      c = Company.new(name: row[0], country_id: country_id, city: row[3], founded_year: row[4])
      puts "Created company '#{c.name}'"
      c.build_company_profile(
        goal_year: Time.zone.now.year + 1
      )
      i = Industry.find_by(name: row[1])
      i ||= Industry.create(name: row[1])
      c.industry_id = i.id
      puts "Added industry data to company '#{c.name}'"
      c.save
    end
  end

  task load_norway: :environment do
    CSV.foreach(Rails.root.join('csv_seeds', '100_Norway.csv'), col_sep: ';') do |row|
      c = Company.find_by(name: row[0])
      if c
        puts "Company '#{row[0]}' already exists, omitting.."
        next
      end
      country_id = Country.find_by(en_name: 'Norway').id
      c = Company.new(name: row[0], country_id: country_id)
      puts "Created company '#{c.name}'"
      c.build_company_profile(
        goal_year: Time.zone.now.year + 1
      )
      c.save
    end
  end

  task load_sweden: :environment do
    CSV.foreach(Rails.root.join('csv_seeds', '100_Sweden.csv'), col_sep: ';') do |row|
      c = Company.find_by(name: row[0])
      if c
        puts "Company '#{row[0]}' already exists, omitting.."
        next
      end
      country_id = Country.find_by(en_name: 'Sweden').id
      c = Company.new(name: row[0], country_id: country_id)
      puts "Created company '#{c.name}'"
      c.build_company_profile(
        goal_year: Time.zone.now.year + 1
      )
      c.save
    end
  end
end
# rubocop:enable all
