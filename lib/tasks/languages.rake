# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
namespace :languages do
  desc 'Seed Languages'
  task seed: :environment do
    [
      'Afrikaans',
      'Albanian',
      'Arabic',
      'Armenian',
      'Basque',
      'Bengali',
      'Bulgarian',
      'Catalan',
      'Cambodian',
      'Chinese (Mandarin)',
      'Croatian',
      'Czech',
      'Danish',
      'Dutch',
      'English',
      'Estonian',
      'Fiji',
      'Finnish',
      'French',
      'Georgian',
      'German',
      'Greek',
      'Gujarati',
      'Hebrew',
      'Hindi',
      'Hungarian',
      'Icelandic',
      'Indonesian',
      'Irish',
      'Italian',
      'Japanese',
      'Javanese',
      'Korean',
      'Latin',
      'Latvian',
      'Lithuanian',
      'Macedonian',
      'Malay',
      'Malayalam',
      'Maltese',
      'Maori',
      'Marathi',
      'Mongolian',
      'Nepali',
      'Norwegian',
      'Persian',
      'Polish',
      'Portuguese',
      'Punjabi',
      'Quechua',
      'Romanian',
      'Russian',
      'Samoan',
      'Serbian',
      'Slovak',
      'Slovenian',
      'Spanish',
      'Swahili',
      'Swedish',
      'Tamil',
      'Tatar',
      'Telugu',
      'Thai',
      'Tibetan',
      'Tonga',
      'Turkish',
      'Ukrainian',
      'Urdu',
      'Uzbek',
      'Vietnamese',
      'Welsh',
      'Xhosa'
    ].each do |language|
      if Language.find_by(name: language)
        puts "Language: #{language} already exists"
      else
        Language.create(name: language)
        puts "Language: #{language} created"
      end
    end
  end
end
