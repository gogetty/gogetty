1. Technological infrastructure
2. Each of the Company's products/components including size (lines of code).
3. Databases deployed
4. IT policies and procedures

# 1. Technological infrastructure
## PROJECT REQUIREMENTS:

* Ruby 2.4.1
* Rails 5.1.2
* Node & Yarn
* PostgreSQL + unaccent extension

## Ruby on Rails
Application is written in Ruby programming language and uses Rails framework to provide the best possible experiences to both application users and developers involved in creation of GoGetty.

Rails provides basis for web application and consist of solid conventions, including MVC. 

MVC is a software architectural pattern for implementing user interfaces on computers. It divides a given application into three interconnected parts. This is done to separate internal representations of information from the ways information is presented to, and accepted from, the user. The MVC design pattern decouples these major components allowing for efficient code reuse and parallel development.

## Form objects (Virtus)
GoGetty uses Ruby gem Virtus to decouple form processing from controller code. Form objects provide data validation and parameters processing for corresponding forms in user interface.

Form objects are stored in `/app/forms/` folder and live in `::Form` namespace.

## Services (CQRS)
Form objects and services provide complete way to simplify controllers and make them only process HTTP requests and call other objects to process data. Parts of code, thanks to CQRS, can be moved away from controllers and also can be reused in profficient way.

Services are divided into commands (`/app/services/command/`) and queries (`/app/services/query/`).

Commands are used to process data (usually provided by form objects). Commands work directly with data in database and provide responses in form of `::Command::Response` objects.

Queries are used for complicated data lookups and also use `::Command::Response` to send responses back to the calling objects.

All commands live in `::Command` namespace and all queries live in `::Query` namespace.

## Decorators (Draper)
To simplify view templates Ruby gem Draper is used. It provides way to add methods to object before view rendering starts. Thanks to usage of decorators view templates can be made significantly simpler.

## Automatic testing (Rspec)
GoGetty application utilizes power of automatic testing using Rspec as a way of providing test specifiations and FactoryGirl as a way to provide test data.

All tests live in `/spec` folder and can be run by usign `rspec` command.

Tests are forced before every `git push` by overcommit hooks.

## Static code analyzer
Rubocop is used in development process to provide the best code quality possible. File `.rubocop.yml` provides Rubocop configurations used in development process.

Rubocop gem has to be installed in the system to use static code analyzer.

Rubocop code analyze is forced before every `git commit` by overcommit hooks.

## Overcommit
Overcommit is used to run git hooks.

To manage automatic Rubocop and Rspec running before `git commit` and `git push` user `.overcommit.yml` file. It provides Overcommit settings. To make use of this file overcommit has to be installed in the system.


_________________
# 2. Each of the Company's products/components including size (lines of code).
GoGetty files statistics showed by `rake stats` command:

```
+----------------------+--------+--------+---------+---------+-----+-------+
| Name                 |  Lines |    LOC | Classes | Methods | M/C | LOC/M |
+----------------------+--------+--------+---------+---------+-----+-------+
| Controllers          |   1782 |   1474 |      58 |     187 |   3 |     5 |
| Helpers              |    162 |    125 |       1 |      22 |  22 |     3 |
| Jobs                 |      4 |      2 |       1 |       0 |   0 |     0 |
| Models               |    330 |    237 |      30 |       9 |   0 |    24 |
| Mailers              |    245 |    221 |       6 |      12 |   2 |    16 |
| Channels             |     12 |      8 |       2 |       0 |   0 |     0 |
| Libraries            |     57 |     51 |       0 |       0 |   0 |     0 |
| Policies             |    119 |     88 |       7 |      22 |   3 |     2 |
| Decorators           |    589 |    458 |      12 |      97 |   8 |     2 |
| Forms                |    916 |    737 |      29 |      57 |   1 |    10 |
| Services             |   2952 |   2450 |      62 |     307 |   4 |     5 |
| Uploaders            |     32 |     22 |       2 |       2 |   1 |     9 |
| JavaScripts          |    493 |    468 |       0 |      57 |   0 |     6 |
| Controller specs     |    180 |    159 |       0 |       0 |   0 |     0 |
| Feature specs        |    235 |    116 |       0 |       0 |   0 |     0 |
| Model specs          |     13 |      8 |       0 |       0 |   0 |     0 |
| Service specs        |   1183 |   1048 |       0 |       0 |   0 |     0 |
+----------------------+--------+--------+---------+---------+-----+-------+
| Total                |   9304 |   7672 |     210 |     772 |   3 |     7 |
+----------------------+--------+--------+---------+---------+-----+-------+
  Code LOC: 6341     Test LOC: 1331     Code to Test Ratio: 1:0.2
```

_________________
# 3. Databases deployed
GoGetty application runs on Heroku platform and uses PostgreSQL database deployed on the same platform.

Database schematics are included in file `/model_domain.pdf`.

_________________
# 4. IT policies and procedures

## Environments

### Development
To start development server follow this steps:

1. Setup Ruby in your favorite way (usign rbenv, rvm etc.). Ruby version used in project at time of writing this document is version 2.4.1, current version is stored in `.ruby-version` file in project root.
1. Install nodeJS (usign apt-get, brew or etc. depending on your system) to manage JavaScript dependencies.
1. Run 'gem install bundler' to install bundler, which manages Ruby dependencies.
1. Clone git repository.
1. Provide `secrets.yml.key` file (copy it to `/config/` directory) or `SECRET_KEY_BASE` enviroment variable.
1. Run `bundle && yarn` to install Ruby gem and JavaScript dependencies.
1. Copy `/config/database.yml.orig` to `/config/database.yml` and input your credentials to PostgreSQL database into the latter file.
2. Setup database by running `rake db:create && rake db:migrate && rake db:seed`.
3. Add `unaccent` extension to database by running `CREATE EXTENSION unaccent;`.
1. Configure external services by following descriptions of them in next sections of this documentation.
1. Run `foreman start` to start your development server on port 5000 (puma + webpack-dev-server).

### Staging (Ideamotive)
* Staging runs on production configuration, although it's independent environment due to usage of different secrets than production
* Staging address: `https://gogetty.ideamotive.co`
* Login (HTTP Authentication): `gogetty`
* Password (HTTP Authentication): `diversity`
* Run `bundle exec cap staging deploy && bundle exec cap staging puma:restart` to deploy 'dev' branch from repository to staging using capistrano. You have to have your access keys configured properly first.

### Production
Production application and database runs on Heroku. For more information about how Heroku works please refer to their documentation at `https://devcenter.heroku.com/articles`.

Here is summary of the most needed knowledge about production server maintenance and application deployment:

* Production address: [https://www.gogetty.co]().
* SECURITY: Production uses free SSL from Let's Encrypt with automatic reneval configured on Heroku to encrypt data send between user browser and application server.
* SCALING: Production uses Heroku PRO plan which allows dynamic horizontal application scaling. It can be set by using GUI accessible at [https://heroku.com](). Please refer to documentation to learn more about this process.
* DATABASE: First database setup can be done by using `heroku run rake db:create && heroku run rake db:migrate && heroku run rake db:seed` and adding `unaccent` extension by running `CREATE EXTENSION unaccent`. To access database directly from CLI run `heroku pg:psql`. For automatic database migrations set environment variable `RAKE_DB_COMMAND` to `rake db:migrate`, otherwise set to `read`.
* DATABASE BACKUP & RECOVERY: Heroku free database plan (used at the time of writing this document) offers automatic backup scheduling. Backup is set to be done at 3:00 UTC every day and up to 2 backups can be retained.
Paid plans offer additional layer of security allowing database to be rolled back to exact moment in time. Time period availble varies for different plans, more information can be obtained by followind this link: [https://elements.heroku.com/addons/heroku-postgresql](). Again, to learn more details about this process and to get current information please refer to Heroku documentation.
* DEPLOYMENT: To push code to production login into Heroku from command line run 'git push heroku master'. Remember that push is done from your local version of repository! Also remember that pushing code doesn't run database migrations, they have to be run separately.
* OTHER SETUP: You have to provide `SECRET_KEY_BASE` for Heroku to be able to decrypt secrets file.
* PRODUCTION ERRORS & PROBLEMS: Heroku is set to e-mail subscribed users when configured criteria are met, for example error level or CPU usage rises above set level. This settings can be change in Heroku panel at [https://heroku.com]().

### Test
Test environment is used for automatic testing.

## Security & data privacy
### Data encryption
Production uses free SSL from Let's Encrypt with automatic reneval configured on Heroku to encrypt data send between user browser and application server. All communication to and from application forces end-to-end data encryption.

### Database
Application connects to database using internal Heroku infrastructure and automatic credentials enrollment. Database password can be rotated by running `heroku pg:credentials --reset`.

### Sensitive data - SECRETS file
All sensitive application configurations for environments other than test are stored in encrypted `/config/secrets.enc.yml` file. To edit this file you must provide encryption key and run `EDITOR=nano bin/rails secrets:edit`. You can use your favorite text editor instead of `nano`.

Secrets file was transfered to shared project Google Drive and through there is availble for eligible GoGetty personel. It's availible at [https://drive.google.com/open?id=0B24h7AuUqd7WTk1VVS1ScmFLMmc]().

## External services
### Stripe configuration:

Stripe is payment service. Stripe is used to process all payment information. Note that credit card info never touches GoGetty directly - it's PCI compliant solution.

Stripe API depends on two-way communication. If you want to test payments locally, your application has to be accessible over the web (you can use tools like ngrok). Stripe uses webhooks to communicate with GoGetty, address of test webhook is configurable in Stripe admin panel (remember to switch to 'Test Data').

Stripe webhook processor is accessible for POST requests on `/stripe_hooks` inside GoGetty. HTTP Authentication data has to be included in address so full address looks like `https://user:password@www.gogetty.co/stripe_hooks`.
User, password and Stripe API keys are stored in SECRETS.

Endpoint configuration in Stripe should have Event filter set to send only this types of webhooks: 

* `customer.subscription.deteted`
* `invoice.payment_failed`
* `invoice.payment_succeeded`

When in development or staging webhooks are set to fail silently so Stripe doesn't throw endpoint errors every time tests are run. Production webhooks errors are returned with HTTP 400 to Stripe.

Stripe payments depend on preconfigured plans. Plans configuration has to be provided in both Stripe and GoGetty Platform Admin Console. Note that GoGetty is ready to have only 2 paid plan names configured, one for monthly subscription and one for annual subscription.

Discount coupons are configured only in Stripe and are checked dynamically in billing process.

Stripe API keys are stored in SECRETS (test keys for development and staging environments and production keys for production environment).

### LinkedIn configuration:

LinkedIn is used to authenticate users.

You have to configure your authentication callback in LinkedIn. It's only redirection - it doesn't have to be visible in web. Callback path is `/users/auth/linkedin/callback`, so for example for local development you can use `http://localhost:5000/users/auth/linkedin/callback`.

LinkedIn API keys are stored in SECRETS.


### Mandrill:

Mandrill is MailChimp extension used to send transactional email using templates prepared in MailChimp console. After updating MailChimp template in MailChimp console remeber to send it to Mandrill.

Mandrill uses `mail_domain_name` from SECRETS file as a domain to send mails from. It has to be configured to work in Mandrill (see Mandrill domain configuration manual if you want to learn more about domain configuration). In opposition to `domain_name` (which is used to generate urls), `mail_domain_name` can be the same for every environment. 

Mandrill API keys are stored in SECRETS.

ActionMailer shouldn't be used.


### Rollbar (Ideamotive):

Rollbar is service to aggregate application errors and it's configured to report errors from staging and production environments.

Rollbar token is stored in SECRETS


### Amazon AWS S3:

Amazon S3 is used on every environment except test to store user uploaded files and other static assets such as favicons. Amazon S3 configurations is stored in `/config/initializers/carrierwave.rb`.

Amazon API keys are stored in SECRETS.


### Recaptcha:

Recaptcha can be used to secure user sign up process.

Recaptcha is turned off due to decision made during development process. It can be turned on by uncommenting some code in user registration and providing valid API keys into SECRETS file.
