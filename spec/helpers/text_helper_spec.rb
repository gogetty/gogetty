# frozen_string_literal: true

require 'text_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#format_text' do
    include ERB::Util

    it 'formats the text' do
      text = "foo\r\nbar\r\n\r\nfoo"
      expect(helper.format_text(text)).to eq(simple_format(text))
    end

    it 'escapes html tags' do
      text = "<script>alert('pwned')</script>"
      expect(helper.format_text(text)).to eq(simple_format(h(text)))
      expect(helper.format_text(text)).to_not eq(simple_format(text))
    end

    it 'wraps valid URLs with a http or https protocol in <a> tags' do
      url = Faker::Internet.url
      text = "text #{url} text"
      expect(helper.format_text(text)).to eq("<p>text <a href=\"#{url}\">#{url}</a> text</p>")
    end

    it 'wraps valid URLs without a http or https protocol in <a> tags' do
      urls = ['google.com', 'www.google.com', 'www.google.com/test']
      urls.each do |url|
        text = "text #{url} text"
        expect(helper.format_text(text)).to eq("<p>text <a href=\"//#{url}\">#{url}</a> text</p>")
      end
    end

    it 'does not wrap valid URLs in <a> tags' do
      urls = ['ftp://foobar', 'website:', 'www.']

      urls.each do |url|
        expect(helper.format_text(url)).to eq("<p>#{url}</p>")
      end
    end
  end
end
