# frozen_string_literal: true

FactoryGirl.define do
  factory :employees_count do
    count { '1000+' + ('a'..'z').to_a.shuffle.join }
  end
end
