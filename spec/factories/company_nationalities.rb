# frozen_string_literal: true

FactoryGirl.define do
  factory :company_nationality do
    nationality
    company
    count { rand(1..10) }
  end
end
