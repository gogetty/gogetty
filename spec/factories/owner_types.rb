# frozen_string_literal: true

FactoryGirl.define do
  factory :owner_type do
    name { 'Test owner type' + ('a'..'z').to_a.shuffle.join }
  end
end
