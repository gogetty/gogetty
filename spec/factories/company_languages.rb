# frozen_string_literal: true

FactoryGirl.define do
  factory :company_language do
    language
    company
    count { rand(1..10) }
  end
end
