# frozen_string_literal: true

FactoryGirl.define do
  factory :nationality do
    name { Faker::Address.unique.country }
  end
end
