# frozen_string_literal: true

FactoryGirl.define do
  factory :language do
    name { Faker::Address.unique.country }
  end
end
