# frozen_string_literal: true

FactoryGirl.define do
  factory :company do
    name 'Test company'
    city 'Test city'
    industry

    trait :second do
      name 'Second test company'
    end

    trait :danish do
      association :country, :denmark
      cvr_number { rand(10_000_000..99_999_999) }
    end
  end
end
