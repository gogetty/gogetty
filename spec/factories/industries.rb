# frozen_string_literal: true

FactoryGirl.define do
  factory :industry do
    name { 'Test industry' + ('a'..'z').to_a.shuffle.join }
  end
end
