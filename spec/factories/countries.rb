# frozen_string_literal: true

FactoryGirl.define do
  factory :country do
    en_name { 'Country' }

    trait :denmark do
      en_name { 'Denmark' }
    end
  end
end
