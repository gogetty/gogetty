# frozen_string_literal: true

FactoryGirl.define do
  factory :user do
    confirmed_at Time.current
    name 'John'
    surname 'Smith'
    email 'test@example.com'
    password 'please123'
    company_name 'Test Company'
    title 'Nothing Manager'
    terms_agreed true

    trait :admin do
      role 'admin'
    end

    trait :second do
      email 'test2@example.com'
    end

    trait :company_admin do
      company
    end
  end
end
