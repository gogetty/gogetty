# frozen_string_literal: true

FactoryGirl.define do
  factory :plans_user do
    payed_for_to { Time.zone.today + 3.days }
    activated_on { Time.zone.today - 3.days }
    association :plan, :paid
    stripe_sub 'qwerty'
  end
end
