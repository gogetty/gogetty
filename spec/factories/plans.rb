# frozen_string_literal: true

FactoryGirl.define do
  factory :plan do
    tax 23

    trait :paid do
      name 'diverse_business'
      monthly_cost 34_900
      annual_cost 376_920
      commit 'Choose Business'
      monthly_stripe_name 'diverse_business_monthly'
      annual_stripe_name 'diverse_business_yearly'
    end

    trait :inclusive do
      name 'inclusive_business'
      monthly_cost 120_000
      annual_cost 1_115_200
      commit 'Choose Business'
      monthly_stripe_name 'inclusive_business_monthly'
      annual_stripe_name 'inclusive_business_yearly'
    end

    trait :paid_once do
      name 'diverse_business'
      monthly_cost 34_900
      annual_cost 376_920
      commit 'Choose Business'
    end

    trait :free do
      name 'business_basic'
      monthly_cost 0
      annual_cost 0
      commit 'Choose Business Basic'
    end
  end
end
