# frozen_string_literal: true

FactoryGirl.define do
  factory :company_profile do
    goal_year Time.zone.today.year + 1
    company

    trait :second do
      association :company, :second
    end

    trait :danish do
      association :company, :danish
    end

    trait :full do
      now_director_woman_count 1
      now_director_man_count 1
      goal_director_woman_count 1
      goal_director_man_count 1
      now_cxo_woman_count 1
      now_cxo_man_count 1
      goal_cxo_woman_count 1
      goal_cxo_man_count 1
      now_vp_woman_count 1
      now_vp_man_count 1
      goal_vp_woman_count 1
      goal_vp_man_count 1
      goal_year 1
    end
  end
end
