# frozen_string_literal: true

FactoryGirl.define do
  factory :post do
    text 'Test post'
    association :creator, factory: :user
    company
  end
end
