# frozen_string_literal: true

FactoryGirl.define do
  factory :diversity_policy do
    name 'Test diversity'
  end
end
