# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::DiversityRequest::Create, type: :controller do
  describe 'signed in user creates new info request diversity for company' do
    it 'succedess' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      create_params = {
        company_id: company_profile.company_id
      }
      diversity_request_form = ::Form::UserLogged::DiversityRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::DiversityRequest::Create.new(diversity_request_form).call
      expect(result.success?).to be
    end
  end

  describe 'signed in user creates new diversity request for company with diveristy score' do
    it 'failes, because no question was asked' do
      company_profile = FactoryGirl.create(:company_profile, :full)
      user = FactoryGirl.create(:user, company: company_profile.company)
      FactoryGirl.create(:plans_user, user: user)

      sign_in(user)
      create_params = {
        company_id: company_profile.company_id
      }
      diversity_request_form = ::Form::UserLogged::DiversityRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::DiversityRequest::Create.new(diversity_request_form).call
      expect(result.success?).to_not be
    end
  end
end
