# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Plan::Update, type: :controller do
  describe 'user tries to update plan' do
    it 'expects plan to be updates' do
      user = FactoryGirl.create(:user, :admin)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      @form = ::Form::UserLogged::PlatformSettings::Plans::Edit.find(plan)
      @form.attributes = {
        monthly_cost:         '210',
        monthly_stripe_name:  'mothly_diverse',
        annual_cost:          '2100',
        annual_stripe_name:   'yearly_diverse',
        tax:                  '23'
      }
      result = ::Command::Plan::Update.new(@form, id: plan.id).call
      expect(result.success?).to be
      expect(plan.reload.monthly_cost).to eq 210
    end
  end

  describe 'user tries to update plan' do
    it 'expects fail because of lacking params' do
      user = FactoryGirl.create(:user, :admin)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      @form = ::Form::UserLogged::PlatformSettings::Plans::Edit.find(plan)
      @form.attributes = {
        monthly_cost:         '210',
        monthly_stripe_name:  'mothly_diverse',
        annual_cost:          '',
        annual_stripe_name:   'yearly_diverse',
        tax:                  '23'
      }
      result = ::Command::Plan::Update.new(@form, id: plan.id).call
      # puts
      # puts result.inspect
      expect(result.success?).to_not be
    end
  end

  describe 'user tries to update plan' do
    it 'expects fail because cost not a number' do
      user = FactoryGirl.create(:user, :admin)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      @form = ::Form::UserLogged::PlatformSettings::Plans::Edit.find(plan)
      @form.attributes = {
        monthly_cost:         '210a',
        monthly_stripe_name:  'mothly_diverse',
        annual_cost:          '2100',
        annual_stripe_name:   'yearly_diverse',
        tax:                  '23'
      }
      result = ::Command::Plan::Update.new(@form, id: plan.id).call
      # puts
      # puts result.inspect
      expect(result.success?).to_not be
    end
  end
end
