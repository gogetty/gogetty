# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanyInformationRequest::Create, type: :controller do
  describe 'information request sent by a signed in user' do
    it 'creates an information request' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      company = company_profile.company
      create_params = {comment: 'F00b4r', company_id: company_profile.company_id, user_id: user.id}

      form = ::Form::UserLogged::CompanyInformationRequest::New.new(create_params)

      result = ::Command::CompanyInformationRequest::Create.new(form).call

      expect(result.success?).to be
      expect(company.company_information_requests.count).to eq 1
    end
  end
end
