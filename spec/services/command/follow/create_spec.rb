# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Follow::Toggle, type: :controller do
  describe 'tries to add valid follow to post' do
    it 'expects to succeed, second time removes follow' do
      company_profile = FactoryGirl.create(:company_profile)
      user = FactoryGirl.create(:user)
      create_params = {
        company_id: company_profile.company_id
      }
      form = ::Form::UserLogged::Follow::New.new(create_params.merge(user_id: user.id))
      result = ::Command::Follow::Toggle.new(form).call
      # puts result.inspect
      expect(result.success?).to be
      expect(Follow.all.count).to eq 1
      result = ::Command::Follow::Toggle.new(form).call
      # puts result.inspect
      expect(result.success?).to be
      expect(Follow.all.count).to eq 0
    end
  end
end
