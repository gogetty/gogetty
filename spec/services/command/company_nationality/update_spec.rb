# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanyNationality::Update, type: :controller do
  let(:company_nationality) { FactoryGirl.create(:company_nationality) }
  let(:user) { FactoryGirl.create(:user, company: company_nationality.company) }
  let(:plan) { FactoryGirl.create(:plans_user, user: user) }

  before do
    company_nationality.save!
    sign_in(user)
  end

  context 'form sent by a privileged user' do
    describe 'with single nationality with valid data' do
      it 'updates the nationality' do
        parameters = [{
          id: company_nationality.id,
          count: 4
        }]
        form = ::Form::UserLogged::CompanyNationality::Edit.find(user.company_id)
        result = ::Command::CompanyNationality::Update.new(form, parameters).call
        expect(result.success?).to be
        company_nationality.reload
        expect(company_nationality.count).to be_eql(parameters.first[:count])
      end
    end

    describe 'without data being changed' do
      it 'does not update nationality' do
        form = ::Form::UserLogged::CompanyNationality::Edit.find(user.company_id)
        result = ::Command::CompanyNationality::Update.new(form, [{}]).call
        expect(result.success?).to be
        expect(company_nationality).to be_equal(company_nationality.reload)
        expect(result.data).to include('No record has been updated')
      end
    end

    describe 'with multiple nationalities' do
      it 'updates each nationality' do
        FactoryGirl.create_list(:company_nationality, 5, company: company_nationality.company)

        form = ::Form::UserLogged::CompanyNationality::Edit.find(user.company_id)
        parameters = []
        form.each do |n|
          parameters << {
            'id' => n.id,
            'count' => n.count + 1
          }
        end

        result = ::Command::CompanyNationality::Update.new(form, parameters).call
        expect(result.success?).to be
        form.each_with_index do |n, i|
          expect(n.reload.attributes).to include(parameters[i])
        end
        expect(result.data).to include('6 records successfully updated')
      end
    end

    describe 'with invalid data' do
      it 'returns errors for empty input' do
        parameters = [{
          count: ''
        }]
        form = ::Form::UserLogged::CompanyNationality::Edit.find(user.company_id)
        result = ::Command::CompanyNationality::Update.new(form, parameters).call

        expect(result.success?).to_not be
        expect(company_nationality).to be_equal(company_nationality.reload)
        expect(result.error).to include("Count can't be blank",
                                        'Count is not a number')
      end

      it 'returns errors for negative count' do
        parameters = [{
          count: -1
        }]
        form = ::Form::UserLogged::CompanyNationality::Edit.find(user.company_id)
        result = ::Command::CompanyNationality::Update.new(form, parameters).call

        expect(result.success?).to_not be
        expect(company_nationality).to be_equal(company_nationality.reload)
        expect(result.error).to include('Count must be greater than 0')
      end
    end
  end
end
