# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::GoalRequest::Create, type: :controller do
  describe 'signed in user creates new info request way for company' do
    it 'succedess' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      create_params = {
        company_id: company_profile.company_id
      }
      goal_request_form = ::Form::UserLogged::GoalRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::GoalRequest::Create.new(goal_request_form).call
      expect(result.success?).to be
    end
  end
end
