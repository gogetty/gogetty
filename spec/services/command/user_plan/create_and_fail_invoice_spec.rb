# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::UserPlan::Cancel, type: :controller do
  describe 'user creates subsription having invalid card' do
    it 'expects to not create user plan, '\
       'invoice.payment_failed hook creates billing with error state' do
      user = FactoryGirl.create(:user)
      plan = FactoryGirl.create(:plan, :paid)
      expect(Billing.all.count).to eq 0
      billing = ::Command::Billing::CreateBillingByPlan.new(
        user,
        plan,
        true
      ).call.data
      expect(Billing.all.count).to eq 1
      session[:current_billing_id] = billing.id
      expect(user.stripe_customer_id).to_not be
      customer = ::Command::StripeCustomer::CreateStripeCustomer.new(
        user,
        stripeToken: 'tok_chargeCustomerFail'
      ).call.data
      session[:current_billing_id] = Billing.first.id
      expect(user.stripe_customer_id).to be
      subscription = ::Command::Charge::CreateSubscritpion.new(
        customer,
        user,
        session,
        plan.monthly_stripe_name
      ).call
      # puts
      # puts subscription.inspect
      expect(subscription.success?).to be false
      params = {}
      params[:data] = {}
      params[:data][:object] = {}
      params[:data][:object][:lines] = {}
      params[:data][:object][:lines][:data] = []
      params[:data][:object][:lines][:data][0] = {}
      params[:data][:object][:lines][:data][0][:plan] = {}
      params[:data][:object][:amount_due] = 34_900
      params[:data][:object][:id] = 'in_mockup'
      params[:data][:object][:charge] = 'ch_mockup'
      params[:data][:object][:lines][:data][0][:plan][:id] = plan.monthly_stripe_name
      params[:type] = 'invoice.payment_failed'
      params[:data][:object][:subscription] = 'mockup_subscription'
      params[:data][:object][:customer] = customer.id
      params[:data][:object][:total] = 34_900 * (100 + plan.tax) / BigDecimal.new('100')
      params[:data][:object][:tax_percent] = plan.tax
      params[:data][:object][:tax] = 34_900 * plan.tax / BigDecimal.new('100')
      expect(::PlansUser.all.count).to eq 0
      ::Command::StripeHooks::HooksDispatcher.new(params).call
      expect(::PlansUser.all.count).to eq 0
      expect(::Billing.where(state: ::Billing.states[:error]).count).to eq 2
      expect(::PlansUser.all.count).to eq 0
    end
  end
end
