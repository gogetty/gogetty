# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::UserPlan::Cancel, type: :controller do
  describe 'user creates subsription, other user tries to cancel, '\
           'then owner cancels his own subscription' do
    it 'expects to create user plan then set cancel flag to subscription, '\
       'for other trying to cancel subscription stays untouched' do
      user = FactoryGirl.create(:user)
      second_user = FactoryGirl.create(:user, :second)
      plan = FactoryGirl.create(:plan, :paid)
      expect(Billing.all.count).to eq 0
      billing = ::Command::Billing::CreateBillingByPlan.new(user,
                                                            plan,
                                                            true).call.data
      expect(Billing.all.count).to eq 1
      session[:current_billing_id] = billing.id
      expect(user.stripe_customer_id).to_not be
      customer = ::Command::StripeCustomer::CreateStripeCustomer.new(user,
                                                                     stripeToken: 'tok_visa').call.data
      session[:current_billing_id] = Billing.first.id
      expect(user.stripe_customer_id).to be
      subscription = ::Command::Charge::CreateSubscritpion.new(customer,
                                                               user,
                                                               session,
                                                               plan.monthly_stripe_name).call.data
      params = {}
      params[:data] = {}
      params[:data][:object] = {}
      params[:data][:object][:lines] = {}
      params[:data][:object][:lines][:data] = []
      params[:data][:object][:lines][:data][0] = {}
      params[:data][:object][:lines][:data][0][:plan] = {}
      params[:data][:object][:amount_due] = 34_900
      params[:data][:object][:id] = 'in_mockup'
      params[:data][:object][:charge] = 'ch_mockup'
      params[:data][:object][:lines][:data][0][:plan][:id] = plan.monthly_stripe_name
      params[:type] = 'invoice.payment_succeeded'
      params[:data][:object][:subscription] = subscription.id
      params[:data][:object][:customer] = customer.id
      params[:data][:object][:total] = 34_900 * (100 + plan.tax) / BigDecimal.new('100')
      params[:data][:object][:tax_percent] = plan.tax
      params[:data][:object][:tax] = 34_900 * plan.tax / BigDecimal.new('100')
      expect(PlansUser.all.count).to eq 0
      ::Command::StripeHooks::HooksDispatcher.new(params).call
      expect(PlansUser.all.count).to eq 1
      expect(PlansUser.where(cancel_scheduled: true).count).to eq 0
      ::Command::UserPlan::Cancel.new(second_user, PlansUser.first.id).call
      expect(PlansUser.where(cancel_scheduled: true).count).to eq 0
      ::Command::UserPlan::Cancel.new(user, PlansUser.first.id).call
      expect(PlansUser.where(cancel_scheduled: true).count).to eq 1
      cancel_params = {}
      cancel_params[:data] = {}
      cancel_params[:data][:object] = {}
      cancel_params[:type] = 'customer.subscription.deleted'
      cancel_params[:data][:object][:id] = subscription.id
      cancel_params[:data][:object][:customer] = customer.id
      expect(PlansUser.where('cancelled_on IS NOT NULL').count).to eq 0
      ::Command::StripeHooks::HooksDispatcher.new(cancel_params).call
      expect(PlansUser.where('cancelled_on IS NOT NULL').count).to eq 1
    end
  end
end
