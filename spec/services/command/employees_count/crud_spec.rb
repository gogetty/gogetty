# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::EmployeesCount::Create, type: :controller do
  describe 'user creates, edits and destroys employees count' do
    it 'succedess' do
      # create fail
      new_form = ::Form::UserLogged::PlatformSettings::EmployeesCounts::New.new(
        count: '',
        order: '1'
      )
      result = ::Command::EmployeesCount::Create.new(new_form).call
      expect(result.success?).to_not be
      expect(EmployeesCount.all.count).to eq 0
      # create
      new_form = ::Form::UserLogged::PlatformSettings::EmployeesCounts::New.new(
        count: '100',
        order: '1'
      )
      result = ::Command::EmployeesCount::Create.new(new_form).call
      expect(result.success?).to be
      expect(EmployeesCount.all.count).to eq 1
      expect(EmployeesCount.first.count).to eq '100'
      # edit
      edit_form = ::Form::UserLogged::PlatformSettings::EmployeesCounts::Edit.find(
        EmployeesCount.first
      )
      edit_form.attributes = {count: '200'}
      result = ::Command::EmployeesCount::Update.new(edit_form, EmployeesCount.first).call
      expect(result.success?).to be
      expect(EmployeesCount.all.count).to eq 1
      expect(EmployeesCount.first.count).to eq '200'
      # destroy
      result = ::Command::EmployeesCount::Destroy.new(EmployeesCount.first).call
      expect(result.success?).to be
      expect(EmployeesCount.all.count).to eq 0
    end
  end
end
