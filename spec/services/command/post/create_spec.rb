# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Post::Create, type: :controller do
  describe 'tries to add valid post to post' do
    it 'expects to succeed' do
      user = FactoryGirl.create(:user)
      company_profile = FactoryGirl.create(:company_profile)
      create_params = {
        company_id: company_profile.company_id,
        text: 'Test',
        photo: '',
        video: ''
      }
      form = ::Form::UserLogged::Post::New.new(create_params.merge(created_by: user.id))
      result = ::Command::Post::Create.new(form).call
      expect(result.success?).to be
      expect(Post.all.count).to eq 1
    end
  end

  describe 'tries to add invalid post to post' do
    it 'expects to fail' do
      user = FactoryGirl.create(:user)
      company_profile = FactoryGirl.create(:company_profile)
      create_params = {
        company_id: company_profile.company_id,
        text: '',
        photo: '',
        video: ''
      }
      form = ::Form::UserLogged::Post::New.new(create_params.merge(created_by: user.id))
      result = ::Command::Post::Create.new(form).call
      expect(result.success?).to_not be
      expect(Post.all.count).to eq 0
    end
  end
end
