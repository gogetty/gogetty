# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Like::Toggle, type: :controller do
  describe 'tries to add valid like to post' do
    it 'expects to succeed, second time removes like' do
      post = FactoryGirl.create(:post)
      create_params = {
        post_id: post.id
      }
      form = ::Form::UserLogged::Like::New.new(create_params.merge(created_by: post.created_by))
      result = ::Command::Like::Toggle.new(form).call
      # puts result.inspect
      expect(result.success?).to be
      expect(Like.all.count).to eq 1
      result = ::Command::Like::Toggle.new(form).call
      # puts result.inspect
      expect(result.success?).to be
      expect(Like.all.count).to eq 0
    end
  end
end
