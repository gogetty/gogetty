# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::GoalsWay::Create, type: :controller do
  describe 'privileged user creates new goals way for company' do
    it 'creates valid goals way, error for duplicate' do
      user = FactoryGirl.create(:user, :company_admin)
      sign_in(user)
      create_params = {
        description: 'Testing'
      }
      form = ::Form::UserLogged::GoalsWay::New.new(
        create_params.merge(company_id: user.company_id)
      )
      result = ::Command::GoalsWay::Create.new(form).call
      expect(result.success?).to be
      result = ::Command::GoalsWay::Create.new(form).call
      expect(result.success?).to_not be
    end
  end
end
