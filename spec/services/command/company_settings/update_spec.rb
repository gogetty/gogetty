# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanySettings::Update, type: :controller do
  describe 'user tries to update company' do
    it 'expects company to be updates' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      PlansUser.create(user_id: user.id,
                       plan_id: plan.id,
                       activated_on: Time.zone.today,
                       payed_for_to: Time.zone.today,
                       stripe_sub: 'mock sub')
      company_profile = FactoryGirl.create(:company_profile)
      user.company_id = company_profile.company_id
      user.save!
      industry = FactoryGirl.create(:industry)
      employees_count = FactoryGirl.create(:employees_count)
      owner_type = FactoryGirl.create(:owner_type)
      timestamps_created = company_profile.company.created_at
      timestamps_updated = company_profile.company.updated_at
      @company = ::Form::UserLogged::CompanySettings::Edit.find(user.company)
      @company.attributes = {
        name:               'Testing',
        industry_id:        industry.id,
        website:            'www.test.com',
        city:               'City',
        employees_count_id: employees_count.id,
        founded_year:       2009,
        owner_type_id:      owner_type.id
      }
      result = ::Command::CompanySettings::Update.new(user.company, @company).call
      expect(result.success?).to be
      c_at = Company.find(company_profile.company_id).created_at
      expect(
        c_at.change(usec: c_at.usec)
      ).to eq timestamps_created.change(usec: timestamps_created.usec)
      u_at = Company.find(company_profile.company_id).updated_at
      expect(
        u_at.change(usec: u_at.usec)
      ).to_not eq timestamps_updated.change(usec: timestamps_updated.usec)
    end
  end
  it 'expects to validate presence of cvr number only for a Danish company' do
    user = FactoryGirl.create(:user)
    sign_in(user)
    plan = FactoryGirl.create(:plan, :paid)
    PlansUser.create(user_id: user.id,
                     plan_id: plan.id,
                     activated_on: Time.zone.today,
                     payed_for_to: Time.zone.today,
                     stripe_sub: 'mock sub')
    company_profile = FactoryGirl.create(:company_profile, :danish)
    user.company_id = company_profile.company_id
    user.save!
    FactoryGirl.create(:industry)
    FactoryGirl.create(:employees_count)
    FactoryGirl.create(:owner_type)
    @company = ::Form::UserLogged::CompanySettings::Edit.find(user.company)
    @company.attributes = {cvr_number: ''}
    result = ::Command::CompanySettings::Update.new(user.company, @company).call
    expect(result.success?).to be false
  end

  describe 'platfrom admin tries to update company' do
    it 'expects company to be updates with timestampts not touched' do
      user = FactoryGirl.create(:user, :admin)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      industry = FactoryGirl.create(:industry)
      employees_count = FactoryGirl.create(:employees_count)
      owner_type = FactoryGirl.create(:owner_type)
      timestamps_created = company_profile.company.created_at
      timestamps_updated = company_profile.company.updated_at
      company = ::Form::UserLogged::CompanySettings::Edit.find(company_profile.company)
      company.attributes = {
        name:               'Testing',
        industry_id:        industry.id,
        website:            'www.test.com',
        city:               'City',
        employees_count_id: employees_count.id,
        founded_year:       2009,
        owner_type_id:      owner_type.id,
        cvr_number:         rand(10_000_000..99_999_999)
      }
      result = ::Command::CompanySettings::Update.new(company_profile.company, company, user).call
      expect(result.success?).to be
      c_at = Company.find(company_profile.company_id).created_at
      expect(
        c_at.change(usec: c_at.usec)
      ).to eq timestamps_created.change(usec: timestamps_created.usec)
      u_at = Company.find(company_profile.company_id).updated_at
      expect(
        u_at.change(usec: u_at.usec)
      ).to eq timestamps_updated.change(usec: timestamps_updated.usec)
    end
  end
end
