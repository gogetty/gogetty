# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanySettings::AdminRequests::Revoke, type: :controller do
  describe 'admin revokes become admin request' do
    it 'request becomes revoked' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      admin_request = AdminRequest.create(user_id: user.id, company_id: company_profile.company_id)
      result = ::Command::CompanySettings::AdminRequests::Revoke.new(id: admin_request.id).call
      expect(result.success?).to be
      expect(AdminRequest.find(admin_request.id).revoked).to be
    end
  end
end
