# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanySettings::AdminRequests::Update, type: :controller do
  describe 'admin accepts become admin request for non-admin' do
    it 'requestee becomes admin' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      admin_request = AdminRequest.create(user_id: user.id, company_id: company_profile.company_id)
      result = ::Command::CompanySettings::AdminRequests::Update.new(id: admin_request.id).call
      # fail without subscription
      expect(result.success?).to_not be
      plan = FactoryGirl.create(:plan, :paid)
      PlansUser.create(user_id: user.id,
                       plan_id: plan.id,
                       activated_on: Time.zone.today,
                       payed_for_to: Time.zone.today,
                       stripe_sub: 'mock sub')
      result = ::Command::CompanySettings::AdminRequests::Update.new(id: admin_request.id).call
      # success with subscription
      expect(result.success?).to be
      expect(User.find(user.id).company_id).to eq company_profile.company_id
      expect(User.find(user.id).superadmin).to_not be
      expect(AdminRequest.where(user_id: user.id, cancelled: false, accepted: false).count).to eq 0
      result = ::Command::CompanySettings::AdminRequests::Update.new(id: admin_request.id).call
      # tries to accept again
      expect(result.success?).to_not be
      admin_request_2 = AdminRequest.create(user_id: user.id, company_id: company_profile.company_id)
      result = ::Command::CompanySettings::AdminRequests::Update.new(id: admin_request_2.id).call
      # tries to accept when user is already an admin
      expect(result.success?).to_not be
    end
  end
end
