# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanySettings::AdminInvites::Create, type: :controller do
  describe 'superadmin tries invite person to be admin' do
    it 'expects to invite user who is not admin or doesnt exist '\
       'and deny inviting user, who already is an admin or is invited' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      user.company_id = company_profile.company_id
      user.superadmin = true
      user.save!
      form = ::Form::UserLogged::CompanySettings::AdminInvites::New.new(
        email: 'test@test.com',
        company_id: user.company_id
      )
      result = ::Command::CompanySettings::AdminInvites::Create.new(form,
                                                                    email: 'test@test.com').call
      expect(result.success?).to be
      expect(AdminInvite.where(email: 'test@test.com').count).to eq 1
      # second invite denied
      form = ::Form::UserLogged::CompanySettings::AdminInvites::New.new(
        email: 'test@test.com',
        company_id: user.company_id
      )
      result = ::Command::CompanySettings::AdminInvites::Create.new(form,
                                                                    email: 'test@test.com').call
      expect(result.success?).to_not be
      expect(AdminInvite.where(email: 'test@test.com').count).to eq 1
      # invite for already admin denied
      second_user = FactoryGirl.create(:user, :second)
      second_user.company_id = company_profile.company_id
      second_user.superadmin = false
      second_user.save!
      form = ::Form::UserLogged::CompanySettings::AdminInvites::New.new(
        email: second_user.email,
        company_id: user.company_id
      )
      result = ::Command::CompanySettings::AdminInvites::Create.new(form,
                                                                    email: second_user.email).call
      expect(result.success?).to_not be
      expect(AdminInvite.where(email: second_user.email).count).to eq 0
    end
  end
end
