# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanySettings::Admins::Destroy, type: :controller do
  describe 'superadmin tries to destroy himself '\
           'then destroy other admin' do
    it 'expects denial for himself, success for other user' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      user.company_id = company_profile.company_id
      user.superadmin = true
      user.save!
      second_user = FactoryGirl.create(:user, :second)
      second_user.company_id = company_profile.company_id
      second_user.superadmin = true
      second_user.save!
      result = ::Command::CompanySettings::Admins::Destroy.new({id: user.id}, user).call
      expect(result.success?).to_not be
      expect(User.find(user.id).company_id).to be
      # destroy other superadmin
      result = ::Command::CompanySettings::Admins::Destroy.new({id: second_user.id}, user).call
      expect(result.success?).to be
      expect(User.find(second_user.id).company_id).to_not be
    end
  end
end
