# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanySettings::Admins::Update, type: :controller do
  describe 'superadmin tries to revoke superadmin to admin for himself '\
           'then give and revoke for other admin' do
    it 'expects denial for himself, success for other user' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      user.company_id = company_profile.company_id
      user.superadmin = true
      user.save!
      second_user = FactoryGirl.create(:user, :second)
      second_user.company_id = company_profile.company_id
      second_user.superadmin = false
      second_user.save!
      admins_form = ::Form::UserLogged::CompanySettings::Admins::Edit.find(id: user.id)
      admins_form.attributes = {
        superadmin: false
      }
      result = ::Command::CompanySettings::Admins::Update.new(admins_form,
                                                              {id: user.id},
                                                              user).call
      expect(result.success?).to_not be
      expect(User.find(user.id).superadmin).to be
      # add other superadmin
      admins_form = ::Form::UserLogged::CompanySettings::Admins::Edit.find(id: second_user.id)
      admins_form.attributes = {
        superadmin: true
      }
      result = ::Command::CompanySettings::Admins::Update.new(admins_form,
                                                              {id: second_user.id},
                                                              user).call
      expect(result.success?).to be
      expect(User.find(second_user.id).superadmin).to be
      # revoke other superadmin
      admins_form = ::Form::UserLogged::CompanySettings::Admins::Edit.find(id: second_user.id)
      admins_form.attributes = {
        superadmin: false
      }
      result = ::Command::CompanySettings::Admins::Update.new(admins_form,
                                                              {id: second_user.id},
                                                              user).call
      expect(result.success?).to be
      expect(User.find(second_user.id).superadmin).to_not be
    end
  end
end
