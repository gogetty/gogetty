# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Industry::Create, type: :controller do
  describe 'user creates, edits and destroys industries' do
    it 'succedess' do
      # create fail
      new_form = ::Form::UserLogged::PlatformSettings::Industries::New.new(
        name: ''
      )
      result = ::Command::Industry::Create.new(new_form).call
      expect(result.success?).to_not be
      expect(Industry.all.count).to eq 0
      # create
      new_form = ::Form::UserLogged::PlatformSettings::Industries::New.new(
        name: 'Tech'
      )
      result = ::Command::Industry::Create.new(new_form).call
      expect(result.success?).to be
      expect(Industry.all.count).to eq 1
      expect(Industry.first.name).to eq 'Tech'
      # edit
      edit_form = ::Form::UserLogged::PlatformSettings::Industries::Edit.find(
        Industry.first
      )
      edit_form.attributes = {name: 'Tech'}
      result = ::Command::Industry::Update.new(edit_form, Industry.first).call
      expect(result.success?).to be
      expect(Industry.all.count).to eq 1
      expect(Industry.first.name).to eq 'Tech'
      # destroy
      result = ::Command::Industry::Destroy.new(Industry.first).call
      expect(result.success?).to be
      expect(Industry.all.count).to eq 0
    end
  end
end
