# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanyRequest::Create, type: :controller do
  describe 'signed in user creates new request from search' do
    it 'succedess' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      create_params = {
        name: 'Test',
        notify: '1'
      }
      company_request_form = ::Form::UserLogged::CompanyRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::CompanyRequest::Create.new(company_request_form).call
      expect(result.success?).to be
    end
  end

  describe 'signed in user creates new request from search without checking box' do
    it 'failes' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      create_params = {
        name: 'Test'
      }
      company_request_form = ::Form::UserLogged::CompanyRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::CompanyRequest::Create.new(company_request_form).call
      expect(result.success?).to_not be
    end
  end
end
