# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanyData::Update, type: :controller do
  describe 'privileged user updates company data' do
    it 'updates for valid info, error for invalid' do
      user = FactoryGirl.create(:user)
      company_profile = FactoryGirl.create(:company_profile)
      user.company_id = company_profile.company_id
      company_profile.save!
      employees_count = FactoryGirl.create(:employees_count)
      sign_in(user)
      update_params = {
        employees_count_id: employees_count.id,
        liabilities: 2010.20,
        revenue: 201.22
      }
      form = ::Form::UserLogged::CompanyData::Edit.find(user.company_id)
      form.attributes = update_params
      result = ::Command::CompanyData::Update.new(form).call
      expect(result.success?).to be
      update_params[:liabilities] = 'aaa'
      form.attributes = update_params
      result = ::Command::CompanyData::Update.new(form).call
      expect(result.success?).to_not be
      update_params[:liabilities] = 2010
      update_params[:employees_count_id] = nil
      form.attributes = update_params
      result = ::Command::CompanyData::Update.new(form).call
      expect(result.success?).to_not be
    end
  end
end
