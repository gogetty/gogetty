# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanySettings::AdminInvites::Update, type: :controller do
  describe 'person accepts admin invite ' do
    it 'expects to become admin, when active subscription and isnt already admin, '\
       'expects to cancel other invites after accepting, expects to not become admin ' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile_1 = FactoryGirl.create(:company_profile)
      company_profile_2 = FactoryGirl.create(:company_profile, :second)
      user.save!
      admin_invite_1 = AdminInvite.create(user_id: user.id, company_id: company_profile_1.company_id)
      admin_invite_2 = AdminInvite.create(user_id: user.id, company_id: company_profile_2.company_id)
      plan = FactoryGirl.create(:plan, :paid)
      PlansUser.create(user_id: user.id,
                       plan_id: plan.id,
                       activated_on: Time.zone.today,
                       payed_for_to: Time.zone.today,
                       stripe_sub: 'mock sub')
      result = ::Command::AccountSettings::AdminInvites::Update.new({id: admin_invite_1.id},
                                                                    user).call
      # success with subscription
      expect(result.success?).to be
      expect(User.find(user.id).company_id).to eq company_profile_1.id
      expect(User.find(user.id).superadmin).to be
      expect(AdminInvite.where(user_id: user.id, cancelled: false, accepted: false).count).to eq 0
      expect(AdminInvite.find(admin_invite_2.id).cancelled).to be
    end

    it 'expects to become admin with no paid subscription' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile_1 = FactoryGirl.create(:company_profile)
      company_profile_2 = FactoryGirl.create(:company_profile, :second)
      user.save!
      admin_invite_1 = AdminInvite.create(user_id: user.id, company_id: company_profile_1.company_id)
      admin_invite_2 = AdminInvite.create(user_id: user.id, company_id: company_profile_2.company_id)
      result = ::Command::AccountSettings::AdminInvites::Update.new({id: admin_invite_1.id},
                                                                    user).call
      expect(result.success?).to be
      expect(User.find(user.id).company_id).to eq company_profile_1.id
      expect(User.find(user.id).superadmin).to be
      expect(AdminInvite.where(user_id: user.id, cancelled: false, accepted: false).count).to eq 0
      expect(AdminInvite.find(admin_invite_2.id).cancelled).to be
    end
  end
end
