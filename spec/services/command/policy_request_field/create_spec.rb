# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::PolicyRequestField::Create, type: :controller do
  describe 'signed in user creates new field in policy request form' do
    it 'succedess' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      create_params = ActionController::Parameters.new(
        policy_request: {
          comment: 'Testing',
          company_id: company_profile.company_id
        }
      )
      result = ::Command::PolicyRequestField::Create.new(create_params).call
      expect(result.success?).to be
      expect(result.data.class).to eq ::Form::UserLogged::PolicyRequest::New
      expect(result.data.diversity_policies_policy_requests.count).to eq 1
    end
  end
end
