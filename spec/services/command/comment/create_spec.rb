# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Comment::Create, type: :controller do
  describe 'tries to add valid comment to post' do
    it 'expects to succeed' do
      post = FactoryGirl.create(:post)
      create_params = {
        text: 'Test',
        post_id: post.id
      }
      form = ::Form::UserLogged::Comment::New.new(create_params.merge(created_by: post.created_by))
      result = ::Command::Comment::Create.new(form).call
      expect(result.success?).to be
      expect(Comment.all.count).to eq 1
    end
  end

  describe 'tries to add invalid comment to post' do
    it 'expects to fail' do
      post = FactoryGirl.create(:post)
      create_params = {
        text: '',
        post_id: post.id
      }
      form = ::Form::UserLogged::Comment::New.new(create_params.merge(created_by: post.created_by))
      result = ::Command::Comment::Create.new(form).call
      expect(result.success?).to_not be
      expect(Comment.all.count).to eq 0
    end
  end
end
