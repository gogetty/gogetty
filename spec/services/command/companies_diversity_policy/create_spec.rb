# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompaniesDiversityPolicy::Create, type: :controller do
  describe 'privileged user creates new diversity policies for company' do
    it 'creates valid diversity policy, error for duplicate' do
      user = FactoryGirl.create(:user, :company_admin)
      sign_in(user)
      diversity_policy = FactoryGirl.create(:diversity_policy)
      create_params = {
        diversity_policy_id: diversity_policy.id
      }
      form = ::Form::UserLogged::CompaniesDiversityPolicy::New.new(
        create_params.merge(company_id: user.company_id)
      )
      result = ::Command::CompaniesDiversityPolicy::Create.new(form).call
      expect(result.success?).to be
      result = ::Command::CompaniesDiversityPolicy::Create.new(form).call
      expect(result.success?).to_not be
    end
  end
end
