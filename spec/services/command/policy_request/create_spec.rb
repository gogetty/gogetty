# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::PolicyRequest::Create, type: :controller do
  describe 'signed in user creates new policy request way for company with predefined' do
    it 'succedess' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      diversity_policy = FactoryGirl.create(:diversity_policy)
      create_params = {
        company_id: company_profile.company_id,
        diversity_policies_policy_requests_attributes: {
          '0': {
            diversity_policy_id: diversity_policy.id
          }
        }
      }
      policy_request_form = ::Form::UserLogged::PolicyRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::PolicyRequest::Create.new(policy_request_form).call
      expect(result.success?).to be
    end
  end

  describe 'signed in user creates new policy request way for company with comment' do
    it 'succedess' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      create_params = {
        company_id: company_profile.company_id,
        comment: 'Test'
      }
      policy_request_form = ::Form::UserLogged::PolicyRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::PolicyRequest::Create.new(policy_request_form).call
      expect(result.success?).to be
    end
  end

  describe 'signed in user creates new policy request way for company' do
    it 'failes, becouse non sub requests where choosen and non comment provied' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company_profile = FactoryGirl.create(:company_profile)
      create_params = {
        company_id: company_profile.company_id
      }
      policy_request_form = ::Form::UserLogged::PolicyRequest::New.new(
        create_params.merge(user_id: user.id)
      )
      result = ::Command::PolicyRequest::Create.new(policy_request_form).call
      expect(result.success?).to_not be
    end
  end
end
