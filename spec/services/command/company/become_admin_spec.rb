# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Company::BecomeAdmin, type: :controller do
  describe 'user with active subscription tries to become admin for company with no admins' \
           'then second user tries to become an admin' do
    it 'expects user to become superadmin for given company' \
       'then second user to not become admin of any kind' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      PlansUser.create(user_id: user.id,
                       plan_id: plan.id,
                       activated_on: Time.zone.today,
                       payed_for_to: Time.zone.today,
                       stripe_sub: 'mock sub')
      company = FactoryGirl.create(:company)
      expect(User.find(user.id).company_id).to_not be
      form = ::Form::Company::BecomeAdmin.new(acceptance: true)
      ::Command::Company::BecomeAdmin.new(user, form, company.id).call
      expect(User.find(user.id).company_id).to eq company.id
      expect(User.find(user.id).superadmin).to be
      # can't automatically become admin if there's already admin for company
      second_user = FactoryGirl.create(:user, :second)
      PlansUser.create(user_id: second_user.id,
                       plan_id: plan.id,
                       activated_on: Time.zone.today,
                       payed_for_to: Time.zone.today,
                       stripe_sub: 'mock sub')
      expect(User.find(second_user.id).company_id).to_not be
      result = ::Command::Company::BecomeAdmin.new(second_user, form, company.id).call
      expect(result.success?).to be
      expect(User.find(second_user.id).company_id).to_not be
      expect(User.find(second_user.id).superadmin).to_not be
      expect(AdminRequest.where(user_id: second_user.id,
                                company_id: company.id,
                                cancelled: false,
                                accepted: false,
                                revoked: false).count).to eq 1
      # can't become admin if already is for another comapny
      second_company = FactoryGirl.create(:company, :second)
      result = ::Command::Company::BecomeAdmin.new(user, form, second_company.id).call
      expect(result.success?).to_not be
    end
  end

  describe 'user with inactive subscription tries to become admin for company with no admins' do
    it 'expects user to not become superadmin for given company' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      company = FactoryGirl.create(:company)
      expect(user.company_id).to_not be
      form = ::Form::Company::BecomeAdmin.new(acceptance: true)
      ::Command::Company::BecomeAdmin.new(user, form, company.id).call
      expect(user.company_id).to_not be
      expect(user.superadmin).to_not be
    end
  end
end
