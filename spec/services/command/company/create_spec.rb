# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::Company::BecomeAdmin, type: :controller do
  describe 'user with active subscription tries create companies' do
    it 'expects only first company to be created, new company has matching timestamps' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      PlansUser.create(user_id: user.id,
                       plan_id: plan.id,
                       activated_on: Time.zone.today,
                       payed_for_to: Time.zone.today,
                       stripe_sub: 'mock sub')
      form = ::Form::UserLogged::Company::New.new(name: 'Test', acceptance: true)
      result = ::Command::Company::Create.new(form, user).call
      expect(result.success?).to be
      expect(Company.all.count).to eq 1
      expect(Company.first.created_at).to eq Company.first.updated_at
      # duplicate name
      form = ::Form::UserLogged::Company::New.new(name: 'Test', acceptance: true)
      result = ::Command::Company::Create.new(form, user).call
      expect(result.success?).to_not be
      # puts "Duplicate name: #{result.error}"
      expect(Company.all.count).to eq 1
      # missing acceptance
      form = ::Form::UserLogged::Company::New.new(name: 'Test1', acceptance: false)
      result = ::Command::Company::Create.new(form, user).call
      expect(result.success?).to_not be
      # puts "Missing acceptance: #{result.error}"
      expect(Company.all.count).to eq 1
      # already an admin
      form = ::Form::UserLogged::Company::New.new(name: 'Test3', acceptance: true)
      result = ::Command::Company::Create.new(form, user).call
      expect(result.success?).to_not be
      # puts "Already an admin: #{result.error}"
      expect(Company.all.count).to eq 1
    end
  end

  describe 'user with inactive subscription tries to create company' do
    it 'expects company to be created' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      form = ::Form::UserLogged::Company::New.new(name: 'Test', acceptance: true)
      result = ::Command::Company::Create.new(form, user).call
      expect(result.success?).to be
      expect(Company.all.count).to eq 1
    end
  end
end
