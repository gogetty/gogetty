# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanyProfile::Update, type: :controller do
  let(:company_profile) { FactoryGirl.create(:company_profile) }
  let(:user) { FactoryGirl.create(:user, company: company_profile.company) }
  let(:plan) { FactoryGirl.create(:plans_user, user: user) }

  before do
    company_profile.save!
    sign_in(user)
  end

  describe 'privileged user updates company profile' do
    it 'updates for valid info, error for invalid' do
      update_params = {
        now_director_woman_count: 1,
        now_director_man_count: 2,
        goal_director_woman_count: 3,
        goal_director_man_count: 4,
        now_cxo_woman_count: 5,
        now_cxo_man_count: 6,
        goal_cxo_woman_count: 7,
        goal_cxo_man_count: 8,
        now_vp_woman_count: 4,
        now_vp_man_count: 5,
        goal_vp_woman_count: 6,
        goal_vp_man_count: 7,
        now_middle_woman_count: 2,
        now_middle_man_count: 2,
        goal_middle_woman_count: 2,
        goal_middle_man_count: 2,
        goal_year: Time.zone.today.year + 1
      }
      form = ::Form::UserLogged::CompanyProfile::Edit.find(user.company_id)
      form.attributes = update_params
      result = ::Command::CompanyProfile::Update.new(form).call
      expect(result.success?).to be
      expect(company_profile.reload.diversity_score).to eq 44
      expect(company_profile.reload.goal_diversity_score).to eq 46
      update_params[:goal_year] = 2016
      form.attributes = update_params
      result = ::Command::CompanyProfile::Update.new(form).call
      expect(result.success?).to_not be
      update_params[:goal_year] = Time.zone.today.year + 1
      update_params[:goal_vp_woman_count] = nil
      form.attributes = update_params
      result = ::Command::CompanyProfile::Update.new(form).call
      expect(result.success?).to_not be
    end

    it 'does not count hidden level' do
      update_params = {
        now_director_woman_count: 5,
        now_director_man_count: 5,
        goal_director_woman_count: 0,
        goal_director_man_count: 0,
        now_cxo_woman_count: 10,
        now_cxo_man_count: 5,
        goal_cxo_woman_count: 0,
        goal_cxo_man_count: 0,
        now_vp_woman_count: 5,
        now_vp_man_count: 5,
        goal_vp_woman_count: 0,
        goal_vp_man_count: 0,
        now_middle_woman_count: 5,
        now_middle_man_count: 5,
        goal_middle_woman_count: 0,
        goal_middle_man_count: 0,
        goal_year: Time.zone.today.year + 1,
        cxo_hide: true
      }
      form = ::Form::UserLogged::CompanyProfile::Edit.find(user.company_id)
      form.attributes = update_params
      result = ::Command::CompanyProfile::Update.new(form).call
      expect(result.success?).to be
      expect(company_profile.reload.diversity_score).to eq 50
    end
  end
end
