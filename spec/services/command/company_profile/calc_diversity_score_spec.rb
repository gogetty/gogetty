# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanyProfile::CalcDiversityScore, type: :controller do
  it 'updating company profile recalculates diversity score' do
    company_profile = FactoryGirl.create(:company_profile)

    result = ::Command::CompanyProfile::CalcDiversityScore.new(company_profile).call
    expect(result.success?).to be
    expect(company_profile.reload.diversity_score).to eq 0
    expect(company_profile.reload.goal_diversity_score).to eq 0

    company_profile.update_attributes(now_director_woman_count: 5,
                                      now_director_man_count: 2,
                                      now_cxo_woman_count: 2,
                                      now_cxo_man_count: 2,
                                      now_vp_woman_count: 4,
                                      now_vp_man_count: 5,

                                      goal_director_woman_count: 5,
                                      goal_director_man_count: 4,
                                      goal_cxo_woman_count: 4,
                                      goal_cxo_man_count: 4,
                                      goal_vp_woman_count: 6,
                                      goal_vp_man_count: 5)

    result = ::Command::CompanyProfile::CalcDiversityScore.new(company_profile).call
    expect(result.success?).to be
    expect(company_profile.reload.diversity_score).to eq 45
    expect(company_profile.reload.goal_diversity_score).to eq 46

    company_profile.update_attributes(now_director_woman_count: 0, goal_director_woman_count: 0)
    result = ::Command::CompanyProfile::CalcDiversityScore.new(company_profile).call
    expect(result.success?).to be
    expect(company_profile.reload.diversity_score).to eq 40
    expect(company_profile.reload.goal_diversity_score).to eq 43
  end
end
