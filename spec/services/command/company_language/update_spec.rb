# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Command::CompanyLanguage::Update, type: :controller do
  let(:company_language) { FactoryGirl.create(:company_language) }
  let(:user) { FactoryGirl.create(:user, company: company_language.company) }
  let(:plan) { FactoryGirl.create(:plans_user, user: user) }

  before do
    company_language.save!
    sign_in(user)
  end

  context 'form sent by a privileged user' do
    describe 'with single language with valid data' do
      it 'updates the language' do
        parameters = [{
          id: company_language.id,
          count: rand(10..20)
        }]
        form = ::Form::UserLogged::CompanyLanguage::Edit.find(user.company_id)
        result = ::Command::CompanyLanguage::Update.new(form, parameters).call
        expect(result.success?).to be
        company_language.reload
        expect(company_language.count).to be_eql(parameters.first[:count])
      end
    end

    describe 'without data being changed' do
      it 'does not update language' do
        form = ::Form::UserLogged::CompanyLanguage::Edit.find(user.company_id)
        result = ::Command::CompanyLanguage::Update.new(form, [{}]).call
        expect(result.success?).to be
        expect(company_language).to be_equal(company_language.reload)
        expect(result.data).to include('No record has been updated')
      end
    end

    describe 'with multiple languages' do
      it 'updates each language' do
        FactoryGirl.create_list(:company_language, 5, company: company_language.company)

        form = ::Form::UserLogged::CompanyLanguage::Edit.find(user.company_id)
        parameters = []
        form.each do |n|
          parameters << {
            'id' => n.id,
            'count' => n.count + 1
          }
        end

        result = ::Command::CompanyLanguage::Update.new(form, parameters).call
        expect(result.success?).to be
        form.each_with_index do |n, i|
          expect(n.reload.attributes).to include(parameters[i])
        end
        expect(result.data).to include('6 records successfully updated')
      end
    end

    describe 'with invalid data' do
      it 'returns errors for empty input' do
        parameters = [{
          count: ''
        }]
        form = ::Form::UserLogged::CompanyLanguage::Edit.find(user.company_id)
        result = ::Command::CompanyLanguage::Update.new(form, parameters).call

        expect(result.success?).to_not be
        expect(company_language).to be_equal(company_language.reload)
        expect(result.error).to include("Count can't be blank",
                                        'Count is not a number')
      end

      it 'returns errors for negative count' do
        parameters = [{
          count: -1
        }]
        form = ::Form::UserLogged::CompanyLanguage::Edit.find(user.company_id)
        result = ::Command::CompanyLanguage::Update.new(form, parameters).call

        expect(result.success?).to_not be
        expect(company_language).to be_equal(company_language.reload)
        expect(result.error).to include('Count must be greater than 0')
      end
    end
  end
end
