# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe ::Query::CompanySearch::Find, type: :controller do
  describe 'creates search object' do
    it 'expects companies query to have parts according to object' do
      search_params = {name: 'Test Name',
                       diversity_score_from: '3',
                       diversity_score_to: '5',
                       location: 'Warszawa',
                       country_id: 1,
                       industry_id: 1}
      company_search_form = ::Form::CompanySearch::New.new(search_params)
      company_search_result = ::Query::CompanySearch::Find.new(company_search_form).call
      expect(company_search_result.success?).to be
      expect(company_search_result.data.to_sql.include?(
               "UNACCENT(LOWER(companies.name)) LIKE UNACCENT('%test name%')"
      )).to be
      expect(company_search_result.data.to_sql.include?('company_profiles.diversity_score >= 3')).to be
      expect(company_search_result.data.to_sql.include?('company_profiles.diversity_score <= 5')).to be
      expect(company_search_result.data.to_sql.include?(
               "UNACCENT(LOWER(companies.city)) LIKE UNACCENT('%warszawa%')"
      )).to be
      expect(company_search_result.data.to_sql.include?('"companies"."country_id" = 1')).to be
      expect(company_search_result.data.to_sql.include?('"companies"."industry_id" = 1')).to be
      expect(company_search_result.data.to_sql.include?(
               'ORDER BY -company_profiles.diversity_score'
      )).to be
    end
  end

  describe 'creates search object with only name' do
    it 'expects companies query to have parts according to object' do
      search_params = {name: 'Test Name'}
      company_search_form = ::Form::CompanySearch::New.new(search_params)
      company_search_result = ::Query::CompanySearch::Find.new(company_search_form).call
      expect(company_search_result.success?).to be
      expect(company_search_result.data.to_sql.include?(
               "UNACCENT(LOWER(companies.name)) LIKE UNACCENT('%test name%')"
      )).to be
      expect(company_search_result.data.to_sql.include?('company_profiles.diversity_score >= 0')).to be
      expect(company_search_result.data.to_sql.include?('company_profiles.diversity_score <= 50')).to be
      expect(company_search_result.data.to_sql.include?(
               'UNACCENT(LOWER(companies.city)) LIKE UNACCENT'
      )).to_not be
      expect(company_search_result.data.to_sql.include?('"companies"."country_id"  =')).to_not be
      expect(company_search_result.data.to_sql.include?('"companies"."industry_id" =')).to_not be
      expect(company_search_result.data.to_sql.include?(
               'ORDER BY -company_profiles.diversity_score'
      )).to be
    end
  end

  describe 'creates empty search object' do
    it 'expects only deafult params in search query' do
      search_params = {name: ''}
      company_search_form = ::Form::CompanySearch::New.new(search_params)
      company_search_result = ::Query::CompanySearch::Find.new(company_search_form).call
      expect(company_search_result.success?).to be
      expect(company_search_result.data.to_sql.include?(
               'UNACCENT(LOWER(companies.name)) LIKE UNACCENT'
      )).to_not be
      expect(company_search_result.data.to_sql.include?('company_profiles.diversity_score >= 0')).to be
      expect(company_search_result.data.to_sql.include?('company_profiles.diversity_score <= 50')).to be
      expect(company_search_result.data.to_sql.include?(
               'UNACCENT(LOWER(companies.city)) LIKE UNACCENT'
      )).to_not be
      expect(company_search_result.data.to_sql.include?('"companies"."country_id"  =')).to_not be
      expect(company_search_result.data.to_sql.include?('"companies"."industry_id" =')).to_not be
      expect(company_search_result.data.to_sql.include?(
               'ORDER BY -company_profiles.diversity_score'
      )).to be
    end
  end

  describe 'creates invalid search object' do
    it 'expects search to return error' do
      search_params = nil
      company_search_form = ::Form::CompanySearch::New.new(search_params)
      company_search_result = ::Query::CompanySearch::Find.new(company_search_form).call
      expect(company_search_result.success?).to_not be
    end
  end
end
