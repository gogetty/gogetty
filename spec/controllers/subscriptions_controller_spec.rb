# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe SubscriptionsController, type: :controller do
  describe 'choose business basic when user signed in' do
    it 'expects to redirect sign up, devise redirects back' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      b_count = Billing.where(user_id: user.id).count
      post :create, params: {commit: FactoryGirl.create(:plan, :free).commit,
                             subscription: {annual: '1', coupon: ''}}
      expect(b_count).to be(Billing.where(user_id: user.id).count)
      expect(response).to redirect_to user_logged_walls_path
    end
  end

  describe 'choose business basic when user not signed in' do
    it 'expects to redirect to signup form' do
      b_count = Billing.where(user_id: nil).count
      post :create, params: {commit: FactoryGirl.create(:plan, :free).commit,
                             subscription: {annual: '1', coupon: ''}}
      expect(b_count).to be(Billing.where(user_id: nil).count)
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe 'choose diverse business when user signed in' do
    it 'expects to redirect to payment' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      b_count = Billing.where(user_id: user.id).count
      plan = FactoryGirl.create(:plan, :paid)
      post :create, params: {commit: plan.commit,
                             subscription: {annual: '1', coupon: ''}}
      expect(b_count).to be(Billing.where(user_id: user.id).count - 1)
      expect(response).to redirect_to new_user_logged_charge_path
      plan_total = plan.annual_cost + (plan.annual_cost * plan.tax / BigDecimal.new('100')).to_i
      expect(Billing.first.cost).to eq plan_total
    end
  end

  describe 'choose diverse business with coupon when user signed in' do
    it 'expects to redirect to payment' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      b_count = Billing.where(user_id: user.id).count
      plan = FactoryGirl.create(:plan, :paid)
      post :create, params: {commit: plan.commit,
                             subscription: {annual: '1', coupon: '25OFF'}}
      expect(b_count).to be(Billing.where(user_id: user.id).count - 1)
      expect(response).to redirect_to new_user_logged_charge_path
      plan_total = (plan.annual_cost * 0.75) +
                   (plan.annual_cost * 0.75 * plan.tax / BigDecimal.new('100')).to_i
      expect(Billing.first.cost).to eq plan_total
    end
  end

  describe '#create when user is not signed in' do
    it 'redirects to sign_in and new billing is not created' do
      b_count = Billing.where(user_id: nil).count
      post :create, params: {commit: FactoryGirl.create(:plan, :paid).commit,
                             subscription: {annual: '1', coupon: ''}}
      expect(b_count).to be(Billing.where(user_id: nil).count)
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe 'send invalid message' do
    it 'expects to redirect to root' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      b_count = Billing.where(user_id: nil).count
      post :create, params: {commit: 'Invalid subscription',
                             subscription: {annual: '1', coupon: ''}}
      expect(b_count).to be(Billing.where(user_id: nil).count)
      expect(response).to redirect_to root_path
    end
  end

  describe 'choose monthly subscription without permission' do
    it 'expects to redirect to subscription page' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      b_count = Billing.where(user_id: user.id).count
      plan = FactoryGirl.create(:plan, :paid)
      post :create, params: {commit: plan.commit,
                             subscription: {annual: '0', coupon: ''}}
      expect(b_count).to be(Billing.where(user_id: user.id).count)
      expect(response).to redirect_to new_subscription_path
    end
  end

  describe 'choose monthly subscription without permission' do
    it 'expects to redirect to payment' do
      user = FactoryGirl.create(:user)
      user.update(monthly_billing_permission: true)
      sign_in(user)
      b_count = Billing.where(user_id: user.id).count
      plan = FactoryGirl.create(:plan, :paid)
      post :create, params: {commit: plan.commit,
                             subscription: {annual: '0', coupon: ''}}
      expect(b_count).to be(Billing.where(user_id: user.id).count - 1)
      expect(response).to redirect_to new_user_logged_charge_path
      plan_total = plan.monthly_cost + (plan.monthly_cost * plan.tax / BigDecimal.new('100')).to_i
      expect(Billing.first.cost).to eq plan_total
    end
  end
end
