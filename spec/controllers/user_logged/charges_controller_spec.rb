# frozen_string_literal: true

require 'rails_helper'
require 'devise'

describe UserLogged::ChargesController, type: :controller do
  describe 'user pays with valid credit card' do
    it 'expects to stripe customer info be saved in user, payment completes' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      billing = Billing.create(plan_id: plan.id,
                               amount_in_cents: plan.annual_cost,
                               monthly: 1,
                               tax: 23,
                               tax_value: plan.annual_cost * 0.23,
                               cost: plan.annual_cost * 1.23)
      session[:current_billing_id] = billing.id
      post :create, params: {stripeEmail: 'test@test.co', stripeToken: 'tok_visa'}
      expect(response).to render_template :create
      expect(Billing.find(session[:current_billing_id]).state).to eq 'pending'
    end
  end

  describe 'user pays with valid credit card and then refreshes' do
    it 'second payment returns to root with alert' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      billing = Billing.create(plan_id: plan.id,
                               amount_in_cents: plan.annual_cost,
                               monthly: 1,
                               tax: 23,
                               tax_value: plan.annual_cost * 0.23,
                               cost: plan.annual_cost * 1.23)
      session[:current_billing_id] = billing.id
      post :create, params: {stripeEmail: 'test@test.co', stripeToken: 'tok_visa'}
      expect(response).to render_template :create
      expect(Billing.find(session[:current_billing_id]).state).to eq 'pending'
      post :create, params: {stripeEmail: 'test@test.co', stripeToken: 'tok_visa'}
      expect(response).to redirect_to root_path
    end
  end

  describe 'user send invalid data in payment form' do
    it 'expects to return to the root_path' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      plan = FactoryGirl.create(:plan, :paid)
      billing = Billing.create(plan_id: plan.id,
                               amount_in_cents: plan.annual_cost,
                               monthly: 0,
                               tax: 23,
                               tax_value: plan.annual_cost * 0.23,
                               cost: plan.annual_cost * 1.23)
      session[:current_billing_id] = billing.id
      post :create # params missing
      expect(response).to redirect_to root_path
      expect(Billing.find(session[:current_billing_id]).state).to eq 'error'
    end
  end

  describe 'something goes terribly wrong' do
    it 'expects to return to the new root_path' do
      user = FactoryGirl.create(:user)
      sign_in(user)
      # no billing data
      post :create, params: {stripeEmail: 'test@test.co', stripeToken: 'tok_visa'}
      expect(response).to redirect_to root_path
    end
  end
end
