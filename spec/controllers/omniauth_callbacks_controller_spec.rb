# frozen_string_literal: true

require 'rails_helper'

describe OmniauthCallbacksController, type: :controller do
  before(:each) do
    mock_auth_hash
    request.env['devise.mapping'] = Devise.mappings[:user]
    request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:linkedin]
  end

  after(:each) do
    OmniAuth.config.mock_auth[:linkedin] = nil
  end

  describe 'GET #callback' do
    it 'expects omniauth.auth to be be_truthy' do
      get :linkedin
      expect(request.env['omniauth.auth']).to be_truthy
    end

    it 'saves user info and takes you to user_wall' do
      expect do
        get :linkedin
      end.to change { User.count }.by(1)
      expect(response).to redirect_to root_path
    end
  end
end
