# frozen_string_literal: true

# Feature: Sign up
#   As a visitor
#   I want to sign up
#   So I can visit protected areas of the site
feature 'Sign Up', :devise do
  # for every test
  before :each do
    visit new_user_registration_path
    # fill_in 'Name', with: Faker::Name.first_name
    # fill_in 'Surname', with: Faker::Name.last_name
    fill_in 'register_user_email', with: Faker::Internet.email
    fill_in 'register_user_password', with: 'abcabc'
    fill_in 'Repeat password', with: 'abcabc'
    # fill_in 'Company name', with: Faker::Company.name
    # fill_in 'Title', with: 'Manager'
    # page.check('user_terms_agreed')
  end
  # Scenario: Visitor can sign up with valid email address and password
  #   Given I am not signed in
  #   When I sign up with a valid email name, surname, address, password,
  #   company name, title and accept terms
  #   Then I see a successful sign up message
  scenario 'visitor can sign up with valid data' do
    page.check('user_terms_agreed')
    click_button 'CREATE ACCOUNT'
    expect(page).to have_content('A confirmation e-mail has been sent to your inbox')
  end

  # Scenario: Visitor cannot sign up with invalid email address
  #   Given I am not signed in
  #   When I sign up with an invalid email address
  #   Then I see an invalid email message
  scenario 'visitor cannot sign up with invalid email address' do
    fill_in 'register_user_email', with: 'bogus'
    click_button 'CREATE ACCOUNT'
    expect(page).to have_content 'Email is invalid'
  end

  # Scenario: Visitor cannot sign up without password
  #   Given I am not signed in
  #   When I sign up without a password
  #   Then I see a missing password message
  scenario 'visitor cannot sign up without password' do
    fill_in 'register_user_password', with: ''
    fill_in 'Repeat password', with: ''
    click_button 'CREATE ACCOUNT'
    expect(page).to have_content "Password can't be blank"
  end

  # Scenario: Visitor cannot sign up with a short password
  #   Given I am not signed in
  #   When I sign up with a short password
  #   Then I see a 'too short password' message
  scenario 'visitor cannot sign up with a short password' do
    fill_in 'register_user_password', with: 'abc'
    fill_in 'Repeat password', with: 'abc'
    click_button 'CREATE ACCOUNT'
    expect(page).to have_content 'Password is too short'
  end

  # Scenario: Visitor cannot sign up without password confirmation
  #   Given I am not signed in
  #   When I sign up without a password confirmation
  #   Then I see a missing password confirmation message
  scenario 'visitor cannot sign up without password confirmation' do
    fill_in 'Repeat password', with: ''
    click_button 'CREATE ACCOUNT'
    expect(page).to have_content "Password confirmation doesn't match"
  end

  # Scenario: Visitor cannot sign up with mismatched password and confirmation
  #   Given I am not signed in
  #   When I sign up with a mismatched password confirmation
  #   Then I should see a mismatched password message
  scenario 'visitor cannot sign up with mismatched password and confirmation' do
    fill_in 'Repeat password', with: 'abcdef'
    click_button 'CREATE ACCOUNT'
    expect(page).to have_content "Password confirmation doesn't match"
  end

  # Scenario: Visitor cannot sign up without accepting terms
  #   Given I am not signed in
  #   When I sign up without company name
  #   Then I should see terms acceptance missing message
  scenario 'visitor cannot sign up without accepting terms' do
    click_button 'CREATE ACCOUNT'
    expect(page).to have_content 'GoGetty Terms and Conditions privacy policy must be accepted'
  end
end
