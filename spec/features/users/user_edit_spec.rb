# frozen_string_literal: true

include Warden::Test::Helpers
Warden.test_mode!

# Feature: User edit
#   As a user
#   I want to edit my user profile
#   So I can change my email address
feature 'User edit', :devise do
  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User changes email address
  #   Given I am signed in
  #   When I change my email address
  #   Then I see an account updated message
  scenario 'user changes email address' do
    user = FactoryGirl.create(:user)
    login_as(user, scope: :user)
    visit edit_user_logged_account_settings_path
    fill_in 'Email', with: 'newemail@example.com'
    click_button 'Update settings'
    expect(page).to have_content('Data saved successfully.')
  end
end
