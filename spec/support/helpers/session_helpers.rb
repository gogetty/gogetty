module Features
  module SessionHelpers
    def sign_up_with(email, password, confirmation)
      visit new_user_registration_path
      fill_in 'register_user_email', with: email
      fill_in 'register_user_password', with: password
      fill_in 'Repeat password', with: confirmation
      click_button 'CREATE ACCOUNT'
    end

    def signin(email, password)
      visit new_user_session_path
      fill_in 'user_email', with: email
      fill_in 'user_password', with: password
      click_button 'LOG IN'
    end
  end
end
