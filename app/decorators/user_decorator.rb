# frozen_string_literal: true

class UserDecorator < ApplicationDecorator
  delegate_all
  def company_title
    if company_name.to_s != ''
      "#{title} (#{company_name})"
    else
      title
    end
  end

  def congratulations_part1
    return 'Congratulations!' unless name
    'Congratulations'
  end

  def congratulations_part2
    return nil unless name
    "#{name}!"
  end

  def congratulations_all
    return 'Congratulations!' unless name
    "Congratulations #{name}!"
  end

  def congratulations_another_email
    return nil unless company_name
    "Another e-mail from a '#{company_name}' admin will be sent once they prove your identity."
  end

  def avatar
    if object.avatar?
      ActionController::Base.helpers.image_tag(object.avatar.url,
                                               class: 'img-fluid rounded-circle w-60px h-60px')
    else
      ActionController::Base.helpers.image_tag('icons/avatar_empty.svg',
                                               class: 'img-fluid rounded-circle w-60px h-60px')
    end
  end

  def avatar_thumb
    if object.avatar?
      ActionController::Base.helpers.image_tag(object.avatar.url(:thumb),
                                               class: 'img-fluid rounded-circle w-60px h-60px')
    else
      ActionController::Base.helpers.image_tag('icons/avatar_empty.svg',
                                               class: 'img-fluid rounded-circle w-60px h-60px')
    end
  end
end
