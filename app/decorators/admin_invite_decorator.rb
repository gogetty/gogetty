# frozen_string_literal: true

class AdminInviteDecorator < ApplicationDecorator
  delegate_all

  def user_name
    user.name
  rescue StandardError
    ''
  end

  def user_surname
    user.surname
  rescue StandardError
    ''
  end

  def email
    user.email
  rescue StandardError
    super
  end

  def cancelled
    super.humanize
  end

  def accepted
    super.humanize
  end

  def editable
    !object.cancelled && !object.accepted
  end

  def cancellable
    editable
  end

  def acceptable
    editable
  end
end
