# frozen_string_literal: true

class SubscriptionDecorator < ApplicationDecorator
  delegate_all
  def business_basic_commit
    Plan.find_by(name: 'business_basic').commit
  end

  def diverse_business_commit
    Plan.find_by(name: 'diverse_business').commit
  end

  def diverse_business_monthly
    h.number_to_currency(
      Plan.find_by(name: 'diverse_business').monthly_cost / BigDecimal.new('100'),
      precision: 0,
      locale: :da
    )
  end

  def diverse_business_annual
    h.number_to_currency(
      Plan.find_by(name: 'diverse_business').annual_cost / BigDecimal.new('100') / 12,
      precision: 0,
      locale: :da
    )
  end

  def inclusive_business_commit
    Plan.find_by(name: 'inclusive_business').commit
  end

  def inclusive_business_monthly
    h.number_to_currency(
      Plan.find_by(name: 'inclusive_business').monthly_cost / BigDecimal.new('100'),
      precision: 0,
      locale: :da
    )
  end

  def inclusive_business_annual
    h.number_to_currency(
      Plan.find_by(name: 'inclusive_business').annual_cost / BigDecimal.new('100') / 12,
      precision: 0,
      locale: :da
    )
  end
end
