# frozen_string_literal: true

class PlanDecorator < ApplicationDecorator
  delegate_all

  def name
    object.name.humanize.titleize
  end

  def total_cost(interval)
    case interval
    when 'month'
      h.number_to_currency(total_monthly_cost / BigDecimal.new('100'), locale: :da)
    when 'year'
      h.number_to_currency(total_annual_cost / BigDecimal.new('100'), locale: :da)
    else
      0
    end
  end

  def monthly_cost
    h.number_to_currency(object.monthly_cost / BigDecimal.new('100'), locale: :da)
  end

  def annual_cost
    h.number_to_currency(object.annual_cost / BigDecimal.new('100'), locale: :da)
  end

  def monthly_cost_null
    object.monthly_cost.zero?
  end

  def tax
    "#{object.tax} %"
  end

  def job_board_listings
    case object.name
    when 'business_basic' then 1
    when 'diverse_business' then 10
    when 'inclusive_business' then 'UNLIMITED'
    end
  end

  def reports_included
    case object.name
    when 'business_basic' then 10
    when 'diverse_business' then 20
    when 'inclusive_business' then 'UNLIMITED'
    end
  end

  def paid_plan_icon
    return 'check' if %w[diverse_business inclusive_business].include? object.name
    'close'
  end

  def inclusive_business_icon
    return 'check' if object.name == 'inclusive_business'
    'close'
  end

  private

  def total_monthly_cost
    object.monthly_cost + (object.monthly_cost * object.tax / 100)
  end

  def total_annual_cost
    object.annual_cost + (object.annual_cost * object.tax / 100)
  end
end
