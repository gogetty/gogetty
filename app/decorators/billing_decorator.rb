# frozen_string_literal: true

class BillingDecorator < ApplicationDecorator
  delegate_all

  def amount_in_cents
    return object.amount_in_cents unless object.coupon
    @cp ||= Stripe::Coupon.retrieve(object.coupon)
    return amount_off_cents if @cp[:amount_off]
    return percent_off_cents if @cp[:percent_off]
  end

  def payment_amount_to_charge
    h.number_to_currency(amount_in_cents / BigDecimal.new('100'), locale: :da, precision: 0)
  end

  def amount_in_cents_local
    h.number_to_currency(object.amount_in_cents / BigDecimal.new('100'), locale: :da, precision: 0)
  end

  def cost
    h.number_to_currency(object.cost / BigDecimal.new('100'), locale: :da, precision: 0)
  end

  def coupon
    return '' unless object.coupon
    @cp ||= Stripe::Coupon.retrieve(object.coupon)
    str = "Coupon: '#{@cp.id}'"
    str += ", #{coupon_value_str}"
    str += ", valid #{@cp[:duration_in_months]} months" if @cp[:duration] == 'repeating'
    str
  end

  def coupon_value_str_local
    return '' unless object.coupon
    if coupon_amount_off
      return "#{h.number_to_currency(
        coupon_amount_off / BigDecimal.new('100'),
        locale: :da
      )} off"
    end
    return "#{coupon_percent_off}% off" if coupon_percent_off
  end

  def tax
    "#{object.tax}% VAT"
  end

  def tax_value
    h.number_to_currency(object.tax_value / BigDecimal.new('100'), locale: :da, precision: 0)
  end

  def tax_full
    "#{tax}, #{tax_value}"
  end

  private

  def coupon_value_str
    return '' unless object.coupon
    @cp ||= Stripe::Coupon.retrieve(object.coupon)
    if @cp[:amount_off]
      return "#{h.number_to_currency(
        @cp[:amount_off] / BigDecimal.new('100'),
        locale: :da
      )} off"
    end
    return "#{@cp[:percent_off]}% off" if @cp[:percent_off]
  end

  def amount_off_cents
    object.amount_in_cents - @cp[:amount_off]
  end

  def percent_off_cents
    object.amount_in_cents * (100 - @cp[:percent_off]) / 100
  end
end
