# frozen_string_literal: true

class CompanyProfileDecorator < ApplicationDecorator # rubocop:disable Metrics/ClassLength
  delegate_all
  delegate :diversity_score, :now_director_woman_count, :now_director_man_count,
           :goal_director_woman_count, :goal_director_man_count, :now_vp_woman_count,
           :now_vp_man_count, :goal_vp_woman_count, :goal_vp_man_count, :now_cxo_woman_count,
           :now_cxo_man_count, :goal_cxo_woman_count, :goal_cxo_man_count, :now_middle_man_count,
           :now_middle_woman_count, :goal_middle_man_count, :goal_middle_woman_count, to: :hideable_value

  def employees_count
    employees_count_count
  rescue StandardError
    ''
  end

  def c_liabilities
    h.number_to_currency(liabilities, locale: :da)
  end

  def c_revenue
    h.number_to_currency(revenue, locale: :da)
  end

  ### show_diversity
  def state_director
    (now_director_woman_count * 100.0 /
          (now_director_woman_count + now_director_man_count)).to_i
  rescue StandardError
    nil
  end

  def state_compliance
    state_director > 40 ? 'In compliance' : 'Not in compliance'
  rescue StandardError
    nil
  end

  def goal_director
    (goal_director_woman_count * 100.0 /
      (goal_director_woman_count + goal_director_man_count)).to_i
  rescue StandardError
    nil
  end

  def goal_compliance
    goal_director > 40 ? 'In compliance' : 'Not in compliance'
  rescue StandardError
    nil
  end

  def state_vp
    (now_vp_woman_count * 100.0 /
      (now_vp_woman_count + now_vp_man_count)).to_i
  rescue StandardError
    nil
  end

  def goal_vp
    (goal_vp_woman_count * 100.0 /
      (goal_vp_woman_count + goal_vp_man_count)).to_i
  rescue StandardError
    nil
  end

  def state_cxo
    (now_cxo_woman_count * 100.0 /
      (now_cxo_woman_count + now_cxo_man_count)).to_i
  rescue StandardError
    nil
  end

  def goal_cxo
    (goal_cxo_woman_count * 100.0 /
      (goal_cxo_woman_count + goal_cxo_man_count)).to_i
  rescue StandardError
    nil
  end

  def state_middle
    (now_middle_woman_count * 100.0 /
      (now_middle_woman_count + now_middle_man_count)).to_i
  rescue StandardError
    nil
  end

  def goal_middle
    (goal_middle_woman_count * 100.0 /
      (goal_middle_woman_count + goal_middle_man_count)).to_i
  rescue StandardError
    nil
  end

  def women_and_men_numbers
    women = [now_director_woman_count,
             now_vp_woman_count,
             now_cxo_woman_count,
             now_middle_woman_count].compact.sum
    men   = [now_director_man_count,
             now_vp_man_count,
             now_cxo_man_count,
             now_middle_man_count].compact.sum
    "#{women}F/#{men}M"
  end

  def women_number
    [now_director_woman_count,
     now_vp_woman_count,
     now_cxo_woman_count,
     now_middle_woman_count].compact.sum
  end

  def men_number
    [now_director_man_count,
     now_vp_man_count,
     now_cxo_man_count,
     now_middle_man_count].compact.sum
  end

  def state_director_numbers
    "#{now_director_woman_count}/#{now_director_man_count}"
  rescue StandardError
    nil
  end

  def goal_director_numbers
    "#{goal_director_woman_count}/#{goal_director_man_count}"
  rescue StandardError
    nil
  end

  def state_vp_numbers
    "#{now_vp_woman_count}/#{now_vp_man_count}"
  rescue StandardError
    nil
  end

  def goal_vp_numbers
    "#{goal_vp_woman_count}/#{goal_vp_man_count}"
  rescue StandardError
    nil
  end

  def state_cxo_numbers
    "#{now_cxo_woman_count}/#{now_cxo_man_count}"
  rescue StandardError
    nil
  end

  def goal_cxo_numbers
    "#{goal_cxo_woman_count}/#{goal_cxo_man_count}"
  rescue StandardError
    nil
  end

  def state_middle_numbers
    "#{now_middle_woman_count}/#{now_middle_man_count}"
  rescue StandardError
    nil
  end

  def goal_middle_numbers
    "#{goal_middle_woman_count}/#{goal_middle_man_count}"
  rescue StandardError
    nil
  end

  def state_values_present
    state_director || state_cxo || state_vp || state_middle
  end

  def goal_values_present
    goal_director || goal_cxo || goal_vp || goal_middle
  end

  def diversity_present
    state_values_present || goal_values_present
  end

  def current_goal_year
    object.goal_year.present? ? object.goal_year : Time.current.year + 1
  end

  private

  def hideable_value
    object.company.active_admins? || object.always_visible? ? object : HiddenValue.new
  end

  class HiddenValue
    def method_missing(_method)
      0
    rescue StandardError
      super
    end

    def respond_to_missing?
      super
    end
  end
end
