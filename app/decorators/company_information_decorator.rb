# frozen_string_literal: true

class CompanyInformationDecorator < ApplicationDecorator
  delegate_all

  def diversity_plans_title
    return object.diversity_plans_title if object.diversity_plans_title.present?
    "Read our diversity plans for #{Time.zone.now.year + 1}"
  end
end
