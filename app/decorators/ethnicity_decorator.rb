# frozen_string_literal: true

class EthnicityDecorator < ApplicationDecorator
  delegate_all

  def json_short
    if count > 7
      first_seven = first(7)
      rest = self[7..-1]
      rest_count = rest.collect(&:count).sum
      first_seven_json = json_name_and_count(first_seven)
      first_seven_json << {name: 'Others', count: rest_count}
    else
      json_name_and_count(object)
    end
  end

  def json_name_and_count(collection)
    collection.map { |n| {name: n.name, count: n.count} }
  end
end
