# frozen_string_literal: true

class CompanyDecorator < ApplicationDecorator
  delegate_all

  ### show_summary
  # rubocop:disable CyclomaticComplexity
  # rubocop:disable PerceivedComplexity
  def industry_city
    temp_string = ''
    temp_string += industry.name if industry
    temp_string += "#{', ' if industry}#{city}" if city
    temp_string += "#{', ' if industry || city}#{country.en_name}" if country
    return temp_string
  rescue StandardError
    ''
  end
  # rubocop:enable all

  ### show_policies
  def additional_policies
    policies = DiversityPolicy.where(always_visible: true)
    if diversity_policy.count.positive?
      policies = policies.where.not('id IN (?)', diversity_policy.pluck(:id))
    end
    policies.each do |policy|
      policy.additional = true
    end
    policies
  end

  def all_policies
    # XXX: can be done in one SQL if slow
    if object.active_admins? || object.company_profile.always_visible?
      ((diversity_policy + additional_policies).sort_by(&:name) + text_policies)
    else
      []
    end
  end

  ### show_goals
  def goals_way
    if object.active_admins? || object.company_profile.always_visible?
      super.order(:order)
    else
      []
    end
  end

  def editors_all
    object.admins.not_superadmin.count
  end

  def editors_active
    object.active_admins.not_superadmin.count
  end

  def admins_all
    object.admins.superadmin.count
  end

  def admins_active
    object.active_admins.superadmin.count
  end

  def updated_at_str
    updated_at.strftime('%B %Y')
  end

  def logo
    if object.logo?
      ActionController::Base.helpers.image_tag(object.logo.url,
                                               class: 'img-fluid rounded-circle w-130px h-130px')
    else
      ActionController::Base.helpers.image_tag('icons/logo_empty.svg',
                                               class: 'img-fluid rounded-circle w-130px h-130px')
    end
  end

  def logo_small
    if object.logo?
      ActionController::Base.helpers.image_tag(object.logo.url,
                                               class: 'img-fluid rounded-circle')
    else
      ActionController::Base.helpers.image_tag('icons/logo_empty.svg',
                                               class: 'img-fluid rounded-circle w-80px h-80px')
    end
  end

  def logo_very_small
    if object.logo?
      ActionController::Base.helpers.image_tag(object.logo.url,
                                               class: 'img-fluid rounded-circle w-50px h-50px')
    else
      ActionController::Base.helpers.image_tag('icons/logo_empty.svg',
                                               class: 'img-fluid rounded-circle w-50px h-50px')
    end
  end

  def logo_extra_small
    if object.logo?
      ActionController::Base.helpers.image_tag(object.logo.url,
                                               class: 'img-fluid rounded-circle')
    else
      ActionController::Base.helpers.image_tag('icons/logo_empty.svg',
                                               class: 'img-fluid rounded-circle w-60px h-60px')
    end
  end

  def current_year
    Time.current.year
    # object.company_profile_histories.last&.created_at&.year || object.company_profile.created_at.year
  end
end
