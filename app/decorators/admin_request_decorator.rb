# frozen_string_literal: true

class AdminRequestDecorator < ApplicationDecorator
  delegate_all

  def user_name
    user.name
  rescue StandardError
    ''
  end

  def user_surname
    user.surname
  rescue StandardError
    ''
  end

  def email
    user.email
  rescue StandardError
    super
  end

  def cancelled
    super.humanize
  end

  def accepted
    super.humanize
  end

  def revoked
    super.humanize
  end

  def editable
    !object.revoked && !object.cancelled && !object.accepted
  end

  def cancellable
    editable
  end

  def acceptable
    editable
  end

  def revokable
    editable
  end
end
