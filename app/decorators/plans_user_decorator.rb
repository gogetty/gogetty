# frozen_string_literal: true

class PlansUserDecorator < ApplicationDecorator
  delegate_all

  def interval
    billing.monthly? ? 'Monthly' : 'Yearly'
  end
end
