# frozen_string_literal: true

module StripeHooksDecorators
  class InvoicePayment
    attr_accessor :params

    def initialize(params)
      @params = params
      raise 'No user account found with given stripe customer id!' unless user
    end

    def persisted?
      true
    end

    def plan_cost
      @params.dig(:data, :object, :amount_due)
    end

    def invoice_id
      @params.dig(:data, :object, :id)
    end

    def charge_id
      @params.dig(:data, :object, :charge)
    end

    def plan_id
      @params.dig(:data, :object, :lines, :data)[0].dig(:plan, :id)
    end

    def type
      @params.dig(:type)
    end

    def subscription_id
      @params.dig(:data, :object, :subscription)
    end

    def customer_id
      @params.dig(:data, :object, :customer)
    end

    def tax
      @params.dig(:data, :object, :tax)
    end

    def tax_percent
      @params.dig(:data, :object, :tax_percent)
    end

    def subtotal
      @params.dig(:data, :object, :subtotal)
    end

    def total
      @params.dig(:data, :object, :total)
    end

    def user
      User.find_by(stripe_customer_id: customer_id)
    end
  end
end
