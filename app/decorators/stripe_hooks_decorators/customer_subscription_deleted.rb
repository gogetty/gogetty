# frozen_string_literal: true

module StripeHooksDecorators
  class CustomerSubscriptionDeleted
    attr_accessor :params

    def initialize(params)
      @params = params
      raise 'No user account found with given stripe customer id!' unless user
      raise 'No active subscription found with five strip subscription id' unless plans_user
    end

    def persisted?
      true
    end

    def type
      @params.dig(:type)
    end

    def subscription_id
      @params.dig(:data, :object, :id)
    end

    def customer_id
      @params.dig(:data, :object, :customer)
    end

    def user
      User.find_by(stripe_customer_id: customer_id)
    end

    def plans_user
      PlansUser.find_by(stripe_sub: subscription_id)
    end
  end
end
