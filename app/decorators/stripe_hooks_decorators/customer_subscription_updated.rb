# frozen_string_literal: true

module StripeHooksDecorators
  class CustomerSubscriptionUpdated
    attr_accessor :params

    def initialize(params)
      @params = params
      raise 'No user account found with given stripe customer id!' unless user
      raise 'No active subscription found with five strip subscription id' unless plans_user
    end

    def persisted?
      true
    end

    def user
      User.find_by(stripe_customer_id: customer_id)
    end

    def subscription_id
      @params.dig(:data, :object, :id)
    end

    def customer_id
      @params.dig(:data, :object, :customer)
    end

    def plans_user
      PlansUser.find_by(stripe_sub: subscription_id)
    end

    def plan_name
      @params.dig(:data, :object, :plan, :id)
    end
  end
end
