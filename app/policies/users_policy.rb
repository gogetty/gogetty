# frozen_string_literal: true

class UsersPolicy < ApplicationPolicy
  def pro?
    !@user.nil? && (@user.pro? || platform_admin?)
  end

  def subscribed_to_inclusive_business?
    !@user.nil? && (@user.subscribed_to_inclusive_business? || platform_admin?)
  end

  def any_company_admin?
    !@user.nil? && (@user.company_id? || platform_admin?)
  end

  def any_company_superadmin?
    (any_company_admin? && user.superadmin) || platform_admin?
  end

  def platform_admin?
    @user&.admin?
  end
end
