# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  def can_destroy?
    @user&.admin? || CompanyPolicy.new(@user, @record.company).company_admin_with_paid_plan?
  end
end
