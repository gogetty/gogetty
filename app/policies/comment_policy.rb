# frozen_string_literal: true

class CommentPolicy < ApplicationPolicy
  def can_destroy?
    @user&.admin? ||
      CompanyPolicy.new(@user, @record.post.company).company_admin? ||
      @record.creator == @user
  end
end
