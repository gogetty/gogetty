# frozen_string_literal: true

class CompanyPolicy < ApplicationPolicy
  def company_admin?
    !@user.nil? && (@user.company_id == @record.id || UsersPolicy.new(@user, @user).platform_admin?)
  end

  def company_superadmin?
    company_admin? && (@user.superadmin || UsersPolicy.new(@user, @user).platform_admin?)
  end

  def company_with_basic_plan?
    @record.active_basic_business_plan?
  end

  def company_admin_with_basic_plan?
    (company_admin? && company_with_basic_plan?) || @user.platform_admin?
  end

  def company_with_paid_plan?
    @record.active_paid_plan?
  end

  def company_admin_with_paid_plan?
    (company_admin? && company_with_paid_plan?) || @user.platform_admin?
  end

  def company_with_inclusive_plan?
    @record.active_inclusive_business_plan?
  end

  def company_admin_with_inclusive_plan?
    (company_admin? && company_with_inclusive_plan?) || @user.platform_admin?
  end
end
