# frozen_string_literal: true

module Command
  module PolicyRequestField
    class Create
      attr_reader :params
      attr_accessor :policy_request_form

      def initialize(params)
        @params = params
        @policy_request_form = ::Form::UserLogged::PolicyRequest::New.new(create_params)
      end

      def call
        create_subform
        Result::Success.new(@policy_request_form)
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_subform
        if @policy_request_form.diversity_policies_policy_requests
          @policy_request_form.diversity_policies_policy_requests.append(
            ::Form::UserLogged::DiversityPoliciesPolicyRequest::New.new
          )
        else
          @policy_request_form.diversity_policies_policy_requests =
            [::Form::UserLogged::DiversityPoliciesPolicyRequest::New.new]
        end
      end

      def create_params
        params.require(:policy_request)
              .permit(:company_id,
                      :comment,
                      diversity_policies_policy_requests_attributes: %i[id
                                                                        diversity_policy_id
                                                                        _destroy])
      end
    end
  end
end
