# frozen_string_literal: true

module Command
  module StripeHooks
    class CustomerSubscriptionDeleted
      attr_reader :stripe_event

      def initialize(stripe_event)
        @stripe_event = stripe_event
      end

      def call
        plans_user_cancel
        Result::Success.new('Subscription deletion successfully processed.')
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def plans_user_cancel
        plans_user = stripe_event.plans_user
        raise 'Subscription already cancelled!' if plans_user.cancelled_on
        plans_user.cancelled_on = Time.zone.today
        plans_user.save!
      end
    end
  end
end
