# frozen_string_literal: true

module Command
  module StripeHooks
    class InvoicePaymentSucceeded
      attr_reader :stripe_event
      attr_accessor :billing
      attr_accessor :billing_result

      def initialize(stripe_event)
        @stripe_event = stripe_event
      end

      def call
        # first find, create only if not existing
        prepare_billing
        return billing_result unless billing_result.success?
        set_billing_to_finalized
        modify_user_plan
        Result::Success.new('Invoice success failure successfully processed.')
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def prepare_billing
        @billing_result = ::Command::Billing::PrepareBillingByStripe.new(stripe_event).call
        @billing = @billing_result.data if @billing_result.success?
      end

      def set_billing_to_finalized
        @billing.state = ::Billing.states[:finalized]
        @billing.save!
      end

      def modify_user_plan
        ::Command::UserPlan::CreateOrExtend.new(stripe_event.user, billing, stripe_event).call
      end
    end
  end
end
