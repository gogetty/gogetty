# frozen_string_literal: true

module Command
  module StripeHooks
    class CustomerSubscriptionUpdated
      attr_reader :stripe_event

      def initialize(stripe_event)
        @stripe_event = stripe_event
      end

      def call
        update_user_plan
        Result::Success.new('Subscription update successfully processed.')
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def update_user_plan
        user_plan = stripe_event.plans_user
        plan = ::Plan.where(monthly_stripe_name: stripe_event.plan_name)
                     .or(::Plan.where(annual_stripe_name: stripe_event.plan_name)).first
        user_plan.plan_id = plan.id
        user_plan.save!
      end
    end
  end
end
