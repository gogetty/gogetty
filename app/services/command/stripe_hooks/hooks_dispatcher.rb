# frozen_string_literal: true

module Command
  module StripeHooks
    class HooksDispatcher
      attr_reader :params
      attr_accessor :result

      def initialize(params)
        @params = params
      end

      def call
        find_method_for_type
        return @result unless @result.success?
        Result::Success.new('Stripe webhook processed successfully.')
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def webhook_method_name
        @params[:type].tr('.', '_')
      end

      def find_method_for_type
        @result = if private_methods.include? webhook_method_name.to_sym
                    send(webhook_method_name)
                  else
                    ::Command::Result::Error.new(StandardError.new('Unsupported webhook type.'))
                  end
      end

      def invoice_payment_succeeded
        stripe_event = ::StripeHooksDecorators::InvoicePayment.new(params)
        ::Command::StripeHooks::InvoicePaymentSucceeded.new(stripe_event).call
      end

      def invoice_payment_failed
        stripe_event = ::StripeHooksDecorators::InvoicePayment.new(params)
        ::Command::StripeHooks::InvoicePaymentFailed.new(stripe_event).call
      end

      def customer_subscription_deleted
        stripe_event = ::StripeHooksDecorators::CustomerSubscriptionDeleted.new(params)
        ::Command::StripeHooks::CustomerSubscriptionDeleted.new(stripe_event).call
      end

      def customer_subscription_updated
        stripe_event = ::StripeHooksDecorators::CustomerSubscriptionUpdated.new(params)
        ::Command::StripeHooks::CustomerSubscriptionUpdated.new(stripe_event).call
      end
    end
  end
end
