# frozen_string_literal: true

module Command
  module StripeHooks
    class InvoicePaymentFailed
      attr_reader :stripe_event
      attr_accessor :billing
      attr_accessor :billing_result

      def initialize(stripe_event)
        @stripe_event = stripe_event
      end

      def call
        prepare_billing
        return billing_result unless billing_result.success?
        set_billing_to_error
        set_subscription_not_payed
        Result::Success.new('Invoice payment failure successfully processed.')
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def prepare_billing
        @billing_result = ::Command::Billing::PrepareBillingByStripe.new(stripe_event).call
        @billing = @billing_result.data if @billing_result.success?
      end

      def set_billing_to_error
        @billing.state = ::Billing.states[:error]
        @billing.save!
      end

      def set_subscription_not_payed
        user_plan = ::PlansUser.find_by(stripe_sub: stripe_event.subscription_id)
        user_plan.not_payed_pro = true
        user_plan.save!
      end
    end
  end
end
