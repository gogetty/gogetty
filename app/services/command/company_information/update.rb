# frozen_string_literal: true

module Command
  module CompanyInformation
    class Update
      attr_reader :form
      attr_accessor :company_information

      def initialize(form)
        @form = form
        @company_information = ::CompanyInformation.find(form.id)
      end

      def call
        return Result::Error.new(form.errors.full_messages) unless form.valid?
        assign_company_information_attributes
        save
      end

      private

      def assign_company_information_attributes
        company_information.attributes = {
          first_video: form.first_video,
          second_video: form.second_video,
          diversity_plans_title: form.diversity_plans_title,
          gender_diversity_plans: form.gender_diversity_plans,
          ethnicity_diversity_plans: form.ethnicity_diversity_plans,
          contact: form.contact
        }
      end

      def save
        if company_information.save
          Result::Success.new('Updated successfully.')
        else
          Result::Error.new(company_information.errors.full_messages)
        end
      end
    end
  end
end
