# frozen_string_literal: true

module Command
  module Post
    class Create
      attr_reader :form
      attr_accessor :post

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_post
        check_name
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_post
        @post = ::Post.new(
          title: @form.title,
          text: @form.text,
          photo: @form.photo,
          video: @form.video,
          created_by: @form.created_by,
          company_id: @form.company_id
        )
      end

      def check_name
        error = 'Please update your name in your user profile
        to be able to post an update on your Company Profile.'
        raise error if @post.creator.name.nil? || @post.creator.surname.nil?
      end

      def save
        if @post.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(@post.errors.full_messages)
        end
      end
    end
  end
end
