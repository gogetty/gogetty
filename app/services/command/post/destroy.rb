# frozen_string_literal: true

module Command
  module Post
    class Destroy
      attr_accessor :post

      def initialize(post)
        @post = post
      end

      def call
        @post.destroy!
        Result::Success.new('Post deleted successfully.')
      rescue StandardError => e
        Result::Error.new(e)
      end
    end
  end
end
