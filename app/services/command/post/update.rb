# frozen_string_literal: true

module Command
  module Post
    class Update
      attr_reader :form
      attr_accessor :post

      def initialize(form)
        @form = form
        @post = ::Post.find(form.id)
      end

      def call
        return Result::Error.new(form.errors.full_messages) unless form.valid?
        assign_post_attributes
        remove_photo if form.remove_photo == '1'
        check_name
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_post_attributes
        post.attributes = {
          title: form.title,
          text: form.text,
          photo: form.photo,
          video: form.remove_video == '1' ? '' : form.video
        }
      end

      def remove_photo
        post.remove_photo!
      end

      def check_name
        error = 'You have to input your name in profile settings to perform this action!'
        raise error if post.creator.name.nil? || post.creator.surname.nil?
      end

      def save
        if @post.save
          Result::Success.new('Updated successfully.')
        else
          Result::Error.new(@post.errors.full_messages)
        end
      end
    end
  end
end
