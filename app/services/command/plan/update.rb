# frozen_string_literal: true

module Command
  module Plan
    class Update
      attr_accessor :form
      attr_accessor :plan

      def initialize(form, params)
        @form = form
        @plan = ::Plan.find(params[:id])
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_plan_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_plan_attributes
        @plan.attributes = {
          monthly_cost:         @form.monthly_cost,
          monthly_stripe_name:  @form.monthly_stripe_name,
          annual_cost:          @form.annual_cost,
          annual_stripe_name:   @form.annual_stripe_name,
          tax:                  @form.tax
        }
      end

      def save
        if @plan.save
          Result::Success.new('Data saved successfully.')
        else
          Result::Error.new(@plan.errors.full_messages)
        end
      end
    end
  end
end
