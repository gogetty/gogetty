# frozen_string_literal: true

module Command
  module Company
    class Create
      attr_reader :form
      attr_accessor :current_user
      attr_accessor :company
      attr_accessor :becoming_admin_result

      def initialize(form, current_user)
        @current_user = current_user
        @form = form
      end

      def call
        check_if_already_admin unless @current_user.platform_admin?
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_company
        create_company_profile
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_if_already_admin
        raise 'You are already admin for another company!' if current_user.company_id
      end

      def create_company
        @company = ::Company.new(
          name: @form.name
        )
      end

      def save
        ::Company.no_touching do
          if @company.save
            become_admin unless current_user.platform_admin?
            return cleanup unless current_user.platform_admin? || becoming_admin_result.success?
            Result::Success.new('Added successfully.')
          else
            Result::Error.new(@company.errors.full_messages)
          end
        end
      end

      def create_company_profile
        @company.build_company_profile(goal_year: Time.zone.today.year + 1)
      end

      def become_admin
        @becoming_admin_result = ::Command::Company::BecomeAdmin.new(@current_user,
                                                                     form,
                                                                     @company.id,
                                                                     true).call
      end

      def cleanup
        @company.company_profile.destroy
        @company.destroy
        @becoming_admin_result
      end
    end
  end
end
