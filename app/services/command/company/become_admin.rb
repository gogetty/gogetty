# frozen_string_literal: true

module Command
  module Company
    class BecomeAdmin
      attr_reader :current_user
      attr_reader :company
      attr_reader :form
      attr_reader :persist
      attr_accessor :result

      def initialize(current_user, form, company_id, persist = false)
        @current_user = current_user
        @form = form
        @company = ::Company.find(company_id)
        @persist = persist
      end

      def call
        check_acceptance
        check_pro
        check_if_already_admin
        become_superadmin
        Result::Success.new(result)
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_acceptance
        raise 'You must be company representative to become an admin!' unless form.valid?
      end

      def check_pro
        raise "You don't have an active subscription!" unless current_user.pro? || persist
      end

      def check_if_already_admin
        raise 'You are already admin for another company!' if current_user.company_id
      end

      def become_superadmin
        if ::User.find_by(company_id: @company.id)
          admin_request_send
        else
          become_admin
        end
      end

      def admin_request_send
        admin_request = AdminRequest.find_by(user_id: current_user.id,
                                             company_id: @company.id,
                                             cancelled: false,
                                             accepted: false,
                                             revoked: false)
        if admin_request
          raise 'There is already pending request from you to '\
                'become an admin for this company.'
        end
        AdminRequest.create(user_id: current_user.id, company_id: @company.id)
        MandrillAdminRequestMailer.request_new(@company, @current_user)
        @result = 'There is already an admin for this company profile! '\
              "You're request was send to be reviewed by this company admin."
      end

      def become_admin
        current_user.company_id = @company.id
        current_user.superadmin = true
        current_user.save!
        @result = 'Congratulations! '\
                  "You were assigned status of a superadmin for company '#{@company.name}'."
      end
    end
  end
end
