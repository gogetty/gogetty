# frozen_string_literal: true

module Command
  module TextPolicy
    class Create
      attr_reader :form
      attr_reader :text_policy

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        check_duplicates
        create_text_policy
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_duplicates
        raise 'Entry already exists!' if ::TextPolicy.find_by(
          name: @form.name,
          company_id:  @form.company_id
        )
      end

      def create_text_policy
        @text_policy = ::TextPolicy.new(
          name: @form.name,
          company_id:  @form.company_id
        )
      end

      def save
        if @text_policy.save
          Result::Success.new('Added successfully')
        else
          Result::Error.new(@text_policy.errors.full_messages)
        end
      end
    end
  end
end
