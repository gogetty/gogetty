# frozen_string_literal: true

module Command
  module UserPlan
    class Cancel
      attr_reader :current_user
      attr_reader :plans_user

      def initialize(current_user, plans_user_id)
        @current_user = current_user
        @plans_user = PlansUser.find(plans_user_id)
      end

      def call
        raise 'Invalid request!' if plans_user.user_id != current_user.id
        cancel_plan
        Result::Success.new('Plan cancelled. '\
                            'Your PRO account will be active to the end of paid period.')
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def cancel_plan
        sub = Stripe::Subscription.retrieve(plans_user.stripe_sub)
        if Rails.env.test?
          sub.delete(at_period_end: false)
        else
          sub.delete(at_period_end: true)
        end
        plans_user.cancel_scheduled = true
        plans_user.save!
      end
    end
  end
end
