# frozen_string_literal: true

module Command
  module UserPlan
    class CreateOrExtend
      attr_reader :current_user
      attr_reader :billing
      attr_reader :stripe_event

      def initialize(current_user, billing, stripe_event = nil)
        @current_user = current_user
        @billing = billing
        @stripe_event = stripe_event
      end

      def call
        set_active_plan
        Result::Success.new('User plan modified.')
      end

      def set_active_plan
        if stripe_event && PlansUser.find_by(stripe_sub: stripe_event.subscription_id)
          modify_user_pro
        else
          create_new_user_pro
        end
      end

      def modify_user_pro
        user_plan = PlansUser.find_by(stripe_sub: stripe_event.subscription_id)
        user_plan.payed_for_to = payed_for_to(user_plan.payed_for_to)
        user_plan.not_payed_pro = false
        user_plan.save!
      end

      def create_new_user_pro
        user_plan = PlansUser.new(user_id: current_user.id,
                                  plan_id: billing.plan.id,
                                  activated_on: today,
                                  payed_for_to: payed_for_to,
                                  stripe_sub: billing_stripe_sub)
        user_plan.save!
      end

      def billing_stripe_sub
        if billing.stripe_sub
          billing.stripe_sub
        else
          'one time payment'
        end
      end

      def payed_for_to(start = today)
        if billing.monthly
          start + 1.month
        else
          start + 1.year
        end
      end

      def today
        Time.zone.today
      end
    end
  end
end
