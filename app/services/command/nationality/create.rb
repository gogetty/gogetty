# frozen_string_literal: true

module Command
  module Nationality
    class Create
      attr_reader :form
      attr_accessor :nationality

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(form.errors.full_messages) unless form.valid?
        create_nationality
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_nationality
        @nationality = ::Nationality.new(
          name: form.name
        )
      end

      def save
        if nationality.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(nationality.errors.full_messages)
        end
      end
    end
  end
end
