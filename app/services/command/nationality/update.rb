# frozen_string_literal: true

module Command
  module Nationality
    class Update
      attr_accessor :nationality
      attr_reader :form

      def initialize(form, nationality)
        @form = form
        @nationality = nationality
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_nationality_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_nationality_attributes
        @nationality.attributes = {
          name: @form.name
        }
      end

      def save
        if @nationality.save
          Result::Success.new('Data saved successfully.')
        else
          Result::Error.new(@nationality.errors.full_messages)
        end
      end
    end
  end
end
