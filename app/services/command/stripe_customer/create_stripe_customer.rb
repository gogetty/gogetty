# frozen_string_literal: true

module Command
  module StripeCustomer
    class CreateStripeCustomer
      attr_reader :current_user
      attr_reader :params

      def initialize(current_user, params)
        @current_user = current_user
        @params = params
      end

      def call
        customer = Stripe::Customer.create(
          email: current_user.email,
          source: params[:stripeToken]
        )
        current_user.stripe_customer_id = customer.id
        current_user.save!
        Result::Success.new(customer)
      rescue StandardError => e
        Result::Error.new(e)
      end
    end
  end
end
