# frozen_string_literal: true

module Command
  module CompanyNationality
    class Create
      attr_reader :form
      attr_accessor :nationality

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(form.errors.full_messages) unless form.valid?
        create_nationality
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_nationality
        @nationality = ::CompanyNationality.new(
          company_id: form.company_id,
          nationality_id: form.nationality_id
        )
      end

      def save
        if nationality.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(nationality.errors.full_messages)
        end
      end
    end
  end
end
