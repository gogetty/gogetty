# frozen_string_literal: true

module Command
  module CompanyNationality
    class Destroy
      attr_accessor :nationality

      def initialize(nationality)
        @nationality = nationality
      end

      def call
        @nationality.destroy!
        Result::Success.new('Nationality deleted successfully.')
      rescue StandardError => e
        Result::Error.new(e)
      end
    end
  end
end
