# frozen_string_literal: true

module Command
  module CompanyData
    class Update
      attr_reader :form
      attr_accessor :company_profile
      attr_accessor :platform_admin

      def initialize(form, platform_admin = false)
        @form = form
        @company_profile = ::CompanyProfile.find(form.id)
        @platform_admin = platform_admin
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_attributes
        @company_profile.attributes = {
          employees_count_id: form.employees_count_id,
          liabilities: form.liabilities,
          revenue: form.revenue
        }
      end

      # rubocop:disable SkipsModelValidations
      def save
        if @company_profile.save
          @company_profile.company.touch unless @platform_admin
          Result::Success.new('Updated successfully.')
        else
          Result::Error.new(@company_profile.errors.full_messages)
        end
      end
      # rubocop:enable all
    end
  end
end
