# frozen_string_literal: true

module Command
  module Industry
    class Destroy
      attr_accessor :industry
      attr_accessor :usege
      attr_reader :form

      def initialize(industry)
        @industry = industry
      end

      def call
        check_usage
        return Result::Error.new("Entry used somewhere else, can't delete!") if @usage
        destroy
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_usage
        @usage = ::Company.find_by(industry_id: @industry.id)
      end

      def destroy
        @industry.destroy
        Result::Success.new('Data destroyed successfully.')
      end
    end
  end
end
