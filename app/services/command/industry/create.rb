# frozen_string_literal: true

module Command
  module Industry
    class Create
      attr_reader :form
      attr_accessor :industry

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_industry
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_industry
        @industry = ::Industry.new(
          name: @form.name
        )
      end

      def save
        if @industry.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(@industry.errors.full_messages)
        end
      end
    end
  end
end
