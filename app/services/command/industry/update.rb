# frozen_string_literal: true

module Command
  module Industry
    class Update
      attr_accessor :industry
      attr_reader :form

      def initialize(form, industry)
        @form = form
        @industry = industry
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_industry_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_industry_attributes
        @industry.attributes = {
          name: @form.name
        }
      end

      def save
        if @industry.save
          Result::Success.new('Data saved successfully.')
        else
          Result::Error.new(@industry.errors.full_messages)
        end
      end
    end
  end
end
