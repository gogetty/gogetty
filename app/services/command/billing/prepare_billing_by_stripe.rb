# frozen_string_literal: true

module Command
  module Billing
    class PrepareBillingByStripe
      attr_reader :stripe_event
      attr_accessor :billing_result

      def initialize(stripe_event)
        @stripe_event = stripe_event
      end

      def call
        find_billing
        create_billing unless billing_result.success?
        billing_result
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def find_billing
        @billing_result = ::Query::Billing::FindPendingBillingByStripe.new(stripe_event).call
        modify_billing_value if billing_result.success?
      end

      def modify_billing_value
        billing = @billing_result.data
        billing.tax = stripe_event.tax_percent
        billing.tax_value = stripe_event.tax
        billing.cost = stripe_event.total
        billing.save!
      end

      def create_billing
        @billing_result = ::Command::Billing::CreateBillingByStripe.new(stripe_event).call
      end
    end
  end
end
