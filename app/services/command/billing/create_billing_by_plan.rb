# frozen_string_literal: true

module Command
  module Billing
    class CreateBillingByPlan
      attr_reader :current_user
      attr_reader :plan
      attr_reader :monthly
      attr_reader :stripe_event_params
      attr_reader :coupon
      attr_accessor :billing

      def initialize(current_user, plan, monthly, coupon = nil)
        @current_user = current_user
        @plan = plan
        @monthly = monthly
        @coupon = coupon
        @billing = ::Billing.new(plan_id: plan.id,
                                 amount_in_cents: plan_cost,
                                 monthly: monthly,
                                 tax: plan.tax,
                                 tax_value: plan_cost * plan.tax / BigDecimal.new('100'))
      end

      def call
        set_coupon_data
        set_user_data
        count_cost
        billing.save!
        Result::Success.new(billing)
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def set_coupon_data
        billing.coupon = coupon.id if coupon
        billing.coupon_amount_off = coupon.amount_off if coupon
        billing.coupon_percent_off = coupon.percent_off if coupon
      end

      def set_user_data
        billing.user_id = current_user.id if current_user
      end

      def count_cost
        billing.cost = if coupon&.amount_off
                         discount_amount
                       elsif coupon&.percent_off
                         discount_percent
                       else
                         plan_cost
                       end
        add_tax
      end

      def discount_amount
        plan_cost - coupon.amount_off
      end

      def discount_percent
        plan_cost - (plan_cost * coupon.percent_off / BigDecimal.new('100'))
      end

      def add_tax
        billing.cost = billing.cost + (billing.cost * plan.tax / BigDecimal.new('100'))
      end

      def plan_cost
        ::Query::Charge::FindPlanCost.new(plan, monthly).call.data
      end
    end
  end
end
