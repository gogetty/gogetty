# frozen_string_literal: true

module Command
  module Billing
    class CreateBillingByStripe
      attr_reader :monthly
      attr_reader :stripe_event
      attr_accessor :plan
      attr_accessor :billing

      def initialize(stripe_event)
        @stripe_event = stripe_event
      end

      def call
        find_plan_by_stripe_name
        create_billing
        Result::Success.new(@billing)
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def find_plan_by_stripe_name
        @plan = ::Plan.find_by(monthly_stripe_name: stripe_event.plan_id)
        if @plan
          @monthly = true
        else
          @plan = ::Plan.find_by(annual_stripe_name: stripe_event.plan_id)
          @monthly = false
        end
        raise "Plan with name '#{stripe_event.plan_id}' not found!" unless @plan
      end

      def create_billing
        prepare_billing
        @billing.save!
      end

      # rubocop:disable AbcSize
      def prepare_billing
        @billing = ::Billing.new(plan_id: plan.id,
                                 amount_in_cents: stripe_event.plan_cost,
                                 cost: stripe_event.total,
                                 monthly: monthly,
                                 stripe_invoice: stripe_event.invoice_id,
                                 stripe_charge: stripe_event.charge_id,
                                 stripe_sub: stripe_event.subscription_id,
                                 user_id: stripe_event.user,
                                 tax: stripe_event.tax_percent,
                                 tax_value: stripe_event.tax)
      end
      # rubocop:enable all
    end
  end
end
