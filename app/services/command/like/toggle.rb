# frozen_string_literal: true

module Command
  module Like
    class Toggle
      attr_reader :form
      attr_accessor :like
      attr_accessor :success_message

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        check_name
        check_unique
        create_or_destroy_like
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_unique
        @like = ::Like.find_by(
          post_id: @form.post_id,
          created_by: @form.created_by
        )
      end

      def create_or_destroy_like
        if check_unique
          @like.destroy
          Result::Success.new('Unliked successfully.')
        else
          @like = ::Like.new(
            created_by: @form.created_by,
            post_id: @form.post_id
          )
          save
        end
      end

      def check_name
        error = 'You have to input your name in profile settings to perform this action!'
        user = ::User.find(form.created_by)
        raise error if user.name.nil? || user.surname.nil?
      end

      def save
        if @like.save
          Result::Success.new('Liked successfully.')
        else
          Result::Error.new(@like.errors.full_messages)
        end
      end
    end
  end
end
