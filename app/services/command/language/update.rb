# frozen_string_literal: true

module Command
  module Language
    class Update
      attr_accessor :language
      attr_reader :form

      def initialize(form, language)
        @form = form
        @language = language
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_language_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_language_attributes
        @language.attributes = {
          name: @form.name
        }
      end

      def save
        if @language.save
          Result::Success.new('Data saved successfully.')
        else
          Result::Error.new(@language.errors.full_messages)
        end
      end
    end
  end
end
