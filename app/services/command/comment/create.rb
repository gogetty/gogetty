# frozen_string_literal: true

module Command
  module Comment
    class Create
      attr_reader :form
      attr_accessor :comment

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_comment
        check_name
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_comment
        @comment = ::Comment.new(
          text: @form.text,
          created_by: @form.created_by,
          post_id: @form.post_id
        )
      end

      def check_name
        error = 'You have to input your name in profile settings to perform this action!'
        raise error if @comment.creator.name.nil? || @comment.creator.surname.nil?
      end

      def save
        if @comment.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(@comment.errors.full_messages)
        end
      end
    end
  end
end
