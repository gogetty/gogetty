# frozen_string_literal: true

module Command
  module Comment
    class Destroy
      attr_accessor :comment

      def initialize(comment)
        @comment = comment
      end

      def call
        @comment.destroy!
        Result::Success.new('Comment deleted successfully.')
      rescue StandardError => e
        Result::Error.new(e)
      end
    end
  end
end
