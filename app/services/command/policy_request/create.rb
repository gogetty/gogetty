# frozen_string_literal: true

module Command
  module PolicyRequest
    class Create
      attr_reader :form
      attr_accessor :policy_request

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_policy_request
        create_subs
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_policy_request
        @policy_request = ::PolicyRequest.new(
          comment: @form.comment,
          company_id: @form.company_id,
          user_id: @form.user_id
        )
      end

      def create_subs
        create_diversity_policies_policy_requests
      end

      def create_diversity_policies_policy_requests
        return unless @form.diversity_policies_policy_requests
        diversity_policies_policy_requests =
          @form.diversity_policies_policy_requests.map do |diversity_policies_policy_request|
            ::DiversityPoliciesPolicyRequest.new(
              diversity_policy_id: diversity_policies_policy_request.diversity_policy_id
            )
          end
        @policy_request.diversity_policies_policy_requests = diversity_policies_policy_requests
      end

      def save
        if @policy_request.save
          Result::Success.new('Request send successfully.')
        else
          Result::Error.new(@policy_request.errors.full_messages)
        end
      end
    end
  end
end
