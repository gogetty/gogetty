# frozen_string_literal: true

module Command
  module GoalRequest
    class Create
      attr_reader :form
      attr_accessor :goal_request

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_goal_request
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_goal_request
        @goal_request = ::GoalRequest.new(
          company_id: @form.company_id,
          user_id: @form.user_id
        )
      end

      def save
        if @goal_request.save
          Result::Success.new('Request send successfully.')
        else
          Result::Error.new(@goal_request.errors.full_messages)
        end
      end
    end
  end
end
