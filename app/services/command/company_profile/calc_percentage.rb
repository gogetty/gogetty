# frozen_string_literal: true

# Changes made by James Wilson without applying Rubocop rules
# rubocop:disable all
module Command
  module CompanyProfile
    class CalcPercentage
      attr_accessor :company_profile

      def initialize(company_profile)
        @company_profile = company_profile
      end

      def call(type, management, year)
        calculate(type, management, year)
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def calculate(type, management, year)
        if type == 'number'
          calculate_percentage(management, year)
        else
          calculate_number(management, year)
        end
      end

      def calculate_percentage(management, year)
        if year == 'current'
          calculate_now_percentage management
        else
          calculate_goal_percentage management
        end
      end

      def calculate_now_percentage(management)
        case management
        when 'director'
          @company_profile.now_director_woman_percentage = ((@company_profile.now_director_woman_count.to_f / (@company_profile.now_director_woman_count + @company_profile.now_director_man_count)) * 100).round
          @company_profile.now_director_man_percentage = 100 - @company_profile.now_director_woman_percentage.round
          @company_profile.now_director_total = @company_profile.now_director_woman_count + @company_profile.now_director_man_count
        when 'cxo'
          @company_profile.now_cxo_woman_percentage = (@company_profile.now_cxo_woman_count.to_f / (@company_profile.now_cxo_woman_count + @company_profile.now_cxo_man_count)) * 100
          @company_profile.now_cxo_man_percentage = 100 - @company_profile.now_cxo_woman_percentage.round
          @company_profile.now_cxo_total = @company_profile.now_cxo_woman_count + @company_profile.now_cxo_man_count
        when 'vp'
          @company_profile.now_vp_woman_percentage = (@company_profile.now_vp_woman_count.to_f / (@company_profile.now_vp_woman_count + @company_profile.now_vp_man_count)) * 100
          @company_profile.now_vp_man_percentage = 100 - @company_profile.now_vp_woman_percentage.round
          @company_profile.now_vp_total = @company_profile.now_vp_woman_count + @company_profile.now_vp_man_count
        when 'middle'
          @company_profile.now_middle_woman_percentage = (@company_profile.now_middle_woman_count.to_f / (@company_profile.now_middle_woman_count + @company_profile.now_middle_man_count)) * 100
          @company_profile.now_middle_man_percentage = 100 - @company_profile.now_middle_woman_percentage.round
          @company_profile.now_middle_total = @company_profile.now_middle_woman_count + @company_profile.now_middle_man_count
        end
      end

      def calculate_goal_percentage(management)
        case management
        when 'director'
          @company_profile.goal_director_woman_percentage = ((@company_profile.goal_director_woman_count.to_f / (@company_profile.goal_director_woman_count + @company_profile.goal_director_man_count)) * 100).round
          @company_profile.goal_director_man_percentage = 100 - @company_profile.goal_director_woman_percentage.round
          @company_profile.goal_director_total = @company_profile.goal_director_woman_count + @company_profile.goal_director_man_count
        when 'cxo'
          @company_profile.goal_cxo_woman_percentage = (@company_profile.goal_cxo_woman_count.to_f / (@company_profile.goal_cxo_woman_count + @company_profile.goal_cxo_man_count)) * 100
          @company_profile.goal_cxo_man_percentage = 100 - @company_profile.goal_cxo_woman_percentage.round
          @company_profile.goal_cxo_total = @company_profile.goal_cxo_woman_count + @company_profile.goal_cxo_man_count
        when 'vp'
          @company_profile.goal_vp_woman_percentage = (@company_profile.goal_vp_woman_count.to_f / (@company_profile.goal_vp_woman_count + @company_profile.goal_vp_man_count)) * 100
          @company_profile.goal_vp_man_percentage = 100 - @company_profile.goal_vp_woman_percentage.round
          @company_profile.goal_vp_total = @company_profile.goal_vp_woman_count + @company_profile.goal_vp_man_count
        when 'middle'
          @company_profile.goal_middle_woman_percentage = (@company_profile.goal_middle_woman_count.to_f / (@company_profile.goal_middle_woman_count + @company_profile.goal_middle_man_count)) * 100
          @company_profile.goal_middle_man_percentage = 100 - @company_profile.goal_middle_woman_percentage.round
          @company_profile.goal_middle_total = @company_profile.goal_middle_woman_count + @company_profile.goal_middle_man_count
        end
      end

      def calculate_number(management, year)
        if year == 'current'
          calculate_now_number management
        else
          calculate_goal_number management
        end
      end

      def calculate_now_number(management)
        case management
        when 'director'
          @company_profile.now_director_woman_count = (@company_profile.now_director_woman_percentage.to_f / 100) * @company_profile.now_director_total
          @company_profile.now_director_man_count = @company_profile.now_director_total - @company_profile.now_director_woman_count
        when 'cxo'
          @company_profile.now_cxo_woman_count = (@company_profile.now_cxo_woman_percentage.to_f / 100) * @company_profile.now_cxo_total
          @company_profile.now_cxo_man_count = @company_profile.now_cxo_total - @company_profile.now_cxo_woman_count
        when 'vp'
          @company_profile.now_vp_woman_count = (@company_profile.now_vp_woman_percentage.to_f / 100) * @company_profile.now_vp_total
          @company_profile.now_vp_man_count = @company_profile.now_vp_total - @company_profile.now_vp_woman_count
        when 'middle'
          @company_profile.now_middle_woman_count = (@company_profile.now_middle_woman_percentage.to_f / 100) * @company_profile.now_middle_total
          @company_profile.now_middle_man_count = @company_profile.now_middle_total - @company_profile.now_middle_woman_count
        end
      end

      def calculate_goal_number(management)
        case management
        when 'director'
          @company_profile.goal_director_woman_count = (@company_profile.goal_director_woman_percentage.to_f / 100) * @company_profile.goal_director_total
          @company_profile.goal_director_man_count = @company_profile.goal_director_total - @company_profile.goal_director_woman_count
        when 'cxo'
          @company_profile.goal_cxo_woman_count = (@company_profile.goal_cxo_woman_percentage.to_f / 100) * @company_profile.goal_cxo_total
          @company_profile.goal_cxo_man_count = @company_profile.goal_cxo_total - @company_profile.goal_cxo_woman_count
        when 'vp'
          @company_profile.goal_vp_woman_count = (@company_profile.goal_vp_woman_percentage.to_f / 100) * @company_profile.goal_vp_total
          @company_profile.goal_vp_man_count = @company_profile.goal_vp_total - @company_profile.goal_vp_woman_count
        when 'middle'
          @company_profile.goal_middle_woman_count = (@company_profile.goal_middle_woman_percentage.to_f / 100) * @company_profile.goal_middle_total
          @company_profile.goal_middle_man_count = @company_profile.goal_middle_total - @company_profile.goal_middle_woman_count
        end
      end

      def now_woman_count
        [
          @company_profile.now_director_woman_count,
          @company_profile.now_cxo_woman_count,
          @company_profile.now_vp_woman_count,
          @company_profile.now_middle_woman_count
        ].compact.sum
      end

      def goal_woman_count
        [
          @company_profile.goal_director_woman_count,
          @company_profile.goal_cxo_woman_count,
          @company_profile.goal_vp_woman_count,
          @company_profile.goal_middle_woman_count
        ].compact.sum
      end

      def now_man_count
        [
          @company_profile.now_director_man_count,
          @company_profile.now_cxo_man_count,
          @company_profile.now_vp_man_count,
          @company_profile.now_middle_man_count
        ].compact.sum
      end

      def goal_man_count
        [
          @company_profile.goal_director_man_count,
          @company_profile.goal_cxo_man_count,
          @company_profile.goal_vp_man_count,
          @company_profile.goal_middle_man_count
        ].compact.sum
      end

      def calculate_now
        total_count = now_woman_count + now_man_count

        if total_count.positive?
          score = (now_woman_count.to_f / total_count) * 100.0
          score = 50 - (score - 50) if score > 50
          @company_profile.diversity_score = score.round
        else
          @company_profile.diversity_score = 0
        end
      end

      def calculate_goal
        total_count = goal_woman_count + goal_man_count

        if total_count.positive?
          score = (goal_woman_count.to_f / total_count) * 100.0
          score = 50 - (score - 50) if score > 50
          @company_profile.goal_diversity_score = score.round
        else
          @company_profile.goal_diversity_score = 0
        end
      end
    end
  end
end
