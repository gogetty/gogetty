# frozen_string_literal: true

module Command
  module CompanyProfile
    class Update
      attr_reader :form
      attr_accessor :company_profile
      attr_accessor :platform_admin

      def initialize(form, platform_admin = false)
        @form = form
        @company_profile = ::CompanyProfile.find(form.id)
        @platform_admin = platform_admin
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

       # rubocop:disable all
      def assign_attributes
        @company_profile.attributes = {
          now_director_woman_count:  form.director_hide ? 0 : form.now_director_woman_count,
          now_director_man_count:    form.director_hide ? 0 : form.now_director_man_count,
          goal_director_woman_count: form.director_hide ? 0 : form.goal_director_woman_count,
          goal_director_man_count:   form.director_hide ? 0 : form.goal_director_man_count,
          now_cxo_woman_count:       form.cxo_hide ? 0 : form.now_cxo_woman_count,
          now_cxo_man_count:         form.cxo_hide ? 0 : form.now_cxo_man_count,
          goal_cxo_woman_count:      form.cxo_hide ? 0 : form.goal_cxo_woman_count,
          goal_cxo_man_count:        form.cxo_hide ? 0 : form.goal_cxo_man_count,
          now_vp_woman_count:        form.vp_hide ? 0 : form.now_vp_woman_count,
          now_vp_man_count:          form.vp_hide ? 0 : form.now_vp_man_count,
          goal_vp_woman_count:       form.vp_hide ? 0 : form.goal_vp_woman_count,
          goal_vp_man_count:         form.vp_hide ? 0 : form.goal_vp_man_count,
          now_middle_woman_count:    form.middle_hide ? 0 : form.now_middle_woman_count,
          now_middle_man_count:      form.middle_hide ? 0 : form.now_middle_man_count,
          goal_middle_woman_count:   form.middle_hide ? 0 : form.goal_middle_woman_count,
          goal_middle_man_count:     form.middle_hide ? 0 : form.goal_middle_man_count,
          director_hide:             form.director_hide,
          cxo_hide:                  form.cxo_hide,
          vp_hide:                   form.vp_hide,
          middle_hide:               form.middle_hide,
          goal_year:                 form.goal_year,
          director_name:             form.director_name,
          cxo_name:                  form.cxo_name,
          vp_name:                   form.vp_name,
          middle_name:               form.middle_name,
          now_director_woman_percentage:   form.now_director_woman_percentage,
          now_director_man_percentage:     form.now_director_man_percentage,
          now_director_total:              form.now_director_total,
          goal_director_total:             form.goal_director_total,
          goal_director_woman_percentage:  form.goal_director_woman_percentage,
          goal_director_man_percentage:    form.goal_director_man_percentage,
          now_cxo_woman_percentage:        form.now_cxo_woman_percentage,
          now_cxo_man_percentage:          form.now_cxo_man_percentage,
          now_cxo_total:                   form.now_cxo_total,
          goal_cxo_total:                  form.goal_cxo_total,
          goal_cxo_woman_percentage:       form.goal_cxo_woman_percentage,
          goal_cxo_man_percentage:         form.goal_cxo_man_percentage,
          now_vp_woman_percentage:         form.now_vp_woman_percentage,
          now_vp_man_percentage:           form.now_vp_man_percentage,
          now_vp_total:                    form.now_vp_total,
          goal_vp_total:                   form.goal_vp_total,
          goal_vp_woman_percentage:        form.goal_vp_woman_percentage,
          goal_vp_man_percentage:          form.goal_vp_man_percentage,
          now_middle_woman_percentage:     form.now_middle_woman_percentage,
          now_middle_man_percentage:       form.now_middle_man_percentage,
          now_middle_total:                form.now_middle_total,
          goal_middle_total:               form.goal_middle_total,
          goal_middle_woman_percentage:    form.goal_middle_woman_percentage,
          goal_middle_man_percentage:      form.goal_middle_man_percentage,
          director_in_percenatge:          form.director_in_percenatge,
          cxo_in_percenatge:               form.cxo_in_percenatge,
          middle_in_percenatge:            form.middle_in_percenatge,
          vp_in_percenatge:                form.vp_in_percenatge

        }
      end
      # rubocop:enable all

      def calc_diversity_score
        ::Command::CompanyProfile::CalcDiversityScore.new(@company_profile).call
      end

      def create_company_profile_history
        ::Command::CompanyProfileHistory::Create.new(@company_profile).call
      end

      # rubocop:disable SkipsModelValidations
      def save
        if @company_profile.save
          @company_profile.company.touch unless @platform_admin
          calc_diversity_score
          create_company_profile_history
          Result::Success.new('Updated successfully.')
        else
          Result::Error.new(@company_profile.errors.full_messages)
        end
      end
    end
  end
end
