# frozen_string_literal: true

module Command
  module CompanyProfile
    class CalcDiversityScore
      attr_accessor :company_profile

      def initialize(company_profile)
        @company_profile = company_profile
      end

      def call
        calculate
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def calculate
        calculate_now
        calculate_goal
        calculate_total
      end

      def now_woman_count
        [
          @company_profile.now_director_woman_count,
          @company_profile.now_cxo_woman_count,
          @company_profile.now_vp_woman_count,
          @company_profile.now_middle_woman_count
        ].compact.sum
      end

      def goal_woman_count
        [
          @company_profile.goal_director_woman_count,
          @company_profile.goal_cxo_woman_count,
          @company_profile.goal_vp_woman_count,
          @company_profile.goal_middle_woman_count
        ].compact.sum
      end

      def now_man_count
        [
          @company_profile.now_director_man_count,
          @company_profile.now_cxo_man_count,
          @company_profile.now_vp_man_count,
          @company_profile.now_middle_man_count
        ].compact.sum
      end

      def goal_man_count
        [
          @company_profile.goal_director_man_count,
          @company_profile.goal_cxo_man_count,
          @company_profile.goal_vp_man_count,
          @company_profile.goal_middle_man_count
        ].compact.sum
      end

      def calculate_now
        total_count = now_woman_count + now_man_count

        if total_count.positive?
          score = (now_woman_count.to_f / total_count) * 100.0
          score = 50 - (score - 50) if score > 50
          @company_profile.diversity_score = score.round
        else
          @company_profile.diversity_score = 0
        end
      end

      def calculate_goal
        total_count = goal_woman_count + goal_man_count

        if total_count.positive?
          score = (goal_woman_count.to_f / total_count) * 100.0
          score = 50 - (score - 50) if score > 50
          @company_profile.goal_diversity_score = score.round
        else
          @company_profile.goal_diversity_score = 0
        end
      end

      # Changes made by James Wilson without applying Rubocop rules
      def calculate_total # rubocop:disable Metrics/AbcSize
        total_count = goal_woman_count + goal_man_count + now_woman_count + now_man_count
        total_women_count = goal_woman_count + now_woman_count

        if total_count.positive?
          score = (total_women_count.to_f / total_count) * 100.0
          score = 50 - (score - 50) if score > 50
          @company_profile.total_diversity_score = score.round
        else
          @company_profile.total_diversity_score = 0
        end
      end

      def save
        if @company_profile.save
          Result::Success.new('Updated successfully.')
        else
          Result::Error.new(@company_profile.errors.full_messages)
        end
      end
    end
  end
end
