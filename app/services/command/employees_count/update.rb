# frozen_string_literal: true

module Command
  module EmployeesCount
    class Update
      attr_accessor :employees_count
      attr_reader :form

      def initialize(form, employees_count)
        @form = form
        @employees_count = employees_count
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_employees_count_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_employees_count_attributes
        @employees_count.attributes = {
          order: @form.order,
          count: @form.count
        }
      end

      def save
        if @employees_count.save
          Result::Success.new('Data saved successfully.')
        else
          Result::Error.new(@employees_count.errors.full_messages)
        end
      end
    end
  end
end
