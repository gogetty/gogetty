# frozen_string_literal: true

module Command
  module EmployeesCount
    class Create
      attr_reader :form
      attr_accessor :employees_count

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_employees_count
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_employees_count
        @employees_count = ::EmployeesCount.new(
          order: @form.order,
          count: @form.count
        )
      end

      def save
        if @employees_count.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(@employees_count.errors.full_messages)
        end
      end
    end
  end
end
