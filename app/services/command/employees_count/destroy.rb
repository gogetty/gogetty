# frozen_string_literal: true

module Command
  module EmployeesCount
    class Destroy
      attr_accessor :employees_count
      attr_accessor :usege
      attr_reader :form

      def initialize(employees_count)
        @employees_count = employees_count
      end

      def call
        check_usage
        return Result::Error.new("Entry used somewhere else, can't delete!") if @usage
        destroy
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_usage
        @usage = ::CompanyProfile.find_by(employees_count_id: @employees_count.id)
      end

      def destroy
        @employees_count.destroy
        Result::Success.new('Data destroyed successfully.')
      end
    end
  end
end
