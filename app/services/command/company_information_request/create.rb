# frozen_string_literal: true

module Command
  module CompanyInformationRequest
    class Create
      attr_reader :form
      attr_accessor :company

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_company_information_request
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_company_information_request
        @company_information_request = ::CompanyInformationRequest.new(
          company_id: @form.company_id,
          user_id: @form.user_id,
          comment: @form.comment
        )
      end

      def save
        if @company_information_request.save
          Result::Success.new('Request send successfully.')
        else
          Result::Error.new(@company_information_request.errors.full_messages)
        end
      end
    end
  end
end
