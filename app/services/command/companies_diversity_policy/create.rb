# frozen_string_literal: true

module Command
  module CompaniesDiversityPolicy
    class Create
      attr_reader :form
      attr_reader :companies_diversity_policy

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        check_duplicates
        create_companies_diversity_policy
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_duplicates
        raise 'Entry already exists!' if ::CompaniesDiversityPolicy.find_by(
          diversity_policy_id: @form.diversity_policy_id,
          company_id:  @form.company_id
        )
      end

      def create_companies_diversity_policy
        @companies_diversity_policy = ::CompaniesDiversityPolicy.new(
          diversity_policy_id: @form.diversity_policy_id,
          company_id:  @form.company_id
        )
      end

      def save
        if @companies_diversity_policy.save
          Result::Success.new('Added successfully')
        else
          Result::Error.new(@companies_diversity_policy.errors.full_messages)
        end
      end
    end
  end
end
