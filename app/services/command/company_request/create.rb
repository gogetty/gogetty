# frozen_string_literal: true

module Command
  module CompanyRequest
    class Create
      attr_reader :form
      attr_accessor :company_request

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_company_request
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_company_request
        @company_request = ::CompanyRequest.new(
          name: @form.name,
          user_id: @form.user_id
        )
      end

      def save
        if @company_request.save
          Result::Success.new('Request send successfully.')
        else
          Result::Error.new(@company_request.errors.full_messages)
        end
      end

      def create_company_profile
        company_profile = ::CompanyProfile.new(company_id: @company.id,
                                               goal_year: Time.zone.today.year + 1)
        company_profile.save!
      end

      def become_admin
        @becoming_admin_result = ::Command::Company::BecomeAdmin.new(@current_user,
                                                                     form,
                                                                     @company.id).call
      end

      def cleanup
        @company.destroy
        @becoming_admin_result
      end
    end
  end
end
