# frozen_string_literal: true

module Command
  module Subscription
    class Update
      attr_reader :current_user
      attr_reader :plan_name
      attr_reader :interval
      attr_reader :coupon

      def initialize(current_user, params)
        @current_user = current_user
        @plan_name = params[:plan_name]
        @coupon = params[:coupon]
      end

      def call
        if update_subscription
          Result::Success.new(@plan_name)
        else
          Result::Error.new(StandardError.new('Subscription has not been upgraded'))
        end
      end

      private

      def update_subscription
        Stripe::Subscription.update(
          current_user.active_paid_plans.first.stripe_sub,
          cancel_at_period_end: false,
          coupon: coupon,
          plan: plan_name
        )
      end
    end
  end
end
