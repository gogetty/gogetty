# frozen_string_literal: true

module Command
  module Subscription
    class VerifyUpgrade
      include PoroUrlHelper
      attr_reader :form
      attr_reader :current_user
      attr_accessor :plan
      attr_accessor :coupon
      attr_accessor :session

      def initialize(data)
        @form = data[0]
        @current_user = data[1]
        @session = data[2]
      end

      def call
        return payment_in_progres_error if pending_payment
        check_coupon
        find_plan
        set_billing
        return invalid_plan_configuration unless @plan
        return subscription_upgrade unless subscription_upgrade.success?
        Result::Success.new(plan: plan,
                            interval: current_user.current_subscription_interval,
                            coupon: @coupon,
                            upgrade: @upgrade)
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def set_billing
        annual = current_user.current_subscription_interval == 'month'
        billing_result = ::Command::Billing::CreateBillingByPlan.new(current_user,
                                                                     plan,
                                                                     annual,
                                                                     @coupon).call
        session[:current_billing_id] = billing_result.data.id
      end

      def invalid_plan_configuration
        Result::Error.new(StandardError.new('Invalid plan configuration!'))
      end

      def check_coupon
        @coupon = Stripe::Coupon.retrieve(@form.coupon) if @form.coupon != ''
      end

      def pending_payment
        ::Billing.find_by(user_id: current_user.id, state: ::Billing.states[:pending])
      end

      def subscription_upgrade
        user_active_plan = current_user.current_subscription_plan
        return subscription_downgrade_error if user_active_plan.monthly_cost > plan.monthly_cost
        if user_active_plan == plan
          subscription_same_level_error
        else
          @upgrade = true
          Result::Success.new('Subscription can be upgraded')
        end
      end

      def subscription_downgrade_error
        Result::Error.new(StandardError.new('You are already subscribed to a higher plan.'))
      end

      def subscription_same_level_error
        Result::Error.new(StandardError.new('You are already subscribed to this plan.'))
      end

      def payment_in_progres_error
        Result::Error.new(StandardError.new("There's payment in progress. "\
                                            'Check billings section in account settings.'))
      end

      def find_plan
        @plan = ::Plan.find_by(commit: form.commit)
      end
    end
  end
end
