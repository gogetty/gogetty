# frozen_string_literal: true

module Command
  module Subscription
    class Decide
      include PoroUrlHelper
      attr_reader :form
      attr_reader :current_user
      attr_accessor :paid
      attr_accessor :redirection_msg
      attr_accessor :redirection_url
      attr_accessor :session
      attr_accessor :plan
      attr_accessor :other_subscription_result
      attr_accessor :check_coupon_result
      attr_accessor :coupon

      def initialize(data)
        @form = data[0]
        @session = data[1]
        @current_user = data[2]
        @become_admin_company_id = data[3]
      end

      # rubocop:disable AbcSize
      # rubocop:disable CyclomaticComplexity
      # rubocop:disable PerceivedComplexity
      def call
        return other_subscription_not_exists unless other_subscription_not_exists.success?
        check_coupon
        find_plan_and_paid
        return can_subscribe_monthly unless can_subscribe_monthly.success?
        redirection
        set_billing if @plan && plan_cost.positive?
        return Result::Error.new(StandardError.new('Invalid plan configuration!')) if !@plan && @paid
        Result::Success.new(redirection_url: @redirection_url,
                            redirection_msg: @redirection_msg)
      rescue StandardError => e
        Result::Error.new(e)
      end
      # rubocop:enable all

      private

      def check_coupon
        @coupon = Stripe::Coupon.retrieve(@form.coupon) if @form.coupon != ''
      end

      def can_subscribe_monthly
        return Result::Success.new('Yearly subscription') if form.annual
        if current_user.monthly_billing_permission
          Result::Success.new('Can be billed monthly')
        else
          Result::Error.new(StandardError.new('You cannot buy monthly subscription'))
        end
      end

      def other_subscription_not_exists
        # XXX: check with stripe?
        return Result::Success.new('No user signed in.') unless current_user
        if ::PlansUser.find_by(user_id: current_user.id, cancelled_on: nil)
          subscription_active_error
        elsif ::Billing.find_by(user_id: current_user.id, state: ::Billing.states[:pending])
          payment_in_progres_error
        else
          Result::Success.new('No other subscriptions detected.')
        end
      end

      def subscription_active_error
        Result::Error.new(StandardError.new('You already have an active subscription.'))
      end

      def payment_in_progres_error
        Result::Error.new(StandardError.new("There's payment in progress. "\
                                            'Check billings section in account settings.'))
      end

      def find_plan_and_paid
        @plan = ::Plan.find_by(commit: form.commit)
        @paid = plan_cost.positive? if @plan
      end

      def redirection
        if @plan.nil?
          set_invalid_redirection
        elsif @paid
          set_paid_redirection
        elsif !@paid
          set_unpaid_redirection
        else
          set_invalid_redirection
        end
      end

      def set_invalid_redirection
        @redirection_url = url_helpers.root_path
        @redirection_msg = 'Invalid request'
      end

      def set_paid_redirection
        @redirection_url = url_helpers.new_user_logged_charge_path(company_id: @become_admin_company_id)
      end

      def set_unpaid_redirection
        @redirection_url = url_helpers.user_logged_walls_path
      end

      def set_billing
        billing_result = ::Command::Billing::CreateBillingByPlan.new(current_user,
                                                                     plan,
                                                                     !form.annual,
                                                                     @coupon).call
        session[:current_billing_id] = billing_result.data.id
      end

      def plan_cost
        ::Query::Charge::FindPlanCost.new(plan, !form.annual).call.data
      end
    end
  end
end
