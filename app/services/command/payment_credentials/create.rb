# frozen_string_literal: true

module Command
  module PaymentCredentials
    class Create
      attr_reader :form
      attr_accessor :payment_credential

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_payment_credential
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_payment_credential
        @payment_credential = ::PaymentCredential.new(
          stripe_customer_id: @form.stripe_customer_id,
          user_id: @form.user_id,
          last4: @form.last4,
          exp_month: @form.exp_month,
          exp_year: @form.exp_year,
          card_type: @form.card_type
        )
      end

      def save
        if @payment_credential.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(@payment_credential.errors.full_messages)
        end
      end
    end
  end
end
