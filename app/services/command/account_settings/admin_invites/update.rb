# frozen_string_literal: true

module Command
  module AccountSettings
    module AdminInvites
      class Update
        attr_accessor :admin_invite
        attr_accessor :current_user

        def initialize(params, current_user)
          @admin_invite = AdminInvite.find(params[:id])
          @current_user = current_user
        end

        def call
          check_possibility
          set_accepted
          cancel_other_invites
          become_admin
          notify_superadmins
          save
        rescue StandardError => e
          Result::Error.new(e)
        end

        private

        def check_possibility
          raise 'You are alredy an admin for some company!' if @current_user.company_id
          raise 'Admin invite already accepted!' if @admin_invite.accepted
          raise 'Admin invite already cancelled!' if @admin_invite.cancelled
        end

        def set_accepted
          @admin_invite.accepted = true
        end

        def cancel_other_invites
          AdminInvite.active.for_user(current_user)
                     .where.not(id: @admin_invite.id).each do |admin_invite|
            admin_invite.cancelled = true
            admin_invite.save!
          end
        end

        def become_admin
          @current_user.company_id = @admin_invite.company_id
          @current_user.superadmin = true
        end

        def notify_superadmins
          MandrillAdminInviteMailer.invite_accepted(@admin_invite.company, @current_user)
        end

        def save
          if @admin_invite.save && @current_user.save
            Result::Success.new('Admin invite accepted successfully.')
          else
            Result::Error.new(@admin_invite.errors.full_messages + @current_user.errors.full_messages)
          end
        end
      end
    end
  end
end
