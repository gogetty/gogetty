# frozen_string_literal: true

module Command
  module AccountSettings
    class Update
      attr_reader :user
      attr_reader :form

      def initialize(user, form)
        @user = user
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_user_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_user_attributes
        @user.attributes = {
          name:                    @form.name,
          remove_avatar:           @form.remove_avatar,
          surname:                 @form.surname,
          email:                   @form.email,
          company_name:            @form.company_name,
          title:                   @form.title,
          company_representative:  @form.company_representative,
          newsletter_subscription: @form.newsletter_subscription
        }
        @user.avatar = @form.avatar
      end

      def save
        if @user.save
          Result::Success.new('Data saved successfully.')
        else
          Result::Error.new(@user.errors.full_messages)
        end
      end
    end
  end
end
