# frozen_string_literal: true

module Command
  module CompanyLanguage
    class Create
      attr_reader :form
      attr_accessor :language

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(form.errors.full_messages) unless form.valid?
        create_language
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_language
        @language = ::CompanyLanguage.new(
          company_id: form.company_id,
          language_id: form.language_id,
          count: form.count
        )
      end

      def save
        if language.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(language.errors.full_messages)
        end
      end
    end
  end
end
