# frozen_string_literal: true

module Command
  module CompanyLanguage
    class Update
      attr_reader :form
      attr_reader :parameters
      attr_accessor :platform_admin

      def initialize(form, parameters, platform_admin = false)
        @form = form
        @parameters = parameters
        @platform_admin = platform_admin
      end

      def call
        update_form_attributes
        return form_errors if form.map(&:invalid?).any?

        results = update_languages

        Result::Success.new(success_text(results.count(&:success?)))
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def form_errors
        Result::Error.new(form.map { |f| f.errors.full_messages }.flatten.uniq)
      end

      def update_form_attributes
        form.each_with_index do |language, index|
          language.attributes = parameters[index]
        end
      end

      def update_languages
        results = []
        form.each do |f|
          language = ::CompanyLanguage.find(f.id)
          update_attributes = assign_update_attributes(f, language)
          if update_attributes.any?
            language.attributes = update_attributes
            results << save(language)
          end
        end
        results
      end

      def assign_update_attributes(f, language)
        update_attributes = {}
        update_attributes[:count] = f.count unless language.count == f.count
        update_attributes
      end

      # rubocop:disable SkipsModelValidations
      def save(language)
        if language.save
          language.company.touch unless @platform_admin
          Result::Success.new('Updated successfully.')
        else
          Result::Error.new(language.errors.full_messages)
        end
      end

      def success_text(updates_count)
        case updates_count
        when 0 then 'No record has been updated.'
        when 1 then '1 record successfully updated.'
        else "#{updates_count} records successfully updated."
        end
      end
    end
  end
end
