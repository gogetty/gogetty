# frozen_string_literal: true

module Command
  module CompanyLanguage
    class Destroy
      attr_accessor :language

      def initialize(language)
        @language = language
      end

      def call
        @language.destroy!
        Result::Success.new('Language deleted successfully.')
      rescue StandardError => e
        Result::Error.new(e)
      end
    end
  end
end
