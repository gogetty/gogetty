# frozen_string_literal: true

module Command
  module TellFriend
    class Create
      attr_reader :form
      attr_reader :user
      attr_accessor :tell_friend

      def initialize(form, user)
        @form = form
        @user = user
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_tell_friend
        send_mail
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_tell_friend
        @tell_friend = ::TellFriend.new(
          name: @form.name,
          email: @form.email,
          custom_message: @form.custom_message,
          full_name: @form.full_name
        )
        @tell_friend.user_id = user.id if user
      end

      def send_mail
        MandrillHelperMailer.tell_a_friend(@tell_friend)
      end

      def save
        if @tell_friend.save
          Result::Success.new('Mail sent successfully.')
        else
          Result::Error.new(@tell_friend.errors.full_messages)
        end
      end
    end
  end
end
