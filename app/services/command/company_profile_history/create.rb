# frozen_string_literal: true

module Command
  module CompanyProfileHistory
    class Create
      def initialize(company_profile)
        @company_profile = company_profile
      end

      def call
        @company_profile_history = ::CompanyProfileHistory.new(copy_attributes)
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      # rubocop:disable all
      def copy_attributes
        {
          company:                   @company_profile.company,
          now_director_woman_count:  @company_profile.now_director_woman_count,
          now_director_man_count:    @company_profile.now_director_man_count,
          goal_director_woman_count: @company_profile.goal_director_woman_count,
          goal_director_man_count:   @company_profile.goal_director_man_count,
          now_cxo_woman_count:       @company_profile.now_cxo_woman_count,
          now_cxo_man_count:         @company_profile.now_cxo_man_count,
          goal_cxo_woman_count:      @company_profile.goal_cxo_woman_count,
          goal_cxo_man_count:        @company_profile.goal_cxo_man_count,
          now_vp_woman_count:        @company_profile.now_vp_woman_count,
          now_vp_man_count:          @company_profile.now_vp_man_count,
          goal_vp_woman_count:       @company_profile.goal_vp_woman_count,
          goal_vp_man_count:         @company_profile.goal_vp_man_count,
          now_middle_woman_count:    @company_profile.now_middle_woman_count,
          now_middle_man_count:      @company_profile.now_middle_man_count,
          goal_middle_woman_count:   @company_profile.goal_middle_woman_count,
          goal_middle_man_count:     @company_profile.goal_middle_man_count,
          goal_year:                 @company_profile.goal_year,
          diversity_score:           @company_profile.diversity_score
        }
      end
      # rubocop:enable all

      def save
        if @company_profile_history.save
          Result::Success.new('Request send successfully.')
        else
          Result::Error.new(@company_request.errors.full_messages)
        end
      end
    end
  end
end
