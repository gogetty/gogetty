# frozen_string_literal: true

module Command
  module PartnerRequest
    class Create
      attr_reader :form
      attr_accessor :partner_request

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_partner_request
        send_email
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_partner_request
        @partner_request = ::PartnerRequest.new(
          name:         @form.name,
          surname:      @form.surname,
          email:        @form.email,
          company_name: @form.company_name,
          product:      @form.product
        )
      end

      def send_email
        MandrillHelperMailer.partner_request(@partner_request)
      end

      def save
        if @partner_request.save
          Result::Success.new('Your request has been sent.')
        else
          Result::Error.new(@partner_request.errors.full_messages)
        end
      end
    end
  end
end
