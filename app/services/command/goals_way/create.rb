# frozen_string_literal: true

module Command
  module GoalsWay
    class Create
      attr_reader :form
      attr_reader :goals_way

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        check_duplicates
        create_goals_way
        set_goals_way_order
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_duplicates
        raise 'Entry already exists!' if ::GoalsWay.find_by(
          description: @form.description,
          company_id:  @form.company_id
        )
      end

      def create_goals_way
        @goals_way = ::GoalsWay.new(
          description: @form.description,
          company_id:  @form.company_id
        )
      end

      def last_goals_way
        ::GoalsWay.where(company_id: @form.company_id).order(order: :desc).first
      end

      def set_goals_way_order
        order = if last_goals_way
                  last_goals_way.order + 1
                else
                  1
                end
        @goals_way.order = order
      end

      def save
        if @goals_way.save
          Result::Success.new('Added successfully.')
        else
          Result::Error.new(@goals_way.errors.full_messages)
        end
      end
    end
  end
end
