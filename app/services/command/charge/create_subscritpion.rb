# frozen_string_literal: true

module Command
  module Charge
    class CreateSubscritpion
      attr_reader :current_user
      attr_reader :session
      attr_reader :stripe_customer
      attr_reader :plan_name
      attr_accessor :billing
      attr_accessor :subscription

      def initialize(stripe_customer, current_user, session, plan_name)
        @current_user = current_user
        @session = session
        @stripe_customer = stripe_customer
        @billing = ::Billing.find(session[:current_billing_id])
        @plan_name = plan_name
      end

      def call
        billing_set ::Billing.states[:pending]
        create_subscription
        save_subscription_id
        Result::Success.new(subscription)
      rescue StandardError => e
        billing_set ::Billing.states[:error]
        Result::Error.new(e)
      end

      private

      def create_subscription
        @subscription = Stripe::Subscription.create(
          customer: stripe_customer.id,
          plan: plan_name,
          coupon: billing.coupon,
          tax_percent: billing.plan.tax
        )
      end

      def save_subscription_id
        billing.stripe_sub = subscription.id
        billing.save!
      end

      def billing_set(state)
        billing.state = state
        billing.save!
      end
    end
  end
end
