# frozen_string_literal: true

module Command
  module Charge
    class CreateCharge
      attr_reader :current_user
      attr_reader :session
      attr_reader :stripe_customer
      attr_accessor :billing
      attr_accessor :charge

      def initialize(stripe_customer, current_user, session)
        @current_user = current_user
        @session = session
        @stripe_customer = stripe_customer
        @billing = ::Billing.find(session[:current_billing_id])
      end

      def call
        billing_set ::Billing.states[:pending]
        charge_with_stripe
        billing.stripe_charge = charge.id
        billing_set ::Billing.states[:finalized]
        Result::Success.new(@charge)
      rescue StandardError => e
        Result::Error.new(e)
      end

      def billing_set(state)
        billing.state = state
        billing.save!
      end

      def charge_with_stripe
        @charge = Stripe::Charge.create(customer: stripe_customer.id,
                                        amount: billing.amount_in_cents,
                                        description: 'gogetty payment',
                                        currency: 'dkk')
      end
    end
  end
end
