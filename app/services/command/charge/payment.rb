# frozen_string_literal: true

module Command
  module Charge
    class Payment
      attr_reader :current_user
      attr_reader :session
      attr_accessor :customer
      attr_accessor :charge
      attr_accessor :params
      attr_accessor :billing
      attr_accessor :customer_result
      attr_accessor :charge_result
      attr_accessor :pay_once

      def initialize(current_user, session, params)
        @current_user = current_user
        @session = session
        @params = params
        @billing = ::Billing.find(session[:current_billing_id])
      end

      def call
        check_billing
        set_customer
        return @customer_result unless @customer_result.success?
        save_token
        charge_or_subscribe_customer
        return @charge_result unless @charge_result.success?
        set_active_plan if @pay_once
        Result::Success.new(charge)
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_billing
        error = 'Your payment has been already processed! '\
                'Check billings in user profile for more information.'
        payment_in_progres_error = "There's payment in progress. "\
                                   'Check billings section in account settings.'
        raise StandardError, error if (@billing.state == 'finalized') || (@billing.state == 'cancelled')
        raise StandardError, payment_in_progres_error if @billing.state == 'pending'
      end

      def set_customer
        @customer_result = ::Query::StripeCustomer::FindStripeCustomer.new(current_user).call
        unless @customer_result.success?
          @customer_result = ::Command::StripeCustomer::CreateStripeCustomer.new(current_user,
                                                                                 params).call
        end
        @customer = @customer_result.data if @customer_result.success?
      end

      def save_token
        billing.stripe_token = params[:stripeToken]
        billing.save!
      end

      def charge_or_subscribe_customer
        if billing.monthly && billing.plan.monthly_stripe_name
          subscribe_customer_monthly
        elsif !billing.monthly && billing.plan.annual_stripe_name
          subscribe_customer_annualy
        else
          raise 'There was an error while processing your request.'
          # @pay_once = true
          # charge_customer
        end
      end

      def subscribe_customer_monthly
        @charge_result = ::Command::Charge::CreateSubscritpion.new(customer,
                                                                   current_user,
                                                                   session,
                                                                   billing.plan.monthly_stripe_name).call
        @charge = @charge_result.data if @charge_result.success?
      end

      def subscribe_customer_annualy
        @charge_result = ::Command::Charge::CreateSubscritpion.new(customer,
                                                                   current_user,
                                                                   session,
                                                                   billing.plan.annual_stripe_name).call
        @charge = @charge_result.data if @charge_result.success?
      end

      def charge_customer
        @charge_result = ::Command::Charge::CreateCharge.new(customer, current_user, session).call
        @charge = @charge_result.data if @charge_result.success?
      end

      def set_active_plan
        ::Command::UserPlan::CreateOrExtend.new(current_user, billing).call
      end
    end
  end
end
