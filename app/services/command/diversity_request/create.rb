# frozen_string_literal: true

module Command
  module DiversityRequest
    class Create
      attr_reader :form
      attr_accessor :diversity_request

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_diversity_request
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_diversity_request
        @diversity_request = ::DiversityRequest.new(
          comment:        @form.comment,
          company_id:     @form.company_id,
          user_id:        @form.user_id
        )
      end

      def save
        if @diversity_request.save
          Result::Success.new('Request send successfully.')
        else
          Result::Error.new(@diversity_request.errors.full_messages)
        end
      end
    end
  end
end
