# frozen_string_literal: true

module Command
  module CompanySettings
    module Admins
      class Destroy
        attr_accessor :user
        attr_reader :current_user

        def initialize(params, current_user)
          @user = ::User.find(params[:id])
          @current_user = current_user
        end

        def call
          check_self
          check_last
          revoke_admin_status
          # XXX: notify user
          save
        rescue StandardError => e
          Result::Error.new(e)
        end

        private

        def check_self
          raise "You can't delete yourself!" if @user.id == @current_user.id
        end

        # rubocop:disable GuardClause
        def check_last
          if @user.superadmin && @user.company.admins.where(superadmin: true).count == 1
            raise "You can't delete last admin!"
          end
        end
        # rubocop:enable all

        def revoke_admin_status
          @user.attributes = {
            superadmin: false,
            company_id: nil
          }
        end

        def save
          if @user.save
            Result::Success.new('Admin deleted successfully.')
          else
            Result::Error.new(@user.errors.full_messages)
          end
        end
      end
    end
  end
end
