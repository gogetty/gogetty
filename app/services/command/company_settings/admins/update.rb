# frozen_string_literal: true

module Command
  module CompanySettings
    module Admins
      class Update
        attr_accessor :user
        attr_reader :form
        attr_reader :current_user

        def initialize(form, params, current_user)
          @form = form
          @user = ::User.find(params[:id])
          @current_user = current_user
        end

        def call
          check_change
          check_self
          check_last
          return Result::Error.new(@form.errors.full_messages) unless @form.valid?
          assign_admin_status
          # XXX: notify user
          save
        rescue StandardError => e
          Result::Error.new(e)
        end

        private

        def check_change
          raise 'Nothing changed.' if @form.superadmin == @user.superadmin
        end

        def check_self
          raise "You can't revoke superadmin status for yourself!" if @user.id == @current_user.id
        end

        # rubocop:disable GuardClause
        def check_last
          if !@form.superadmin && @user.company.admins.where(superadmin: true).count == 1
            raise "You can't revoke superadmin for the last superadmin!"
          end
        end
        # rubocop:enable all

        def assign_admin_status
          @user.attributes = {
            superadmin: @form.superadmin
          }
        end

        def save
          if @user.save
            Result::Success.new('Data saved successfully.')
          else
            Result::Error.new(@user.errors.full_messages)
          end
        end
      end
    end
  end
end
