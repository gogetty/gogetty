# frozen_string_literal: true

module Command
  module CompanySettings
    module AdminRequests
      class Update
        attr_accessor :admin_request
        attr_accessor :user

        def initialize(params)
          @admin_request = AdminRequest.find(params[:id])
          @user = ::User.find(@admin_request.user_id)
        end

        def call
          check_possibility
          set_accepted
          set_admin
          notify_requestee
          save
        rescue StandardError => e
          Result::Error.new(e)
        end

        private

        def check_possibility
          raise "User doesn't have an active subscription!" unless @user.pro?
          raise 'User already became admin of some company!' if @user.company_id
          raise 'Admin request already accepted!' if @admin_request.accepted
          raise 'Admin request already cancelled!' if @admin_request.cancelled
          raise 'Admin request already revoked!' if @admin_request.revoked
        end

        def set_accepted
          @admin_request.accepted = true
        end

        def set_admin
          @user.company_id = @admin_request.company_id
          @user.superadmin = false
        end

        def notify_requestee
          MandrillAdminRequestMailer.request_accepted(@admin_request.company,
                                                      @admin_request.user)
        end

        def save
          if @admin_request.save && @user.save
            Result::Success.new('Admin request accepted successfully.')
          else
            Result::Error.new(@admin_request.errors.full_messages + @user.errors.full_messages)
          end
        end
      end
    end
  end
end
