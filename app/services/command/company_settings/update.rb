# frozen_string_literal: true

module Command
  module CompanySettings
    class Update
      attr_accessor :company
      attr_accessor :company_profile
      attr_accessor :company_save
      attr_accessor :company_profile_save
      attr_reader :form
      attr_reader :current_user

      def initialize(company, form, current_user = nil)
        @company = company
        @company_profile = company.company_profile
        @form = form
        @current_user = current_user
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        assign_company_attributes
        assign_company_profile_attributes
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def assign_company_attributes
        @company.attributes = {
          remove_logo:        @form.remove_logo,
          name:               @form.name,
          industry_id:        @form.industry_id,
          website:            @form.website,
          country_id:         @form.country_id,
          city:               @form.city,
          founded_year:       @form.founded_year,
          owner_type_id:      @form.owner_type_id,
          stock_listed:       @form.stock_listed,
          cvr_number:         @form.cvr_number
        }
        @company.logo = @form.logo
      end

      def assign_company_profile_attributes
        @company_profile.attributes = {
          employees_count_id: @form.employees_count_id,
          always_visible: @form.always_visible
        }
      end

      def presave
        if current_user&.platform_admin?
          ::Company.no_touching do
            @company_profile_save = @company_profile.save
          end
          @company_save = @company.save(touch: false)
        else
          @company_save = @company.save
          @company_profile_save = @company_profile.save
        end
      end

      def save
        presave
        if @company_save && @company_profile_save
          Result::Success.new('Data saved successfully.')
        else
          Result::Error.new(@company.errors.full_messages + @company_profile.errors.full_messages)
        end
      end
    end
  end
end
