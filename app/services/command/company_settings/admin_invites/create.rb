# frozen_string_literal: true

module Command
  module CompanySettings
    module AdminInvites
      class Create
        attr_accessor :invite
        attr_reader :params
        attr_reader :form

        def initialize(form, params)
          @form = form
          @params = params
          @invite = AdminInvite.new
        end

        def call
          return Result::Error.new(@form.errors.full_messages) unless @form.valid?
          find_user_by_email
          check_if_not_admin
          check_invites
          create_invite
          notify_invitee
          save
        rescue StandardError => e
          Result::Error.new(e)
        end

        private

        def find_user_by_email
          @form.user_id = ::User.find_by(email: @form.email).id if @form.user_id.nil?
        rescue StandardError
          nil
        end

        def check_if_not_admin
          raise 'This person is already an admin for some company!' if @form.user_id &&
                                                                       ::User.find(@form.user_id)
                                                                             .company_id
        end

        def check_invites
          admin_invite = if @form.user_id
                           AdminInvite.active
                                      .find_by(user_id: @form.user_id,
                                               company_id: @form.company_id)
                         else
                           AdminInvite.active
                                      .find_by(email: @form.email,
                                               company_id: @form.company_id)
                         end
          raise 'This person is already invited to be admin of your company!' if admin_invite
        end

        def create_invite
          @invite.attributes = {
            user_id: @form.user_id,
            email: @form.email,
            company_id: @form.company_id
          }
        end

        def notify_invitee
          MandrillAdminInviteMailer.invite_new(@invite.company, @invite.email)
        end

        def save
          if @invite.save
            Result::Success.new('Admin invite created successfully.')
          else
            Result::Error.new(@invite.errors.full_messages)
          end
        end
      end
    end
  end
end
