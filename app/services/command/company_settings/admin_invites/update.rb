# frozen_string_literal: true

module Command
  module CompanySettings
    module AdminInvites
      class Update
        attr_accessor :admin_invite

        def initialize(params)
          @admin_invite = AdminInvite.find(params[:id])
        end

        def call
          check_possibility
          set_cancelled
          notify_invitee
          save
        rescue StandardError => e
          Result::Error.new(e)
        end

        private

        def check_possibility
          raise 'Admin invite already accepted!' if @admin_invite.accepted
          raise 'Admin invite already cancelled!' if @admin_invite.cancelled
        end

        def set_cancelled
          @admin_invite.cancelled = true
        end

        def notify_invitee
          MandrillAdminInviteMailer.invite_cancelled(@admin_invite.company, @admin_invite.email)
        end

        def save
          if @admin_invite.save
            Result::Success.new('Admin invite cancelled successfully.')
          else
            Result::Error.new(@admin_invite.errors.full_messages)
          end
        end
      end
    end
  end
end
