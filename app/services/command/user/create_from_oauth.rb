# frozen_string_literal: true

module Command
  module User
    class CreateFromOauth
      attr_reader :auth

      def initialize(auth)
        @auth = auth
      end

      def call
        user = create_user
        if user.persisted?
          Result::Success.new(user)
        else
          Result::Error.new(user)
        end
      end

      private

      # rubocop:disable AbcSize
      def create_user
        ::User.where(provider: @auth.provider, uid: @auth.uid).first_or_create do |user|
          user.email = @auth.info.email
          user.password = Devise.friendly_token[0, 20]
          user.name = @auth.info.first_name
          user.surname = @auth.info.last_name
          user.company_name = begin
                                @auth.extra.raw_info.positions.values[1][0].company.name
                              rescue StandardError
                                nil
                              end
          user.title = begin
                         @auth.extra.raw_info.positions.values[1][0].title
                       rescue StandardError
                         nil
                       end
          # user.image = @auth.info.image
          user.skip_confirmation!
        end
      end
      # rubocop:enable all
    end
  end
end
