# frozen_string_literal: true

module Command
  module User
    class CreateAdminService
      def call
        ::User.transaction do
          ::User.find_or_create_by!(email: Rails.application.secrets.admin_email) do |user|
            user.password = Rails.application.secrets.admin_password
            user.password_confirmation = Rails.application.secrets.admin_password
            user.confirm
            user.admin!
          end
        end
      end
    end
  end
end
