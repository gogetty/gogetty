# frozen_string_literal: true

module Command
  module Follow
    class Toggle
      attr_reader :form
      attr_accessor :follow

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        check_unique
        create_or_destroy_follow
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def check_unique
        @follow = ::Follow.find_by(
          user_id: @form.user_id,
          company_id: @form.company_id
        )
      end

      def create_or_destroy_follow
        if check_unique
          @follow.destroy
          Result::Success.new('Company unfollowed successfully.')
        else
          @follow = ::Follow.new(
            user_id: @form.user_id,
            company_id: @form.company_id
          )
          save
        end
      end

      def save
        if @follow.save
          Result::Success.new('Company followed succesfully.')
        else
          Result::Error.new(@follow.errors.full_messages)
        end
      end
    end
  end
end
