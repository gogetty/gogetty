# frozen_string_literal: true

module Command
  module SolutionRequest
    class Create
      attr_reader :form
      attr_accessor :solution_request

      def initialize(form)
        @form = form
      end

      def call
        return Result::Error.new(@form.errors.full_messages) unless @form.valid?
        create_solution_request
        send_email
        save
      rescue StandardError => e
        Result::Error.new(e)
      end

      private

      def create_solution_request
        @solution_request = ::SolutionRequest.new(
          company: @form.company,
          name: @form.name,
          user_id: @form.user_id
        )
      end

      def send_email
        MandrillHelperMailer.solution_request(@solution_request)
      end

      def save
        if @solution_request.save
          Result::Success.new('Your request has been sent.')
        else
          Result::Error.new(@solution_request.errors.full_messages)
        end
      end
    end
  end
end
