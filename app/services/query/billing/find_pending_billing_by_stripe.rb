# frozen_string_literal: true

module Query
  module Billing
    class FindPendingBillingByStripe
      attr_reader :stripe_event
      attr_accessor :billing_result

      def initialize(stripe_event)
        @stripe_event = stripe_event
      end

      def call
        find_billing
        @billing_result
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end

      private

      def find_billing
        if stripe_event.subscription_id
          billing = ::Billing.find_by(stripe_sub: stripe_event.subscription_id,
                                      state: ::Billing.states[:pending],
                                      user_id: stripe_event.user.id)
          unless billing
            @billing_result = ::Command::Result::Error.new('Billing not found.')
            return
          end
          @billing_result = ::Command::Result::Success.new(billing)
        else
          @billing_result = ::Command::Result::Error.new('Subscription id empty.')
        end
      end
    end
  end
end
