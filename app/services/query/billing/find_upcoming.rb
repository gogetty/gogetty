# frozen_string_literal: true

module Query
  module Billing
    class FindUpcoming
      attr_accessor :current_user

      def initialize(current_user)
        @current_user = current_user
      end

      def call
        find_invoice
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end

      private

      def find_invoice
        return ::Command::Result::Success.new(nil) unless current_user.stripe_customer_id
        subscription = current_user.plans_users.where(cancel_scheduled: false,
                                                      cancelled_on: nil).first
        return ::Command::Result::Success.new(nil) unless subscription
        upcoming_invoice = ::Stripe::Invoice.upcoming(customer: current_user.stripe_customer_id,
                                                      subscription: subscription.stripe_sub)
        ::Command::Result::Success.new(upcoming_invoice)
      end
    end
  end
end
