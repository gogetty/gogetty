# frozen_string_literal: true

module Query
  module StripeCustomer
    class FindStripeCustomer
      attr_reader :current_user

      def initialize(current_user)
        @current_user = current_user
      end

      def call
        return Result::Error.new(e) unless current_user.stripe_customer_id
        ::Command::Result::Success.new(Stripe::Customer.retrieve(current_user.stripe_customer_id.to_s))
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end
    end
  end
end
