# frozen_string_literal: true

module Query
  module Companies
    class ExportedCompanies
      def call # rubocop:disable Metrics/MethodLength
        exported_companies = ::Company.all
                                      .includes(:company_profile)
                                      .includes(:company_profile_histories)
                                      .includes(:industry)
                                      .includes(:country)
                                      .where('company_profiles.now_director_woman_count > 0 or
                                              company_profiles.now_director_man_count > 0 or
                                              company_profiles.goal_director_woman_count > 0 or
                                              company_profiles.goal_director_man_count > 0 or
                                              company_profiles.now_cxo_woman_count > 0 or
                                              company_profiles.now_cxo_man_count > 0 or
                                              company_profiles.goal_cxo_woman_count > 0 or
                                              company_profiles.goal_cxo_man_count > 0 or
                                              company_profiles.now_vp_woman_count > 0 or
                                              company_profiles.now_vp_man_count > 0 or
                                              company_profiles.goal_vp_woman_count > 0 or
                                              company_profiles.goal_vp_man_count > 0')
                                      .order('company_profiles.diversity_score DESC')
        ::Command::Result::Success.new(exported_companies)
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end
    end
  end
end
