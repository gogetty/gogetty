# frozen_string_literal: true

module Query
  module Charge
    class FindPlanCost
      attr_reader :plan
      attr_reader :monthly

      def initialize(plan, monthly)
        @plan = plan
        @monthly = monthly
      end

      def call
        if @monthly
          ::Command::Result::Success.new(@plan.monthly_cost)
        else
          ::Command::Result::Success.new(@plan.annual_cost)
        end
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end
    end
  end
end
