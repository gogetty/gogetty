# frozen_string_literal: true

module Query
  module Wall
    class Posts
      attr_reader :user
      attr_reader :posts_seen
      attr_reader :gogetty_profile_id

      def initialize(user, posts_seen, gogetty_id)
        @user = user
        @posts_seen = posts_seen
        @gogetty_profile_id = gogetty_id
      end

      def call
        posts = posts_feed

        ::Command::Result::Success.new(posts)
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end

      private

      def posts_feed
        ::Post.where('company_id IN (?)', user.follows.pluck(:company_id))
              .or(::Post.where(company_id: gogetty_profile_id, id: posts_seen))
              .order(created_at: :desc)
              .limit(5)
      end
    end
  end
end
