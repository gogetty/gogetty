# frozen_string_literal: true

module Query
  module Wall
    class Featured
      attr_accessor :featured

      def call
        calculate_highest
        calculate_lowest
        # @featured = @featured.decorate
        ::Command::Result::Success.new(@featured)
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end

      private

      def calculate_highest
        @featured = Company.joins(:company_profile)
                           .where.not(name: 'GoGetty IVS')
                           .where(company_profiles: {diversity_score: 25..50})
                           .order('RANDOM()')
                           .limit(3)
                           .decorate
      end

      def calculate_lowest
        @featured += Company.joins(:company_profile)
                            .where.not(name: 'GoGetty IVS')
                            .where(company_profiles: {diversity_score: 1...25})
                            .order('RANDOM()')
                            .decorate
                            .last(3)
      end
    end
  end
end
