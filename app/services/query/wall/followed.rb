# frozen_string_literal: true

module Query
  module Wall
    class Followed
      attr_reader :user

      def initialize(user)
        @user = user
      end

      def call
        followed = Company.where('id IN (?)', user.follows.select(:company_id)).decorate
        ::Command::Result::Success.new(followed)
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end
    end
  end
end
