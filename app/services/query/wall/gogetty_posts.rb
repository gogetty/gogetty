# frozen_string_literal: true

module Query
  module Wall
    class GogettyPosts
      attr_reader :posts_seen
      attr_reader :gogetty_profile_id

      def initialize(posts_seen, gogetty_id)
        @posts_seen = posts_seen
        @gogetty_profile_id = gogetty_id
      end

      def call
        posts = gogetty_profile_id ? gogetty_posts : []

        ::Command::Result::Success.new(posts)
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end

      private

      def gogetty_posts
        ::Post.where(company_id: gogetty_profile_id)
              .where.not(id: posts_seen)
              .order(created_at: :desc)
              .limit(5)
      end
    end
  end
end
