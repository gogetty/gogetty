# frozen_string_literal: true

module Query
  module CompanySearch
    class Find
      attr_reader :form
      attr_accessor :companies

      def initialize(form)
        @form = form
        @companies = ::Company.joins(:company_profile).all
      end

      def call
        return ::Command::Result::Error.new(@form.errors.full_messages) unless @form.valid?
        search_by_name
        search_by_diversity_score
        search_by_ethnicity_score
        search_by_employees_count
        search_by_location
        search_by_country
        search_by_industry
        sort
        ::Command::Result::Success.new(@companies)
      rescue StandardError => e
        ::Command::Result::Error.new(e)
      end

      private

      def search_by_name
        return unless @form.name != ''
        @companies = @companies.where('UNACCENT(LOWER(companies.name)) LIKE UNACCENT(?)',
                                      "%#{@form.name.downcase}%")
      end

      def search_by_diversity_score
        return unless @form.diversity_score_from.to_s != ''
        @companies =
          @companies.where('company_profiles.diversity_score IS NOT NULL')
                    .where('company_profiles.diversity_score >= ?', @form.diversity_score_from)
                    .where('company_profiles.diversity_score <= ?', @form.diversity_score_to)
      end

      def search_by_ethnicity_score
        return unless @form.ethnicity_score_from.to_s != ''
        @companies = @companies.where(
          'nationalities_count BETWEEN ? AND ?',
          @form.ethnicity_score_from,
          @form.ethnicity_score_to
        )
      end

      def search_by_location
        return unless @form.location.to_s != ''
        @companies = @companies.where('UNACCENT(LOWER(companies.city)) LIKE UNACCENT(?)',
                                      "%#{@form.location.downcase}%")
      end

      def search_by_employees_count
        return if @form.employees_count_id.blank? || @form.employees_count_id == '-1'
        @companies =
          @companies.where('company_profiles.employees_count_id = ?', @form.employees_count_id)
      end

      def search_by_country
        return if @form.country_id.blank? || @form.country_id == '-1'
        @companies = @companies.where(country_id: @form.country_id)
      end

      def search_by_industry
        return if @form.industry_id.blank? || @form.industry_id == '-1'
        @companies = @companies.where(industry_id: @form.industry_id)
      end

      def sort
        @companies = @companies.order(order_parameter)
      end

      def order_parameter
        parameters = [
          '-company_profiles.diversity_score',
          'company_profiles.diversity_score',
          '-nationalities_count',
          'nationalities_count',
          'unaccent(companies.name) DESC',
          'unaccent(companies.name)'
        ]
        parameters[@form.sort_id - 1]
      end
    end
  end
end
