# frozen_string_literal: true

require 'mandrill'

class MandrillHelperMailer < MandrillMailer
  def self.tell_a_friend(form)
    @template_name = 'mandrill-tell_a_friend'
    @message =
      {
        'subject': 'GoGetty invite from friend',
        'to': [{'type': 'to', 'email': form.email, 'name': form.name}],
        'global_merge_vars': [
          {'name': 'invitee_name', 'content': form.name},
          {'name': 'user_name', 'content': form.full_name},
          {'name': 'cta_url', 'content': url_helpers.root_url}
        ],
        'tags': ['admin-request new']
      }
    send_message
  end

  def self.solution_request(solution_request)
    @template_name = 'mandrill-solution_request'
    @message =
      {
        'subject': 'New solution request',
        'to': [{'type': 'to',
                'email': 'ewelina@gogetty.co',
                'name': 'GoGetty Administration'}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': solution_request.user.full_name},
          {'name': 'user_email', 'content': solution_request.user.email},
          {'name': 'course_name', 'content': solution_request.name},
          {'name': 'company', 'content': solution_request.company},
          {'name': 'cta_url', 'content': url_helpers.root_url}
        ],
        'tags': ['admin-request new']
      }
    send_message
  end

  def self.partner_request(partner_request)
    @template_name = 'mandrill-partner_request'
    @message =
      {
        'subject': 'New partner request',
        'to': [{'type': 'to',
                'email': 'ewelina@gogetty.co',
                'name': 'GoGetty Administration'}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': partner_request.name},
          {'name': 'user_surname', 'content': partner_request.surname},
          {'name': 'user_email', 'content': partner_request.email},
          {'name': 'user_company_name', 'content': partner_request.company_name},
          {'name': 'user_product', 'content': partner_request.product},
          {'name': 'cta_url', 'content': url_helpers.root_url}
        ],
        'tags': ['admin-request new']
      }
    send_message
  end
end
