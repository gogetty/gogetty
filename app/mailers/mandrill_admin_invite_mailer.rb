# frozen_string_literal: true

require 'mandrill'

class MandrillAdminInviteMailer < MandrillMailer
  def self.invite_new(company, email)
    user_name = begin
                  User.find_by(email: email).full_name
                rescue StandardError
                  ''
                end
    @template_name = 'mandrill-new_become_admin_invite'
    @message =
      {
        'subject': 'GoGetty admin invite',
        'to': [{'type': 'to', 'email': email, 'name': user_name}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': user_name},
          {'name': 'company_name', 'content': company.name},
          {'name': 'cta_url', 'content': url_helpers.user_logged_account_settings_admin_invites_url}
        ],
        'tags': ['admin-invite new']
      }
    send_message
  end

  def self.invite_accepted(company, user)
    company.active_admins.superadmin.each do |company_superadmin|
      @template_name = 'mandrill-become_admin_invite_accepted'
      @message =
        {
          'subject': 'GoGetty admin invite accepted',
          'to': [{'type': 'to',
                  'email': company_superadmin.email,
                  'name': company_superadmin.full_name}],
          'global_merge_vars': [
            {'name': 'user_name', 'content': company_superadmin.full_name},
            {'name': 'invitee_name', 'content': user.full_name_someone},
            {'name': 'company_name', 'content': company.name},
            {'name': 'cta_url', 'content': url_helpers.user_logged_company_settings_admins_url}
          ],
          'tags': ['admin-invite accepted']
        }
      send_message
    end
  end

  def self.invite_cancelled(company, email)
    user_name = begin
                  User.find_by(email: email).full_name
                rescue StandardError
                  ''
                end
    @template_name = 'mandrill-become_admin_invite_cancelled'
    @message =
      {
        'subject': 'GoGetty admin invite cancelled',
        'to': [{'type': 'to', 'email': email, 'name': user_name}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': user_name},
          {'name': 'company_name', 'content': company.name},
          {'name': 'cta_url', 'content': url_helpers.root_url} # XXX: url_helpers.contact_us},
        ],
        'tags': ['admin-invite cancelled']
      }
    send_message
  end
end
