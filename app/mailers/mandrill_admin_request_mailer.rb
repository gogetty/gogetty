# frozen_string_literal: true

require 'mandrill'

class MandrillAdminRequestMailer < MandrillMailer
  def self.request_new(company, user)
    company.active_admins.superadmin.each do |company_superadmin|
      @template_name = 'mandrill-new_become_admin_request'
      @message =
        {
          'subject': 'GoGetty become admin request',
          'to': [{'type': 'to',
                  'email': company_superadmin.email,
                  'name': company_superadmin.full_name}],
          'global_merge_vars': [
            {'name': 'user_name', 'content': company_superadmin.full_name},
            {'name': 'requestee_name', 'content': user.full_name_someone},
            {'name': 'company_name', 'content': company.name},
            {'name': 'cta_url', 'content': url_helpers.user_logged_company_settings_admins_url}
          ],
          'tags': ['admin-request new']
        }
      send_message
    end
  end

  def self.request_revoked(company, user)
    @template_name = 'mandrill-become_admin_request_revoked'
    @message =
      {
        'subject': 'GoGetty become admin request revoked',
        'to': [{'type': 'to', 'email': user.email, 'name': user.full_name}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': user.full_name},
          {'name': 'company_name', 'content': company.name},
          {'name': 'cta_url', 'content': url_helpers.root_url} # XXX: url_helpers.contact_us},
        ],
        'tags': ['admin-request revoked']
      }
    send_message
  end

  def self.request_accepted(company, user)
    @template_name = 'mandrill-become_admin_request_accepted'
    @message =
      {
        'subject': 'GoGetty become admin request accepted',
        'to': [{'type': 'to', 'email': user.email, 'name': user.full_name}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': user.full_name},
          {'name': 'company_name', 'content': company.name},
          {'name': 'cta_url', 'content': url_helpers.user_logged_company_url(company.id)}
        ],
        'tags': ['admin-request accepted']
      }
    send_message
  end
end
