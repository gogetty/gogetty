# frozen_string_literal: true

include PoroUrlHelper

class MandrillMailer
  def self.send_message
    return true if Rails.env.test?
    mandrill = Mandrill::API.new Rails.application.secrets.mandrill_api_key
    result = mandrill.messages.send_template(
      @template_name,
      [],
      message_to_send,
      true
    )
    ApplicationController.logger.debug "Mandrill: #{result.inspect}"
  end

  def self.basic_message
    {
      'from_email': 'no-reply@' + Rails.application.secrets.mail_domain_name,
      'from_name': 'GoGetty',
      'headers': {'Reply-To': 'welcome@gogetty.co'},
      'global_merge_vars': [
        {'name': 'update_profile', 'content': url_helpers.user_logged_account_settings_url},
        {'name': 'unsub', 'content': url_helpers.user_logged_account_settings_notifications_url}
      ]
    }
  end

  def self.message_to_send
    # COMMENTED CODE CAN BE USED TO REDIRECT EMAILS FROM STAGING
    # if Rails.env.production?
    basic_message.deep_merge(@message)
    # else
    # basic_message.deep_merge(@message)
    # .deep_merge('to': [{'email': Rails.application.secrets.forward_intercepted_email_to}])
    # end
  end
end
