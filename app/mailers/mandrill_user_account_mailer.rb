# frozen_string_literal: true

require 'mandrill'

class MandrillUserAccountMailer < MandrillMailer
  def self.confirm_email(user)
    @template_name = 'mandrill-confirm_your_email'
    @message =
      {
        'subject': 'GoGetty confirm your e-mail address',
        'to': [{'type': 'to',
                'email': user.email,
                'name': user.full_name}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': user.full_name},
          {
            'name': 'cta_url',
            'content': url_helpers.user_confirmation_url(confirmation_token: user.confirmation_token)
          }
        ],
        'tags': ['admin-request new']
      }
    send_message
  end

  def self.forgot_password(user, token)
    @template_name = 'mandrill-forgot_password'
    @message =
      {
        'subject': 'GoGetty forgot password',
        'to': [{'type': 'to',
                'email': user.email,
                'name': user.full_name}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': user.full_name},
          {
            'name': 'cta_url',
            'content': url_helpers.edit_user_password_url(reset_password_token: token)
          }
        ],
        'tags': ['admin-request new']
      }
    send_message
  end

  def self.welcome_email(user)
    @template_name = 'mandrill-welcome_template'
    @message =
      {
        'subject': 'Welcome to GoGetty',
        'to': [{'type': 'to',
                'email': user.email,
                'name': user.full_name}],
        'global_merge_vars': [
          {'name': 'user_name', 'content': user.full_name}
        ],
        'tags': ['admin-request new']
      }
    send_message
  end
end
