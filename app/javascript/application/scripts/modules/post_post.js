$(document).ready(function() {
  bind_post_comment_section();
});

function refresh_post_parent_section(result) {
  $("#" + result.currentTarget.parentElement.id).html(
    result.originalEvent.detail[2].response
  );
  bind_post_comment_section();
}

function refresh_post_parent_section_deleted(result) {
  $("#" + result.currentTarget.parentElement.parentElement.parentElement.parentElement.parentElement.id).html(
    result.originalEvent.detail[2].response
  );
  bind_post_comment_section();
}

function bind_post_comment_section() {
  $(".user_logged_like_form")
    .on("ajax:success", function(result) {
      refresh_post_parent_section(result);
    })
    .on("ajax:error", function(result) {
      alert(result.originalEvent.detail[0].error);
    });
  $(".user_logged_comment_form")
    .on("ajax:success", function(result) {
      refresh_post_parent_section(result);
    })
    .on("ajax:error", function(result) {
      alert(result.originalEvent.detail[0].error);
    });
  $(".delete-comment-link")
    .on("ajax:success", function(result) {
      refresh_post_parent_section_deleted(result);
    })
    .on("ajax:error", function(result) {
      alert(result.originalEvent.detail[0].error);
    });
}

// show/hide post content
$(document).ready(function() {
  $(".post__actions .paragraph-link").click(function() {
    var postId = $(this).attr("data-id-post");
    $("#" + postId + "-short").toggleClass("hidden");
    $("#" + postId + "-long").toggleClass("hidden");
    $("#" + postId + "-show-more").toggleClass("hidden");
    $("#" + postId + "-show-less").toggleClass("hidden");
  });
});

$(document).ready(function() {
  $('#reset-image-field').on('click', function(event){
    event.preventDefault();
    $('#post_photo')[0].value = "";
  });
  $('#reset-video-field').on('click', function(event){
    event.preventDefault();
    $('#post_video')[0].value = "";
  });
});
