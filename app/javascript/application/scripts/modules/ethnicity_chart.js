import Chart from "chart.js";

const ctx = document.getElementById("ethnicityChart");
const nationalities = $(".nationalities_list_temp").data("nationalities");

if (nationalities) {
  var nationalitiesNames = [];
  nationalities.forEach(function(element) {
    nationalitiesNames.push(element.name);
  });

  var nationalitiesCount = [];
  nationalities.forEach(function(element) {
    nationalitiesCount.push(element.count);
  });
}

const data = {
  labels: nationalitiesNames,
  datasets: [
    {
      data: nationalitiesCount,
      backgroundColor: [
        "#F44336",
        "#9C27B0",
        "#FFCE56",
        "#03A9F4",
        "#009688",
        "#8BC34A",
        "#FFEB3B",
        "#231F20"
      ],
      hoverBackgroundColor: [
        "#F44336",
        "#9C27B0",
        "#FFCE56",
        "#03A9F4",
        "#009688",
        "#8BC34A",
        "#FFEB3B",
        "#231F20"
      ]
    }
  ]
};

const options = {
  cutoutPercentage: 88,
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  tooltips: {
    backgroundColor: "#000",
    fontSize: 16,
    caretSize: 0,
    cornerRadius: 3,
    borderWidth: 0,
    xPadding: 10,
    yPadding: 10,
    borderColor: "#000",
    callbacks: {
      label: function(tooltipItem, data) {
        const value = data["datasets"][0]["data"][tooltipItem["index"]];
        const label = data["labels"][tooltipItem["index"]];

        return "  " + value + "  " + label;
      }
    }
  }
};

if (ctx) {
  const ethnicityChart = new Chart(ctx, {
    type: "doughnut",
    data: data,
    options: options
  });

  ctx.style.position = "relative";
}

// tiles nationalities dropdown
$(document).ready(function() {
  $("#dropdown-other-nationalities").click(function() {
    $("#nationalities-arrow-down").toggleClass("hidden");
    $("#nationalities-arrow-up").toggleClass("hidden");
    $("#nationalities-dropdown").toggleClass("hidden");
  });
});

// tiles languages dropdown
$(document).ready(function() {
  $("#dropdown-other-languages").click(function() {
    $("#languages-arrow-down").toggleClass("hidden");
    $("#languages-arrow-up").toggleClass("hidden");
    $("#languages-dropdown").toggleClass("hidden");
  });
});

$(document).ready(function() {
  $("#container-ethnicity-chart").hover(
    function() {
      $("#upgrade-tooltip").removeClass("hidden-soft");
    },
    function() {
      $("#upgrade-tooltip").addClass("hidden-soft");
    }
  );
  $("#container-diversity-chart").hover(
    function() {
      $("#upgrade-tooltip-gender").removeClass("hidden-soft");
    },
    function() {
      $("#upgrade-tooltip-gender").addClass("hidden-soft");
    }
  );
});
