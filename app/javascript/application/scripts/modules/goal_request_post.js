$(document).ready(function(){
  $('#user_logged_goal_request_form').on('ajax:success', function(result){
    $('#goal_request').modal('hide');
    $('#request_success').modal('show');
  }).on('ajax:error',function(result){
    alert(result.originalEvent.detail[0].error);
  });
});
