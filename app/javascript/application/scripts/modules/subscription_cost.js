$(document).ready(function() {
  var switchButton = $("#switch-button").attr("aria-pressed");
  if (switchButton === true) {
    $("#subscription_form .annual_cost").hide();
  } else {
    $("#subscription_form .monthly_cost").hide();
  }

  var switchButton = document.getElementById("switch-button");

  if (switchButton) {
    switchButton.addEventListener("click", function() {
      var switchButtonState = document
        .getElementById("switch-button")
        .getAttribute("aria-pressed");
      var annualHiddenInput = document.getElementById("subscription_annual");

      if (switchButtonState === "false") {
        annualHiddenInput.checked = true;
        $("#subscription_form .annual_cost").show();
        $("#subscription_form .monthly_cost").hide();
      } else {
        annualHiddenInput.checked = false;
        $("#subscription_form .annual_cost").hide();
        $("#subscription_form .monthly_cost").show();
      }
    });
  }
  $(window).bind("pageshow", function() {
    var subscriptionAnnual = document.getElementById("subscription_annual");
    if (subscriptionAnnual) {
      subscriptionAnnual.checked = true;
    }
  });
});
