$(document).ready(function(){
  $('#user_logged_policy_request_form').on('ajax:success', function(result){
    $('#policy_request').modal('hide');
    $('#request_success').modal('show');
  }).on('ajax:error',function(result){
    alert(result.originalEvent.detail[0].error);
  });
});
