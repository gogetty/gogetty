$('a[data-tab="chart"]').on("shown.bs.tab", function() {
  $("#disabled-diversity-chart").toggleClass("hidden");
  $("#disabled-ethnicity-chart").toggleClass("hidden");
  $("#chart-diversity-active").toggleClass("hidden-soft");
  $("#chart-ethnicity-active").toggleClass("hidden-soft");
});

$('a[data-tab="plan"]').on("shown.bs.tab", function() {
  $("#plan-diversity-active").toggleClass("hidden-soft");
  $("#plan-ethnicity-active").toggleClass("hidden-soft");
});

$("#gender-tab").on("click", function() {
  $("#plan-gender-tab").tab("show");
});

$("#ethnicity-tab").on("click", function() {
  $("#plan-ethnicity-tab").tab("show");
});

$("#plan-ethnicity-tab").on("click", function() {
  $("#ethnicity-tab").tab("show");
});

$("#plan-gender-tab").on("click", function() {
  $("#gender-tab").tab("show");
});

$('a[data-tab="form"]').on("shown.bs.tab", function() {
  $("#languages-active").toggleClass("hidden-soft");
  $("#nationalities-active").toggleClass("hidden-soft");
});
