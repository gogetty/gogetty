$(document).ready(function(){
  $('.cancel-subscription-btn').on('click', function(result){
    $('#cancel_subscription_btn').attr('href', $(this).data('link'));
    $('#end_date').html($(this).data('enddate'));
  });
  $('#dont_cancel_subscription_btn').on('click', function(result){
    $('#cancel_subscription').modal('hide');
  });
});
