$(document).ready(function(){
  bind_checkbox_change();
  bind_form_post();
});

function bind_checkbox_change() {
  $("#form_company_become_admin_acceptance").change(function() {
    if (this.checked) {
      if ($('#form_company_become_admin_acceptance_null').length != 0) {
        $('#form_company_become_admin_acceptance_null').remove();
      }
    }
  });
}

function bind_form_post() {
  $('#become_admin_form').on('submit', function(result) {
    if ($('#form_company_become_admin_acceptance').is(':checked')) {
    } else {
      if ($('#form_company_become_admin_acceptance_null').length == 0) {
        $('#become_admin .modal-body').append("<p id='form_company_become_admin_acceptance_null' class='text-danger'>You must be company representative to become an admin!</p>")
      }
      return false;
    }
  });
}
