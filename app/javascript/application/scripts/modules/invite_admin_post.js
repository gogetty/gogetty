$(document).ready(function(){
  $('#user_logged_company_settings_admin_invite_form').on('ajax:success', function(result){
    $('#add_admin').modal('hide');
    $('#invite_admin_success').modal('show');
  }).on('ajax:error',function(result){
    alert(result.originalEvent.detail[0].error);
  });
});
