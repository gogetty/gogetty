$(document).ready(function(){
  $('#user_logged_company_requests_form').on('ajax:success', function(result){
    $('#no_companies_found').modal('hide');
    $('#company_request_success').modal('show');
  }).on('ajax:error',function(result){
    alert(result.originalEvent.detail[0].error);
  });
});
