$('a.animateScroll').click(function() {
  var hrefAbsolute = this.href;
  var hrefRelative = hrefAbsolute.substr(hrefAbsolute.indexOf('#'));
  $("html, body").animate({
    scrollTop: $('' + hrefRelative).offset().top
  }, 800);

    return false;
});
