import ScrollMagic from "scrollmagic";

var controller = new ScrollMagic.Controller();
var alertContainer = document.getElementById("alert-container");

if (alertContainer) {
  new ScrollMagic.Scene({
    offset: 50,
    triggerHook: 0
  })
    .setClassToggle(alertContainer, "hidden")
    .addTo(controller)
    .reverse(false)
}