$(document).ready(function(){
  bind_search_form();
});

function refresh_search_list_container(result) {
  $('#search_list_container').html(result.originalEvent.detail[2].response);
  score_graph_build();
}

function bind_search_form_elements() {
  $('#full_company_search_form input#form_company_search_new_name').keyup(function(){
    let $el = $(this);
    post_search_form();
    $('header input#form_company_search_new_name').val($el.val());
  });
  $('#full_company_search_form #form_company_search_new_sort_id').change(function(){
    post_search_form();
  });
  $('#full_company_search_form #form_company_search_new_industry_id').change(function(){
    post_search_form();
  });
  $('#full_company_search_form #form_company_search_new_employees_count_id').change(function(){
    post_search_form();
  });
  $('#full_company_search_form #form_company_search_new_location').keyup(function(){
    post_search_form();
  });
  $('#full_company_search_form #form_company_search_new_country_id').change(function(){
    post_search_form();
  });
  $('#full_company_search_form #form_company_search_new_diversity_score').change(function(){
    post_search_form();
  });
}

var wto;

function post_search_form() {
  clearTimeout(wto);
  wto = setTimeout(function() {
    $.post('create_company_searches.json', $('#full_company_search_form').serialize(), function(data) {
      $('#search_list_container').html(data);
      score_graph_build();
    })
  }, 600);
}

function bind_search_form() {
  bind_search_letters();
  bind_search_form_elements();
  $('#full_company_search_form').on('ajax:success', function(result){
    refresh_search_list_container(result)
  }).on('ajax:error',function(result){
    alert(result.originalEvent.detail[0].error);
  });
}

function bind_search_letters() {
  $('.search .search_letters input').click(function(){
    $('.search .search_letters input').removeClass('btn-link--current');
    $(this).addClass('btn-link--current');
    $('#form_company_search_new_alphabet_letter').val($(this).val());
  });
}

function score_graph_build() {
  $('.score-graph').each(function(){
      let $el = $(this);
      let value = $el.find('.score-graph__graph').data('score');

      if(value < 0){
          value = 0;
      }

      if (value > 50){
          value = 50;
      }

      let $progress = $el.find('.score-graph__progress');
      let stroke_max = $progress.attr('stroke-dashoffset');
      $progress.attr('stroke-dashoffset', (50-value)/50*stroke_max);

      if (isInteger(value)){
          $el.find('.score-graph__value').text(value+'%');
      } else {
          $el.find('.score-graph__value').text('0%');
      }
  });
}

function isInteger(num) {
  return (num ^ 0) === num;
}
