$(function() {
  $("#slider-range-0-50").slider({
    range: true,
    min: 0,
    max: 50,
    values: [
      $("#form_company_search_new_diversity_score_from").val(),
      $("#form_company_search_new_diversity_score_to").val()
    ],
    slide: function(event, ui) {
      $("#form_company_search_new_diversity_score").val(
        ui.values[0] + " - " + ui.values[1]
      );
      $("#form_company_search_new_diversity_score_from").val(ui.values[0]);
      $("#form_company_search_new_diversity_score_to").val(ui.values[1]);
      $("#form_company_search_new_diversity_score").trigger("change");
    }
  });
  $("#form_company_search_new_diversity_score").val(
    "" +
      $("#slider-range-0-50").slider("values", 0) +
      " - " +
      $("#slider-range-0-50").slider("values", 1)
  );
});

$(function () {
  $("#slider-range-0-50-2").slider({
    range: true,
    min: 0,
    max: 50,
    values: [
      $("#form_company_search_new_ethnicity_score_from").val(),
      $("#form_company_search_new_ethnicity_score_to").val()
    ],
    slide: function (event, ui) {
      $("#form_company_search_new_ethnicity_score").val(
        ui.values[0] + " - " + ui.values[1]
      );
      $("#form_company_search_new_ethnicity_score_from").val(ui.values[0]);
      $("#form_company_search_new_ethnicity_score_to").val(ui.values[1]);
      $("#form_company_search_new_ethnicity_score").trigger("change");
    }
  });
  $("#form_company_search_new_ethnicity_score").val(
    "" +
    $("#slider-range-0-50-2").slider("values", 0) +
    " - " +
    $("#slider-range-0-50-2").slider("values", 1)
  );
});