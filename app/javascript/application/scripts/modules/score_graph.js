score_graph_build();

function score_graph_build() {
  $('.score-graph').each(function(){
      let $el = $(this);
      let value = $el.find('.score-graph__graph').data('score');
      let small_value_women = $el.find('.score-graph-small__graph').data('women');
      let small_value_men = $el.find('.score-graph-small__graph').data('men');

      if(value < 0){
          value = 0;
      }

      if (value > 50){
          value = 50;
      }

      let $progress = $el.find('.score-graph__progress');
      let stroke_max = $progress.attr('stroke-dashoffset');
      $progress.attr('stroke-dashoffset', (50-value)/50*stroke_max);

      let $progress_small = $el.find('.score-graph__progress--small');
      let stroke_max_small = $progress_small.attr('stroke-dashoffset');
      let small_value = small_value_women + small_value_men
      $progress_small.attr('stroke-dashoffset', small_value_men/small_value*stroke_max_small);

      if (isInteger(value)){
          $el.find('.score-graph__value').text(value+'%');
      } else {
          $el.find('.score-graph__value').text('0%');
      }

      $el.find('.score-graph-small__value--women').text(small_value_women);
      $el.find('.score-graph-small__value--men').text(small_value_men);
  });
}

function isInteger(num) {
  return (num ^ 0) === num;
}
