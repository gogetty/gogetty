$(document).ready(function() {
  $(".copyBtn").on("click", function(event) {
    var button = this;
    copyInputValue(button.id);
    var spanId = "span-" + button.id;
    $("#" + spanId).fadeIn("fast", function() {
      $("#" + spanId)
        .delay(2000)
        .fadeOut();
    });
  });
});

function copyInputValue(inputId) {
  /* Get the text field */
  var copyText = document.getElementById(inputId);
  /* Select the text field */
  copyText.select();
  /* Copy the text inside the text field */
  document.execCommand("copy");
}
