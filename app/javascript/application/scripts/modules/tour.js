$(document).ready(function() {
    let tour = new Tour({
        backdrop: true,
        smartPlacement: true,
        steps: [
            {
                path: '/user_logged/walls',
                element: ".wall__container_summary",
                title: "Profile summary",
                content: "This is where your profile picture and personal profile summary is displayed. You can edit it at any time by choosing ‘My Profile’, which you find by clicking on the following icon (or your profile picture) in the upper right corner (in the menu).<br>" +
                "<center><img src='https://s3.eu-central-1.amazonaws.com/gogetty/support/for_clients.svg' height='90' width='90'/></center>",
                placement: 'right',
                reflex: true
            },
            {
                path: '/user_logged/walls',
                element: ".wall__container_featured",
                title: "Featured companies",
                content: "We’ve made it easy for you to begin your GoGetty journey. Feel free to follow any of the Featured Companies. <br>" +
                "They change randomly. The upper raw shows companies with the highest GoGetty diversity score. The bottom raw shows those with the lowest score. <br>" +
                "Please bear in mind that some companies have not yet updated their GoGetty company profiles and hence have lower scores. Give them a chance too! 😊",
                placement: 'right',
                reflex: true
            },
            {
                path: '/user_logged/walls',
                element: ".wall__container_followed",
                title: "Followed companies",
                content: "This is where you find companies that you follow.<br>" +
                "As with Featured Companies, you can always click on any of the companies and learn more about their diversity initiatives, as well as ask them for more information. We’ll explain how!",
                placement: 'left',
                reflex: true
            },
            {
                path: '/user_logged/walls',
                element: ".wall__container_stream",
                title: "Posts stream",
                content: "This is where you will see posts and updates, related to diversity and inclusion, from the Companies you follow.",
                placement: 'top',
                reflex: true
            },
            {
                path: '/user_logged/walls',
                element: "#home-nav-link",
                title: "Navigation",
                content: "This is the navigation bar. Depending on your subscription you will have access to different pages. <br>" +
                "E.g.: Home is your wall, as you know it from other SoMe platforms – this is where you find the companies you follow, featured companies and updates from companies of your interest.",
                placement: 'bottom',
                reflex: true,
                backdrop: false,
            },
            {
                path: '/user_logged/walls',
                element: "#navbar-button",
                title: "Navigation",
                content: "This is the navigation bar. Depending on your subscription you will have access to different pages. <br>" +
                "E.g.: Home is your wall, as you know it from other SoMe platforms – this is where you find the companies you follow, featured companies and updates from companies of your interest.",
                placement: 'bottom',
                reflex: true,
                backdrop: false,
            },
            {
                path: '/user_logged/walls',
                element: "#avatar_dropdown_menu",
                title: "Drop down menu",
                content: "Click here to get access to settings and sign-out. <br>" +
                "<br>" +
                "Depending on your subscription you will have access to Your Profile. <br>" +
                "<br>" +
                "If you are also an administrator of a company, you will be able to access your Company profile from here as well.",
                placement: 'bottom',
                reflex: true,
                backdrop: false
            },
            {
                path: '/company_searches',
                element: "#full_company_search_form .form_company_search_new_name",
                title: "Company search",
                content: "This is the search bar that will take you to any company of your interest registered on GoGetty. <br>" +
                "<br>" +
                "If you have no luck finding the company of your interest, we give you an absolutely unique possibility of requesting that company to be ADDED to the GoGetty list.<br>" +
                "<br>" +
                "Unless, of course, you’re the official representative of that company – then we invite you to add it yourself.",
                placement: 'bottom',
                reflex: true
            },
            {
                path: '/company_searches',
                element: "#full_company_search_form .add_company_button",
                title: "Company search",
                content: "As the official representative of a company, please feel free to add your company to the GoGetty register – i.e. if you couldn’t find it using the Search function. <br>" +
                "<br>" +
                "Don’t worry! We will notify you if you should try to add an already existing company by mistake.",
                placement: 'top',
                reflex: true
            },
            {
                path: '/user_logged/companies/-1',
                element: ".container-summary",
                title: "Company summary",
                content: "This is where you find basic financial information about the company of your interest. <br>" +
                "<br>" +
                "If you are administering this company’s profile – you can edit this information at any time by clicking the ‘pencil’ button in the upper right corner.",
                placement: 'right',
                reflex: true
            },
            {
                path: '/user_logged/companies/-1',
                element: ".container-diversity-score-score",
                title: "Company Diversity Score",
                content: "This is the very heart of GoGetty – the Diversity Score and Dashboard.<br>" +
                "<br>" +
                "If any data is missing or if you have questions to the displayed data – please feel free to ‘REQUEST MORE INFO’  - simply by clicking on the ‘?’ <br>" +
                "<br>" +
                "The large, colorful circle to the left is an average of the numbers to the right.<br>",
                placement: 'right',
                reflex: true
            },
            {
                path: '/user_logged/companies/-1',
                element: ".container-diversity-score-diversity",
                title: "Company Diversity Score",
                content: "The Diversity Score to the left comes from percentages of women in company's top management levels that you see in this section<br>" +
                "<br>" +
                " - Status as it is today (e.g. Status in 2017), as well as<br>" +
                " - Goals for the future (e.g.: Goals for 2018), according to company's’ own plans. <br>" +
                "<br>" +
                "On GoGetty we focus on the ratio between men and women on the following three top management levels:<br>" +
                "<br>" +
                " - Board of Directors<br>" +
                " - CxO level<br>" +
                " - VP level<br>" +
                "<br>" +
                "An ideal GoGetty diversity score equals 50%. A GoGetty diversity score of e.g. 30% means that 30% of the above three top management positions are held and/or are planned to be held by women. <br>" +
                "<br>" +
                "Hence the more ambitious goals companies have, the higher their GoGetty diversity score.<br>" +
                "<br>" +
                "As a Company Administrator, please make sure that all data here is updated. This is your image and employer brand! This is also your key to more diverse talents for your organization.",
                placement: 'left',
                reflex: true
            },
            {
                path: '/user_logged/companies/-1',
                element: ".container-policies",
                title: "Company Diversity & Inclusion Policies",
                content: "The second crucial part of the GoGetty diversity dashboard is a list of company’s current Diversity & Inclusion Policies.<br>" +
                "<br>" +
                "What do they do to make sure that diversity and equality is sustained throughout the whole organization?<br>" +
                "<br>" +
                "Also here, we invite you to ‘Request more info’.<br>" +
                "<br>" +
                "As a Company Administrator, please make sure to add relevant policies here. We’ve made it easy for you. There is a pre-defined list that you can choose from. You can also add free hand text.",
                placement: 'right',
                reflex: true
            },
            {
                path: '/user_logged/companies/-1',
                element: ".container-goals",
                title: "Company ways to reach the goals",
                content: "Just as you, we are also curious to learn how companies plan to reach their ambitious goals.<br>" +
                "<br>" +
                "This is where they will be sharing their plans and ideas. <br>" +
                "<br>" +
                "Once again – we invite you to ‘Request more info’, if you have any questions to the information  presented here and/or if you are missing any answers.<br>" +
                "<br>" +
                "As a Company Administrator, please make sure to add relevant strategies here. They will make your goals and policies even more trustworthy. They will contribute to your comapny's image and employer brand.",
                placement: 'left',
                reflex: true
            },
            {
                path: '/user_logged/companies/-1',
                element: ".container-stream",
                title: "Company stream",
                content: "Do visit the Company stream and check the latest diversity & inclusion updates from the companies you follow.<br>" +
                "<br>" +
                "Show them your appreciation and interest by leaving a ‘like’ and commenting. <br>" +
                "<br>" +
                "As a Company Administrator, please make sure to keep your followers updated on your company’s diversity and inclusion related initiatives.<br>" +
                "Maybe a new female top manager has joined your company, or you have introduced a mentoring program for your female talents. Whatever it is, do share it here.",
                placement: 'top',
                reflex: true,
                onHidden: (function(){window.location.replace('/user_logged/walls');})
            }
        ]
    });
    tour.init();
    if ($('.btn-signup').length == 0) {
        tour.start();
    }
    $('#restart_tour').click(function(){
        console.log(tour);
        tour.restart();
    });
});
