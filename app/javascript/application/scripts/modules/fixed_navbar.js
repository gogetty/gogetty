import ScrollMagic from "scrollmagic";

var controller = new ScrollMagic.Controller();

var triggerElement = document.getElementById("trigger-point");
var fixedNavbar = document.getElementById("fixed-navbar");

if (triggerElement && fixedNavbar) {
  new ScrollMagic.Scene({
    offset: 50,
    triggerHook: 0,
    triggerElement: triggerElement
  })
    .setClassToggle(fixedNavbar, "show-fixed-navbar")
    .addTo(controller);
}
