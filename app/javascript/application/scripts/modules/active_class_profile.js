var pathname = window.location.pathname;
var hostname = window.location.href;

var profile = document.getElementById("profile");
var password = document.getElementById("password");
var billings = document.getElementById("billings");
var subscriptions = document.getElementById("subscriptions");
var payments = document.getElementById("payments");
var invites = document.getElementById("invites");
var body = document.getElementById("bodyStatic");

function addActiveClass(el) {
  el.classList.add("list-inline-item-active");
}

if (pathname.includes("/account_settings/edit")) {
  addActiveClass(profile);
} else if (pathname.includes("/users/edit")) {
  addActiveClass(password);
} else if (pathname.includes("/account_settings/billings")) {
  addActiveClass(billings);
} else if (pathname.includes("/account_settings/plans_users")) {
  addActiveClass(subscriptions);
} else if (pathname.includes("/account_settings/upcoming_payments")) {
  addActiveClass(payments);
} else if (pathname.includes("/account_settings/admin_invites")) {
  addActiveClass(invites);
}

if (
  pathname.includes("/about") ||
  pathname.includes("/users/sign_in") ||
  pathname.includes("/users/sign_up") ||
  pathname.includes("/solutions")
) {
  body.style.backgroundColor = "#fff";
  body.style.backgroundImage = "none";
}
if (
  hostname === "http://localhost:5000/" ||
  hostname === "https://gogetty.ideamotive.co/" ||
  hostname === "https://www.gogetty.co/"
) {
  body.style.backgroundColor = "#fff";
  body.style.backgroundImage = "none";
  body.style.maxWidth = "100%";
}
