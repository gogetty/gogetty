$('.edit-ethnicity').on('show.bs.modal', function () {
  bind_ethnicity_forms();
});

function refresh_languages_form(result) {
  $('#languages').html(result.originalEvent.detail[2].response);
  bind_ethnicity_forms();
}
function refresh_nationalities_form(result) {
  $('#nationalities').html(result.originalEvent.detail[2].response);
  $('div#nationalities').removeClass('fade');
  bind_ethnicity_forms();
}

function bind_ethnicity_forms() {
  $('#edit_company_language').on('ajax:success', function(result){
    refresh_languages_form(result);
  }).on('ajax:error', function(result){
    alert(result.originalEvent.detail[0].error);
  });

  $('#edit_language .remove-tile a').on('ajax:success', function(result){
    refresh_languages_form(result);
  }).on('ajax:error', function(result){
    alert(result.originalEvent.detail[0].error);
  });

  $('#update_languages').on('ajax:success', function(result){
    refresh_languages_form(result);
  }).on('ajax:error', function(result){
    alert(result.originalEvent.detail[0].error);
  });

  $('#edit_company_nationality').on('ajax:success', function(result){
    refresh_nationalities_form(result);
  }).on('ajax:error', function(result){
    alert(result.originalEvent.detail[0].error);
  });

  $('#edit_nationality .remove-tile a').on('ajax:success', function(result){
    refresh_nationalities_form(result);
  }).on('ajax:error', function(result){
    alert(result.originalEvent.detail[0].error);
  });

  $('#update_nationalities').on('ajax:success', function(result){
    refresh_nationalities_form(result);
  }).on('ajax:error', function(result){
    alert(result.originalEvent.detail[0].error);
  });

  $('.edit-ethnicity').on('hide.bs.modal', function(){
    location.reload();
  });
}