$(document).ready(function(){
    $('.keynote__button').on('click', function(result){
        solutions_set_content($(this));
    });
    $('.full-offer-button').on('click', function(result){
        solutions_set_content($(this));
    });
});

function solutions_set_content(link) {
    $('#solutions_more_info__name').html(link.data("name"));
    $('#solutions_more_info__company').html(link.data("company"));
    $('#solution_request_name').val(link.data("name"));
    $('#solution_request_company').val(link.data("company"));
}
