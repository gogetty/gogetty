$.each($('.edit-diversity .isNotLevel'), function(index, value) {
  if ($(this).prop('checked')) {
    $(this).parents('.row').children().children('.slidePercentage').hide();
    $(this).parents('.row').next().hide();
    $(this).parents('.row').next().next().hide();
  }
});

$('.edit-diversity .isNotLevel').change(function() {
  if ($(this).prop('checked')) {
    $(this).parents('.row').next().fadeOut();
  } else {
    $(this).parents('.row').next().fadeIn();
  }
})


// Percentage calculation for Men, Women get updated
$(document).ready(function() {
  // first div  Board of Directors
  $('#company_profile_now_director_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_now_director_man_percentage').val(men_per)
  });
  $('#company_profile_now_director_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_now_director_woman_percentage').val(woman_per)
  });
  $('#company_profile_goal_director_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_goal_director_man_percentage').val(men_per)
  });
  $('#company_profile_goal_director_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_goal_director_woman_percentage').val(woman_per)
  });
  // Second div company CXo
  $('#company_profile_now_cxo_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_now_cxo_man_percentage').val(men_per)
  });
  $('#company_profile_now_cxo_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_now_cxo_woman_percentage').val(woman_per)
  });

  $('#company_profile_goal_cxo_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_goal_cxo_man_percentage').val(men_per)
  });
  $('#company_profile_goal_cxo_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_goal_cxo_woman_percentage').val(woman_per)
  });
  // Second div company VP
  $('#company_profile_now_vp_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_now_vp_man_percentage').val(men_per)
  });
  $('#company_profile_now_vp_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_now_vp_woman_percentage').val(woman_per)
  });

  $('#company_profile_goal_vp_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_goal_vp_man_percentage').val(men_per)
  });
  $('#company_profile_goal_vp_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_goal_vp_woman_percentage').val(woman_per)
  });

  // third div  Middle Management
  $('#company_profile_now_middle_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_now_middle_man_percentage').val(men_per)
  });
  $('#company_profile_now_middle_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_now_middle_woman_percentage').val(woman_per)
  });

  $('#company_profile_goal_middle_woman_percentage').keyup(function(){
      var woman_per = Number($(this).val());
      var men_per = 100 - woman_per
      $('#company_profile_goal_middle_man_percentage').val(men_per)
  });
  $('#company_profile_goal_middle_man_percentage').keyup(function(){
      var men_per = Number($(this).val());
      var woman_per = 100 - men_per
      $('#company_profile_goal_middle_woman_percentage').val(woman_per)
  });

});

$.each($('.edit-diversity .isNotCheckbox'), function(index, value) {
  if ($(this).prop('checked')) {
    $(this).parents('.row').next().next().hide();
    $(this).parents('.cstm_switch').children('.spn_num').addClass("red_text");
  }else{
    $(this).parents('.row').next().hide();
    $(this).parents('.cstm_switch').children('.spn_per').addClass("red_text");
  }
});

$('.edit-diversity .isNotCheckbox').change(function() {
  if ($(this).prop('checked')) {
    id = $(this).attr('id');
    $('#company_profile_'+id).val(false);
    $(this).parents('.row').next().fadeIn();
    $(this).parents('.row').next().next().fadeOut();
    $(this).parents('.cstm_switch').children('.spn_per').removeClass("red_text");
    $(this).parents('.cstm_switch').children('.spn_num').addClass("red_text");
  } else {
    id = $(this).attr('id');
    $('#company_profile_'+id).val(true);
    $(this).parents('.row').next().fadeOut();
    $(this).parents('.row').next().next().fadeIn();
    $(this).parents('.cstm_switch').children('.spn_per').addClass("red_text");
    $(this).parents('.cstm_switch').children('.spn_num').removeClass("red_text");
  }
})
