$(document).ready(function(){
  if ($('div#no_companies_found').length) {
    $('#no_companies_found').modal('show');
  }
  $('#no_companies_found #add_company').click(function(data) {
    $('#no_companies_found').modal('hide');
    $('#how_this_works_add_company').modal('show');
    return false
  });
});
