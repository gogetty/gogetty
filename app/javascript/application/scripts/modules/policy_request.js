$(document).ready(function(){
  bind_policy_request_add_field();
});

function add_policy_request_field(){
  $.post( "/user_logged/policy_request_fields",
          $('#user_logged_policy_request_form').serialize() + '&' + $('#user_logged_policy_request_add_form').serialize(),
          function( data ) {
    $("#user_logged_policy_request_form").html(data);
    bind_policy_request_add_field();
  });
  return false
}

function bind_policy_request_add_field(){
  $('#policy_request_add_field').bind('click', function() {
    add_policy_request_field();
  });
}
