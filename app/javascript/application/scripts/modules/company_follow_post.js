$(document).ready(function(){
  bind_follow_buttons();
});

function bind_follow_buttons() {
  $('.user_logged_follow_form').on('ajax:success', function(result){
    $(this).find('.follow').toggle();
    $(this).find('.unfollow').toggle();
  }).on('ajax:error',function(result){
    alert(result.originalEvent.detail[0].error);
  });
}
