# frozen_string_literal: true

module Form
  module CompanySearch
    class New
      include ActiveModel::Model
      include Virtus.model

      attribute :name
      attribute :diversity_score_from, Integer, default: 0
      attribute :diversity_score_to, Integer, default: 50
      attribute :ethnicity_score_from, Integer, default: 0
      attribute :ethnicity_score_to, Integer, default: 50
      attribute :employees_count_id
      attribute :location
      attribute :country_id
      attribute :industry_id
      attribute :alphabet_letter
      attribute :sort_id, Integer, default: 1

      validates :diversity_score_from,
                presence: true,
                if: proc { |a| a.diversity_score_to.to_s != '' }
      validates :diversity_score_to,
                presence: true,
                if: proc { |a| a.diversity_score_from.to_s != '' }

      validates :diversity_score_from,
                numericality: {
                  only_integer: true,
                  greater_than_or_equal: 0,
                  less_than_or_equal_to: 50,
                  allow_blank: true
                }

      validates :diversity_score_to,
                numericality: {
                  only_integer: true,
                  greater_than_or_equal: 0,
                  less_than_or_equal_to: 50,
                  allow_blank: true
                }

      validates :ethnicity_score_from,
                numericality: {
                  only_integer: true,
                  greater_than_or_equal: 0,
                  less_than_or_equal_to: 50,
                  allow_blank: true
                }

      validates :ethnicity_score_to,
                numericality: {
                  only_integer: true,
                  greater_than_or_equal: 0,
                  less_than_or_equal_to: 50,
                  allow_blank: true
                }
    end
  end
end
