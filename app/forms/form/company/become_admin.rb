# frozen_string_literal: true

module Form
  module Company
    class BecomeAdmin
      include ActiveModel::Model
      include Virtus.model

      attribute :acceptance

      validates :acceptance, acceptance: true

      def persisted?
        true
      end
    end
  end
end
