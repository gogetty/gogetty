# frozen_string_literal: true

module Form
  module PartnerRequest
    class New
      include ActiveModel::Model
      include Virtus.model

      attribute :name
      attribute :surname
      attribute :email
      attribute :company_name
      attribute :product

      validates :name, presence: true
      validates :surname, presence: true
      validates :email, presence: true
      validates :company_name, presence: true
      validates :product, presence: true

      def self.model_name
        ActiveModel::Name.new(self, nil, 'PartnerRequest')
      end
    end
  end
end
