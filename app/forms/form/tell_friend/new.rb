# frozen_string_literal: true

module Form
  module TellFriend
    class New
      include ActiveModel::Model
      include Virtus.model

      attribute :name
      attribute :email
      attribute :custom_message
      attribute :full_name

      validates :name, presence: true
      validates :email, presence: true
      validates :full_name, presence: true

      def self.model_name
        ActiveModel::Name.new(self, nil, 'TellFriend')
      end

      def persisted?
        true
      end
    end
  end
end
