# frozen_string_literal: true

module Form
  module UserLogged
    module Like
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :post_id
        attribute :created_by

        validates :post_id, presence: true
        validates :created_by, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Like')
        end

        def persisted?
          true
        end
      end
    end
  end
end
