# frozen_string_literal: true

module Form
  module UserLogged
    module GoalRequest
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :company_id
        attribute :comment
        attribute :user_id

        validates :company_id, presence: true
        validates :user_id, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'GoalRequest')
        end
      end
    end
  end
end
