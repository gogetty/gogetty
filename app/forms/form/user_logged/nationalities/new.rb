# frozen_string_literal: true

module Form
  module UserLogged
    module Nationalities
      class New
        include ActiveModel::Model
        include ActiveModel::Validations
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :name
        attribute :count, Integer, default: 1

        validates :name, presence: true
        validates :count, presence: true
        validates :count, numericality: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Nationality')
        end

        def persisted?
          true
        end
      end
    end
  end
end
