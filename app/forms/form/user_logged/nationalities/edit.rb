# frozen_string_literal: true

module Form
  module UserLogged
    module Nationalities
      class Edit
        include ActiveModel::Model
        include ActiveModel::Validations
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :name
        attribute :count, Integer, default: 1

        validates :name, presence: true
        validates :count, presence: true
        validates :count, numericality: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Nationality')
        end

        def self.find(company_id)
          nationalities = ::Nationality.where(company_id: company_id)
          nationalities.each do |nationality|
            attr_hash = {
              name: nationality.name,
              count: nationality.count
            }
            ::Form::UserLogged::Nationalities::Edit.new(
              attr_hash.merge(
                id: nationality.id,
                company_id: nationality.company_id
              )
            )
          end
        end

        def persisted?
          true
        end
      end
    end
  end
end
