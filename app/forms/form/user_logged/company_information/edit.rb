# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyInformation
      class Edit
        include ActiveModel::Model
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :first_video
        attribute :second_video
        attribute :diversity_plans_title
        attribute :gender_diversity_plans
        attribute :ethnicity_diversity_plans
        attribute :contact

        validate do |form|
          youtube_regex = %r{youtu\.be\/([^\?]*)|^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*}
          vimeo_regex = %r{^https?:\/\/(?:.*?)\.?(vimeo)\.com\/(\d+).*$}

          %i[first_video second_video].each do |video|
            next if form[video].empty?
            valid_url = form[video].match(youtube_regex) || form[video].match(vimeo_regex)

            errors.add video, 'has to be a valid YouTube or Vimeo URL' unless valid_url
          end
        end

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyInformation')
        end

        def self.find(company_id)
          company_information = ::CompanyInformation.find_or_create_by(company_id: company_id)
          attr_hash = {
            first_video: company_information.first_video,
            second_video: company_information.second_video,
            diversity_plans_title: company_information.diversity_plans_title,
            gender_diversity_plans: company_information.gender_diversity_plans,
            ethnicity_diversity_plans: company_information.ethnicity_diversity_plans,
            contact: company_information.contact
          }
          ::Form::UserLogged::CompanyInformation::Edit.new(
            attr_hash.merge(
              id: company_information.id,
              company_id: company_information.company_id
            )
          )
        end
      end
    end
  end
end
