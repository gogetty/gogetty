# frozen_string_literal: true

module Form
  module UserLogged
    module Company
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :name
        attribute :acceptance

        validates :name, presence: true
        validates :acceptance, acceptance: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Company')
        end

        def persisted?
          true
        end
      end
    end
  end
end
