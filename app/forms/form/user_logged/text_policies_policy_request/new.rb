# frozen_string_literal: true

module Form
  module UserLogged
    module TextPoliciesPolicyRequest
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :name, String

        validates :name, presence: true
      end
    end
  end
end
