# frozen_string_literal: true

module Form
  module UserLogged
    module CompanySettings
      class Edit
        include ActiveModel::Model
        include Virtus.model

        attribute :name
        attribute :logo
        attribute :remove_logo
        attribute :industry_id
        attribute :website
        attribute :country_id
        attribute :city
        attribute :employees_count_id
        attribute :founded_year
        attribute :owner_type_id
        attribute :always_visible, Boolean
        attribute :cvr_number
        attribute :stock_listed, Boolean

        validate :cvr_number_for_danish_company

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Company')
        end

        def self.find(company)
          ::Form::UserLogged::CompanySettings::Edit.new(
            logo:               company.logo,
            name:               company.name,
            industry_id:        company.industry_id,
            website:            company.website,
            country_id:         company.country_id,
            city:               company.city,
            employees_count_id: company.company_profile.employees_count_id,
            founded_year:       company.founded_year,
            owner_type_id:      company.owner_type_id,
            always_visible:     company.company_profile.always_visible,
            stock_listed:       company.stock_listed,
            cvr_number:         company.cvr_number
          )
        end

        def persisted?
          true
        end

        private

        def cvr_number_for_danish_company
          company_country_name = ::Country.find_by(id: country_id)&.en_name
          danish_company = company_country_name == 'Denmark' && cvr_number.blank?

          errors.add(:cvr_number, 'must be added for a Danish company') if danish_company
        end
      end
    end
  end
end
