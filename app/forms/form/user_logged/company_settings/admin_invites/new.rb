# frozen_string_literal: true

module Form
  module UserLogged
    module CompanySettings
      module AdminInvites
        class New
          include ActiveModel::Model
          include Virtus.model

          attribute :email
          attribute :user_id
          attribute :company_id

          validates :email, presence: true, unless: ->(invite) { invite.user_id.present? }
          validates :user_id, presence: true, unless: ->(invite) { invite.email.present? }
          validates :company_id, presence: true

          def self.model_name
            ActiveModel::Name.new(self, nil, 'AdminInvite')
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
