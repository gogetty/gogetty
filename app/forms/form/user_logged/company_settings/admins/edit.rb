# frozen_string_literal: true

module Form
  module UserLogged
    module CompanySettings
      module Admins
        class Edit
          include ActiveModel::Model
          include Virtus.model

          attribute :superadmin

          def self.model_name
            ActiveModel::Name.new(self, nil, 'User')
          end

          def self.find(params)
            user = User.find(params[:id])
            ::Form::UserLogged::CompanySettings::Admins::Edit.new(
              superadmin: user.superadmin
            )
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
