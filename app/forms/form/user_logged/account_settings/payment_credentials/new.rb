# frozen_string_literal: true

module Form
  module UserLogged
    module AccountSettings
      module PaymentCredentials
        class New
          include ActiveModel::Model
          include Virtus.model

          attribute :last4
          attribute :exp_month
          attribute :exp_year
          attribute :card_type
          attribute :stripe_customer_id
          attribute :user_id

          def self.model_name
            ActiveModel::Name.new(self, nil, 'PaymentCredential')
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
