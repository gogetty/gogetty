# frozen_string_literal: true

module Form
  module UserLogged
    module AccountSettings
      class Edit
        include ActiveModel::Model
        include Virtus.model

        attribute :name
        attribute :surname
        attribute :email
        attribute :company_name
        attribute :title
        attribute :avatar
        attribute :remove_avatar
        attribute :company_representative
        attribute :newsletter_subscription

        validates :name, presence: true
        validates :surname, presence: true
        validates :email, presence: true
        validates :company_name, presence: true
        validates :title, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'User')
        end

        def self.find(user)
          ::Form::UserLogged::AccountSettings::Edit.new(
            avatar:                  user.avatar,
            name:                    user.name,
            surname:                 user.surname,
            email:                   user.email,
            company_name:            user.company_name,
            title:                   user.title,
            company_representative:  user.company_representative,
            newsletter_subscription: user.newsletter_subscription
          )
        end

        def persisted?
          true
        end
      end
    end
  end
end
