# frozen_string_literal: true

module Form
  module UserLogged
    module TextPolicy
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :name
        attribute :company_id

        validates :name, presence: true
        validates :company_id, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompaniesDiversityPolicy')
        end

        def persisted?
          true
        end
      end
    end
  end
end
