# frozen_string_literal: true

module Form
  module UserLogged
    module PlatformSettings
      module Nationalities
        class Edit
          include ActiveModel::Model
          include Virtus.model

          attribute :name

          validates :name, presence: true

          def self.find(nationality)
            ::Form::UserLogged::PlatformSettings::Nationalities::Edit.new(
              name: nationality.name
            )
          end

          def self.model_name
            ActiveModel::Name.new(self, nil, 'Nationality')
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
