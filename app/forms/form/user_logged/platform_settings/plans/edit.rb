# frozen_string_literal: true

module Form
  module UserLogged
    module PlatformSettings
      module Plans
        class Edit
          include ActiveModel::Model
          include Virtus.model

          attribute :monthly_cost
          attribute :monthly_stripe_name
          attribute :annual_cost
          attribute :annual_stripe_name
          attribute :tax

          validates :monthly_cost, numericality: {only_integer: true}
          validates :monthly_stripe_name, presence: true
          validates :annual_cost, numericality: {only_integer: true}
          validates :annual_stripe_name, presence: true
          validates :tax, numericality: {only_integer: true}

          def self.model_name
            ActiveModel::Name.new(self, nil, 'Plan')
          end

          def self.find(plan)
            ::Form::UserLogged::PlatformSettings::Plans::Edit.new(
              monthly_cost:         plan.monthly_cost,
              monthly_stripe_name:  plan.monthly_stripe_name,
              annual_cost:          plan.annual_cost,
              annual_stripe_name:   plan.annual_stripe_name,
              tax:                  plan.tax
            )
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
