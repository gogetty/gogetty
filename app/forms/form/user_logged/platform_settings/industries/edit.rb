# frozen_string_literal: true

module Form
  module UserLogged
    module PlatformSettings
      module Industries
        class Edit
          include ActiveModel::Model
          include Virtus.model

          attribute :name

          validates :name, presence: true

          def self.find(industry)
            ::Form::UserLogged::PlatformSettings::Industries::Edit.new(
              name: industry.name
            )
          end

          def self.model_name
            ActiveModel::Name.new(self, nil, 'Industry')
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
