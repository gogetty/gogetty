# frozen_string_literal: true

module Form
  module UserLogged
    module PlatformSettings
      module Languages
        class Edit
          include ActiveModel::Model
          include Virtus.model

          attribute :name

          validates :name, presence: true

          def self.find(language)
            ::Form::UserLogged::PlatformSettings::Languages::Edit.new(
              name: language.name
            )
          end

          def self.model_name
            ActiveModel::Name.new(self, nil, 'Language')
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
