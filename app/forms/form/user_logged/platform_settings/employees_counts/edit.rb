# frozen_string_literal: true

module Form
  module UserLogged
    module PlatformSettings
      module EmployeesCounts
        class Edit
          include ActiveModel::Model
          include Virtus.model

          attribute :order
          attribute :count

          validates :order, numericality: {only_integer: true}
          validates :count, presence: true

          def self.find(employees_count)
            ::Form::UserLogged::PlatformSettings::EmployeesCounts::Edit.new(
              order: employees_count.order,
              count: employees_count.count
            )
          end

          def self.model_name
            ActiveModel::Name.new(self, nil, 'EmployeesCount')
          end

          def persisted?
            true
          end
        end
      end
    end
  end
end
