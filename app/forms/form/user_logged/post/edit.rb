# frozen_string_literal: true

module Form
  module UserLogged
    module Post
      class Edit
        include ActiveModel::Model
        include Virtus.model

        attribute :id
        attribute :title
        attribute :text
        attribute :photo
        attribute :remove_photo
        attribute :video
        attribute :remove_video
        attribute :company_id
        attribute :created_by

        validates :text, presence: true
        validates :company_id, presence: true
        validates :created_by, presence: true

        validate do |form|
          unless form.video.empty?
            youtube_regex = %r{youtu\.be\/([^\?]*)|^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*}
            vimeo_regex = %r{^https?:\/\/(?:.*?)\.?(vimeo)\.com\/(\d+).*$}

            valid_url = form.video.match(youtube_regex) || form.video.match(vimeo_regex)

            errors.add :video, 'has to be a valid YouTube or Vimeo URL' unless valid_url
          end

          errors.add :photo, 'size should be lower than 2MB' if form.photo&.size.to_f / (2**20) > 2
        end

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Post')
        end

        def self.find(post_id)
          post = ::Post.find(post_id)
          attr_hash = {
            title: post.title,
            text: post.text,
            photo: post.photo,
            video: post.video
          }
          ::Form::UserLogged::Post::Edit.new(
            attr_hash.merge(
              id: post.id,
              company_id: post.company_id,
              created_by: post.created_by
            )
          )
        end

        def persisted?
          true
        end
      end
    end
  end
end
