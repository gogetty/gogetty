# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyNationality
      class Edit
        include ActiveModel::Model
        include ActiveModel::Validations
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :nationality_id
        attribute :count, Integer, default: 1

        validates :count, presence: true
        validates :count, numericality: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyNationality')
        end

        def self.find(company_id)
          nationalities = ::CompanyNationality.where(company_id: company_id)
          nationalities.each do |nationality|
            attr_hash = {
              count: nationality.count
            }
            ::Form::UserLogged::CompanyNationality::Edit.new(
              attr_hash.merge(
                id: nationality.id,
                nationality_id: nationality.nationality_id,
                company_id: nationality.company_id
              )
            )
          end
        end

        def persisted?
          true
        end
      end
    end
  end
end
