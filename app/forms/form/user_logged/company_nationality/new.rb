# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyNationality
      class New
        include ActiveModel::Model
        include ActiveModel::Validations
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :nationality_id
        attribute :count, Integer, default: 1

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyNationality')
        end

        def persisted?
          true
        end
      end
    end
  end
end
