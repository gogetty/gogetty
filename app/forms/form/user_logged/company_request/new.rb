# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyRequest
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :notify
        attribute :name
        attribute :user_id

        validates :notify, presence: true, acceptance: true
        validates :name, presence: true
        validates :user_id, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyRequest')
        end

        def persisted?
          true
        end
      end
    end
  end
end
