# frozen_string_literal: true

module Form
  module UserLogged
    module CompaniesDiversityPolicy
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :diversity_policy_id
        attribute :company_id

        validates :diversity_policy_id, presence: true
        validates :company_id, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompaniesDiversityPolicy')
        end

        def persisted?
          true
        end
      end
    end
  end
end
