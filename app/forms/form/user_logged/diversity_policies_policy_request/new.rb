# frozen_string_literal: true

module Form
  module UserLogged
    module DiversityPoliciesPolicyRequest
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :diversity_policy_id, Integer

        validates :diversity_policy_id, presence: true
      end
    end
  end
end
