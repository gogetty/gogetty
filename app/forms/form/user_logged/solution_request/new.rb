# frozen_string_literal: true

module Form
  module UserLogged
    module SolutionRequest
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :company
        attribute :name
        attribute :user_id
        attribute :back_url

        validates :name, presence: true
        validates :company, presence: true
        validates :user_id, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'SolutionRequest')
        end
      end
    end
  end
end
