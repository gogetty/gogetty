# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyProfile
      # Changes made by James Wilson without applying Rubocop rules
      class Edit # rubocop:disable Metrics/ClassLength
        include ActiveModel::Model
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :now_director_woman_count, Integer, default: 0
        attribute :now_director_man_count, Integer, default: 0
        attribute :goal_director_woman_count, Integer, default: 0
        attribute :goal_director_man_count, Integer, default: 0
        attribute :now_cxo_woman_count, Integer, default: 0
        attribute :now_cxo_man_count, Integer, default: 0
        attribute :goal_cxo_woman_count, Integer, default: 0
        attribute :goal_cxo_man_count, Integer, default: 0
        attribute :now_vp_woman_count, Integer, default: 0
        attribute :now_vp_man_count, Integer, default: 0
        attribute :goal_vp_woman_count, Integer, default: 0
        attribute :goal_vp_man_count, Integer, default: 0
        attribute :now_middle_woman_count, Integer, default: 0
        attribute :now_middle_man_count, Integer, default: 0
        attribute :goal_middle_woman_count, Integer, default: 0
        attribute :goal_middle_man_count, Integer, default: 0
        attribute :director_hide, Boolean, default: false
        attribute :cxo_hide, Boolean, default: false
        attribute :vp_hide, Boolean, default: false
        attribute :middle_hide, Boolean, default: false
        attribute :director_name, String, default: 'Board of Directors'
        attribute :cxo_name, String, default: 'CxO Level'
        attribute :vp_name, String, default: 'VP Level'
        attribute :middle_name, String, default: 'MM'
        attribute :goal_year
        attribute :now_director_woman_percentage, Integer, default: 0
        attribute :now_director_man_percentage, Integer, default: 0
        attribute :goal_director_woman_percentage, Integer, default: 0
        attribute :goal_director_man_percentage, Integer, default: 0
        attribute :now_cxo_woman_percentage, Integer, default: 0
        attribute :now_cxo_man_percentage, Integer, default: 0
        attribute :goal_cxo_woman_percentage, Integer, default: 0
        attribute :goal_cxo_man_percentage, Integer, default: 0
        attribute :now_middle_woman_percentage, Integer, default: 0
        attribute :now_middle_man_percentage, Integer, default: 0
        attribute :goal_middle_woman_percentage, Integer, default: 0
        attribute :goal_middle_man_percentage, Integer, default: 0
        attribute :now_vp_woman_percentage, Integer, default: 0
        attribute :now_vp_man_percentage, Integer, default: 0
        attribute :goal_vp_woman_percentage, Integer, default: 0
        attribute :goal_vp_man_percentage, Integer, default: 0
        attribute :now_director_total, Integer, default: 0
        attribute :goal_director_total, Integer, default: 0
        attribute :now_cxo_total, Integer, default: 0
        attribute :goal_cxo_total, Integer, default: 0
        attribute :now_middle_total, Integer, default: 0
        attribute :goal_middle_total, Integer, default: 0
        attribute :now_vp_total, Integer, default: 0
        attribute :goal_vp_total, Integer, default: 0
        attribute :director_in_percenatge, Boolean, default: false
        attribute :cxo_in_percenatge, Boolean, default: false
        attribute :middle_in_percenatge, Boolean, default: false
        attribute :vp_in_percenatge, Boolean, default: false

        validates :now_director_woman_count, numericality: true
        validates :now_director_man_count, numericality: true
        validates :goal_director_woman_count, numericality: true
        validates :goal_director_man_count, numericality: true
        validates :now_cxo_woman_count, numericality: true
        validates :now_cxo_man_count, numericality: true
        validates :goal_cxo_woman_count, numericality: true
        validates :goal_cxo_man_count, numericality: true
        validates :now_vp_woman_count, numericality: true
        validates :now_vp_man_count, numericality: true
        validates :goal_vp_woman_count, numericality: true
        validates :goal_vp_man_count, numericality: true
        validates :now_middle_woman_count, numericality: true
        validates :now_middle_man_count, numericality: true
        validates :goal_middle_woman_count, numericality: true
        validates :goal_middle_man_count, numericality: true
        validates :now_director_woman_percentage, numericality: true
        validates :now_director_man_percentage, numericality: true
        validates :goal_director_woman_percentage, numericality: true
        validates :goal_director_man_percentage, numericality: true
        validates :now_cxo_woman_percentage, numericality: true
        validates :now_cxo_man_percentage, numericality: true
        validates :goal_cxo_woman_percentage, numericality: true
        validates :now_vp_woman_percentage, numericality: true
        validates :now_vp_man_percentage, numericality: true
        validates :goal_vp_woman_percentage, numericality: true
        validates :goal_vp_man_percentage, numericality: true
        validates :now_director_total, numericality: true
        validates :goal_director_total, numericality: true
        validates :now_cxo_total, numericality: true
        validates :goal_cxo_total, numericality: true
        validates :now_vp_total, numericality: true
        validates :goal_vp_total, numericality: true

        validates :goal_year, numericality: {greater_than_or_equal_to: Time.zone.now.year + 1,
                                             less_than_or_equal_to: Time.zone.now.year + 11}

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyProfile')
        end

        # rubocop:disable AbcSize
        # rubocop:disable MethodLength
        def self.find(company_id)
          company_profile = ::CompanyProfile.find_by(company_id: company_id)
          attr_hash = {
            now_director_woman_count: company_profile.now_director_woman_count,
            now_director_man_count: company_profile.now_director_man_count,
            goal_director_woman_count: company_profile.goal_director_woman_count,
            goal_director_man_count: company_profile.goal_director_man_count,
            now_cxo_woman_count: company_profile.now_cxo_woman_count,
            now_cxo_man_count: company_profile.now_cxo_man_count,
            goal_cxo_woman_count: company_profile.goal_cxo_woman_count,
            goal_cxo_man_count: company_profile.goal_cxo_man_count,
            now_vp_woman_count: company_profile.now_vp_woman_count,
            now_vp_man_count: company_profile.now_vp_man_count,
            goal_vp_woman_count: company_profile.goal_vp_woman_count,
            goal_vp_man_count: company_profile.goal_vp_man_count,
            now_middle_woman_count: company_profile.now_middle_woman_count,
            now_middle_man_count: company_profile.now_middle_man_count,
            goal_middle_woman_count: company_profile.goal_middle_woman_count,
            goal_middle_man_count: company_profile.goal_middle_man_count,
            director_hide: company_profile.director_hide,
            cxo_hide: company_profile.cxo_hide,
            vp_hide: company_profile.vp_hide,
            middle_hide: company_profile.middle_hide,
            director_name: company_profile.director_name,
            cxo_name: company_profile.cxo_name,
            vp_name: company_profile.vp_name,
            middle_name: company_profile.middle_name,
            now_director_woman_percentage: company_profile.now_director_woman_percentage,
            now_director_man_percentage: company_profile.now_director_man_percentage,
            now_director_total: company_profile.now_director_total,
            goal_director_total: company_profile.goal_director_total,
            goal_director_woman_percentage: company_profile.goal_director_woman_percentage,
            goal_director_man_percentage: company_profile.goal_director_man_percentage,
            now_cxo_woman_percentage: company_profile.now_cxo_woman_percentage,
            now_cxo_man_percentage: company_profile.now_cxo_man_percentage,
            now_cxo_total: company_profile.now_cxo_total,
            goal_cxo_total: company_profile.goal_cxo_total,
            goal_cxo_woman_percentage: company_profile.goal_cxo_woman_percentage,
            goal_cxo_man_percentage: company_profile.goal_cxo_man_percentage,
            now_vp_woman_percentage: company_profile.now_vp_woman_percentage,
            now_vp_man_percentage: company_profile.now_vp_man_percentage,
            now_vp_total: company_profile.now_vp_total,
            goal_vp_total: company_profile.goal_vp_total,
            goal_vp_woman_percentage: company_profile.goal_vp_woman_percentage,
            goal_vp_man_percentage: company_profile.goal_vp_man_percentage,
            now_middle_woman_percentage: company_profile.now_middle_woman_percentage,
            now_middle_man_percentage: company_profile.now_middle_man_percentage,
            now_middle_total: company_profile.now_middle_total,
            goal_middle_total: company_profile.goal_middle_total,
            goal_middle_woman_percentage: company_profile.goal_middle_woman_percentage,
            goal_middle_man_percentage: company_profile.goal_middle_man_percentage,
            director_in_percenatge: company_profile.director_in_percenatge,
            cxo_in_percenatge: company_profile.cxo_in_percenatge,
            middle_in_percenatge: company_profile.middle_in_percenatge,
            vp_in_percenatge: company_profile.vp_in_percenatge

          }
          ::Form::UserLogged::CompanyProfile::Edit.new(
            attr_hash.merge(
              id: company_profile.id,
              company_id: company_profile.company_id,
              goal_year: company_profile.goal_year
            )
          )
        end
        # rubocop:enable all

        def persisted?
          true
        end
      end
    end
  end
end
