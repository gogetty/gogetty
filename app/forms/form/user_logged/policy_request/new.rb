# frozen_string_literal: true

module Form
  module UserLogged
    module PolicyRequest
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :company_id
        attribute :comment
        attribute :user_id

        validates :company_id, presence: true
        validates :user_id, presence: true
        validates :comment,
                  presence: true,
                  unless: proc { |a| a.diversity_policies_policy_requests }
        validates :diversity_policies_policy_requests,
                  presence: true,
                  unless: proc { |a| a.comment.to_s != '' }

        attr_accessor :diversity_policies_policy_requests
        attr_accessor :text_policies_policy_requests

        def diversity_policies_policy_requests_attributes=(attributes)
          @diversity_policies_policy_requests ||= []
          attributes.each_value do |diversity_policies_policy_request_params|
            diversity_policies_policy_request =
              ::Form::UserLogged::DiversityPoliciesPolicyRequest::New.new(
                diversity_policies_policy_request_params
              )
            @diversity_policies_policy_requests.push(diversity_policies_policy_request)
          end
        end

        def self.model_name
          ActiveModel::Name.new(self, nil, 'PolicyRequest')
        end
      end
    end
  end
end
