# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyLanguage
      class New
        include ActiveModel::Model
        include ActiveModel::Validations
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :language_id
        attribute :count, Integer, default: 1

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyLanguage')
        end

        def persisted?
          true
        end
      end
    end
  end
end
