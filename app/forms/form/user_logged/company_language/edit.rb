# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyLanguage
      class Edit
        include ActiveModel::Model
        include ActiveModel::Validations
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :language_id
        attribute :count, Integer, default: 1

        validates :count, presence: true
        validates :count, numericality: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyLanguage')
        end

        def self.find(company_id)
          languages = ::CompanyLanguage.where(company_id: company_id)
          languages.each do |language|
            attr_hash = {
              count: language.count
            }
            ::Form::UserLogged::CompanyLanguage::Edit.new(
              attr_hash.merge(
                id: language.id,
                nationality_id: language.language_id,
                company_id: language.company_id
              )
            )
          end
        end

        def persisted?
          true
        end
      end
    end
  end
end
