# frozen_string_literal: true

module Form
  module UserLogged
    module Languages
      class Edit
        include ActiveModel::Model
        include ActiveModel::Validations
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :name
        attribute :count, Integer, default: 1

        validates :name, presence: true
        validates :count, presence: true
        validates :count, numericality: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Language')
        end

        def self.find(company_id)
          languages = ::Language.where(company_id: company_id)
          languages.each do |language|
            attr_hash = {
              name: language.name,
              count: language.count
            }
            ::Form::UserLogged::Languages::Edit.new(
              attr_hash.merge(
                id: language.id,
                company_id: language.company_id
              )
            )
          end
        end

        def persisted?
          true
        end
      end
    end
  end
end
