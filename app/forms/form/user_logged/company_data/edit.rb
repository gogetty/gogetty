# frozen_string_literal: true

module Form
  module UserLogged
    module CompanyData
      class Edit
        include ActiveModel::Model
        include Virtus.model

        attribute :id
        attribute :company_id
        attribute :employees_count_id
        attribute :liabilities
        attribute :revenue

        validates :employees_count_id, presence: true
        validates :liabilities, numericality: {allow_blank: true}
        validates :revenue, numericality: {allow_blank: true}

        def self.model_name
          ActiveModel::Name.new(self, nil, 'CompanyProfile')
        end

        def self.find(company_id)
          company_profile = ::CompanyProfile.find_by(company_id: company_id)
          ::Form::UserLogged::CompanyData::Edit.new(
            id: company_profile.id,
            company_id: company_profile.company_id,
            employees_count_id: company_profile.employees_count_id,
            liabilities: company_profile.liabilities,
            revenue: company_profile.revenue
          )
        end

        def persisted?
          true
        end
      end
    end
  end
end
