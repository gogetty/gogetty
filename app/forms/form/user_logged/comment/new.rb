# frozen_string_literal: true

module Form
  module UserLogged
    module Comment
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :text
        attribute :post_id
        attribute :created_by

        validates :text, presence: true
        validates :post_id, presence: true
        validates :created_by, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Comment')
        end

        def persisted?
          true
        end
      end
    end
  end
end
