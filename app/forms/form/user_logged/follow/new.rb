# frozen_string_literal: true

module Form
  module UserLogged
    module Follow
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :company_id
        attribute :user_id

        validates :company_id, presence: true
        validates :user_id, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'Follow')
        end
      end
    end
  end
end
