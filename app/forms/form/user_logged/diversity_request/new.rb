# frozen_string_literal: true

module Form
  module UserLogged
    module DiversityRequest
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :company_id
        attribute :comment
        attribute :user_id

        validates :company_id, presence: true
        validates :user_id, presence: true
        validates :comment, presence: true, if: :company_score_exists

        def self.model_name
          ActiveModel::Name.new(self, nil, 'DiversityRequest')
        end

        private

        def company_score_exists
          ::CompanyProfile.find_by(company_id: @company_id).decorate.diversity_present
        end
      end
    end
  end
end
