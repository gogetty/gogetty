# frozen_string_literal: true

module Form
  module UserLogged
    module GoalsWay
      class New
        include ActiveModel::Model
        include Virtus.model

        attribute :description
        attribute :company_id

        validates :description, presence: true
        validates :company_id, presence: true

        def self.model_name
          ActiveModel::Name.new(self, nil, 'GoalsWay')
        end

        def persisted?
          true
        end
      end
    end
  end
end
