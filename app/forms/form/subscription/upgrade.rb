# frozen_string_literal: true

module Form
  module Subscription
    class Upgrade
      include ActiveModel::Model
      include Virtus.model

      attribute :commit, String
      attribute :coupon

      validates :commit, presence: true

      def self.model_name
        ActiveModel::Name.new(self, nil, 'Subscription')
      end

      def persisted?
        true
      end
    end
  end
end
