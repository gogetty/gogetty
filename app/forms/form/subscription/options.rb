# frozen_string_literal: true

module Form
  module Subscription
    class Options
      include ActiveModel::Model
      include Virtus.model

      attribute :annual, Boolean
      attribute :commit, String
      attribute :coupon, String

      validates :yearly, presence: true
      validates :commit, presence: true

      def self.model_name
        ActiveModel::Name.new(self, nil, 'Subscription')
      end

      def persisted?
        true
      end
    end
  end
end
