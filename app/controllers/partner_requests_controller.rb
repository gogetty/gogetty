# frozen_string_literal: true

class PartnerRequestsController < ApplicationController
  layout 'static'

  def create
    partner_request_form = ::Form::PartnerRequest::New.new(create_params)
    respond(
      ::Command::PartnerRequest::Create.new(partner_request_form).call,
      page_path('solutions/partners'),
      page_path('solutions/partners')
    )
  end

  private

  def create_params
    params.require(:partner_request).permit(:name, :surname, :email, :company_name, :product)
  end
end
