# frozen_string_literal: true

class TellFriendsController < ApplicationController
  def create
    form = ::Form::TellFriend::New.new(form_params)
    result = ::Command::TellFriend::Create.new(form, current_user).call
    respond(result, request.referer, request.referer)
  end

  private

  def create_params
    params.require(:tell_friend).permit(:name, :email, :custom_message, :full_name)
  end

  def form_params
    if current_user && (current_user.full_name != ' ')
      create_params.merge(full_name: current_user.full_name)
    else
      create_params
    end
  end
end
