# frozen_string_literal: true

class CompanySearchesController < ApplicationController
  protect_from_forgery except: :create
  layout 'application'

  def index
    @companies = Company.joins(:company_profile)
                        .order('-company_profiles.diversity_score')
                        .page(params[:page]).per('30')
    @companies_count = Company.count
    @company = ::Form::UserLogged::Company::New.new
    @company_search = ::Form::CompanySearch::New.new
  end

  def create
    @company = ::Form::UserLogged::Company::New.new
    @company_search = ::Form::CompanySearch::New.new(search_params)
    @company_search_simple = ::Form::CompanySearch::New.new(search_params)
    result = ::Query::CompanySearch::Find.new(@company_search).call
    companies_count(result)
    companies_set(result)
    respond
  end

  private

  def respond
    if params[:from_pagination].present?
      render '/company_searches/index.html'
    else
      respond_to do |format|
        format.html { render :index }
        format.json do
          render status: :ok, html: render_to_string(
            '/company_searches/_companies_search_list.html', layout: false
          )
        end
      end
    end
  end

  def companies_count(result)
    @companies_count = result.success? ? result.data.count : Company.count
  end

  def companies_set(result)
    if result.success?
      @companies = result.data.page(params[:page]).per('30')
    else
      @companies = Company.joins(:company_profile)
                          .order('-company_profiles.diversity_score')
                          .page(params[:page]).per('30')
      flash[:error] = result.error
    end
  end

  def search_params
    params.require(:form_company_search_new)
          .permit(:name,
                  :diversity_score_from,
                  :diversity_score_to,
                  :ethnicity_score_from,
                  :ethnicity_score_to,
                  :employees_count_id,
                  :location,
                  :country_id,
                  :industry_id,
                  :sort_id)
  end
end
