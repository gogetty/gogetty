# frozen_string_literal: true

class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def linkedin
    result = ::Command::User::CreateFromOauth.new(request.env['omniauth.auth']).call
    if result.success?
      user = sign_in result.data, event: :authentication
      set_flash_message(:success, :success, kind: 'LinkedIn') if is_navigational_format?
      redirect_to after_sign_in_path_for(user)
    else
      redirect_to user_logged_walls_path
    end
  end

  def failure
    set_flash_message(:error, :failure, kind: 'LinkedIn', reason: 'uknown')
    redirect_to root_path
  end
end
