# frozen_string_literal: true

class ApplicationController < ActionController::Base
  layout :resolve_layout
  include Pundit
  protect_from_forgery with: :exception
  before_action :redirect_from_statics
  before_action :redirect_to_home
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_user_location!, if: :storable_location?
  before_action :nav_forms
  before_action :set_locale
  before_action :set_platform_settings

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || super
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def set_platform_settings
    @platform_settings = PlatformSettings.first
  end

  def redirect_from_statics
    return unless user_signed_in?
    if %w[/
          /pages/talents
          /pages/companies
          /pages/signup_types
          /pages/signup_modes
          /users/sign_up
          /after_signups
          /resend_confirmations/new
          /users/password/new].include? URI.parse(request.url).path
      redirect_to user_logged_walls_path
    end
  end

  def redirect_to_home
    if %w[/company_searches /pages/about /subscriptions/new /solutions]
       .include?(URI.parse(request.url).path) && !user_signed_in?
      flash[:alert] = 'You need to sign in or sign up before continuing.'
      redirect_to root_path
    end
  end

  def set_locale
    I18n.locale = params[:locale] || session[:locale] || I18n.default_locale
    session[:locale] = I18n.locale
  rescue StandardError
    I18n.locale = I18n.default_locale
    session[:locale] = I18n.locale
  end

  # def default_url_options(options = {})
  # logger.debug "default_url_options is passed options: #{options.inspect}\n"
  # {locale: I18n.locale}
  # end

  def resolve_layout
    if user_signed_in?
      'application'
    else
      'static'
    end
  end

  def nav_forms
    @company_search_simple = ::Form::CompanySearch::New.new
    @tell_friend = ::Form::TellFriend::New.new
  end

  def storable_location?
    request.get? && is_navigational_format? && !devise_controller? && !request.xhr?
  end

  def store_user_location!
    # :user is the scope we are authenticating
    store_location_for(:user, request.fullpath)
  end

  def respond(result, success_path, error_path)
    if result.success?
      respond_success(result.data, success_path)
    else
      respond_error(result.error, error_path)
    end
  end

  def respond_success(message, path)
    respond_to do |format|
      format.json { render status: :ok, json: {message: 'success'} }
      format.html { redirect_to path, notice: message }
    end
  end

  def respond_error(error, path)
    error_message = begin
                      error.message
                    rescue StandardError
                      error.join(', ')
                    end
    respond_to do |format|
      format.json { render status: 400, json: {error: error_message} }
      format.html { redirect_to path, alert: error_message }
    end
  end

  def render_error(message, view)
    flash[:error] = message
    render view
  end

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action!'
    redirect_to(request.referer || root_path)
  end
end
