# frozen_string_literal: true

class RegistrationsController < Devise::RegistrationsController
  layout :calculate_layout
  # prepend_before_action :check_captcha, only: [:create]
  # rubocop:disable all
  # POST /resource
  def create
    build_resource(sign_up_params)
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end
  # rubocop:enable all

  protected

  def after_inactive_sign_up_path_for(resource)
    session[:signup_user_id] = resource.id
    after_signups_path
  end

  def after_update_path_for(_resource)
    edit_user_registration_path
  end

  private

  def calculate_layout
    if action_name == 'new' || action_name == 'create'
      'static'
    else
      'mixed'
    end
  end

  def check_captcha
    return if verify_recaptcha
    self.resource = resource_class.new sign_up_params
    resource.validate # Look for any other validation errors besides Recaptcha
    respond_with_navigational(resource) { render :new }
  end

  def sign_up_params
    params.require(:user).permit(:name, :surname, :email, :password, :password_confirmation,
                                 :company_name, :title, :company_representative, :terms_agreed,
                                 :newsletter_subscription)
  end

  def account_update_params
    params.require(:user).permit(:name, :surname, :email, :password, :password_confirmation,
                                 :company_name, :title, :company_representative, :terms_agreed,
                                 :newsletter_subscription, :current_password)
  end
end
