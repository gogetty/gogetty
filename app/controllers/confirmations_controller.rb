# frozen_string_literal: true

class ConfirmationsController < Devise::ConfirmationsController
  # rubocop:disable AbcSize
  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])

    if resource.errors.empty?
      set_flash_message(:notice, :confirmed) if is_navigational_format?
      sign_in(resource_name, resource)
      respond_with_navigational(resource) { redirect_to user_logged_walls_path }
    else
      flash[:error] = resource.errors.full_messages.join(', ')
      redirect_to new_user_session_path
    end
  end
  # rubocop:enable all

  protected

  # The path used after resending confirmation instructions.
  def after_resending_confirmation_instructions_path_for(_resource_name)
    user_logged_walls_path
  end

  # The path used after confirmation.
  def after_confirmation_path_for(_resource_name, _resource)
    flash[:notice] = 'Email confirmed successfully.'
    user_logged_walls_path
  end
end
