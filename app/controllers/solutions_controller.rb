# frozen_string_literal: true

class SolutionsController < ApplicationController
  layout 'static'

  def index; end
end
