# frozen_string_literal: true

class SubscriptionsController < ApplicationController
  layout 'static'
  before_action :authenticate_user!, only: %i[create edit confirm update]

  def new
    redirect_to edit_subscription_path if current_user&.pro?
    @subscription = SubscriptionDecorator.decorate(Subscription.new)
    @subscription_form = ::Form::Subscription::Options.new
    @become_admin_company_id = params[:company_id]
  end

  def create
    result = ::Command::Subscription::Decide.new(decide_command_data).call
    if result.success?
      redirect_to result.data[:redirection_url], alert: result.data[:redirection_msg]
    else
      redirect_to new_subscription_path, alert: result.error.message
    end
  end

  def edit
    redirect_to new_subscription_path unless current_user&.pro?
    @subscription = SubscriptionDecorator.decorate(Subscription.new)
    @subscription_form = ::Form::Subscription::Upgrade.new
    @interval = current_user.current_subscription_interval
    @user_current_plan_name = current_user.current_subscription_plan.name
  end

  def confirm
    result = ::Command::Subscription::VerifyUpgrade.new(verify_upgrade_command_data).call
    if result.success? && result.data[:upgrade]
      plan_data_from_result(result)
      find_billing
      render 'confirm'
    else
      redirect_to edit_subscription_path, alert: result.error.message
    end
  end

  def update
    result = Command::Subscription::Update.new(current_user, subscription_upgrade_params).call
    if result.success?
      @plan_name = result.data.humanize.titleize
      render 'user_logged/charges/create'
    else
      redirect_to edit_subscription_path, alert: result.error.message
    end
  end

  private

  def decide_command_data
    [::Form::Subscription::Options.new(permitted_params.merge(commit: params[:commit])),
     session,
     current_user,
     params[:become_admin_company_id]]
  end

  def permitted_params
    params.require(:subscription).permit(:annual, :coupon)
  end

  def verify_upgrade_command_data
    [::Form::Subscription::Upgrade.new(permitted_params.merge(commit: params[:commit])),
     current_user,
     session]
  end

  def find_billing
    @billing = begin
                  Billing.find(session[:current_billing_id]).decorate
                rescue StandardError
                  nil
                end
  end

  def subscription_upgrade_params
    params.permit(:plan_name, :coupon)
  end

  def plan_data_from_result(result)
    @plan = result.data[:plan].decorate
    @plan_total_cost = @plan.total_cost(result.data[:interval])
    case result.data[:interval]
    when 'year'
      @plan_cost = @plan.annual_cost
      @plan_name = @plan.annual_stripe_name
    when 'month'
      @plan_cost = @plan.monthly_cost
      @plan_name = @plan.monthly_stripe_name
    end
  end
end
