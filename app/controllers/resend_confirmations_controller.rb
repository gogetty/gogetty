# frozen_string_literal: true

class ResendConfirmationsController < ApplicationController
  def new
    redirect_to user_logged_walls_path, notice: 'You are signed in.' if user_signed_in?
  end

  def create
    user = User.find_by(email: params[:resend_confirmation][:email])
    user.send_confirmation_instructions unless user&.confirmed?
    redirect_to(
      new_user_session_path,
      notice: 'If you provided correct email, then new confirmation message should be in you inbox.'
    )
  end
end
