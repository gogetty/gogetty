# frozen_string_literal: true

module UserLogged
  class BaseController < ApplicationController
    before_action :authenticate_user!
  end
end
