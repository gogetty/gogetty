# frozen_string_literal: true

module UserLogged
  class SolutionRequestsController < BaseController
    layout 'static'

    def create
      solution_request_form = ::Form::UserLogged::SolutionRequest::New.new(
        create_params.merge(user_id: current_user.id)
      )
      respond(
        ::Command::SolutionRequest::Create.new(solution_request_form).call,
        solution_request_form.back_url || solutions_path,
        solution_request_form.back_url || solutions_path
      )
    end

    private

    def create_params
      params.require(:solution_request).permit(:name, :company, :back_url)
    end
  end
end
