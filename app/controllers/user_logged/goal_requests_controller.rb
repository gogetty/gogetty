# frozen_string_literal: true

module UserLogged
  class GoalRequestsController < BaseController
    def create
      goal_request_form = ::Form::UserLogged::GoalRequest::New.new(
        create_params.merge(user_id: current_user.id)
      )
      respond(
        ::Command::GoalRequest::Create.new(goal_request_form).call,
        user_logged_company_path(Company.find(goal_request_form.company_id)),
        user_logged_company_path(Company.find(goal_request_form.company_id))
      )
    end

    private

    def create_params
      params.require(:goal_request)
            .permit(:company_id)
    end
  end
end
