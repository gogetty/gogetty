
# frozen_string_literal: true

module UserLogged
  class CompanyDataController < ::UserLogged::CompaniesController
    def update
      @company = Company.find(params[:id])
      authorize @company, :company_admin_with_paid_plan?
      form = ::Form::UserLogged::CompanyData::Edit.find(@company.id)
      form.attributes = update_params
      respond(
        ::Command::CompanyData::Update.new(
          form,
          ::UsersPolicy.new(current_user, current_user).platform_admin?
        ).call
      )
    end

    private

    def update_params
      params.require(:company_profile).permit(
        :employees_count_id,
        :liabilities,
        :revenue
      )
    end
  end
end
