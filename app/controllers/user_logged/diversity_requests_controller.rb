# frozen_string_literal: true

module UserLogged
  class DiversityRequestsController < BaseController
    def create
      diversity_request_form = ::Form::UserLogged::DiversityRequest::New.new(
        create_params.merge(user_id: current_user.id)
      )
      respond(
        ::Command::DiversityRequest::Create.new(diversity_request_form).call,
        user_logged_company_path(Company.find(diversity_request_form.company_id)),
        user_logged_company_path(Company.find(diversity_request_form.company_id))
      )
    end

    private

    def create_params
      params.require(:diversity_request)
            .permit(:company_id,
                    :comment)
    end
  end
end
