# frozen_string_literal: true

module UserLogged
  class PlatformSettingsController < BasePlatformController
    layout 'mixed'
    def show; end

    def update
      @platform_settings.update(update_params)
      redirect_to user_logged_platform_settings_path
    end

    private

    def update_params
      params.require(:platform_settings).permit(:google_analytics_head, :google_analytics_body)
    end
  end
end
