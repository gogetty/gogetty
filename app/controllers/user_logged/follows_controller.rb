# frozen_string_literal: true

module UserLogged
  class FollowsController < BaseController
    def create
      follow_form = ::Form::UserLogged::Follow::New.new(
        create_params.merge(user_id: current_user.id)
      )
      respond(
        ::Command::Follow::Toggle.new(follow_form).call,
        user_logged_company_path(follow_form.company_id),
        user_logged_company_path(follow_form.company_id)
      )
    end

    private

    def create_params
      params.require(:follow).permit(:company_id)
    end
  end
end
