# frozen_string_literal: true

module UserLogged
  class CompanyAdminsController < BaseController
    def create
      form = ::Form::Company::BecomeAdmin.new(become_admin_params)
      result = ::Command::Company::BecomeAdmin.new(current_user, form, params[:id]).call
      respond(result)
    end

    private

    def become_admin_params
      params.require(:form_company_become_admin).permit(:acceptance)
    end

    def success_respond_path
      user_has_name = current_user.name.present? && current_user.surname.present?
      user_has_name ? user_logged_company_path(params[:id]) : edit_user_logged_account_settings_path
    end

    def failure_respond_path
      current_user.company_id? ? success_respond_path : user_logged_company_path(params[:id])
    end

    def respond(result)
      super(result, success_respond_path, failure_respond_path)
    end
  end
end
