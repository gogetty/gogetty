# frozen_string_literal: true

module UserLogged
  class AccountSettingsController < BaseController
    layout 'mixed'
    def show
      redirect_to edit_user_logged_account_settings_path
    end

    def edit
      @user = ::Form::UserLogged::AccountSettings::Edit.find(current_user)
    end

    def update
      @user = ::Form::UserLogged::AccountSettings::Edit.find(current_user)
      @user.attributes = update_user_params
      result = ::Command::AccountSettings::Update.new(current_user, @user).call
      if result.success?
        respond_success(result.data, edit_user_logged_account_settings_path)
      else
        flash[:error] = 'Invalid data in form prohibited it from saving!'
        render :edit
      end
    end

    private

    def update_user_params
      params.require(:user).permit(:avatar,
                                   :remove_avatar,
                                   :name,
                                   :surname,
                                   :email,
                                   :company_name,
                                   :title,
                                   :company_representative,
                                   :newsletter_subscription)
    end
  end
end
