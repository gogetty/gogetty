# frozen_string_literal: true

module UserLogged
  class CompaniesDiversityPoliciesController < ::UserLogged::CompaniesController
    def create
      @company = Company.find(params[:companies_diversity_policies][:company_id])
      authorize @company, :company_admin?
      form = ::Form::UserLogged::CompaniesDiversityPolicy::New.new(create_params)
      respond(::Command::CompaniesDiversityPolicy::Create.new(form).call)
    end

    def destroy
      companies_diversity_policy = CompaniesDiversityPolicy.find(params[:id])
      @company = companies_diversity_policy.company
      authorize @company, :company_admin?
      companies_diversity_policy.destroy
      respond_success('Deleted successfully.')
    end

    private

    def create_params
      params.require(:companies_diversity_policies).permit(:diversity_policy_id, :company_id)
    end
  end
end
