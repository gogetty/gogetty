# frozen_string_literal: true

module UserLogged
  class GoalsWaysController < ::UserLogged::CompaniesController
    def create
      @company = Company.find(params[:goals_ways][:company_id])
      authorize @company, :company_admin?
      form = ::Form::UserLogged::GoalsWay::New.new(create_params)
      respond(::Command::GoalsWay::Create.new(form).call)
    end

    def destroy
      goals_way = GoalsWay.find(params[:id])
      @company = goals_way.company
      authorize @company, :company_admin?
      goals_way.destroy
      respond_success('Deleted successfully.')
    end

    private

    def create_params
      params.require(:goals_ways).permit(:description, :company_id)
    end
  end
end
