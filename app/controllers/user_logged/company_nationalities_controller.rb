# frozen_string_literal: true

module UserLogged
  class CompanyNationalitiesController < ::UserLogged::CompaniesController
    def create
      @company = Company.find(create_params[:company_id])
      authorize @company, :company_admin_with_inclusive_plan?

      form = ::Form::UserLogged::CompanyNationality::New.new(create_params)

      respond(::Command::CompanyNationality::Create.new(form).call)
    end

    def update
      @company = Company.find(params[:id])
      authorize @company, :company_admin_with_inclusive_plan?

      form = ::Form::UserLogged::CompanyNationality::Edit.find(@company.id)

      respond(::Command::CompanyNationality::Update.new(
        form,
        update_params,
        ::UsersPolicy.new(current_user, current_user).platform_admin?
      ).call)
    end

    def destroy
      nationality = CompanyNationality.find(params[:id])

      @company = nationality.company
      authorize @company, :company_admin_with_inclusive_plan?

      respond(::Command::CompanyNationality::Destroy.new(nationality).call)
    end

    private

    def create_params
      params.require(:company_nationality).permit(:company_id, :nationality_id, :count)
    end

    def update_params
      parameters = []
      params.require(:nationalities).each_pair do |_key, value|
        parameters << value.permit(:count)
      end
      parameters
    end

    def respond_success(message, path)
      respond_to do |format|
        format.json do
          @nationalities = EthnicityDecorator.decorate(@company.nationalities)
          @nationalities_new_form = ::Form::UserLogged::CompanyNationality::New.new
          @nationalities_edit_form = ::Form::UserLogged::CompanyNationality::Edit.find(@company.id)
          render status: :ok, html: render_to_string(
            '/user_logged/companies/_edit_ethnicity_nationalities.html', layout: false
          )
        end
        format.html { redirect_to path, notice: message }
      end
    end
  end
end
