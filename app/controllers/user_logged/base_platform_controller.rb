# frozen_string_literal: true

module UserLogged
  class BasePlatformController < BaseController
    before_action :check_if_admin

    private

    def check_if_admin
      redirect_to root_path, flash: {error: 'Access denied!'} unless policy(:users).platform_admin?
    end
  end
end
