# frozen_string_literal: true

module UserLogged
  class CompanyInformationRequestsController < BaseController
    def create
      company_information_request_form = ::Form::UserLogged::CompanyInformationRequest::New.new(
        create_params.merge(user_id: current_user.id)
      )

      respond(
        ::Command::CompanyInformationRequest::Create.new(company_information_request_form).call,
        user_logged_company_path(Company.find(company_information_request_form.company_id)),
        user_logged_company_path(Company.find(company_information_request_form.company_id))
      )
    end

    private

    def create_params
      params.require(:company_information_request).permit(:company_id, :comment)
    end
  end
end
