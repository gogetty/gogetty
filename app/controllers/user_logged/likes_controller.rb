# frozen_string_literal: true

module UserLogged
  class LikesController < BaseController
    def create
      @form = ::Form::UserLogged::Like::New.new(create_params.merge(created_by: current_user.id))
      result = ::Command::Like::Toggle.new(@form).call
      respond(result)
    end

    private

    def create_params
      params.require(:like).permit(:post_id)
    end

    def respond(result)
      super(
        result,
        user_logged_company_path(Post.find(params[:like][:post_id]).company.id),
        user_logged_company_path(Post.find(params[:like][:post_id]).company.id)
      )
    end

    def respond_success(message, path)
      respond_to do |format|
        format.json do
          @post = Post.find(@form.post_id)
          @current_user_likes = current_user.likes.pluck(:post_id)
          render status: :ok, html: render_to_string(
            'user_logged/posts/_likes.html', locals: {post: @post}, layout: false
          )
        end
        format.html { redirect_to path, notice: message }
      end
    end
  end
end
