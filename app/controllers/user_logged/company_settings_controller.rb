# frozen_string_literal: true

module UserLogged
  class CompanySettingsController < BaseController
    layout 'mixed'
    before_action :authorize_user
    before_action :find_company_form, only: %w[edit update]

    rescue_from Pundit::NotAuthorizedError do |exception|
      if exception.query == :company_admin_with_paid_plan?
        flash[:error] = 'You have to purchase paid subscription to edit your company details.'
        redirect_to new_subscription_path
      else
        flash[:error] = 'You are not authorized to do that.'
        redirect_to root_path
      end
    end

    def show; end

    def edit; end

    def update
      authorize current_user.company, :company_admin_with_paid_plan?
      @company.attributes = update_company_params
      result = ::Command::CompanySettings::Update.new(current_user.company, @company).call
      if result.success?
        respond_success(result.data, edit_user_logged_company_settings_path)
      else
        render_error 'Invalid data in form prohibited it from saving!', :edit
      end
    end

    private

    def find_company_form
      @company = ::Form::UserLogged::CompanySettings::Edit.find(current_user.company)
    end

    def authorize_user
      authorize :users, :any_company_admin?
    end

    def update_company_params
      params.require(:company).permit(:logo,
                                      :remove_logo,
                                      :name,
                                      :industry_id,
                                      :employees_count_id,
                                      :website,
                                      :country_id,
                                      :city,
                                      :founded_year,
                                      :stock_listed,
                                      :cvr_number,
                                      :owner_type_id)
    end
  end
end
