# frozen_string_literal: true

module UserLogged
  class WallsController < BaseController
    def show
      @gogetty_id = find_gogetty_profile
      @posts = posts
      @gogetty_posts = gogetty_posts
      append_seen_posts if @gogetty_posts
      @featured = ::Query::Wall::Featured.new.call.data
      @followed = ::Query::Wall::Followed.new(current_user).call.data
      @post_edit_forms = post_edit_forms
      @current_user_likes = current_user_likes
    end

    private

    def current_user_likes
      current_user.likes.pluck(:post_id)
    end

    def post_edit_forms
      posts = @posts + @gogetty_posts
      posts.map { |post| [post.id, ::Form::UserLogged::Post::Edit.find(post.id)] }.to_h
    end

    def seen_posts
      cookies[:seen_posts] || '[]'
    end

    def seen_posts_json
      JSON.parse(seen_posts)
    rescue JSON::ParserError
      return []
    end

    def append_seen_posts
      cookies[:seen_posts] = seen_posts_json
                             .append(@gogetty_posts.pluck(:id))
                             .flatten.uniq.to_s
    end

    def posts
      Post.all.order(created_at: :desc).page(params[:page]).per(5)
    end

    def gogetty_posts
      ::Query::Wall::GogettyPosts.new(seen_posts_json, @gogetty_id).call.data
    end

    def find_gogetty_profile
      Company.find_by(name: 'GoGetty IVS')&.id
    end
  end
end
