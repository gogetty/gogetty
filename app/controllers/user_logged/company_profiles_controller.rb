# frozen_string_literal: true

module UserLogged
  class CompanyProfilesController < ::UserLogged::CompaniesController
    def update
      @company = Company.find(params[:id])
      authorize @company, :company_admin_with_paid_plan?
      form = ::Form::UserLogged::CompanyProfile::Edit.find(@company.id)
      form.attributes = update_params
      respond(
        ::Command::CompanyProfile::Update.new(
          form,
          ::UsersPolicy.new(current_user, current_user).platform_admin?
        ).call
      )
    end

    private

    def update_params # rubocop:disable MethodLength
      up = params.require(:company_profile).permit(
        :now_director_woman_count,
        :now_director_man_count,
        :goal_director_woman_count,
        :goal_director_man_count,
        :now_cxo_woman_count,
        :now_cxo_man_count,
        :goal_cxo_woman_count,
        :goal_cxo_man_count,
        :now_manager_woman_count,
        :now_manager_man_count,
        :goal_manager_woman_count,
        :goal_manager_man_count,
        :now_vp_woman_count,
        :now_vp_man_count,
        :goal_vp_woman_count,
        :goal_vp_man_count,
        :now_middle_man_count,
        :now_middle_woman_count,
        :goal_middle_man_count,
        :goal_middle_woman_count,
        :director_hide,
        :cxo_hide,
        :vp_hide,
        :middle_hide,
        :director_name,
        :cxo_name,
        :vp_name,
        :middle_name,
        :goal_year,
        :director_in_percenatge,
        :cxo_in_percenatge,
        :middle_in_percenatge,
        :vp_in_percenatge,
        :now_director_woman_percentage,
        :now_director_man_percentage,
        :now_director_total,
        :goal_director_total,
        :goal_director_woman_percentage,
        :goal_director_man_percentage,
        :now_cxo_woman_percentage,
        :now_cxo_man_percentage,
        :now_cxo_total,
        :goal_cxo_total,
        :goal_cxo_woman_percentage,
        :goal_cxo_man_percentage,
        :now_vp_woman_percentage,
        :now_vp_man_percentage,
        :now_vp_total,
        :goal_vp_total,
        :goal_vp_woman_percentage,
        :goal_vp_man_percentage,
        :now_middle_woman_percentage,
        :now_middle_man_percentage,
        :now_middle_total,
        :goal_middle_total,
        :goal_middle_woman_percentage,
        :goal_middle_man_percentage
      )
      up.each { |k, v| up[k] = 0 if v.blank? }
    end
  end
end
