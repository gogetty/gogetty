# frozen_string_literal: true

module UserLogged
  module Solutions
    class IntrodusController < UserLogged::SolutionsController; end
  end
end
