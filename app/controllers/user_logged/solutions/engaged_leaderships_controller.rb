# frozen_string_literal: true

module UserLogged
  module Solutions
    class EngagedLeadershipsController < UserLogged::SolutionsController; end
  end
end
