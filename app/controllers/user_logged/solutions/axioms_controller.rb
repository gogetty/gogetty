# frozen_string_literal: true

module UserLogged
  module Solutions
    class AxiomsController < UserLogged::SolutionsController; end
  end
end
