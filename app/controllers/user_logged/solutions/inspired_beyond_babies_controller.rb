# frozen_string_literal: true

module UserLogged
  module Solutions
    class InspiredBeyondBabiesController < UserLogged::SolutionsController; end
  end
end
