# frozen_string_literal: true

module UserLogged
  module Solutions
    class PotentialCompaniesController < UserLogged::SolutionsController; end
  end
end
