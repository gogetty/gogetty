# frozen_string_literal: true

module UserLogged
  class CompaniesController < BaseController
    def new; end

    def create
      form = ::Form::UserLogged::Company::New.new(create_params)
      respond(::Command::Company::Create.new(form, current_user).call)
    end

    def edit; end

    def update; end

    def show
      vars
      ethnicity_vars
      forms_for_visitor unless policy(@company).company_admin?
      forms_for_user if user_signed_in?
      forms_for_admin if policy(@company).company_admin?
      forms_for_superadmin if policy(@company).company_superadmin?
      render :show
    end

    private

    def vars
      @company = Company.friendly.find(params[:id]).decorate
      @company_profile = @company.company_profile.decorate
      @company_information = @company.information.decorate
      @posts = @company.posts.latest_five
      @current_user_likes = current_user.likes.pluck(:post_id)
    end

    def ethnicity_vars
      @languages = EthnicityDecorator.decorate(@company.company_languages)
      @nationalities = EthnicityDecorator.decorate(@company.company_nationalities)
    end

    def company_for_tour
      Company.find_by(name: 'GoGetty IVS').decorate
    rescue StandardError
      Company.joins(:company_profile)
             .order('company_profiles.diversity_score DESC').first.decorate
    end

    def forms_for_visitor
      @become_admin_form = ::Form::Company::BecomeAdmin.new
    end

    def forms_for_user
      @company_information_request_form = ::Form::UserLogged::CompanyInformationRequest::New.new
    end

    def forms_for_admin
      ethnicity_forms
      post_forms
      @company_profile_form = ::Form::UserLogged::CompanyProfile::Edit.find(@company.id)
      @company_data_form = ::Form::UserLogged::CompanyData::Edit.find(@company.id)
      @company_information_form = ::Form::UserLogged::CompanyInformation::Edit.find(@company.id)
    end

    def post_forms
      @post_form = ::Form::UserLogged::Post::New.new
      @post_edit_forms = post_edit_forms
    end

    def ethnicity_forms
      @nationalities_new_form = ::Form::UserLogged::CompanyNationality::New.new
      @nationalities_edit_form = ::Form::UserLogged::CompanyNationality::Edit.find(@company.id)
      @languages_new_form = ::Form::UserLogged::CompanyLanguage::New.new
      @languages_edit_form = ::Form::UserLogged::CompanyLanguage::Edit.find(@company.id)
    end

    def post_edit_forms
      @posts.map { |post| [post.id, ::Form::UserLogged::Post::Edit.find(post.id)] }.to_h
    end

    def forms_for_superadmin
      @create_admin = ::Form::UserLogged::CompanySettings::AdminInvites::New.new
    end

    def create_params
      params.require(:company).permit(:name, :acceptance)
    end

    def respond(result)
      super(result,
            user_logged_company_path(@company),
            user_logged_company_path(@company))
    rescue StandardError
      super(result, company_searches_path, company_searches_path)
    end

    def respond_success(message, path = nil)
      if path
        super(message, path)
      else
        super(message, user_logged_company_path(@company))
      end
    end

    def respond_error(error, path = nil)
      if path
        super(error, path)
      else
        super(error, user_logged_company_path(@company))
      end
    end
  end
end
