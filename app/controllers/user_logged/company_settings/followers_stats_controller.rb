# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class FollowersStatsController < BaseController
      layout 'mixed'
      def index
        authorize :users, :any_company_admin?
        @followers = current_user.company.follows
        @followers_total_count = @followers.count
        @followers_today_count = @followers.today.count
        @followers_week_count = @followers.week.count
        @followers_month_count = @followers.month.count
        @followers_year_count = @followers.year.count
      end
    end
  end
end
