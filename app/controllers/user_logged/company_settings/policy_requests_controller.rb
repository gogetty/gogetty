# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class PolicyRequestsController < BaseController
      layout 'mixed'
      def index
        authorize :users, :any_company_admin?
        @policy_requests = current_user.company
                                       .policy_requests
                                       .order(created_at: :desc)
                                       .page(params[:page])
      end
    end
  end
end
