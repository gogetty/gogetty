# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class GoalRequestsController < BaseController
      layout 'mixed'
      def index
        authorize :users, :any_company_admin?
        @goal_requests = current_user.company.goal_requests.order(created_at: :desc).page(params[:page])
      end
    end
  end
end
