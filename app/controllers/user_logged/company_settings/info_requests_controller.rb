# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class InfoRequestsController < BaseController
      layout 'mixed'
      def index
        authorize :users, :any_company_admin?
        @requests = current_user.company.company_information_requests.order(created_at: :desc)
        @requests_list = @requests.page(params[:page])
      end
    end
  end
end
