# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class AdminRequestsController < BaseController
      def update
        authorize :users, :any_company_superadmin?
        authorize AdminRequest.find(params[:id]).company, :company_superadmin?
        result = ::Command::CompanySettings::AdminRequests::Update.new(params).call
        update_respond(result)
      end

      def destroy
        authorize :users, :any_company_superadmin?
        authorize AdminRequest.find(params[:id]).company, :company_superadmin?
        result = ::Command::CompanySettings::AdminRequests::Revoke.new(params).call
        update_respond(result)
      end

      private

      def update_respond(result)
        respond(result,
                user_logged_company_settings_admins_path,
                user_logged_company_settings_admins_path)
      end
    end
  end
end
