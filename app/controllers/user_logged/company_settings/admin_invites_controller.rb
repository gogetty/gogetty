# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class AdminInvitesController < BaseController
      protect_from_forgery except: :create
      def create
        @company = Company.find(params[:admin_invite][:company_id])
        authorize @company, :company_superadmin?
        form = ::Form::UserLogged::CompanySettings::AdminInvites::New.new(create_admin_params)
        result = ::Command::CompanySettings::AdminInvites::Create.new(form, params).call
        create_respond(result)
      end

      def update
        authorize :users, :any_company_superadmin?
        authorize AdminInvite.find(params[:id]).company, :company_superadmin?
        result = ::Command::CompanySettings::AdminInvites::Update.new(params).call
        update_respond(result)
      end

      private

      def create_admin_params
        params.require(:admin_invite).permit(:email, :user_id, :company_id)
      end

      def create_respond(result)
        respond(result,
                user_logged_company_path(@company),
                user_logged_company_path(@company))
      end

      def update_admin_params
        params.require(:user).permit(:superadmin)
      end

      def update_respond(result)
        respond(result,
                user_logged_company_settings_admins_path,
                user_logged_company_settings_admins_path)
      end
    end
  end
end
