# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class DiversityRequestsController < BaseController
      layout 'mixed'
      def index
        authorize :users, :any_company_admin?
        @diversity_requests = current_user.company
                                          .diversity_requests
                                          .order(created_at: :desc)
                                          .page(params[:page])
      end
    end
  end
end
