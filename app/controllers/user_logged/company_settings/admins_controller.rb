# frozen_string_literal: true

module UserLogged
  module CompanySettings
    class AdminsController < BaseController
      layout 'mixed'
      def index
        authorize :users, :any_company_superadmin?
        @admins = current_user.company.admins.order(:surname, :name)
        admin_invites_set
        admin_requests_set
      end

      def create
        authorize :users, :any_company_superadmin?
        authorize current_user.company, :company_superadmin?
        form = ::Form::UserLogged::CompanySettings::Admins::New.new(create_admin_params)
        result = ::Command::CompanySettings::Admins::Create.new(form, params, current_user).call
        create_respond(result)
      end

      def update
        authorize :users, :any_company_superadmin?
        authorize current_user.company, :company_superadmin?
        form = ::Form::UserLogged::CompanySettings::Admins::Edit.find(params)
        form.attributes = update_admin_params
        result = ::Command::CompanySettings::Admins::Update.new(form, params, current_user).call
        update_respond(result)
      end

      def destroy
        authorize :users, :any_company_superadmin?
        authorize current_user.company, :company_superadmin?
        result = ::Command::CompanySettings::Admins::Destroy.new(params, current_user).call
        update_respond(result)
      end

      private

      def create_admin_params
        params.require(:admin_invite).permit(:email, :user_id)
      end

      def create_respond(result)
        respond(result,
                user_logged_company_path(current_user.company_id),
                user_logged_company_path(current_user.company_id))
      end

      def update_admin_params
        params.require(:user).permit(:superadmin)
      end

      def update_respond(result)
        respond(result,
                user_logged_company_settings_admins_path,
                user_logged_company_settings_admins_path)
      end

      def admin_invites_set
        @admin_invites = current_user.company
                                     .admin_invites
                                     .ordered
                                     .decorate
      end

      def admin_requests_set
        @admin_requests = current_user.company
                                      .admin_requests
                                      .ordered
                                      .decorate
      end
    end
  end
end
