# frozen_string_literal: true

module UserLogged
  class SolutionsController < BaseController
    layout 'static'

    def show; end
  end
end
