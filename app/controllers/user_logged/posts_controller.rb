# frozen_string_literal: true

module UserLogged
  class PostsController < CompaniesController
    skip_before_action :authenticate_user!, only: :show
    layout 'post_show', only: :show

    def show
      @post = Post.find(params[:id])
      @company = @post.company.decorate
      redirect_to user_logged_company_path(@post.company, anchor: "post_#{@post.id}") if current_user
    end

    def create
      @company = current_user.company
      authorize @company, :company_admin_with_paid_plan?
      form = ::Form::UserLogged::Post::New.new(
        create_params.merge(
          created_by: current_user.id,
          company_id: current_user.company_id
        )
      )
      result = ::Command::Post::Create.new(form).call
      respond(result)
    end

    def update
      @post = Post.find(params[:id])
      authorize @post, :can_destroy?

      form = ::Form::UserLogged::Post::Edit.find(@post.id)
      form.attributes = update_params

      result = ::Command::Post::Update.new(form).call
      @company = @post.company
      respond(result)
    end

    def destroy
      @post = Post.find(params[:id])
      authorize @post, :can_destroy?
      result = ::Command::Post::Destroy.new(@post).call
      @company = @post.company
      respond(result)
    end

    private

    def create_params
      params.require(:post).permit(:title, :text, :photo, :video)
    end

    def update_params
      params.require(:post).permit(:title, :text, :remove_photo, :photo, :video, :remove_video)
    end

    def post_edit_forms
      @posts.map { |post| [post.id, ::Form::UserLogged::Post::Edit.find(post.id)] }.to_h
    end

    def respond_success(message, _path)
      respond_to do |format|
        format.json do
          @company = current_user.company
          @posts = @company.posts.latest_five
          @post_form = ::Form::UserLogged::Post::New.new
          @post_edit_forms = post_edit_forms
          render status: :ok, html: render_to_string(
            '/user_logged/companies/_show_stream.html', layout: false
          )
        end
        format.html { redirect_to request.referer, notice: message }
      end
    end
  end
end
