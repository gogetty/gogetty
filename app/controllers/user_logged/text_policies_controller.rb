# frozen_string_literal: true

module UserLogged
  class TextPoliciesController < ::UserLogged::CompaniesController
    def create
      @company = Company.find(params[:text_policies][:company_id])
      authorize @company, :company_admin?
      form = ::Form::UserLogged::TextPolicy::New.new(create_params)
      respond(::Command::TextPolicy::Create.new(form).call)
    end

    def destroy
      text_policy = TextPolicy.find(params[:id])
      @company = text_policy.company
      authorize @company, :company_admin?
      text_policy.destroy
      respond_success('Deleted successfully.')
    end

    private

    def create_params
      params.require(:text_policies).permit(:name, :company_id)
    end
  end
end
