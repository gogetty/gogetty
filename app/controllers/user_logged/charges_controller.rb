# frozen_string_literal: true

module UserLogged
  class ChargesController < BaseController
    before_action :find_billing

    def new
      # if current_user.pro?
      #   redirect_to new_subscription_path,
      #               error: 'You already have an active subscription.'
      # end
      # write user_id if billing was started before singup/signin
      @billing.user_id = current_user.id unless @billing.user_id
      @billing.save!
      @plan = ::PlanDecorator.decorate(@billing.plan)
      @become_admin_company_id = params[:company_id]
    end

    def create
      @plan_name = ::PlanDecorator.decorate(@billing.plan).name
      result = charge_payment
      become_admin_form
      raise result.error unless result.success?
    rescue Stripe::CardError => e
      redirect_to new_user_logged_charge_path, alert: e.message
    rescue StandardError => e
      redirect_to root_path, alert: e.message
    end

    private

    def become_admin_form
      @become_admin_form = ::Form::Company::BecomeAdmin.new
      @company_id = params[:become_admin_company_id]
    end

    def charge_payment
      ::Command::Charge::Payment.new(current_user, session, params).call
    end

    def find_billing
      @billing = begin
                   Billing.find(session[:current_billing_id]).decorate
                 rescue StandardError
                   nil
                 end
    end
  end
end
