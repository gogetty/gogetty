
# frozen_string_literal: true

module UserLogged
  class CompanyInformationsController < ::UserLogged::CompaniesController
    def update
      @company = Company.friendly.find(params[:id])
      authorize @company, :company_admin?
      form = ::Form::UserLogged::CompanyInformation::Edit.find(@company.id)
      form.attributes = update_params
      respond(::Command::CompanyInformation::Update.new(form).call)
    end

    def update_params
      params.require(:company_information).permit(:first_video,
                                                  :second_video,
                                                  :diversity_plans_title,
                                                  :gender_diversity_plans,
                                                  :ethnicity_diversity_plans,
                                                  :contact)
    end
  end
end
