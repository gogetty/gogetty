# frozen_string_literal: true

module UserLogged
  class CouponsController < BaseController
    def create
      form = ::Form::UserLogged::Coupon::New.new
      form.attributes = redeem_params
      result = ::Command::Coupon::Redeem.new(form, current_user).call
      respond(result)
    end

    private

    def redeem_params
      params.require(:coupon).permit(:code)
    end

    def respond(result)
      super(result, new_subscription_path, new_subscription_path)
    end
  end
end
