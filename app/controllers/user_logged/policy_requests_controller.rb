# frozen_string_literal: true

module UserLogged
  class PolicyRequestsController < BaseController
    def create
      policy_request_form = ::Form::UserLogged::PolicyRequest::New.new(
        create_params.merge(user_id: current_user.id)
      )
      respond(
        ::Command::PolicyRequest::Create.new(policy_request_form).call,
        user_logged_company_path(Company.find(policy_request_form.company_id)),
        user_logged_company_path(Company.find(policy_request_form.company_id))
      )
    end

    private

    def create_params
      params.require(:policy_request)
            .permit(:company_id,
                    :comment,
                    :type,
                    diversity_policies_policy_requests_attributes: %i[id
                                                                      diversity_policy_id
                                                                      _destroy])
    end
  end
end
