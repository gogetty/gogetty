# frozen_string_literal: true

module UserLogged
  module AccountSettings
    class PaymentCredentialsController < BaseController
      layout 'mixed'

      def new
        redirect_to root_path if current_user.stripe_customer_id.blank?
      end

      # Changes made by James Wilson without applying Rubocop rules
      # rubocop:disable all
      def create
        if current_user.stripe_customer_id.present?
          begin
            customer = Stripe::Customer.retrieve(current_user.stripe_customer_id)
            card = customer.sources.create(source: params[:stripeToken])
            card.save
            customer.default_source = card.id
            if customer.save
              if current_user.payment_credential.present?
                current_user.payment_credential.update_attributes!(
                  last4: card.last4, exp_month: card.exp_month,
                  exp_year: card.exp_year, card_type: card.brand
                )
                flash[:success] = 'Card updated successfully'
                redirect_to new_user_logged_account_settings_payment_credential_path
              else
                params[:card_type] = card.brand
                params[:last4] = card.last4
                params[:stripe_customer_id] = current_user.stripe_customer_id
                params[:user_id] = current_user.id
                form = ::Form::UserLogged::AccountSettings::PaymentCredentials::New.new(create_params)
                respond(::Command::PaymentCredentials::Create.new(form).call,
                        new_user_logged_account_settings_payment_credential_path,
                        new_user_logged_account_settings_payment_credential_path)
              end
            else
              flash[:error] = 'Stripe reported an error while updating your card. Please try again.'
            end
          rescue StandardError => e
            flash[:error] = 'Stripe reported an error while updating your card. Please try again.'
            redirect_to new_user_logged_account_settings_payment_credential_path
          end
        else
          flash[:error] = 'Stripe reported an error while updating your card. Please try again.'
        end
      end
      # rubocop:enabledisable all

      def edit; end

      def update; end

      private

      def create_params
        params.permit(:last4, :exp_month, :stripe_customer_id, :user_id, :exp_year, :card_type)
      end
    end
  end
end
