# frozen_string_literal: true

module UserLogged
  module AccountSettings
    class BillingsController < BaseController
      layout 'mixed'
      def index
        @billings = current_user.billings.order(updated_at: :desc).decorate
      end
    end
  end
end
