# frozen_string_literal: true

module UserLogged
  module AccountSettings
    class UpcomingPaymentsController < BaseController
      layout 'mixed'
      def index
        result = ::Query::Billing::FindUpcoming.new(current_user).call
        if result.success?
          @upcoming_payment = result.data
        else
          respond_error(result.error, user_logged_account_settings_path)
        end
      end
    end
  end
end
