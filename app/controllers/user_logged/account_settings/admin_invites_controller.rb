# frozen_string_literal: true

module UserLogged
  module AccountSettings
    class AdminInvitesController < BaseController
      layout 'mixed'
      def index
        @admin_invites = AdminInvite.for_user(current_user).ordered.decorate
      end

      def update
        result = ::Command::AccountSettings::AdminInvites::Update.new(params, current_user).call
        update_respond(result)
      end

      private

      def update_respond(result)
        respond(result,
                user_logged_account_settings_admin_invites_path,
                user_logged_account_settings_admin_invites_path)
      end
    end
  end
end
