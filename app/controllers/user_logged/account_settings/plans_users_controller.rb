# frozen_string_literal: true

module UserLogged
  module AccountSettings
    class PlansUsersController < BaseController
      layout 'mixed'
      def index
        @plans_users = current_user
                       .plans_users
                       .includes(:plan)
                       .includes(:billing)
                       .order(updated_at: :desc)
                       .decorate
      end

      def destroy
        # XXX: check if owner
        result = ::Command::UserPlan::Cancel.new(current_user, params[:id]).call
        if result.success?
          redirect_to user_logged_account_settings_plans_users_path, notice: result.data
        else
          redirect_to root_path, alert: result.error.message
        end
      end
    end
  end
end
