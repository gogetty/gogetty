# frozen_string_literal: true

module UserLogged
  class CompanyLanguagesController < ::UserLogged::CompaniesController
    def create
      @company = Company.find(create_params[:company_id])
      authorize @company, :company_admin_with_inclusive_plan?

      form = ::Form::UserLogged::CompanyLanguage::New.new(create_params)

      respond(::Command::CompanyLanguage::Create.new(form).call)
    end

    def update
      @company = Company.find(params[:id])
      authorize @company, :company_admin_with_inclusive_plan?

      form = ::Form::UserLogged::CompanyLanguage::Edit.find(@company.id)

      respond(::Command::CompanyLanguage::Update.new(
        form,
        update_params,
        ::UsersPolicy.new(current_user, current_user).platform_admin?
      ).call)
    end

    def destroy
      language = CompanyLanguage.find(params[:id])

      @company = language.company
      authorize @company, :company_admin_with_inclusive_plan?

      respond(::Command::CompanyLanguage::Destroy.new(language).call)
    end

    private

    def create_params
      params.require(:company_language).permit(:company_id, :language_id, :count)
    end

    def update_params
      parameters = []
      params.require(:languages).each_pair do |_key, value|
        parameters << value.permit(:count)
      end
      parameters
    end

    def respond_success(message, path)
      respond_to do |format|
        format.json do
          @languages = EthnicityDecorator.decorate(@company.languages)
          @languages_new_form = ::Form::UserLogged::CompanyLanguage::New.new
          @languages_edit_form = ::Form::UserLogged::CompanyLanguage::Edit.find(@company.id)
          render status: :ok, html: render_to_string(
            '/user_logged/companies/_edit_ethnicity_languages.html', layout: false
          )
        end
        format.html { redirect_to path, notice: message }
      end
    end
  end
end
