# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class RequestsController < BaseController
      layout 'mixed'
      def index
        if session[:requests_company_id].present?
          vars_by_company
        else
          @policy_requests = PolicyRequest.all
          @goal_requests = GoalRequest.all
          @diversity_requests = DiversityRequest.all
          @company_information_requests = CompanyInformationRequest.all
          @solution_requests = solution_requests
          @company_requests = company_requests
        end
      end

      private

      def solution_requests
        SolutionRequest.all.order(created_at: :desc).page(params[:page])
      end

      def company_requests
        CompanyRequest.all.order(created_at: :desc).page(params[:page])
      end

      def vars_by_company
        company_id = session[:requests_company_id]
        @policy_requests = PolicyRequest.where(company_id: company_id)
        @goal_requests = GoalRequest.where(company_id: company_id)
        @diversity_requests = DiversityRequest.where(company_id: company_id)
        @company_information_requests = CompanyInformationRequest.where(company_id: company_id)
        @company_requests = CompanyRequest.where(id: -1).page(params[:page])
      end
    end
  end
end
