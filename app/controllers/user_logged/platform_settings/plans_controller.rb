# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class PlansController < BaseController
      layout 'mixed'
      def index
        @plans = Plan.all.decorate
      end

      def edit
        @plan = ::Form::UserLogged::PlatformSettings::Plans::Edit.find(Plan.find(params[:id]))
      end

      def update
        @plan = ::Form::UserLogged::PlatformSettings::Plans::Edit.find(Plan.find(params[:id]))
        @plan.attributes = update_plan_params
        result = ::Command::Plan::Update.new(@plan, params).call
        if result.success?
          respond_success(result.data, user_logged_platform_settings_plans_path)
        else
          flash[:error] = 'Invalid data in form prohibited it from saving!'
          render :edit
        end
      end

      private

      def update_plan_params
        params.require(:plan)
              .permit(:monthly_cost, :monthly_stripe_name, :annual_cost, :annual_stripe_name, :tax)
      end
    end
  end
end
