# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class RequestCompaniesController < BaseController
      def create
        session[:requests_company_id] = params[:company][:company_id]
        session[:requests_company_id] = nil if params[:company][:company_id] == '-1'
        redirect_to request.referer
      end
    end
  end
end
