# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class CompanyInformationRequestsController < BaseController
      layout 'mixed'
      def index
        company_id = session[:requests_company_id]
        @company_information_requests = if company_id.present?
                                          CompanyInformationRequest.where(company_id: company_id)
                                                                   .order(created_at: :desc)
                                                                   .page(params[:page])
                                        else
                                          CompanyInformationRequest.all
                                                                   .order(created_at: :desc)
                                                                   .page(params[:page])
                                        end
      end
    end
  end
end
