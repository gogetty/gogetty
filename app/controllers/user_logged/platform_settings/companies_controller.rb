# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class CompaniesController < BaseController
      layout 'mixed'
      def index # rubocop:disable Metrics/AbcSize
        @companies = Company.all.order(:name).page(params[:page])

        respond_to do |format|
          format.html
          format.csv do
            exported_companies = ::Query::Companies::ExportedCompanies.new.call.data
            with_history = params[:with_history] == 'true'
            send_data exported_companies.to_csv(with_history)
          end
        end
      end

      def show
        @company = Company.friendly.find(params[:id])
      end

      def new
        @company = ::Form::UserLogged::Company::New.new
      end

      def create
        @company = ::Form::UserLogged::Company::New.new
        @company.attributes = create_company_params.merge(acceptance: true)
        result = Command::Company::Create.new(@company, current_user).call
        respond(result, '/user_logged/platform_settings/companies/new')
      end

      def edit
        @company = ::Form::UserLogged::CompanySettings::Edit.find(Company.friendly.find(params[:id]))
      end

      def update
        company = Company.friendly.find(params[:id])
        @company = ::Form::UserLogged::CompanySettings::Edit.find(company)
        @company.attributes = update_company_params
        result = ::Command::CompanySettings::Update.new(company, @company, current_user).call
        respond(result, '/user_logged/platform_settings/companies/edit')
      end

      def destroy
        company = Company.friendly.find(params[:id])
        User.where(company_id: company.id).update(company_id: nil)
        company.destroy
        redirect_to user_logged_platform_settings_companies_path, notice: 'Company deleted.'
      end

      private

      def create_company_params
        params.require(:company).permit(:name)
      end

      def update_company_params
        params.require(:company).permit(:logo,
                                        :remove_logo,
                                        :name,
                                        :industry_id,
                                        :employees_count_id,
                                        :website,
                                        :city,
                                        :country_id,
                                        :founded_year,
                                        :owner_type_id,
                                        :always_visible,
                                        :stock_listed,
                                        :cvr_number)
      end

      def respond(result, partial)
        if result.success?
          respond_success(result.data, user_logged_platform_settings_companies_path)
        else
          flash[:error] = result.error.to_s
          render partial
        end
      end
    end
  end
end
