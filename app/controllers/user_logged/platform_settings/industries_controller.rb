# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class IndustriesController < BaseController
      layout 'mixed'
      def index
        @industries = Industry.all.order(:name)
      end

      def new
        @industry = ::Form::UserLogged::PlatformSettings::Industries::New.new
      end

      def create
        @industry = ::Form::UserLogged::PlatformSettings::Industries::New.new
        @industry.attributes = industry_params
        result = Command::Industry::Create.new(@industry).call
        respond(result, '/user_logged/platform_settings/industries/new')
      end

      def edit
        @industry = ::Form::UserLogged::PlatformSettings::Industries::Edit.find(
          Industry.find(params[:id])
        )
      end

      def update
        industry = Industry.find(params[:id])
        @industry = ::Form::UserLogged::PlatformSettings::Industries::Edit.find(
          industry
        )
        @industry.attributes = industry_params
        result = ::Command::Industry::Update.new(@industry, industry).call
        respond(result, '/user_logged/platform_settings/industries/edit')
      end

      def destroy
        industry = Industry.find(params[:id])
        result = ::Command::Industry::Destroy.new(industry).call
        if result.success?
          flash[:success] = 'Industry deleted.'
        else
          flash[:error] = result.error
        end
        redirect_to user_logged_platform_settings_industries_path
      end

      private

      def industry_params
        params.require(:industry).permit(:name)
      end

      def respond(result, partial)
        if result.success?
          respond_success(result.data, user_logged_platform_settings_industries_path)
        else
          flash[:error] = result.error.to_s
          render partial
        end
      end
    end
  end
end
