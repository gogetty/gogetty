# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class EmployeesCountsController < BaseController
      layout 'mixed'
      def index
        @employees_counts = EmployeesCount.all.order(:count)
      end

      def new
        @employees_count = ::Form::UserLogged::PlatformSettings::EmployeesCounts::New.new
      end

      def create
        @employees_count = ::Form::UserLogged::PlatformSettings::EmployeesCounts::New.new
        @employees_count.attributes = employees_count_params
        result = Command::EmployeesCount::Create.new(@employees_count).call
        respond(result, '/user_logged/platform_settings/employees_counts/new')
      end

      def edit
        @employees_count = ::Form::UserLogged::PlatformSettings::EmployeesCounts::Edit.find(
          EmployeesCount.find(params[:id])
        )
      end

      def update
        employees_count = EmployeesCount.find(params[:id])
        @employees_count = ::Form::UserLogged::PlatformSettings::EmployeesCounts::Edit.find(
          employees_count
        )
        @employees_count.attributes = employees_count_params
        result = ::Command::EmployeesCount::Update.new(@employees_count, employees_count).call
        respond(result, '/user_logged/platform_settings/employees_counts/edit')
      end

      def destroy
        employees_count = EmployeesCount.find(params[:id])
        result = ::Command::EmployeesCount::Destroy.new(employees_count).call
        if result.success?
          flash[:success] = 'EmployeesCount deleted.'
        else
          flash[:error] = result.error
        end
        redirect_to user_logged_platform_settings_employees_counts_path
      end

      private

      def employees_count_params
        params.require(:employees_count).permit(:count, :order)
      end

      def respond(result, partial)
        if result.success?
          respond_success(result.data, user_logged_platform_settings_employees_counts_path)
        else
          flash[:error] = result.error.to_s
          render partial
        end
      end
    end
  end
end
