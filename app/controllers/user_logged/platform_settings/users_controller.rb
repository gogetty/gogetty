# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class UsersController < BaseController
      layout 'mixed'
      def index
        @users = User.all.order(:email).page(params[:page])
      end

      def show
        @user = User.find(params[:id])
      end

      def update
        @user = User.find(params[:id])
        if @user.update_attributes(secure_params)
          redirect_to user_logged_platform_settings_users_path, notice: 'User updated.'
        else
          redirect_to user_logged_platform_settings_users_path, alert: 'Unable to update user.'
        end
      end

      def destroy
        user = User.find(params[:id])
        user.destroy
        redirect_to user_logged_platform_settings_users_path, notice: 'User deleted.'
      end

      private

      def secure_params
        params.require(:user).permit(:role, :monthly_billing_permission)
      end
    end
  end
end
