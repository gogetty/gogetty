# frozen_string_literal: true

require 'csv'

module UserLogged
  module PlatformSettings
    class UserExportsController < BaseController
      def show
        @users = User.all.order(:email)
        users_csv = CSV.generate(col_sep: ';') do |csv|
          csv << ['Number', 'Name', 'Surname', 'Email', 'Account Type', 'Admin for', 'Platform role']
          @users.each_with_index do |user, index|
            csv << csv_row(user, index)
          end
        end
        send_data users_csv, filename: "users_#{Time.zone.now.strftime('%d-%m-%Y')}.csv"
      end

      private

      def csv_row(user, index)
        [
          index + 1,
          user.name,
          user.surname,
          user.email,
          if UsersPolicy.new(user, current_user).pro?
            'Paid'
          else
            'Free'
          end,
          (user.company&.name),
          user.role
        ]
      end
    end
  end
end
