# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class DiversityRequestsController < BaseController
      layout 'mixed'
      def index
        company_id = session[:requests_company_id]
        @diversity_requests = if company_id.present?
                                DiversityRequest.where(company_id: company_id)
                                                .order(created_at: :desc).page(params[:page])
                              else
                                DiversityRequest.all.order(created_at: :desc).page(params[:page])
                              end
      end
    end
  end
end
