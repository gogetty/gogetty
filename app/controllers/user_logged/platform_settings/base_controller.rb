# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class BaseController < ::UserLogged::BaseController
      before_action :check_with_pundit

      private

      def check_with_pundit
        authorize :users, :platform_admin?
      end
    end
  end
end
