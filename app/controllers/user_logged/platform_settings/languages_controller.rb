# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class LanguagesController < BaseController
      layout 'mixed'
      def index
        @languages = Language.all.order(:name)
      end

      def new
        @language = ::Form::UserLogged::PlatformSettings::Languages::New.new
      end

      def create
        @language = ::Form::UserLogged::PlatformSettings::Languages::New.new
        @language.attributes = language_params
        result = Command::Language::Create.new(@language).call
        respond(result, '/user_logged/platform_settings/languages/new')
      end

      def edit
        @language = ::Form::UserLogged::PlatformSettings::Languages::Edit.find(
          Language.find(params[:id])
        )
      end

      def update
        language = Language.find(params[:id])
        @language = ::Form::UserLogged::PlatformSettings::Languages::Edit.find(
          language
        )
        @language.attributes = language_params
        result = ::Command::Language::Update.new(@language, language).call
        respond(result, '/user_logged/platform_settings/languages/edit')
      end

      def destroy
        language = Language.find(params[:id])
        result = ::Command::Language::Destroy.new(language).call
        if result.success?
          flash[:success] = 'Language deleted.'
        else
          flash[:error] = result.error
        end
        redirect_to user_logged_platform_settings_languages_path
      end

      private

      def language_params
        params.require(:language).permit(:name)
      end

      def respond(result, partial)
        if result.success?
          respond_success(result.data, user_logged_platform_settings_languages_path)
        else
          flash[:error] = result.error.to_s
          render partial
        end
      end
    end
  end
end
