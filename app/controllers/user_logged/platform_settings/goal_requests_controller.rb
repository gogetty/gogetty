# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class GoalRequestsController < BaseController
      layout 'mixed'
      def index
        company_id = session[:requests_company_id]
        @goal_requests = if company_id.present?
                           GoalRequest.where(company_id: company_id)
                                      .order(created_at: :desc).page(params[:page])
                         else
                           GoalRequest.all.order(created_at: :desc).page(params[:page])
                         end
      end
    end
  end
end
