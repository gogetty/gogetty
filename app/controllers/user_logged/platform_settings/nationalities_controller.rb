# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class NationalitiesController < BaseController
      layout 'mixed'
      def index
        @nationalities = Nationality.all.order(:name)
      end

      def new
        @nationality = ::Form::UserLogged::PlatformSettings::Nationalities::New.new
      end

      def create
        @nationality = ::Form::UserLogged::PlatformSettings::Nationalities::New.new
        @nationality.attributes = nationality_params
        result = Command::Nationality::Create.new(@nationality).call
        respond(result, '/user_logged/platform_settings/nationalities/new')
      end

      def edit
        @nationality = ::Form::UserLogged::PlatformSettings::Nationalities::Edit.find(
          Nationality.find(params[:id])
        )
      end

      def update
        nationality = Nationality.find(params[:id])
        @nationality = ::Form::UserLogged::PlatformSettings::Nationalities::Edit.find(
          nationality
        )
        @nationality.attributes = nationality_params
        result = ::Command::Nationality::Update.new(@nationality, nationality).call
        respond(result, '/user_logged/platform_settings/nationalities/edit')
      end

      def destroy
        nationality = Nationality.find(params[:id])
        result = ::Command::Nationality::Destroy.new(nationality).call
        if result.success?
          flash[:success] = 'Nationality deleted.'
        else
          flash[:error] = result.error
        end
        redirect_to user_logged_platform_settings_nationalities_path
      end

      private

      def nationality_params
        params.require(:nationality).permit(:name)
      end

      def respond(result, partial)
        if result.success?
          respond_success(result.data, user_logged_platform_settings_nationalities_path)
        else
          flash[:error] = result.error.to_s
          render partial
        end
      end
    end
  end
end
