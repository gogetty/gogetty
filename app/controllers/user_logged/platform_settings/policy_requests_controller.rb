# frozen_string_literal: true

module UserLogged
  module PlatformSettings
    class PolicyRequestsController < BaseController
      layout 'mixed'
      def index
        company_id = session[:requests_company_id]
        @policy_requests = if company_id.present?
                             PolicyRequest.where(company_id: company_id)
                                          .order(created_at: :desc).page(params[:page])
                           else
                             PolicyRequest.all.order(created_at: :desc).page(params[:page])
                           end
      end
    end
  end
end
