# frozen_string_literal: true

module UserLogged
  class CommentsController < BaseController
    def create
      @form = ::Form::UserLogged::Comment::New.new(create_params.merge(created_by: current_user.id))
      result = ::Command::Comment::Create.new(@form).call
      respond(result)
    end

    def destroy
      @comment = Comment.find(params[:id])
      authorize @comment, :can_destroy?
      result = ::Command::Comment::Destroy.new(@comment).call
      respond(result)
    end

    private

    def create_params
      params.require(:comment).permit(:text, :post_id)
    end

    def respond(result)
      route = begin
                user_logged_company_path(Post.find(params[:comment][:post_id]).company.id)
              rescue StandardError
                user_logged_company_path(@comment.post.company_id)
              end
      super(result, route, route)
    end

    def respond_success(message, path)
      respond_to do |format|
        format.json do
          post = begin
                   Post.find(@form.post_id)
                 rescue StandardError
                   Post.find(@comment.post_id)
                 end
          render status: :ok, html: render_to_string(
            'user_logged/posts/_comments.html', locals: {post: post}, layout: false
          )
        end
        format.html { redirect_to path, notice: message }
      end
    end
  end
end
