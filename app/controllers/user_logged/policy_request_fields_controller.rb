# frozen_string_literal: true

module UserLogged
  class PolicyRequestFieldsController < BaseController
    def create
      result = ::Command::PolicyRequestField::Create.new(params).call
      raise result.error unless result.success?
      @policy_request_form = result.data
      @company = Company.find(@policy_request_form.company_id)
      render partial: '/user_logged/companies/policy_request_form'
    end
  end
end
