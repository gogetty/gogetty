# frozen_string_literal: true

module UserLogged
  class CompanyRequestsController < BaseController
    def create
      form = ::Form::UserLogged::CompanyRequest::New.new(
        create_params.merge(user_id: current_user.id)
      )
      respond(::Command::CompanyRequest::Create.new(form).call,
              company_searches_path,
              company_searches_path)
    end

    private

    def create_params
      params.require(:notify).permit(:notify, :name)
    end
  end
end
