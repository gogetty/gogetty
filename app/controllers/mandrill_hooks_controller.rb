# frozen_string_literal: true

class MandrillHooksController < ApplicationController
  protect_from_forgery except: :create
  def create
    logger.debug '------------------------------------------------------------------'
    logger.debug params.inspect
    logger.debug '------------------------------------------------------------------'
    render status: :ok, json: 'success'
  end
end
