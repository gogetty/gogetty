# frozen_string_literal: true

class AfterSignupsController < ApplicationController
  def index
    if session[:signup_user_id].blank?
      redirect_to root_path, alert: 'Invalid request.'
    else
      user = User.find(session[:signup_user_id]).decorate
      render :show, locals: {user: user}
    end
  end
end
