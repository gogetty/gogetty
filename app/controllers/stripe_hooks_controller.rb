# frozen_string_literal: true

class StripeHooksController < ApplicationController
  protect_from_forgery except: :create
  http_basic_authenticate_with name: 'stripe', password: Rails.application.secrets.stripe_http_password
  def create
    result = ::Command::StripeHooks::HooksDispatcher.new(params).call
    logger.debug "Stripe webhook: #{result.inspect}"
    unless result.success?
      status = if Rails.env.production?
                 400
               else
                 :ok
               end
      render status: status, json: result.error.message
    end
    render status: :ok, json: 'success' if result.success?
  end
end
