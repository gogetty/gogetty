# frozen_string_literal: true

class CompanyLanguage < ApplicationRecord
  default_scope { order(count: :desc) }

  belongs_to :company, counter_cache: :languages_count
  belongs_to :language

  validates :count, presence: true, numericality: {only_integer: true, greater_than: 0}
  validates :language_id, uniqueness: {scope: :company_id}

  delegate :name, to: :language
end
