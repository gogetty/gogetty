# frozen_string_literal: true

class Billing < ApplicationRecord
  include AASM
  belongs_to :plan
  enum state: {new_registered: 0,
               pending: 1,
               finalized: 2,
               to_retry: 3,
               error: 4,
               cancelled: 5}

  aasm column: :state, enum: true do
    state :new_registered, initial: true
    state :pending
    state :finalized
    state :to_retry
    state :error
    state :cancelled
  end
end
