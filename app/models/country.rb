# frozen_string_literal: true

class Country < ApplicationRecord
  validates :en_name, presence: true, uniqueness: true
end
