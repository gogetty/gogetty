# frozen_string_literal: true

class OwnerType < ApplicationRecord
  has_many :company, dependent: :nullify
  validates :name, uniqueness: true
end
