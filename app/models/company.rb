# frozen_string_literal: true

class Company < ApplicationRecord # rubocop:disable Metrics/ClassLength
  extend FriendlyId
  friendly_id :name, use: :slugged
  mount_uploader :logo, LogoUploader
  belongs_to :industry, optional: true
  belongs_to :country, optional: true
  has_one :company_profile, dependent: :destroy
  has_many :company_profile_histories, dependent: :destroy
  has_many :companies_diversity_policies, dependent: :destroy
  has_many :diversity_policy, through: :companies_diversity_policies, dependent: :destroy
  has_many :text_policies, dependent: :destroy
  has_many :goals_way, dependent: :destroy
  has_many :admins, foreign_key: 'company_id', class_name: 'User', dependent: :nullify
  has_many :admin_invites, dependent: :destroy
  has_many :admin_requests, dependent: :destroy
  has_many :policy_requests, dependent: :destroy
  has_many :goal_requests, dependent: :destroy
  has_many :diversity_requests, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :follows, dependent: :destroy
  has_many :company_nationalities, dependent: :destroy
  has_many :nationalities, through: :company_nationalities, dependent: :destroy
  has_many :company_languages, dependent: :destroy
  has_many :languages, through: :company_languages, dependent: :destroy
  has_one :company_information, dependent: :destroy
  has_many :company_information_requests, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  require 'csv'

  def information
    company_information || CompanyInformation.new
  end

  def active_admins
    User.joins(:plans_users)
        .where('payed_for_to >= ?', Time.zone.today)
        .or(User.joins(:plans_users).where('not_payed_pro = TRUE'))
        .where(company_id: id)
  end

  def posted_recently?
    posts.where('created_at >= ?', 3.days.ago).any?
  end

  def active_admins?
    active_admins.count.positive?
  end

  def active_basic_business_plan?
    admins.joins(:plans_users).map(&:pro?).none?
  end

  def active_paid_plan?
    admins.joins(:plans_users).map(&:pro?).any?
  end

  def active_inclusive_business_plan?
    admins.joins(:plans_users).map(&:subscribed_to_inclusive_business?).any?
  end

  # rubocop:disable all
  def self.to_csv(with_history)
    columns = %w[diversity_score now_director_woman_count now_director_man_count
                 goal_director_woman_count goal_director_man_count now_cxo_woman_count
                 now_cxo_man_count goal_cxo_woman_count goal_cxo_man_count
                 now_vp_woman_count now_vp_man_count goal_vp_woman_count goal_vp_man_count
                 now_middle_woman_count now_middle_man_count goal_middle_woman_count goal_middle_man_count ]

    CSV.generate(headers: true, col_sep: ';') do |csv|
      csv << %w[created_at Name stock_listed Company_registration_number Industry Country diversity_score now_board_woman_count now_board_man_count
                goal_board_woman_count goal_board_man_count] + columns.drop(5) + %w[all_policies goals_way]
      all.each do |company|
        company.company_profile_histories.order(created_at: :desc).each do |history|
          row = [history.created_at, company.name, company.stock_listed, company.cvr_number, company.industry&.name, company.country&.en_name]
          columns.each { |col| row << history.attributes[col] }
          row << company.decorate.all_policies.pluck(:name).join('; ')
          row << company.decorate.goals_way.pluck(:description).join('; ')
          csv << row
          break if !with_history
        end
      end
    end
  end

  def self.save_final_data_to_gg
    spreadsheet = Roo::Spreadsheet.open("#{Rails.root}/csv_seeds/Combined_cleaned_BB_and_GG_list_NEW.xlsx", extension: :xlsx)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      next if row.dig('Company Cvr Number').nil?
      company = Company.find_by(name: row.dig('GG Company Name'))
      if company.present?
        company_profile = company.company_profile
        com_name = row.dig('BB Company Name').present? ? row.dig('BB Company Name') : company.name

        company.update_attribute(:cvr_number, row.dig('Company Cvr Number'))
        company.update_attribute(:name, com_name) unless company.admins.present?

        company_profile.update_attributes(
          now_director_woman_count: row.dig('BB now_director_woman_count'),
          now_director_man_count: row.dig('BB now_director_man_count'),
          now_cxo_woman_count: row.dig('BB now_cxo_woman_count'),
          now_cxo_man_count: row.dig('BB now_cxo_man_count')
        ) unless company.admins.present?
        ::Command::CompanyProfile::CalcDiversityScore.new(company_profile).call
      else
        company = Company.new(cvr_number: row.dig('Company Cvr Number'), name: row.dig('BB Company Name'), country_id: 1)
        company.build_company_profile(
          now_director_woman_count: row.dig('BB now_director_woman_count'),
          now_director_man_count: row.dig('BB now_director_man_count'),
          now_cxo_woman_count: row.dig('BB now_cxo_woman_count'),
          now_cxo_man_count: row.dig('BB now_cxo_man_count'), goal_year: Time.current.year + 1)
        if company.save
          ::Command::CompanyProfile::CalcDiversityScore.new(company.company_profile).call
          Rails.logger.debug("Company #{company.name} created" )
        else
          Rails.logger.debug(company.errors)
        end
      end
    end
  end

  # rubocop:enable all
end
