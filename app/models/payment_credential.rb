# frozen_string_literal: true

class PaymentCredential < ApplicationRecord
  belongs_to :user
  validates :stripe_customer_id, :exp_month,
            :exp_year, :card_type, presence: true
end
