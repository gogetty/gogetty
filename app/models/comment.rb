# frozen_string_literal: true

class Comment < ApplicationRecord
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :post
end
