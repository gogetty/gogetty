# frozen_string_literal: true

class CompanyInformation < ApplicationRecord
  belongs_to :company
end
