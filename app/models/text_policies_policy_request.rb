# frozen_string_literal: true

class TextPoliciesPolicyRequest < ApplicationRecord
  belongs_to :policy_request
end
