# frozen_string_literal: true

class DiversityPolicy < ApplicationRecord
  has_many :companies_diversity_policies, dependent: :destroy
  has_many :company, through: :companies_diversity_policies, dependent: :destroy

  attr_accessor :additional
  validates :name, uniqueness: true
end
