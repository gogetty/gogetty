# frozen_string_literal: true

class DiversityPoliciesPolicyRequest < ApplicationRecord
  belongs_to :policy_request
  belongs_to :diversity_policy
end
