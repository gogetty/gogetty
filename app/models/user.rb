# frozen_string_literal: true

class User < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  enum role: {user: 0, vip: 1, admin: 2}
  has_many :billings, dependent: :destroy
  has_many :plans_users, dependent: :destroy
  has_many :follows, dependent: :destroy
  has_many :posts, class_name: 'Post', foreign_key: 'created_by', dependent: :destroy
  has_many :likes, class_name: 'Like', foreign_key: 'created_by', dependent: :destroy
  has_many :comments, class_name: 'Comment', foreign_key: 'created_by', dependent: :destroy
  has_many :diversity_requests, dependent: :destroy
  has_many :company_requests, dependent: :destroy
  has_many :policy_requests, dependent: :destroy
  has_many :goal_requests, dependent: :destroy
  has_many :company_information_requests, dependent: :destroy
  has_many :solution_requests, dependent: :destroy
  has_one :payment_credential, dependent: :destroy
  # validates :name, presence: true
  # validates :surname, presence: true
  validates :email, presence: true
  # validates :company_name, presence: true, if: proc { |a| a.uid.nil? }
  # validates :title, presence: true, if: proc { |a| a.uid.nil? }
  validates :terms_agreed, acceptance: true, if: proc { |a| a.uid.nil? }
  belongs_to :company, optional: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable,
         omniauth_providers: [:linkedin]

  scope :not_superadmin, -> { where(superadmin: false) }
  scope :superadmin, -> { where(superadmin: true) }

  after_save :send_welcome_email, if: proc { |user|
    user.saved_change_to_confirmed_at? && user.confirmed_at_before_last_save.nil?
  }

  def send_welcome_email
    MandrillUserAccountMailer.welcome_email(self)
  end

  def pro?
    active_paid_plans.any?
  end

  def subscribed_to_inclusive_business?
    current_subscription_plan&.name == 'inclusive_business'
  end

  def current_subscription_plan
    active_paid_plans.first&.plan
  end

  def platform_admin?
    role == 'admin'
  end

  def full_name
    "#{name} #{surname}"
  end

  def full_name_someone
    if name.nil? && surname.nil?
      'Someone'
    else
      "#{name} #{surname}"
    end
  end

  def send_confirmation_instructions
    generate_confirmation_token! if confirmation_token.nil?
    MandrillUserAccountMailer.confirm_email(self)
  end

  def send_reset_password_instructions
    token = set_reset_password_token
    MandrillUserAccountMailer.forgot_password(self, token)
  end

  def active_paid_plans
    plans_users.active
  end

  def current_subscription_interval
    Stripe::Subscription.retrieve(active_paid_plans.first.stripe_sub)[:plan][:interval]
  end
end
