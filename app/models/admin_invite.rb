# frozen_string_literal: true

class AdminInvite < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :company

  scope :for_user, ->(user) {
    where('user_id = ? OR email = ?',
          user.id,
          user.email)
  }

  scope :active, -> {
    where(cancelled: false,
          accepted: false)
  }

  scope :ordered, -> {
    order(cancelled: :asc,
          accepted: :asc,
          updated_at: :desc)
  }
end
