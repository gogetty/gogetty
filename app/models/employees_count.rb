# frozen_string_literal: true

class EmployeesCount < ApplicationRecord
  has_many :company_profile, dependent: :nullify
  validates :count, uniqueness: true

  default_scope { order(:order) }
end
