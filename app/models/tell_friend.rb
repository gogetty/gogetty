# frozen_string_literal: true

class TellFriend < ApplicationRecord
  belongs_to :user, optional: true
end
