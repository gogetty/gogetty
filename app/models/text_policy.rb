# frozen_string_literal: true

class TextPolicy < ApplicationRecord
  belongs_to :company, touch: true

  attr_accessor :additional
end
