# frozen_string_literal: true

class DiversityRequest < ApplicationRecord
  belongs_to :user
  belongs_to :company

  scope :today, -> { where('created_at > ?', Time.zone.today) }
  scope :week, -> { where('created_at > ?', Time.zone.today - 7.days) }
  scope :month, -> { where('created_at > ?', Time.zone.today - 1.month) }
  scope :year, -> { where('created_at > ?', Time.zone.today - 1.year) }
end
