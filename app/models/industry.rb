# frozen_string_literal: true

class Industry < ApplicationRecord
  validates :name, uniqueness: true
end
