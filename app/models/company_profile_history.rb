# frozen_string_literal: true

class CompanyProfileHistory < ApplicationRecord
  belongs_to :company
end
