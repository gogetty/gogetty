# frozen_string_literal: true

class PolicyRequest < ApplicationRecord
  has_many :diversity_policies_policy_requests, autosave: true, dependent: :destroy
  accepts_nested_attributes_for :diversity_policies_policy_requests,
                                reject_if: :all_blank,
                                allow_destroy: true
  belongs_to :user
  belongs_to :company

  scope :today, -> { where('created_at > ?', Time.zone.today) }
  scope :week, -> { where('created_at > ?', Time.zone.today - 7.days) }
  scope :month, -> { where('created_at > ?', Time.zone.today - 1.month) }
  scope :year, -> { where('created_at > ?', Time.zone.today - 1.year) }
end
