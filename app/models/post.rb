# frozen_string_literal: true

class Post < ApplicationRecord
  mount_uploader :photo, PostPhotoUploader

  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :company

  scope :latest_five, -> { order(created_at: :desc).limit(5) }
end
