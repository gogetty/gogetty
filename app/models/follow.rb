# frozen_string_literal: true

class Follow < ApplicationRecord
  belongs_to :company
  belongs_to :user
  validates :user_id, presence: true
  validates :company_id, presence: true

  scope :today, -> { where('created_at > ?', Time.zone.today) }
  scope :week, -> { where('created_at > ?', Time.zone.today - 7.days) }
  scope :month, -> { where('created_at > ?', Time.zone.today - 1.month) }
  scope :year, -> { where('created_at > ?', Time.zone.today - 1.year) }
end
