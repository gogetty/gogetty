# frozen_string_literal: true

class Language < ApplicationRecord
  default_scope { order(name: :asc) }

  has_many :company_languages, dependent: :destroy

  validates :name, presence: true
  validates :name, uniqueness: true
end
