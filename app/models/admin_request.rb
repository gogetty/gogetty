# frozen_string_literal: true

class AdminRequest < ApplicationRecord
  belongs_to :user
  belongs_to :company

  scope :ordered, -> {
    order(cancelled: :asc,
          revoked: :asc,
          accepted: :asc,
          updated_at: :desc)
  }
end
