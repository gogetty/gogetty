# frozen_string_literal: true

class CompaniesDiversityPolicy < ApplicationRecord
  belongs_to :company, touch: true
  belongs_to :diversity_policy
end
