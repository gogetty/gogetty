# frozen_string_literal: true

class CompanyNationality < ApplicationRecord
  default_scope { order(count: :desc) }

  belongs_to :company, counter_cache: :nationalities_count
  belongs_to :nationality

  validates :count, presence: true, numericality: {only_integer: true, greater_than: 0}
  validates :nationality_id, uniqueness: {scope: :company_id}

  delegate :name, to: :nationality
end
