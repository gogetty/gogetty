# frozen_string_literal: true

class Nationality < ApplicationRecord
  default_scope { order(name: :asc) }

  has_many :company_nationalities, dependent: :destroy

  validates :name, presence: true
  validates :name, uniqueness: true
end
