# frozen_string_literal: true

class CompanyProfile < ApplicationRecord
  belongs_to :company
  belongs_to :employees_count, optional: true

  delegate :count, to: :employees_count, prefix: true

  after_update :calc_women_man_percentage

  private

  # Changes made by James Wilson without applying Rubocop rules
  # rubocop:disable all
  def calc_women_man_percentage
    if saved_change_to_now_director_woman_count? || saved_change_to_now_director_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'director', 'current')
    end

    if saved_change_to_now_cxo_woman_count? || saved_change_to_now_cxo_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'cxo', 'current')
    end

    if saved_change_to_now_vp_woman_count? || saved_change_to_now_vp_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'vp', 'current')
    end

    if saved_change_to_now_middle_woman_count? || saved_change_to_now_middle_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'middle', 'current')
    end

    if saved_change_to_now_director_woman_percentage? || saved_change_to_now_director_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'director', 'current')
    end

    if saved_change_to_now_cxo_woman_percentage? || saved_change_to_now_cxo_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'cxo', 'current')
    end

    if saved_change_to_now_vp_woman_percentage? || saved_change_to_now_vp_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'vp', 'current')
    end

    if saved_change_to_now_middle_woman_percentage? || saved_change_to_now_middle_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'middle', 'current')
    end

    if saved_change_to_goal_director_woman_count? || saved_change_to_goal_director_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'director', 'goal')
    end

    if saved_change_to_goal_cxo_woman_count? || saved_change_to_goal_cxo_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'cxo', 'goal')
    end

    if saved_change_to_goal_vp_woman_count? || saved_change_to_goal_vp_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'vp', 'goal')
    end

    if saved_change_to_goal_middle_woman_count? || saved_change_to_goal_middle_man_count?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('number', 'middle', 'goal')
    end

    if saved_change_to_goal_director_woman_percentage? || saved_change_to_goal_director_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'director', 'goal')
    end

    if saved_change_to_goal_cxo_woman_percentage? || saved_change_to_goal_cxo_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'cxo', 'goal')
    end

    if saved_change_to_goal_vp_woman_percentage? || saved_change_to_goal_vp_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'vp', 'goal')
    end

    if saved_change_to_goal_middle_woman_percentage? || saved_change_to_goal_middle_man_percentage?
      ::Command::CompanyProfile::CalcPercentage.new(self).call('percentage', 'middle', 'goal')
    end
  end
  # rubocop:enable all
end
