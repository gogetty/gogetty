# frozen_string_literal: true

class SolutionRequest < ApplicationRecord
  belongs_to :user
end
