# frozen_string_literal: true

class CompanyRequest < ApplicationRecord
  belongs_to :user
end
