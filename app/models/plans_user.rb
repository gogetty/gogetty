# frozen_string_literal: true

class PlansUser < ApplicationRecord
  belongs_to :plan
  belongs_to :user
  # rubocop:disable Rails/HasManyOrHasOneDependent
  has_one :billing, primary_key: 'stripe_sub', foreign_key: 'stripe_sub'
  # rubocop:enable all

  def self.active
    where('payed_for_to >= ?', Time.zone.today)
      .or(where(not_payed_pro: true))
      .where(cancelled_on: nil)
  end
end
