# frozen_string_literal: true

module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    safe_join(form_errors(resource))
  end

  private

  def messages(resource)
    safe_join(resource.errors.full_messages.map { |msg| content_tag(:li, msg) })
  end

  def form_errors(resource)
    html = []
    html << content_tag(
      :div,
      class: 'alert alert-danger alert-fismissible fade show',
      role: 'alert',
      style: 'color: white'
    ) do
      content_tag(:button, '', class: 'close', 'data-dismiss' => 'alert') do
        content_tag(:span, 'x', 'aria-hidden' => true)
      end +
        content_tag(:strong, "#{pluralize(resource.errors.count, 'error')} must be fixed") +
        messages(resource)
    end
  end
end
