# frozen_string_literal: true

module TextHelper
  # rubocop:disable LineLength
  URL_REGEXP = %r{((http|https):\/\/)?[\w][a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.[^\s<,]+)?}i
  # rubocop:enable LineLength

  # rubocop:disable Rails/OutputSafety
  def format_text(text)
    simple_format(h(text)).gsub(URL_REGEXP) do |link|
      if link[%r{\Ahttp(s)?:\/\/}]
        content_tag(:a, link, href: link)
      else
        content_tag(:a, link, href: "//#{link}")
      end
    end.html_safe
  end
end
