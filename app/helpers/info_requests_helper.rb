# frozen_string_literal: true

module InfoRequestsHelper
  def all_info_requests
    @policy_requests.count +
      @diversity_requests.count +
      @goal_requests.count +
      @company_information_requests.count
  end

  def today_all_info_requests
    @policy_requests.today.count +
      @diversity_requests.today.count +
      @goal_requests.today.count +
      @company_information_requests.today.count
  end

  def week_all_info_requests
    @policy_requests.week.count +
      @diversity_requests.week.count +
      @goal_requests.week.count +
      @company_information_requests.week.count
  end

  def month_all_info_requests
    @policy_requests.month.count +
      @diversity_requests.month.count +
      @goal_requests.month.count +
      @company_information_requests.month.count
  end

  def year_all_info_requests
    @policy_requests.year.count +
      @diversity_requests.year.count +
      @goal_requests.year.count +
      @company_information_requests.year.count
  end
end
