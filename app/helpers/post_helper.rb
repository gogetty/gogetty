# frozen_string_literal: true

module PostHelper
  def embed_video(url, width = '100%', height = '315')
    if vimeo_id(url)
      vimeo_iframe(vimeo_id(url), width, height)
    elsif youtube_id(url)
      youtube_iframe(youtube_id(url), width, height)
    end
  end

  private

  def youtube_id(youtube_url)
    case sanitize youtube_url
    when %r{youtu\.be\/([^\?]*)} then Regexp.last_match(1)
    when %r{^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*} then Regexp.last_match(5)
    else false
    end
  end

  def vimeo_id(vimeo_url)
    if sanitize(vimeo_url) =~ %r{^https?:\/\/(?:.*?)\.?(vimeo)\.com\/(\d+).*$}
      Regexp.last_match(2)
    else
      false
    end
  end

  def youtube_iframe(youtube_id, width, height)
    content_tag(:iframe,
                '',
                width: width,
                height: height,
                src: "https://www.youtube.com/embed/#{youtube_id}",
                allowfullscreen: true,
                frameborder: 0)
  end

  def vimeo_iframe(vimeo_id, width, height)
    content_tag(:iframe,
                '',
                width: width,
                height: height,
                src: "https://player.vimeo.com/video/#{vimeo_id}",
                allowfullscreen: true,
                frameborder: 0)
  end
end
