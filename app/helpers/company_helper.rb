# frozen_string_literal: true

module CompanyHelper
  def diversity_status_helper(state, label, bar_state)
    full_label = "? #{label}"
    full_label = simple_format("F / M ratio - #{state}\n#{label}") if state
    render partial: 'user_logged/companies/diversity_status',
           locals: {bar_state: bar_state, label: label, full_label: full_label}
  end

  def policy_helper(diversity_policy)
    tick = if diversity_policy.additional
             '&#65533;'
           else
             '&#10004;'
           end
    render partial: 'user_logged/companies/diversity_policy',
           locals: {diversity_policy: diversity_policy, tick: tick}
  end

  def follow_helper(company, user)
    if company.follows.find_by(user_id: user&.id)
      follow = 'display: none'
      unfollow = ''
    else
      follow = ''
      unfollow = 'display: none'
    end
    render partial: 'user_logged/companies/follow.html',
           locals: {company: company, follow: follow, unfollow: unfollow}
  end
end
