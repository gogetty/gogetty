# frozen_string_literal: true

module RequestsHelper
  def options_for_companies
    Company.order(:name).pluck(:name, :id)
  end
end
