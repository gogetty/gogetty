# frozen_string_literal: true

module CompanySearchHelper
  def options_for_employees_count
    EmployeesCount.order(:count).pluck(:count, :id)
  end

  def options_for_industry
    Industry.order(:name).pluck(:name, :id)
  end

  def options_for_country
    Country.order(:en_name).pluck(:en_name, :id)
  end

  def options_for_search_sort
    [
      ['Gender score, descending', '1'],
      ['Gender score, ascending', '2'],
      ['Ethnicity score, descending', '3'],
      ['Ethnicity score, ascending', '4'],
      ['Alphabetically, descending', '5'],
      ['Alphabetically, ascending', '6']
    ]
  end
end
