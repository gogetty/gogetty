# frozen_string_literal: true

module CompaniesDiversityPolicyHelper
  def options_for_policies(company = nil)
    diversity_policy = DiversityPolicy.where(hidden: false)
                                      .order(:name)
    if company&.diversity_policy&.count&.positive?
      diversity_policy = diversity_policy.where.not('id IN (?)',
                                                    company.diversity_policy.pluck(:id))
    end
    diversity_policy.collect { |p| [p.name, p.id] }
  end
end
