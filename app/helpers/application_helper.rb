# frozen_string_literal: true

module ApplicationHelper
  include PostHelper
  include TextHelper
end
