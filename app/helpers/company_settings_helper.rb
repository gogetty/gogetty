# frozen_string_literal: true

module CompanySettingsHelper
  def options_for_industry
    Industry.order(:name).pluck(:name, :id)
  end

  def options_for_country
    Country.order(:en_name).pluck(:en_name, :id)
  end

  def options_for_employees_count
    EmployeesCount.order(:count).pluck(:count, :id)
  end

  def options_for_owner_type
    OwnerType.order(:name).pluck(:name, :id)
  end
end
