# frozen_string_literal: true

module CompanyProfileHelper
  def options_for_employees_count
    EmployeesCount.order(:count).pluck(:count, :id)
  end
end
