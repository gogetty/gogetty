# frozen_string_literal: true

module UserHelper
  def options_for_roles_select
    User.roles.keys.map { |role| [role.titleize, role] }
  end

  def number_for_order(index)
    index + 1 +
      (params[:page].to_i.positive? ? (params[:page].to_i - 1) * Kaminari.config.default_per_page : 0)
  end
end
