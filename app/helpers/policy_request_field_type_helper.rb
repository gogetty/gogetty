# frozen_string_literal: true

module PolicyRequestFieldTypeHelper
  def options_for_type
    options_for_select(::PolicyRequest.types.map { |k, v| [PolicyRequest.human_attribute_name(k), v] })
  end
end
