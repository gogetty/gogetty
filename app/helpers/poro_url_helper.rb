# frozen_string_literal: true

module PoroUrlHelper
  extend ActiveSupport::Concern

  class Base
    include Rails.application.routes.url_helpers

    def default_url_options
      ActionMailer::Base.default_url_options
    end
  end

  def url_helpers
    @url_helpers ||= PoroUrlHelper::Base.new
  end

  def self.method_missing(method, *args, &block)
    @url_helpers ||= PoroUrlHelper::Base.new

    if @url_helpers.respond_to?(method)
      @url_helpers.send(method, *args, &block)
    else
      super
    end
  end

  def self.respond_to_missing?(method, *)
    method =~ /play_(\w+)/ || super
  end
end
